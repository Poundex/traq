package net.poundex.traq.marker;

import lombok.RequiredArgsConstructor;
import net.poundex.tidycli.spring.dispatch.CommandHandler;
import net.poundex.tidycli.ui.Rendering;
import net.poundex.tidycli.ui.rendering.InlineLabel;
import net.poundex.traq.api.client.LedgerByNameProjectionRoot;
import net.poundex.traq.api.client.UpdateMarkerGraphQLQuery;
import net.poundex.traq.api.client.UpdateMarkerProjectionRoot;
import net.poundex.traq.cli.TraqCli;
import net.poundex.traq.cli.format.CliMarkerFormat;
import net.poundex.traq.ledger.LedgerService;
import net.poundex.xmachinery.spring.graphql.client.GraphQlClient;
import org.reactivestreams.Publisher;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
@Component
class SetMarkerTitleCommand implements CommandHandler<TraqCli.Marker.MarkerSetTitle> {

	protected final GraphQlClient graphQlClient;
	protected final LedgerService ledgerService;

	@Override
	public Publisher<Rendering> handle(TraqCli.Marker.MarkerSetTitle command) {
		return ledgerService.findLedgerByName(ledgerService.getLedgerName(command),
						new LedgerByNameProjectionRoot<>()
								.id()
								.name()
								.markers()
									.__typename()
									.id()
									.name()
									.onProperty()
										.value()
										.root())
				.flatMap(ledger -> Mono.justOrEmpty(ledger.markers().stream().filter(it -> it.asString().equalsIgnoreCase(command.marker())).findFirst()))
				.flatMap(m -> graphQlClient.query(UpdateMarkerGraphQLQuery.newRequest()
								.marker(m.id())
								.title(command.newTitle())
								.build(),
						new UpdateMarkerProjectionRoot<>()
								.__typename()
								.name()
								.title()
								.onProperty()
									.value()
									.root()))
				.map(qr -> qr.extractValue(MarkerDto.class))
				.map(m -> InlineLabel.of(CliMarkerFormat.format(m) + " updated"));
	}
}
