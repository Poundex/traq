package net.poundex.traq.marker;

import net.poundex.tidycli.ui.Rendering;
import net.poundex.tidycli.ui.rendering.InlineLabel;
import net.poundex.tidycli.ui.rendering.Text;
import net.poundex.tidycli.ui.rendering.table.Table;
import net.poundex.traq.cli.format.CliMarkerFormat;
import net.poundex.traq.format.SpanFormat;

import java.util.Map;

public class MarkerSummaryTable {
	public static Rendering forMarkerSummaries(Map<MarkerDto, MarkableService.MarkerSummary> markerSummaries) {
		
		return Table.from(markerSummaries.entrySet().stream()
						.sorted(MarkerSummaryComparator.INSTANCE)
						.toList())
				.headerRow()
				.columns(c -> c
						.column("Marker", kv -> InlineLabel.of(CliMarkerFormat.format(kv.getKey())))
						.column("Summary", kv -> Text.of(
								kv.getValue().values() == null
										? (kv.getValue().duration() != null ? SpanFormat.toElapsedTime(kv.getValue().duration().value()) + " " : "")
											+ (kv.getValue().count() != null ? "+" + kv.getValue().count().value() : "")
										: String.join(" ", kv.getValue().values().value()))));
	}
}
