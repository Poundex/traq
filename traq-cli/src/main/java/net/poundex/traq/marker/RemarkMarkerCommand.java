package net.poundex.traq.marker;

import io.vavr.control.Try;
import net.poundex.traq.api.client.SetMarkersGraphQLQuery;
import net.poundex.traq.api.client.SetMarkersProjectionRoot;
import net.poundex.traq.cli.TraqCli;
import net.poundex.traq.ledger.LedgerDto;
import net.poundex.traq.ledger.LedgerService;
import net.poundex.xmachinery.fio.io.IO;
import net.poundex.xmachinery.spring.graphql.client.GraphQlClient;
import net.poundex.xmachinery.spring.graphql.client.QueryResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import reactor.core.publisher.Mono;

import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
class RemarkMarkerCommand extends AbstractMarkerMarkerModifyingCommand<TraqCli.Marker.MarkerRemark> {

	private final IO io;
	
	public RemarkMarkerCommand(GraphQlClient graphQlClient, LedgerService ledgerService, IO io) {
		super(graphQlClient, ledgerService);
		this.io = io;
	}

	@Override
	public Mono<QueryResponse> handleInternal(LedgerDto ledger, TraqCli.Marker.MarkerRemark command) {
		return Mono.justOrEmpty(ledger.markers().stream().filter(
				it -> it.asString().equalsIgnoreCase(command.marker())).findFirst())
				.flatMap(marker -> graphQlClient.query(SetMarkersGraphQLQuery.newRequest()
								.marker(marker.id())
								.markers(editMarkers(marker))
								.build(),
						new SetMarkersProjectionRoot<>()
								.__typename()
								.id()
								.name()
								.title()
								.onProperty()
									.value()
									.parent()
								.markers()
									.__typename()
									.id()
									.name()
									.title()
									.onProperty()
										.value()
										.root()));
	}

	private List<String> editMarkers(MarkerDto marker) {
		Path tempFile = io.createTempFile("traq", ".txt");

		Map<? extends Class<? extends MarkerDto>, List<MarkerDto>> collect = 
				marker.markers().stream().collect(Collectors.groupingBy(MarkerDto::getClass));
		
		StringBuilder sb = new StringBuilder();
		sb.append(collect.getOrDefault(TagDto.class, Collections.emptyList()).stream()
				.map(MarkerDto::asString)
				.collect(Collectors.joining("\n")));
		sb.append("\n\n");

		sb.append(collect.getOrDefault(MentionDto.class, Collections.emptyList()).stream()
				.map(MarkerDto::asString)
				.collect(Collectors.joining("\n")));
		sb.append("\n\n");

		sb.append(collect.getOrDefault(PropertyDto.class, Collections.emptyList()).stream()
				.map(MarkerDto::asString)
				.collect(Collectors.joining("\n")));
		sb.append("\n");
		
		io.writeString(tempFile, sb.toString());

		String editor = Objects.requireNonNull(System.getenv("EDITOR"), "$EDITOR");
		Integer exit = Try.of(() -> new ProcessBuilder(editor, tempFile.toAbsolutePath().toString())
						.inheritIO()
						.start()
						.waitFor())
				.get();
		if (exit != 0)
			throw new IllegalStateException("No clean editor exit");
		
		String edited = io.readString(tempFile);
		return edited.lines().filter(StringUtils::hasText).toList();
	}
}
