package net.poundex.traq.marker;

import lombok.RequiredArgsConstructor;
import net.poundex.tidycli.spring.dispatch.CommandHandler;
import net.poundex.tidycli.ui.Rendering;
import net.poundex.tidycli.ui.rendering.HBox;
import net.poundex.tidycli.ui.rendering.InlineLabel;
import net.poundex.tidycli.ui.rendering.Text;
import net.poundex.tidycli.ui.rendering.VBox;
import net.poundex.traq.api.client.LedgerByNameProjectionRoot;
import net.poundex.traq.cli.TraqCli;
import net.poundex.traq.cli.format.CliMarkerFormat;
import net.poundex.traq.ledger.LedgerDto;
import net.poundex.traq.ledger.LedgerService;
import net.poundex.xmachinery.spring.graphql.client.GraphQlClient;
import net.poundex.xmachinery.spring.graphql.client.QueryResponse;
import org.reactivestreams.Publisher;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
abstract class AbstractMarkerMarkerModifyingCommand<T extends TraqCli.LedgerMixin> implements CommandHandler<T> {

	protected final GraphQlClient graphQlClient;
	protected final LedgerService ledgerService;

	@Override
	public Publisher<Rendering> handle(T command) {
		return ledgerService.findLedgerByName(ledgerService.getLedgerName(command),
				new LedgerByNameProjectionRoot<>()
						.id()
						.name()
						.markers()
							.__typename()
							.id()
							.name()
							.title()
							.onProperty()
								.value()
								.parent()
							.markers()
								.__typename()
								.id()
								.name()
								.title()
								.onProperty()
									.value()
									.root())
				.flatMap(l -> handleInternal(l, command))
				.map(qr -> qr.extractValue(MarkerDto.class))
				.flatMapMany(m -> Flux.just(
						InlineLabel.of(CliMarkerFormat.format(m) + " updated"),
						HBox.of(1,
								Text.of("==>"),
								VBox.of(0, m.markers().isEmpty()
										? new InlineLabel[] { InlineLabel.of("None") }
										: m.markers().stream()
												.map(mm -> InlineLabel.of(CliMarkerFormat.format(mm)))
												.toArray(InlineLabel[]::new))
						)));
	}

	protected abstract Mono<QueryResponse> handleInternal(LedgerDto ledger, T command);
}
