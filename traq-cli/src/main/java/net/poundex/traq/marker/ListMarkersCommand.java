package net.poundex.traq.marker;

import lombok.RequiredArgsConstructor;
import net.poundex.tidycli.spring.dispatch.CommandHandler;
import net.poundex.tidycli.ui.Rendering;
import net.poundex.tidycli.ui.rendering.InlineLabel;
import net.poundex.tidycli.ui.rendering.VBox;
import net.poundex.tidycli.ui.rendering.table.Table;
import net.poundex.traq.api.client.LedgerByNameProjectionRoot;
import net.poundex.traq.cli.TraqCli;
import net.poundex.traq.cli.format.CliMarkerFormat;
import net.poundex.traq.ledger.LedgerService;
import org.reactivestreams.Publisher;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
class ListMarkersCommand implements CommandHandler<TraqCli.Marker.MarkerList> {
	
	private final LedgerService ledgerService;
	
	@Override
	public Publisher<Rendering> handle(TraqCli.Marker.MarkerList command) {
		return ledgerService.findLedgerByName(ledgerService.getLedgerName(command), 
						new LedgerByNameProjectionRoot<>()
								.id()
								.name()
								.markers()
								.__typename()
								.id()
								.name()
								.title()
								.onProperty()
									.value()
									.root())
						.map(ledger -> ledger.markers().stream()
								.collect(Collectors.groupingBy(MarkerDto::getClass, Collectors.<MarkerDto>toList())))
						.map(markers -> VBox.of(0,
								markerTable("Tags", TagDto.class, markers),
								markerTable("Mentions", MentionDto.class, markers),
								markerTable("Properties", PropertyDto.class, markers)));
	}
	
	@SuppressWarnings("unchecked")
	private <T extends MarkerDto> Table<T> markerTable(
			String type,
			Class<T> klass, 
			Map<Class<? extends MarkerDto>, List<MarkerDto>> markers) {
		return Table.from((List<T>)markers.getOrDefault(klass, Collections.emptyList()))
						.title(type)
						.columns(c -> c
								.column("Title/Name", m -> InlineLabel.of(CliMarkerFormat.format(m) + 
										"%s".formatted(m.title() != null ? String.format(" (%s)", m.asString()) : ""))));
	}
}
