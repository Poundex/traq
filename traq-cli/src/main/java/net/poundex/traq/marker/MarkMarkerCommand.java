package net.poundex.traq.marker;

import net.poundex.traq.api.client.ModifyMarkerGraphQLQuery;
import net.poundex.traq.api.client.ModifyMarkerProjectionRoot;
import net.poundex.traq.cli.TraqCli;
import net.poundex.traq.ledger.LedgerDto;
import net.poundex.traq.ledger.LedgerService;
import net.poundex.xmachinery.spring.graphql.client.GraphQlClient;
import net.poundex.xmachinery.spring.graphql.client.QueryResponse;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.stream.Stream;

@Component
class MarkMarkerCommand extends AbstractMarkerMarkerModifyingCommand<TraqCli.Marker.MarkerMark> {

	public MarkMarkerCommand(GraphQlClient graphQlClient, LedgerService ledgerService) {
		super(graphQlClient, ledgerService);
	}

	@Override
	public Mono<QueryResponse> handleInternal(LedgerDto ledger, TraqCli.Marker.MarkerMark command) {
		return graphQlClient.query(ModifyMarkerGraphQLQuery.newRequest()
						.ledger(ledger.id())
						.marker(command.marker())
						.markers(Stream.concat(
										command.markers().stream(),
										ledger.markers().stream()
												.filter(m -> m.asString().equalsIgnoreCase(command.marker()))
												.flatMap(m -> m.markers().stream()
														.map(MarkerDto::asString)))
								.toList())
						.build(), new ModifyMarkerProjectionRoot<>()
						.__typename()
						.name()
						.title()
						.onProperty().value().root()
						.markers()
						.__typename()
							.name()
							.title()
							.onProperty().value().root());
	}
}
