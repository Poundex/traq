package net.poundex.traq.day;

import io.vavr.collection.LinkedHashMap;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.poundex.tidycli.spring.dispatch.CommandHandler;
import net.poundex.tidycli.ui.Rendering;
import net.poundex.tidycli.ui.rendering.HBox;
import net.poundex.tidycli.ui.rendering.Label;
import net.poundex.tidycli.ui.rendering.Text;
import net.poundex.tidycli.ui.rendering.VBox;
import net.poundex.tidycli.ui.rendering.table.Table;
import net.poundex.tidycli.ui.rendering.table.TableCell;
import net.poundex.traq.cli.TraqCli;
import net.poundex.traq.cli.format.CliMarkerFormat;
import net.poundex.traq.cli.format.CliSpanFormat;
import net.poundex.traq.cli.format.CliTemporalFormat;
import net.poundex.traq.format.SpanFormat;
import net.poundex.traq.ledger.LedgerService;
import net.poundex.traq.marker.MarkerSummaryTable;
import net.poundex.traq.timeline.TimelineEventDto;
import net.poundex.traq.timeline.TimelinePointDto;
import net.poundex.traq.timeline.TimelineSpanDto;
import reactor.core.publisher.Mono;

import java.time.LocalDate;

@Slf4j
@RequiredArgsConstructor
@Data
abstract class AbstractDayCommand<T extends TraqCli.DayMixin, PM extends DayPresentationModel> implements CommandHandler<T> {
	
	protected final LedgerService ledgerService;
	protected final DayService dayService;
	
	protected abstract LocalDate getDay(T command);
	protected abstract Mono<PM> getDayModel(String ledgerId, LocalDate day);
	
	@Override
	public Mono<Rendering> handle(T command) {
		String ledgerName = ledgerService.getLedgerName(command);
		LocalDate day = getDay(command);
		return ledgerService.findLedgerByName(ledgerName)
				.flatMap(ledger -> getDayModel(ledger.id(), day))
				.map(dayPm -> VBox.of(0,

						HBox.of(2, 
								Label.of(CliTemporalFormat.longDatePlain(day), "cyan", "bold"),
								Label.of(String.format("(Ledger: %s)", ledgerName))),
						
						Table.from(dayPm.getDay().events())
								.title("Timeline")
								.columns(c -> c
										.column("Sigil", evt -> Text.of(getSigil(evt)), true)
										.column("Time", evt -> TableCell.builder().content(getEventTime(evt)).noRerender(true).build(), true)
										.column("Duration", evt -> Text.of(evt instanceof TimelineSpanDto span
												? SpanFormat.toElapsedTime(span.duration())
												: ""), true)
										.column("Description", evt ->
												CliMarkerFormat.formatAll(evt.description(), evt.markers()))),

						HBox.of(
								Table.from(LinkedHashMap.of(
										"Time",
										getStartTime(dayPm),

										"Len.",
										SpanFormat.toElapsedTime(dayPm.getDay().length()),

										"Dur.",
										SpanFormat.toElapsedTime(dayPm.getDay().duration()),

										"Acct.",
										"",

										"Prod.",
										"").toJavaList()).columns(c -> c
										.column("Summary", kv -> Text.of(kv._1()))
										.column("Value", kv -> kv._2() instanceof Rendering r ? r : Text.of(kv._2().toString()))),

								MarkerSummaryTable.forMarkerSummaries(dayPm.getMarkerSummaries()))));
	}

	private String getSigil(TimelineEventDto event) {
		return switch (event) {
			case TimelineSpanDto ignored -> "-";
			case TimelinePointDto ignored  -> "*";
		};
	}

	private Rendering getEventTime(TimelineEventDto evt) {
		return switch (evt) {
			case TimelineSpanDto span -> CliSpanFormat.timeRange(span.time(), span.endTime());
			default -> Text.of(CliSpanFormat.time(evt.time()));
		};
	}

	private Rendering getStartTime(DayPresentationModel day) {
		if (day.getDay().startTime() == null)
			return Text.of("N/A");

		return TableCell.builder()
				.content(HBox.of(0,
						CliSpanFormat.timeRange(day.getDay().startTime(), day.getDay().endTime()),
						Text.of(day.getDay().projectedEndTime() != null
								? String.format(" (%s)", CliSpanFormat.time(day.getDay().projectedEndTime()))
								: "")))
				.noRerender(true)
				.build();
	}
}
