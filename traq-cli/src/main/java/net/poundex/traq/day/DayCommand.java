package net.poundex.traq.day;

import com.mdimension.jchronic.Chronic;
import com.mdimension.jchronic.Options;
import com.mdimension.jchronic.tags.Pointer;
import lombok.extern.slf4j.Slf4j;
import net.poundex.traq.cli.TraqCli;
import net.poundex.traq.ledger.LedgerService;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.time.LocalDate;
import java.time.ZoneId;

@Slf4j
@Component
class DayCommand extends AbstractDayCommand<TraqCli.Day, DayPresentationModel> {
	
	public DayCommand(LedgerService ledgerService, DayService dayService) {
		super(ledgerService, dayService);
	}

	@Override
	protected LocalDate getDay(TraqCli.Day command) {
		return Chronic.parse(command.getRawDate(), new Options(Pointer.PointerType.PAST))
				.getBeginCalendar()
				.toInstant()
				.atZone(ZoneId.systemDefault())
				.toLocalDate();
	}

	@Override
	protected Mono<DayPresentationModel> getDayModel(String ledgerId, LocalDate day) {
		return dayService.getDayModel(ledgerId, day);
	}
}
