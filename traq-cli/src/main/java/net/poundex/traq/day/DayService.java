package net.poundex.traq.day;

import net.poundex.traq.marker.MarkableService;
import net.poundex.xmachinery.spring.graphql.client.GraphQlClient;
import org.springframework.aot.hint.annotation.RegisterReflectionForBinding;
import org.springframework.stereotype.Service;

@Service
@RegisterReflectionForBinding(DayDto.class)
class DayService extends AbstractDayService<
		TodayPresentationModel, 
		DayPresentationModel, 
		TodayPresentationModel.TodayPresentationModelBuilder<TodayPresentationModel, ?>, 
		DayPresentationModel.DayPresentationModelBuilder<DayPresentationModel, ?>> {

	public DayService(GraphQlClient graphQlClient, MarkableService markableService) {
		super(graphQlClient, markableService);
	}

	@Override
	protected TodayPresentationModel.TodayPresentationModelBuilder<TodayPresentationModel, ?> newTodayPresentationModel() {
		return TodayPresentationModel.newTodayPresentationModelBuilder();
	}

	@Override
	protected DayPresentationModel.DayPresentationModelBuilder<DayPresentationModel, ?> newDayPresentationModel() {
		return DayPresentationModel.newDayPresentationModelBuilder();
	}
}
