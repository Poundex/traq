package net.poundex.traq.day;

import lombok.extern.slf4j.Slf4j;
import net.poundex.traq.cli.TraqCli;
import net.poundex.traq.ledger.LedgerService;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.time.LocalDate;

@Slf4j
@Component
class YesterdayCommand extends AbstractDayCommand<TraqCli.Yesterday, DayPresentationModel> {
	public YesterdayCommand(LedgerService ledgerService, DayService dayService) {
		super(ledgerService, dayService);
	}

	@Override
	protected LocalDate getDay(TraqCli.Yesterday ignored) {
		return LocalDate.now().minusDays(1);
	}

	@Override
	protected Mono<DayPresentationModel> getDayModel(String ledgerId, LocalDate day) {
		return dayService.getDayModel(ledgerId, day);
	}
}
