package net.poundex.traq.day;

import lombok.extern.slf4j.Slf4j;
import net.poundex.traq.cli.TraqCli;
import net.poundex.traq.ledger.LedgerService;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.time.LocalDate;

@Slf4j
@Component
class TodayCommand extends AbstractDayCommand<TraqCli.Today, TodayPresentationModel> {
	public TodayCommand(LedgerService ledgerService, DayService dayService) {
		super(ledgerService, dayService);
	}

	@Override
	protected LocalDate getDay(TraqCli.Today ignored) {
		return LocalDate.now();
	}

	@Override
	protected Mono<TodayPresentationModel> getDayModel(String ledgerId, LocalDate day) {
		return dayService.getDayModelForToday(ledgerId);
	}
}
