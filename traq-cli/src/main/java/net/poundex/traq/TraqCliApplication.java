package net.poundex.traq;

import lombok.RequiredArgsConstructor;
import net.poundex.traq.cli.NativeHints;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.graphql.GraphQlAutoConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportRuntimeHints;

@SpringBootApplication
@ImportRuntimeHints(NativeHints.class)
@RequiredArgsConstructor
@Import(GraphQlAutoConfiguration.class)
public class TraqCliApplication {
	public static void main(String[] args) {
		System.exit(SpringApplication.exit(SpringApplication.run(TraqCliApplication.class, args)));
	}
}
