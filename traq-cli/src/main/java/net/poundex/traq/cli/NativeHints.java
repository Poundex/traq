package net.poundex.traq.cli;

import com.fasterxml.jackson.core.JsonGenerator;
import com.mdimension.jchronic.repeaters.Repeater;
import com.mdimension.jchronic.repeaters.RepeaterDay;
import com.mdimension.jchronic.repeaters.RepeaterFortnight;
import com.mdimension.jchronic.repeaters.RepeaterHour;
import com.mdimension.jchronic.repeaters.RepeaterMinute;
import com.mdimension.jchronic.repeaters.RepeaterMonth;
import com.mdimension.jchronic.repeaters.RepeaterSeason;
import com.mdimension.jchronic.repeaters.RepeaterSecond;
import com.mdimension.jchronic.repeaters.RepeaterWeek;
import com.mdimension.jchronic.repeaters.RepeaterWeekend;
import com.mdimension.jchronic.repeaters.RepeaterYear;
import com.mdimension.jchronic.tags.Grabber;
import com.mdimension.jchronic.tags.Ordinal;
import com.mdimension.jchronic.tags.Pointer;
import com.mdimension.jchronic.tags.Scalar;
import com.mdimension.jchronic.tags.Separator;
import com.mdimension.jchronic.tags.TimeZone;
import org.springframework.aot.hint.ExecutableMode;
import org.springframework.aot.hint.MemberCategory;
import org.springframework.aot.hint.RuntimeHints;
import org.springframework.aot.hint.RuntimeHintsRegistrar;
import org.springframework.boot.logging.java.SimpleFormatter;
import org.springframework.core.io.ClassPathResource;

import java.lang.reflect.RecordComponent;
import java.lang.runtime.ObjectMethods;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

public class NativeHints implements RuntimeHintsRegistrar {
	@SuppressWarnings("KotlinInternalInJava")
	@Override
	public void registerHints(RuntimeHints hints, ClassLoader classLoader) {
		hints.resources().registerResource(new ClassPathResource("version.txt"));
		hints.resources().registerResource(new ClassPathResource("logging.properties"));
		hints.reflection().registerType(SimpleFormatter.class, 
				MemberCategory.INVOKE_PUBLIC_CONSTRUCTORS);

		hints.resources().registerResource(new ClassPathResource("i18n/Parsing.properties"));
		hints.resources().registerResource(new ClassPathResource("i18n/Scalars.properties"));
		hints.resources().registerResource(new ClassPathResource("i18n/Validation.properties"));

		hints.reflection().registerType(ObjectMethods.class, MemberCategory.PUBLIC_CLASSES);
		hints.reflection().registerType(JsonGenerator.class, MemberCategory.PUBLIC_CLASSES);

//		List.of(Request.class).forEach(cls ->
//				hints.reflection().registerType(cls,
//						MemberCategory.INTROSPECT_PUBLIC_METHODS,
//						MemberCategory.INVOKE_PUBLIC_METHODS));
		
		List.of(HashSet.class).forEach(cls ->
				hints.reflection().registerType(cls,
						MemberCategory.INTROSPECT_PUBLIC_CONSTRUCTORS,
						MemberCategory.INVOKE_PUBLIC_CONSTRUCTORS));

		List.of(
				Grabber.class, 
				Pointer.class, 
				Scalar.class, 
				Ordinal.class, 
				Separator.class,
				TimeZone.class, 
				Repeater.class, 
				RepeaterDay.class, 
				RepeaterFortnight.class, 
				RepeaterHour.class, 
				RepeaterMinute.class, 
				RepeaterMonth.class, 
				RepeaterSeason.class,
				RepeaterSecond.class, 
				RepeaterWeek.class, 
				RepeaterWeekend.class,
				RepeaterYear.class).forEach(cls ->
				hints.reflection().registerType(cls,
						MemberCategory.INTROSPECT_PUBLIC_METHODS,
						MemberCategory.INVOKE_PUBLIC_METHODS, 
						MemberCategory.INTROSPECT_PUBLIC_CONSTRUCTORS, 
						MemberCategory.INVOKE_PUBLIC_CONSTRUCTORS));

//		{
//			"name":"java.lang.reflect.RecordComponent",
//				"methods":[{
//			"name":"getAccessor", "parameterTypes":[]},{
//			"name":"getName", "parameterTypes":[]},{
//			"name":"getType", "parameterTypes":[]}]
//		},

		hints.reflection().registerType(RecordComponent.class, b -> 
				b.withMethod("getAccessor", Collections.emptyList(), ExecutableMode.INVOKE)
						.withMethod("getName", Collections.emptyList(), ExecutableMode.INVOKE)
						.withMethod("getType", Collections.emptyList(), ExecutableMode.INVOKE));

	}
}
