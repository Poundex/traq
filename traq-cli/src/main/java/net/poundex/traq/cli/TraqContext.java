package net.poundex.traq.cli;

import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.moandjiezana.toml.Toml;
import com.netflix.graphql.dgs.client.GraphQLError;
import com.netflix.graphql.dgs.client.MonoGraphQLClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.poundex.tidycli.binder.Binder;
import net.poundex.tidycli.spring.dispatch.CommandDispatcher;
import net.poundex.tidycli.spring.natives.ToolInterface;
import net.poundex.traq.TraqModule;
import net.poundex.traq.cli.exception.ClientException;
import net.poundex.xmachinery.fio.io.IO;
import net.poundex.xmachinery.fio.spring.FioConfiguration;
import net.poundex.xmachinery.fio.xdg.Xdg;
import net.poundex.xmachinery.spring.graphql.client.ArgumentCoercingGraphQlClientDecorator;
import net.poundex.xmachinery.spring.graphql.client.ErrorHandlingGraphQlClientDecorator;
import net.poundex.xmachinery.spring.graphql.client.GraphQlClient;
import net.poundex.xmachinery.spring.graphql.client.ResponseFixingGraphQlClientDecorator;
import org.springframework.aot.hint.annotation.RegisterReflectionForBinding;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.autoconfigure.http.codec.CodecsAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.graphql.execution.GraphQlSource;
import org.springframework.web.reactive.function.client.WebClient;

import java.nio.file.Files;
import java.nio.file.Path;

@Configuration
@RequiredArgsConstructor
@Import({FioConfiguration.class, CodecsAutoConfiguration.class})
@Slf4j
@ToolInterface(TraqCli.class)
class TraqContext {
	
	@Bean
	public TraqCli traqCli(ApplicationArguments arguments) {
		return Binder.bind(arguments.getSourceArgs(), TraqCli.class);
	}
	
	@Bean
	public CommandDispatcher<TraqCli> commandDispatcher(TraqCli traqCli) {
		return CommandDispatcher.forCommand(traqCli);
	}
	
	@Bean
	public RuntimeConfiguration runtimeConfiguration(TraqCli traqCli, Xdg xdg, IO io, ObjectMapper objectMapper) {
		Path configFile = traqCli.getConfigFilePath()
				.orElse(xdg.getConfigDirectory("traq").resolve("traq.toml"));
		
		if( ! Files.exists(configFile))
			throw new ClientException("No config file found at " + configFile);
		
		return io.withReader(configFile, 
				r -> new Toml(objectMapper).read(r).to(RuntimeConfiguration.class));
	}
	
	@Bean
	public WebClient webClient(WebClient.Builder webClientBuilder, RuntimeConfiguration runtimeConfiguration) {
		return webClientBuilder
				.baseUrl(runtimeConfiguration.apiEndpoint().toString())
				.build();
	}
	
	@Bean
	@RegisterReflectionForBinding(GraphQLError.class)
	public GraphQlClient graphQLClient(
			RuntimeConfiguration runtimeConfiguration, 
			WebClient webClient, 
			GraphQlSource graphQlSource, 
			ObjectMapper objectMapper) {

		return ResponseFixingGraphQlClientDecorator.wrap(
				ArgumentCoercingGraphQlClientDecorator.wrap(
						ErrorHandlingGraphQlClientDecorator.wrap(MonoGraphQLClient
								.createWithWebClient(webClient.mutate()
										.baseUrl(runtimeConfiguration.apiEndpoint()
												.resolve("graphql")
												.toString())
										.build()), objectMapper), graphQlSource), objectMapper, graphQlSource);
	}
	
	@Bean
	public Module traqModule() {
		return new TraqModule();
	}
}
