package net.poundex.traq.cli.format;

import net.poundex.tidycli.ui.Rendering;
import net.poundex.tidycli.ui.rendering.HBox;
import net.poundex.tidycli.ui.rendering.Label;
import net.poundex.tidycli.ui.rendering.Text;

import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import static net.poundex.traq.cli.format.CliTemporalFormat.instantToDateTime;

public class CliSpanFormat {
	
	private static final DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm", 
			Locale.getDefault());

	public static Rendering timeRange(Instant start, Instant maybeEnd) {
		return HBox.of(0, Text.of(String.format("%s%s",
						timeFormatter.format(instantToDateTime(start)),
						maybeEnd != null
								? String.format(" - %s", timeFormatter.format(instantToDateTime(maybeEnd)))
								: "")),
				Label.of(maybeEnd == null ? "•" : "", "red", "bold"));
	}

	public static String timeRangePlain(Instant start, Instant maybeEnd) {
		return String.format("%s%s",
						timeFormatter.format(instantToDateTime(start)),
						maybeEnd != null
								? String.format(" - %s", timeFormatter.format(instantToDateTime(maybeEnd)))
								: "");
	}
	
	public static String time(Instant time) {
		return timeFormatter.format(instantToDateTime(time));
	}
}
