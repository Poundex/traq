package net.poundex.traq.cli;

import org.springframework.aot.hint.annotation.RegisterReflectionForBinding;

import java.net.URI;

@RegisterReflectionForBinding(RuntimeConfiguration.class)
public record RuntimeConfiguration(URI apiEndpoint, String defaultLedgerName) {
}
