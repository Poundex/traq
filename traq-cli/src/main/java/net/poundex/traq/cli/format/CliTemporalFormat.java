package net.poundex.traq.cli.format;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import static net.poundex.traq.format.TemporalFormat.getOrd;

public class CliTemporalFormat {

	public static String longDatePlain(LocalDate localDate) {
		String ord = getOrd(localDate.getDayOfMonth());
		return DateTimeFormatter.ofPattern("eeee, d'" + ord + "' MMMM yyyy")
				.format(localDate);
	}
	
	public static String instantPlain(Instant instant) {
		return DateTimeFormatter.ofPattern("yyyy-MM-dd @ HH:mm").format(instantToDateTime(instant));
	}

	public static ZonedDateTime instantToDateTime(Instant instant) {
		return ZonedDateTime.ofInstant(instant, ZoneId.systemDefault());
	}
}
