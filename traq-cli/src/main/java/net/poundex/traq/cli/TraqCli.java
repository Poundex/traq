package net.poundex.traq.cli;

import net.poundex.tidycli.model.Argument;
import net.poundex.tidycli.model.Command;
import net.poundex.tidycli.model.Flag;
import net.poundex.tidycli.model.Input;
import net.poundex.tidycli.model.Tool;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.StreamUtils;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

@Tool(name = "traq", title = "Traq")
public interface TraqCli {
	
	@Flag(longName = "verbose", shortName = "v")
	int getVerbosityLevel();
	
	@Argument(longName = "config-file", shortName = "C",
			description = "Path to configuration file")
	Optional<Path> getConfigFilePath();
	
	interface LedgerMixin {
		@Argument(longName = "ledger-name", shortName = "L",
			description = "Ledger name")
		Optional<String> getLedgerName();
	}
	
	interface CreateEventMixin extends LedgerMixin {
		@Input(squash = true)
		String getDescription();
	}
	
	@Command(value = "add", description = "Add a Point to the Timeline")
	interface AddPoint extends TraqCli, CreateEventMixin { }
	
	@Command(value = "start", description = "Start a new Timeline Span")
	interface StartSpan extends TraqCli, CreateEventMixin { }

	@Command(value = "stop", description = "Stops currently active Timeline Span")
	interface StopSpan extends TraqCli, LedgerMixin { }
	
	@Command(value = "marker", description = "Marker commands")
	interface Marker extends TraqCli { 
		@Command(value = "list", description = "List markers")
		interface MarkerList extends TraqCli, LedgerMixin { }
		
		@Command(value = "mark", description = "Add markers to marker") 
		interface MarkerMark extends TraqCli, LedgerMixin {
			@Input(0)
			String marker();
			
			@Input(value = 1, squash = true)
			List<String> markers();
		}

		@Command(value = "remark", description = "Set/replace/remove markers on marker")
		interface MarkerRemark extends TraqCli, LedgerMixin {
			@Input(0)
			String marker();
		}
		
		@Command(value = "title", description = "Update Marker title")
		interface MarkerSetTitle extends TraqCli, LedgerMixin {
			@Input(0)
			String marker();
			
			@Input(value = 1, squash = true)
			String newTitle();
		}
	}
	
	@Command(value = "import", description = "Import Timeline data from CSV")
	interface ImportFromFile extends TraqCli, LedgerMixin {
		@Input(0)
		Path getInputFile();
	}
	
	@Command(value = "amend", description = "Amend the last Span")
	interface Amend extends TraqCli, LedgerMixin {
		@Flag(longName = "stop", description = "Stop the amended Span")
		boolean isStop();
	}
	
	interface DayMixin extends TraqCli, LedgerMixin { }
	
	@Command(value = "today", description = "View day summary for today")
	interface Today extends DayMixin { }
	
	@Command(value = "yesterday", description = "View day summary for yesterday")
	interface Yesterday extends DayMixin { }
	
	@Command(value = "day", description = "View day summary for a day")
	interface Day extends DayMixin { 
		@Input(squash = true)
		String getRawDate();
	}
	
	@Command(value = "summary", description = "Event Summary")
	interface Summary extends LedgerMixin, TraqCli {
		@Input(squash = true)
		String getTimelineQuery();
	}
	
	@Command(value = "again", description = "Repeat a span from recent history")
	interface SpanFromHistory extends TraqCli, LedgerMixin { }
}

class VersionProvider implements Supplier<String> {
	@Override
	public String get() {
		try {
			return StreamUtils.copyToString(
					new ClassPathResource("version.txt").getInputStream(),
					StandardCharsets.UTF_8);
		}
		catch (IOException ioex) {
			throw new UncheckedIOException(ioex);
		}
	}
}
