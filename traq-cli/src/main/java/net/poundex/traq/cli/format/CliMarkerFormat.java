package net.poundex.traq.cli.format;

import net.poundex.tidycli.ui.rendering.InlineLabel;
import net.poundex.traq.format.MarkerFormat;
import net.poundex.traq.marker.BasePropertyDto;
import net.poundex.traq.marker.MarkerDto;
import net.poundex.traq.marker.MentionDto;
import net.poundex.traq.marker.PropertyDto;
import net.poundex.traq.marker.TagDto;

import java.util.Set;
import java.util.regex.Pattern;

public class CliMarkerFormat {
	
	@Deprecated
	private static final Pattern tagPattern = Pattern.compile("\\B(\\+[\\w-]+)");
	@Deprecated
	private static final Pattern mentionPattern = Pattern.compile("\\B(@[\\w-]+)");
	@Deprecated
	private static final Pattern propPattern = Pattern.compile("\\B(:[\\w-]+(=[\\w-]+)?)");
	
	@Deprecated
	public static InlineLabel formatMarkersInString(String string) {
		String s = tagPattern.matcher(string).replaceAll("@|cyan $1|@");
		s = mentionPattern.matcher(s).replaceAll("@|magenta $1|@");
		return InlineLabel.of(propPattern.matcher(s).replaceAll("@|yellow $1|@"));
	}

	private static final MarkerFormat.MarkerFormatter cliFormatter = new MarkerFormat.MarkerFormatter() {
		@Override
		public String formatBaseProperty(String markerWithSigil, BasePropertyDto dto) {
			return doFormat("yellow", markerWithSigil, dto);
		}

		@Override
		public String formatMention(String markerWithSigil, MentionDto dto) {
			return doFormat("magenta", markerWithSigil, dto);
		}

		@Override
		public String formatPropertyDto(String markerWithSigil, PropertyDto dto) {
			return doFormat("yellow", markerWithSigil, dto);
		}

		@Override
		public String formatTag(String markerWithSigil, TagDto dto) {
			return doFormat("cyan", markerWithSigil, dto);
		}
		
		private String doFormat(String code, String markerWithSigil, MarkerDto dto) {
			return String.format("@|%s %s|@", code, markerWithSigil);
		}
	};

	public static String format(MarkerDto dto) {
		return format(dto, null);
	}

	public static String format(MarkerDto dto, String original) {
		return MarkerFormat.format(dto, cliFormatter, original);
	}

	public static InlineLabel formatAll(String stringWithMarkers, Set<? extends MarkerDto> markers) {
		return InlineLabel.of(MarkerFormat.formatAll(stringWithMarkers, markers, CliMarkerFormat::format));
	}
}
