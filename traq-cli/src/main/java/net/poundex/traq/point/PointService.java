package net.poundex.traq.point;

import lombok.RequiredArgsConstructor;
import net.poundex.traq.api.client.CreatePointGraphQLQuery;
import net.poundex.traq.api.client.CreatePointProjectionRoot;
import net.poundex.traq.ledger.LedgerService;
import net.poundex.traq.timeline.CreatePointInputDto;
import net.poundex.traq.timeline.TimelinePointDto;
import net.poundex.xmachinery.spring.graphql.client.GraphQlClient;
import org.springframework.aot.hint.annotation.RegisterReflectionForBinding;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
@RegisterReflectionForBinding({
		TimelinePointDto.class, 
		CreatePointInputDto.class
})
class PointService {
	
	private final GraphQlClient graphQlClient;
	private final LedgerService ledgerService;

	public Mono<TimelinePointDto> createPoint(String ledgerName, String description) {
		return ledgerService.findLedgerByName(ledgerName)
				.flatMap(l ->
						graphQlClient.query(CreatePointGraphQLQuery.newRequest()
										.ledger(l.id())
										.point(new CreatePointInputDto(null, null, description))
										.build(), new CreatePointProjectionRoot<>()
											.description()
											.day()
											.time()
											.markers()
												.__typename()
												.name()
												.title()
												.onProperty().value().root()
											.ledger()
												.name().parent())
								.map(resp -> resp.extractValue("createPoint", TimelinePointDto.class)));
	}
}
