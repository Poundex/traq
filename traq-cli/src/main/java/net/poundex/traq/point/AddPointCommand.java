package net.poundex.traq.point;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.poundex.tidycli.spring.dispatch.CommandHandler;
import net.poundex.tidycli.ui.Rendering;
import net.poundex.traq.cli.TraqCli;
import net.poundex.traq.ledger.LedgerService;
import net.poundex.traq.timeline.TimelineEventModificationRendering;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Slf4j
@RequiredArgsConstructor
@Data
@Component
class AddPointCommand implements CommandHandler<TraqCli.AddPoint> {
	
	private final PointService pointService;
	private final LedgerService ledgerService;

	@Override
	public Mono<Rendering> handle(TraqCli.AddPoint command) {
		return pointService.createPoint(
						ledgerService.getLedgerName(command), command.getDescription())
				.map(p -> TimelineEventModificationRendering.get(
						"📌",
						p.description(),
						p.ledger().name(),
						p.day(),
						p.time(), 
						null, 
						p.markers()));
	}
}
