package net.poundex.traq.timeline;

import net.poundex.traq.ledger.LedgerDto;
import net.poundex.traq.ledger.LedgerService;
import net.poundex.traq.marker.MarkableService;
import net.poundex.traq.summary.EventSummaryDto;
import net.poundex.xmachinery.spring.graphql.client.GraphQlClient;
import org.springframework.aot.hint.annotation.RegisterReflectionForBinding;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
@RegisterReflectionForBinding(EventSummaryDto.class)
class CliTimelineService extends AbstractTimelineService {
	
	private final LedgerService ledgerService;

	public CliTimelineService(GraphQlClient graphQlClient, MarkableService markableService, LedgerService ledgerService) {
		super(graphQlClient, markableService);
		this.ledgerService = ledgerService;
	}

	@Override
	protected Mono<LedgerDto> getLedgerByName(String ledgerName) {
		return ledgerService.findLedgerByName(ledgerName);
	}
}
