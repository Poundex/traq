package net.poundex.traq.timeline;

import lombok.RequiredArgsConstructor;
import net.poundex.tidycli.ui.rendering.HBox;
import net.poundex.tidycli.ui.rendering.Label;
import net.poundex.tidycli.ui.rendering.Text;
import net.poundex.tidycli.ui.rendering.VBox;
import net.poundex.traq.cli.format.CliMarkerFormat;
import net.poundex.traq.cli.format.CliTemporalFormat;
import net.poundex.traq.format.SpanFormat;
import net.poundex.traq.marker.MarkerDto;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Set;

@RequiredArgsConstructor
public class TimelineEventModificationRendering {
	
	public static VBox get(String glyph, String description, String ledgerName, LocalDate day, Instant time, Duration duration, Set<? extends MarkerDto> markers) {
		return VBox.of(0,
				HBox.of(1, Text.of(glyph),
						CliMarkerFormat.formatAll(description, markers)),
				Label.of("Ledger:  " + ledgerName, "faint"),
				Label.of("Day:     " + CliTemporalFormat.longDatePlain(day), "faint"),
				Label.of("Time:    " + CliTemporalFormat.instantPlain(time) + (duration != null ? " (" + SpanFormat.toElapsedTime(duration) + ")" : ""), "faint"));
	}
}
