package net.poundex.traq.timeline;

import io.vavr.collection.LinkedHashMap;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.poundex.tidycli.spring.dispatch.CommandHandler;
import net.poundex.tidycli.ui.Rendering;
import net.poundex.tidycli.ui.rendering.HBox;
import net.poundex.tidycli.ui.rendering.InlineLabel;
import net.poundex.tidycli.ui.rendering.Label;
import net.poundex.tidycli.ui.rendering.Text;
import net.poundex.tidycli.ui.rendering.VBox;
import net.poundex.tidycli.ui.rendering.table.Table;
import net.poundex.tidycli.ui.rendering.table.TableCell;
import net.poundex.traq.cli.TraqCli;
import net.poundex.traq.cli.format.CliMarkerFormat;
import net.poundex.traq.cli.format.CliTemporalFormat;
import net.poundex.traq.format.SpanFormat;
import net.poundex.traq.ledger.LedgerService;
import net.poundex.traq.marker.MarkerSummaryTable;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Slf4j
@RequiredArgsConstructor
@Data
@Component
public class SummaryCommand implements CommandHandler<TraqCli.Summary> {
	
	private final LedgerService ledgerService;
	private final CliTimelineService timelineService;

	@Override
	public Mono<Rendering> handle(TraqCli.Summary command) {
		return timelineService.getEventSummary(
						ledgerService.getLedgerName(command),
						command.getTimelineQuery())
				.map(pm -> VBox.of(0,

						HBox.of(2,
								Label.of(String.format("Summary of (%s) since %s", 
										pm.getEventSummary().parsedQuery(), 
										pm.getEventSummary().since() == null ? "ever" : pm.getEventSummary().since()), "cyan", "bold"),
								Label.of(String.format("(Ledger: %s)", pm.getEventSummary().ledger().name()))),

						Table.from(pm.getSortedGroupedSpans()
										.limit(5)
										.toList())
								.title("Top Spans")
								.columns(c -> c
										.column("Time", evt ->
												TableCell.builder().content(getEventTime(evt)).noRerender(true).build(), true)
										.column("Description", this::getDescription)
										.column("Duration", evt -> Text.of(SpanFormat.toElapsedTime(evt.durationSum())), true)),


						Table.from(pm.getEventSummary().events().stream()
										.filter(e -> e instanceof TimelinePointDto)
										.sorted((l, r) -> r.time().compareTo(l.time()))
										.limit(5)
										.toList())
								.title("Most Recent Points")
								.columns(c -> c
										.column("Time", evt ->
												TableCell.builder().content(Text.of(CliTemporalFormat.instantPlain(evt.time()))).noRerender(true).build(), true)
										.column("Description", evt -> CliMarkerFormat.formatMarkersInString(evt.description()))),

						HBox.of(
								Table.from(LinkedHashMap.of(
												"Duration", SpanFormat.toElapsedTime(pm.getEventSummary().duration())).toJavaList())
										.columns(c -> c
												.column("Summary", kv -> Text.of(kv._1()))
												.column("Value", kv -> Text.of(kv._2()))),

								MarkerSummaryTable.forMarkerSummaries(pm.getMarkerSummaries()))));

	}

	private Rendering getDescription(EventSummaryPresentationModel.GroupedSpans evt) {
		return CliMarkerFormat.formatMarkersInString(evt.description());
	}

	private Rendering getEventTime(EventSummaryPresentationModel.GroupedSpans spans) {
		if(spans.count() == 1)
			return Text.of(CliTemporalFormat.instantPlain(
					spans.spans().get(0).time()));

		return InlineLabel.of("@|bold,green ×" + spans.count() + "|@");
	}
}
