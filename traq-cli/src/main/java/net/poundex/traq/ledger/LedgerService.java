package net.poundex.traq.ledger;

import com.netflix.graphql.dgs.client.codegen.BaseSubProjectionNode;
import lombok.RequiredArgsConstructor;
import net.poundex.traq.api.client.LedgerByNameGraphQLQuery;
import net.poundex.traq.api.client.LedgerByNameProjectionRoot;
import net.poundex.traq.cli.RuntimeConfiguration;
import net.poundex.traq.cli.TraqCli;
import net.poundex.traq.cli.exception.ClientException;
import net.poundex.xmachinery.spring.graphql.client.GraphQlClient;
import org.springframework.aot.hint.annotation.RegisterReflectionForBinding;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.Optional;

@Service
@RequiredArgsConstructor
@RegisterReflectionForBinding(LedgerDto.class)
public class LedgerService {
	
	private final RuntimeConfiguration runtimeConfiguration;
	private final GraphQlClient graphQlClient;
	
	public String getLedgerName(TraqCli.LedgerMixin ledgerMixin) {
		return ledgerMixin.getLedgerName()
				.or(() -> Optional.ofNullable(runtimeConfiguration.defaultLedgerName()))
				.orElseThrow(() -> new ClientException("No Ledger name given with -L and no default name in config"));
	}

	public Mono<LedgerDto> findLedgerByName(String ledgerName) {
		return findLedgerByName(ledgerName, new LedgerByNameProjectionRoot<>().id().name());
	}
	
	public Mono<LedgerDto> findLedgerByName(
				String ledgerName,
				LedgerByNameProjectionRoot<BaseSubProjectionNode<?, ?>, BaseSubProjectionNode<?, ?>> projectionRoot) {
		return graphQlClient.query(LedgerByNameGraphQLQuery.newRequest().name(ledgerName).build(), projectionRoot)
				.map(resp -> resp.extractValue(LedgerDto.class));
	}
}
