package net.poundex.traq.importexport;

import lombok.RequiredArgsConstructor;
import org.springframework.core.io.PathResource;
import org.springframework.http.MediaType;
import org.springframework.http.client.MultipartBodyBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.nio.file.Path;

@Service
@RequiredArgsConstructor
class ImportService {

	private final WebClient webClient;
	
	public Mono<ImportResultDto> importFromFile(String ledgerName, String zoneId, Path inputFile) {
		MultipartBodyBuilder multipartBodyBuilder = new MultipartBodyBuilder();
		multipartBodyBuilder.part("importFile", new PathResource(inputFile));

		return webClient
				.post()
				.uri(String.format("/import/%s?zoneId=%s", ledgerName, zoneId))
				.contentType(MediaType.MULTIPART_FORM_DATA)
				.body(BodyInserters.fromMultipartData(multipartBodyBuilder.build()))
				.retrieve()
				.bodyToMono(ImportResultDto.class);
	}
}
