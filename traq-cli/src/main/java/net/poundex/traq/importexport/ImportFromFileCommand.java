package net.poundex.traq.importexport;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.poundex.tidycli.spring.dispatch.CommandHandler;
import net.poundex.tidycli.ui.Rendering;
import net.poundex.tidycli.ui.rendering.Text;
import net.poundex.traq.cli.TraqCli;
import net.poundex.traq.ledger.LedgerService;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.time.ZoneId;

@Slf4j
@RequiredArgsConstructor
@Data
@Component
public class ImportFromFileCommand implements CommandHandler<TraqCli.ImportFromFile> {
	
	private final ImportService importService;
	private final LedgerService ledgerService;

	@Override
	public Mono<Rendering> handle(TraqCli.ImportFromFile command) {
		return importService.importFromFile(
						ledgerService.getLedgerName(command),
						ZoneId.systemDefault().toString(),
						command.getInputFile())
				.map(r -> Text.of(r.toString()));
	}
}
