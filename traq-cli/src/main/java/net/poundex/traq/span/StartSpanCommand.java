package net.poundex.traq.span;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.poundex.tidycli.spring.dispatch.CommandHandler;
import net.poundex.tidycli.ui.Rendering;
import net.poundex.traq.cli.TraqCli;
import net.poundex.traq.ledger.LedgerService;
import net.poundex.traq.timeline.TimelineEventModificationRendering;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@Data
@Component
public class StartSpanCommand implements CommandHandler<TraqCli.StartSpan> {
	
	private final SpanService spanService;
	private final LedgerService ledgerService;

	@Override
	public Flux<Rendering> handle(TraqCli.StartSpan command) {
		return spanService.startSpan(
						ledgerService.getLedgerName(command), command.getDescription())
				.flatMapMany(ssi -> Flux.concat(
						Flux.just(TimelineEventModificationRendering.get(
								"⏱️",
								ssi.span().description(), 
								ssi.span().ledger().name(), 
								ssi.span().day(), 
								ssi.span().time(), 
								null, 
								ssi.span().markers())),
						Flux.just(StopSpanCommand.respond(Optional.ofNullable(ssi.previousSpan())))));
	}
}
