package net.poundex.traq.span;

import lombok.RequiredArgsConstructor;
import net.poundex.traq.api.client.SpansGraphQLQuery;
import net.poundex.traq.api.client.SpansProjectionRoot;
import net.poundex.traq.api.client.StartSpanGraphQLQuery;
import net.poundex.traq.api.client.StartSpanProjectionRoot;
import net.poundex.traq.api.client.StopActiveSpanGraphQLQuery;
import net.poundex.traq.api.client.StopActiveSpanProjectionRoot;
import net.poundex.traq.api.client.UpdateDescriptionGraphQLQuery;
import net.poundex.traq.api.client.UpdateDescriptionProjectionRoot;
import net.poundex.traq.ledger.LedgerService;
import net.poundex.traq.timeline.StartSpanInfoDto;
import net.poundex.traq.timeline.StartSpanInputDto;
import net.poundex.traq.timeline.TimelineSpanDto;
import net.poundex.xmachinery.spring.graphql.client.GraphQlClient;
import org.springframework.aot.hint.annotation.RegisterReflectionForBinding;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@RegisterReflectionForBinding({
		TimelineSpanDto.class,
		StartSpanInfoDto.class,
		StartSpanInputDto.class
})
class SpanService {
	
	private final GraphQlClient graphQlClient;
	private final LedgerService ledgerService;

	public Mono<StartSpanInfoDto> startSpan(String ledgerName, String description) {
		return ledgerService.findLedgerByName(ledgerName)
				.flatMap(l ->
						graphQlClient.query(StartSpanGraphQLQuery.newRequest()
										.ledger(l.id())
										.span(new StartSpanInputDto(null, null, description))
										.build(), new StartSpanProjectionRoot<>()
											.span()
												.description()
												.day()
												.time()
												.markers()
													.__typename()
													.name()
													.title()
													.onProperty()
														.value()
														.parent()
													.parent()
												.ledger()
													.name()
													.parent()
											.root()
											.previousSpan()
												.day()
												.description()
												.endTime()
												.markers()
													.__typename()
													.name()
													.title()
													.onProperty()
														.value()
														.parent()
													.parent()
												.ledger()
													.name()
													.parent()
												.duration())
								.map(resp -> resp.extractValue("startSpan", StartSpanInfoDto.class)));
	}

	public Mono<Optional<TimelineSpanDto>> stopSpan(String ledgerName) {
		return ledgerService.findLedgerByName(ledgerName)
				.flatMap(l ->
						graphQlClient.query(StopActiveSpanGraphQLQuery.newRequest()
										.ledger(l.id())
								.build(), new StopActiveSpanProjectionRoot<>()
									.description()
									.ledger()
										.name().parent()
									.day()
									.time()
									.endTime()
										.markers()
										.__typename()
										.name()
										.title()
										.onProperty()
											.value()
											.root()
									.duration())
								.mapNotNull(resp -> resp.extractValue("stopActiveSpan", TimelineSpanDto.class)))
				.singleOptional();
	}
	
	public Mono<List<TimelineSpanDto>> getLastSpans(LocalDate day, String ledgerName, Boolean active, int last) {
		return ledgerService.findLedgerByName(ledgerName)
				.flatMap(l ->
						graphQlClient.query(SpansGraphQLQuery.newRequest()
										.ledger(l.id())
										.day(day)
//										.active(active) // TODO
										.last(last)
										.build(), new SpansProjectionRoot<>()
										.id()
										.description()
										.markers()
											.__typename()
											.name()
											.title()
											.onProperty()
												.value()
												.root())
								.mapNotNull(resp -> resp.extractList("spans", TimelineSpanDto.class)));
	}

	public Mono<TimelineSpanDto> amend(TimelineSpanDto lastSpan, String newDescription) {
		return graphQlClient.query(UpdateDescriptionGraphQLQuery.newRequest()
						.event(lastSpan.id())
						.description(newDescription)
						.build(), new UpdateDescriptionProjectionRoot<>()
						.id()
						.description()
						.markers()
							.__typename()
							.name()
							.title()
							.onProperty()
								.value()
								.root())
				.mapNotNull(resp -> resp.extractValue("updateDescription", TimelineSpanDto.class));
	}
}
