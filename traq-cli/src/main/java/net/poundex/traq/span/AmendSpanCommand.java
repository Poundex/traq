package net.poundex.traq.span;

import io.vavr.control.Try;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.poundex.tidycli.spring.dispatch.CommandHandler;
import net.poundex.tidycli.ui.Rendering;
import net.poundex.tidycli.ui.rendering.HBox;
import net.poundex.tidycli.ui.rendering.Label;
import net.poundex.traq.cli.TraqCli;
import net.poundex.traq.cli.format.CliMarkerFormat;
import net.poundex.traq.ledger.LedgerService;
import net.poundex.traq.timeline.TimelineSpanDto;
import net.poundex.xmachinery.fio.io.IO;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.nio.file.Path;
import java.time.LocalDate;
import java.util.Objects;

@Slf4j
@RequiredArgsConstructor
@Data
@Component
public class AmendSpanCommand implements CommandHandler<TraqCli.Amend> {
	
	private final SpanService spanService;
	private final LedgerService ledgerService;
	private final IO io;

	@Override
	public Flux<Rendering> handle(TraqCli.Amend command) {
		String ledgerName = ledgerService.getLedgerName(command);
		return Flux.concat(
				spanService.getLastSpans(LocalDate.now(), ledgerName, null, 1)
						.map(list -> list.get(0))
						.single()
						.flatMap(lastSpan -> spanService.amend(lastSpan, editDescription(lastSpan)))
						.map(span -> HBox.of(0,
								Label.of("Updated Description: ", "bold"), 
								CliMarkerFormat.formatAll(span.description(), span.markers()))),
				maybeStop(command, ledgerName));
	}

	private Mono<Rendering> maybeStop(TraqCli.Amend command, String ledgerName) {
		if( ! command.isStop())
			return Mono.empty();
		
		return spanService.stopSpan(ledgerName).map(StopSpanCommand::respond);
	}

	private String editDescription(TimelineSpanDto lastSpan) {
		Path tempFile = io.createTempFile("traq", ".txt");
		io.writeString(tempFile, lastSpan.description());
		String editor = Objects.requireNonNull(System.getenv("EDITOR"), "$EDITOR");
		Integer exit = Try.of(() -> new ProcessBuilder(editor, tempFile.toAbsolutePath().toString())
						.inheritIO()
						.start()
						.waitFor())
				.get();
		if(exit != 0)
			throw new IllegalStateException("No clean editor exit");
		return io.readString(tempFile);
	}
}
