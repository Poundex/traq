package net.poundex.traq.span;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.poundex.tidycli.spring.dispatch.CommandHandler;
import net.poundex.tidycli.ui.Rendering;
import net.poundex.tidycli.ui.rendering.Text;
import net.poundex.traq.cli.TraqCli;
import net.poundex.traq.ledger.LedgerService;
import net.poundex.traq.timeline.TimelineEventModificationRendering;
import net.poundex.traq.timeline.TimelineSpanDto;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@Data
@Component
public class StopSpanCommand implements CommandHandler<TraqCli.StopSpan> {
	
	private final SpanService spanService;
	private final LedgerService ledgerService;

	@Override
	public Mono<Rendering> handle(TraqCli.StopSpan command) {
		return spanService.stopSpan(ledgerService.getLedgerName(command))
				.map(StopSpanCommand::respond);
	}
	
	public static Rendering respond(Optional<TimelineSpanDto> maybeStoppedSpan) {
		return maybeStoppedSpan
				.<Rendering>map(span -> TimelineEventModificationRendering.get(
						"🛑", span.description(), span.ledger().name(), span.day(), span.endTime(), span.duration(), span.markers()))
				.orElseGet(() -> Text.of("⚪ Stopped nothing"));
	}
}
