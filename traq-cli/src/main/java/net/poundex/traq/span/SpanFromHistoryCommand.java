package net.poundex.traq.span;

import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.poundex.tidycli.spring.dispatch.CommandHandler;
import net.poundex.tidycli.ui.Rendering;
import net.poundex.traq.cli.TraqCli;
import net.poundex.traq.cli.format.CliMarkerFormat;
import net.poundex.traq.ledger.LedgerService;
import net.poundex.traq.timeline.TimelineEventModificationRendering;
import org.jline.consoleui.prompt.ConsolePrompt;
import org.jline.consoleui.prompt.builder.ListPromptBuilder;
import org.jline.consoleui.prompt.builder.PromptBuilder;
import org.jline.terminal.Terminal;
import org.jline.terminal.TerminalBuilder;
import org.reactivestreams.Publisher;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

import java.time.LocalDate;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

@Slf4j
@Component
@RequiredArgsConstructor
class SpanFromHistoryCommand implements CommandHandler<TraqCli.SpanFromHistory> {

	private final SpanService spanService;
	private final LedgerService ledgerService;

	@Override
	public Publisher<Rendering> handle(TraqCli.SpanFromHistory command) {
		String ledgerName = ledgerService.getLedgerName(command);

		Terminal terminal = Try.of(TerminalBuilder::terminal).get();
		ConsolePrompt prompt = new ConsolePrompt(terminal);
		PromptBuilder promptBuilder = prompt.getPromptBuilder();

		ListPromptBuilder builder = promptBuilder
				.createListPrompt()
				.name("description")
				.message("Span from History");

		AtomicReference<String> description = new AtomicReference<>();
		return Flux.concat(
				spanService.getLastSpans(LocalDate.now(), ledgerName, false, 8)
						.map(spans -> (maxWidth, paintbrush) -> {
							spans.forEach(span -> {
								String render = CliMarkerFormat.formatAll(span.description(), span.markers()).render(maxWidth, paintbrush) + " ";
								builder
										.newItem(span.description())
										.text(render)
										.add();
							});
							description.set(Try.of(() -> prompt.prompt(builder.addPrompt().build())).get().get("description").getResult());
							return "";
						}),

				Flux.defer(() -> startSpan(command, description.get())));
	}

	private Publisher<Rendering> startSpan(TraqCli.SpanFromHistory command, String description) {
		return spanService.startSpan(ledgerService.getLedgerName(command), description)
				.flatMapMany(ssi -> Flux.concat(
						Flux.just(TimelineEventModificationRendering.get("⏱️",
								ssi.span().description(), ssi.span().ledger().name(), ssi.span().day(), ssi.span().time(), null, ssi.span().markers())),
						Flux.just(StopSpanCommand.respond(Optional.ofNullable(ssi.previousSpan())))));
	}

}
