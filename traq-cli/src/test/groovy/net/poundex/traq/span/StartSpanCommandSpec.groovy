package net.poundex.traq.span

import net.poundex.tidycli.ui.Rendering
import net.poundex.tidycli.ui.rendering.Text
import net.poundex.traq.cli.TraqCli
import net.poundex.traq.ledger.LedgerService
import net.poundex.traq.timeline.StartSpanInfoDto
import net.poundex.traq.timeline.TimelineSpanDto
import reactor.core.publisher.Mono
import spock.lang.PendingFeature
import spock.lang.Specification
import spock.lang.Subject

import java.nio.file.Path
import java.time.Duration
import java.time.Instant
import java.time.LocalDate

class StartSpanCommandSpec extends Specification {
	
	SpanService spanService = Stub()
	LedgerService ledgerService = Stub() {
		getLedgerName(_) >> "default"
	}

	@Subject
	StartSpanCommand command = new StartSpanCommand(spanService, ledgerService)

	@PendingFeature(reason = "Proper output has not been defined yet")
	void "Starts new Timeline Spans"() {
		given:
		LocalDate day = LocalDate.now()
		Instant now = Instant.now()
		spanService.startSpan(_, "doing a thing") >> 
				Mono.just(new StartSpanInfoDto(new TimelineSpanDto("1", day, "doing a thing", now, null, true, Collections.emptySet(), null, null, Collections.emptySet()), null))

		when:
		List<Rendering> r = command.handle(startSpan("doing a thing")).collectList().block(Duration.ofMillis(40))
		
		then:
		r.size() == 1
		r.every { it instanceof Text }
//		r.first().render().startsWith("Started")
		false
	}
	
	private static TraqCli.StartSpan startSpan(
			String description, 
			Optional<String> ledgerName = Optional.empty(), 
			Optional<Path> configFile = Optional.empty()) {
		
		return new TraqCli.StartSpan() {
			@Override
			String getDescription() {
				return description
			}

			@Override
			int getVerbosityLevel() {
				return 0
			}

			@Override
			Optional<Path> getConfigFilePath() {
				return configFile
			}

			@Override
			Optional<String> getLedgerName() {
				return ledgerName
			}
		}
	}
}
