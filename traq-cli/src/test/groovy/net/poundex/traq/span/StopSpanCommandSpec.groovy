package net.poundex.traq.span

import net.poundex.tidycli.ui.Rendering
import net.poundex.tidycli.ui.rendering.Text
import net.poundex.traq.cli.TraqCli
import net.poundex.traq.ledger.LedgerService
import net.poundex.traq.timeline.TimelineSpanDto
import reactor.core.publisher.Mono
import spock.lang.PendingFeature
import spock.lang.Specification
import spock.lang.Subject

import java.nio.file.Path
import java.time.Duration
import java.time.Instant
import java.time.LocalDate

class StopSpanCommandSpec extends Specification {
	
	SpanService spanService = Stub()
	LedgerService ledgerService = Stub() {
		getLedgerName(_) >> "default"
	}

	@Subject
	StopSpanCommand command = new StopSpanCommand(spanService, ledgerService)

	@PendingFeature(reason = "Proper output has not been defined yet")
	void "Stops Timeline Span"() {
		given:
		LocalDate day = LocalDate.now()
		Instant now = Instant.now()
		spanService.stopSpan(_) >> 
				Mono.just(Optional.of(
						new TimelineSpanDto(
								"1", 
								day,
								"doing a thing", 
								now, 
								null, 
								true,
								Collections.emptySet(),
								null, 
								null, 
								Collections.emptySet())))

		when:
		Rendering r = command.handle(stopSpan()).block(Duration.ofMillis(40))
		
		then:
		r instanceof Text
//		with(r as Text) {
//			it.render() == "Stopped doing a thing"
//		}
		false
	}
	
	private static TraqCli.StopSpan stopSpan(
			Optional<String> ledgerName = Optional.empty(), 
			Optional<Path> configFile = Optional.empty()) {
		
		return new TraqCli.StopSpan() {
			@Override
			int getVerbosityLevel() {
				return 0
			}

			@Override
			Optional<Path> getConfigFilePath() {
				return configFile
			}

			@Override
			Optional<String> getLedgerName() {
				return ledgerName
			}
		}
	}
}
