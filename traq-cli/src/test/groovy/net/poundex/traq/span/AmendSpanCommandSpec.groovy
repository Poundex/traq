package net.poundex.traq.span


import net.poundex.traq.cli.TraqCli
import spock.lang.Specification

import java.nio.file.Path

class AmendSpanCommandSpec extends Specification {

	SpanService spanService = Mock()
	
//	@Subject
//	AmendSpanCommand command = new AmendSpanCommand(spanService)
	
//	void "??"() {
//		when:
//		List<Rendering> r = command.handle(amend(false)).collectList()
//				.block(Duration.ofMillis(40))
//		
//		1 * spanService.
//	}
	
	private static TraqCli.Amend amend(boolean stop) {

		return new TraqCli.Amend() {
			@Override
			int getVerbosityLevel() {
				return 0
			}

			@Override
			Optional<Path> getConfigFilePath() {
				return Optional.empty()
			}
			
			@Override
			boolean isStop() {
				return stop
			}

			@Override
			Optional<String> getLedgerName() {
				return null
			}
		}
	}
}
