package net.poundex.traq.importexport

import net.poundex.tidycli.ui.Rendering
import net.poundex.traq.cli.TraqCli
import net.poundex.traq.ledger.LedgerService
import net.poundex.xmachinery.test.PublisherUtils
import reactor.core.publisher.Mono
import spock.lang.PendingFeature
import spock.lang.Specification
import spock.lang.Subject

import java.nio.file.Path
import java.nio.file.Paths
import java.time.ZoneId

class ImportFromFileCommandSpec extends Specification implements PublisherUtils {

	ImportService importService = Stub()
	LedgerService ledgerService = Stub() {
		getLedgerName(_) >> "default"
	}

	@Subject
	ImportFromFileCommand command = new ImportFromFileCommand(importService, ledgerService)
	
	@PendingFeature(reason = "Proper output has not been defined yet")
	void "Imports data from file"() {
		given:
		Path csv = Paths.get("/the/import/file.csv")
		importService.importFromFile(_, ZoneId.systemDefault().toString(), csv) >> Mono.just(
				new ImportResultDto(5))
		
		when:
		Rendering r = block command.handle(importFromFile(csv))
		
		then:
//		r.render() == new ImportResultDto(5).toString()
		false
	}

	private static TraqCli.ImportFromFile importFromFile(
			Path inputFile,
			Optional<String> ledgerName = Optional.empty(),
			Optional<Path> configFile = Optional.empty()) {

		return new TraqCli.ImportFromFile() {
			@Override
			int getVerbosityLevel() {
				return 0
			}

			@Override
			Optional<Path> getConfigFilePath() {
				return configFile
			}

			@Override
			Optional<String> getLedgerName() {
				return ledgerName
			}

			@Override
			Path getInputFile() {
				return inputFile
			}
		}
	}
	
}
