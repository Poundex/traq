package net.poundex.traq.day


import net.poundex.traq.cli.TraqCli
import net.poundex.traq.ledger.LedgerService
import spock.lang.Specification
import spock.lang.Subject

import java.nio.file.Path

class TodayCommandSpec extends Specification {
	LedgerService ledgerService = Stub() {
		getLedgerName(_) >> "default"
	}
	DayService dayService = Stub()

	@Subject
	TodayCommand command = new TodayCommand(ledgerService, dayService)

//	void "??"() {
//		when:
//		Rendering r = command.handle(today()).block()
//
//		then:
//		r instanceof HBox
//	}

	private static TraqCli.Today today(
			Optional<String> ledgerName = Optional.empty(),
			Optional<Path> configFile = Optional.empty()) {

		return new TraqCli.Today() {
			@Override
			int getVerbosityLevel() {
				return 0
			}

			@Override
			Optional<Path> getConfigFilePath() {
				return configFile
			}

			@Override
			Optional<String> getLedgerName() {
				return ledgerName
			}
		}
	}
}
