package net.poundex.traq.point

import net.poundex.tidycli.ui.Rendering
import net.poundex.tidycli.ui.rendering.Text
import net.poundex.traq.cli.TraqCli.AddPoint
import net.poundex.traq.ledger.LedgerDto
import net.poundex.traq.ledger.LedgerService
import net.poundex.traq.timeline.TimelinePointDto
import reactor.core.publisher.Mono
import spock.lang.Ignore
import spock.lang.Specification
import spock.lang.Subject

import java.nio.file.Path
import java.time.Instant
import java.time.LocalDate

@Ignore
class AddPointCommandSpec extends Specification {
	
	PointService pointService = Stub()
	LedgerService ledgerService = Stub() {
		getLedgerName(_) >> "default"
	}
	
	@Subject
	AddPointCommand command = new AddPointCommand(pointService, ledgerService)
	
	void "Adds new Timeline Points"() {
		given:
		LocalDate day = LocalDate.now()
		Instant now = Instant.now()
		pointService.createPoint(_, "did a thing") >> 
				Mono.just(new TimelinePointDto("1", day, "did a thing", now, Collections.emptySet(), new LedgerDto("lid", "lname", null), Collections.emptySet()))

		when:
		Rendering r = command.handle(addPoint("did a thing")).block()
		
		then:
		r instanceof Text
		with(r as Text) {
			r
		}
	}
	
	private static AddPoint addPoint(
			String description, 
			Optional<String> ledgerName = Optional.empty(), 
			Optional<Path> configFile = Optional.empty()) {
		
		return new AddPoint() {
			@Override
			String getDescription() {
				return description
			}

			@Override
			int getVerbosityLevel() {
				return 0
			}

			@Override
			Optional<Path> getConfigFilePath() {
				return configFile
			}

			@Override
			Optional<String> getLedgerName() {
				return ledgerName
			}
		}
	}
}
