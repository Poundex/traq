package net.poundex.traq.e2e;

import com.fasterxml.jackson.databind.Module;
import net.poundex.traq.TraqModule;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.graphql.GraphQlAutoConfiguration;
import org.springframework.context.annotation.Bean;

@SpringBootApplication(exclude = GraphQlAutoConfiguration.class)
class TraqE2ETestsApplication {
	public static void main(String[] args) {
		SpringApplication.run(TraqE2ETestsApplication.class, args);
	}
	
	@Bean
	public Module traqModule() {
		return new TraqModule();
	}
}
