import net.poundex.traq.e2e.mobile.AbstractMobileE2ESpec
import net.poundex.traq.e2e.web.AbstractWebE2ESpec

runner {
	if(System.getenv("E2E_PROFILE") == "MOBILE")
		include AbstractMobileE2ESpec
	else 
		include AbstractWebE2ESpec
}
