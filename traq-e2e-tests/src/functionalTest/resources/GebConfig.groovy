import net.poundex.geb.appium.GebAppium

reportsDir = "build/geb-reports"
atCheckWaiting = true
templateOptions {
	required = false
}
if(System.getenv("E2E_PROFILE") == "MOBILE")
	GebAppium.install(this)
