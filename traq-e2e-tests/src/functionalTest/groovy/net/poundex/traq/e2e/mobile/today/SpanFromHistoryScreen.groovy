package net.poundex.traq.e2e.mobile.today

import geb.Module
import groovy.transform.PackageScope
import io.appium.java_client.AppiumBy
import net.poundex.traq.e2e.web.TraqPage

@PackageScope
class SpanFromHistoryScreen extends TraqPage {
	
	static at = {
		$("#screenHeading").text() == "Span from History"
	}
	
	static content = {
		recentSpans { $(AppiumBy.xpath("//android.widget.TextView[starts-with(@resource-id, 'net.poundex.traq:id/todayNowRecentSpan_')]")).moduleList(RecentSpanModule) }
	}
}

class RecentSpanModule extends Module {
	static content = {
		description { text() }
	}
}
