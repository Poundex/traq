package net.poundex.traq.e2e.web.day

import geb.Module
import net.poundex.traq.e2e.web.DaySummaryModule
import net.poundex.traq.e2e.web.TimelineModule
import net.poundex.traq.e2e.web.TraqPage

import java.time.LocalDate

class DayPage extends TraqPage {
	static url = "/day"
	
	static content = {
		timeline { $("table.timeline").module(TimelineModule) }
		markerSummary { $("#daysummary").module(DaySummaryModule) }
		calendar { $("table#bigcalendar", dynamic: true).module(DayCalendarModule) }
	}
}

class DayCalendarModule extends Module {
	static content = {
		year { y -> $("a[data-calendar-year='${y}']") }
		month { m -> $("a[data-calendar-month='${m}']") }
		day { d -> $("button[data-calendar-day='${d}']") }
		date { LocalDate date -> 
			year(date.year).click()
			month(date.monthValue).click()
			day(date.dayOfMonth).click()
			return navigator
		}
	}
}
