package net.poundex.traq.e2e.mobile.ledger

import groovy.transform.PackageScope
import net.poundex.traq.e2e.mobile.TraqScreen

@PackageScope
class LedgerIndexScreen extends TraqScreen {
	static at = {
		heading == "Ledgers"
	}
	
	static content = {
		newLedgerButton { $("#newLedgerButton") }
	}
	
	static steps = findMainMenu >> {
		waitFor { $("#menu_ledgers") }.click()
	}
}
