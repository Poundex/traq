package net.poundex.traq.e2e

import geb.spock.GebSpec
import groovy.transform.Memoized

import java.util.concurrent.ThreadLocalRandom

class E2ETestSupport {
	
	private final GebSpec spec;

	E2ETestSupport(GebSpec spec) {
		this.spec = spec
	}
	
	String uniqueName() {
		String noun = noun()
		String adjective = adjective()
		return "${adjective.substring(0, 1).toUpperCase()}${adjective.substring(1).toLowerCase()} ${noun.substring(0, 1).toUpperCase()}${noun.substring(1).toLowerCase()}"
	}
	
	String longText(int sentences = 1) {
		(0..<sentences)
				.collect { lipsum[random.nextInt(0, lipsum.size())] }
				.join(" ")
	}
	
	String noun() {
		return nouns[random.nextInt(0, nouns.size())]
	}

	String adjective() {
		return adjectives[random.nextInt(0, adjectives.size())]
	}

	@Memoized
	private static List<String> getNouns() {
		return E2ETestSupport.getClassLoader().getResource("wordlists/nouns.txt").readLines()
	}

	@Memoized
	private static List<String> getAdjectives() {
		return E2ETestSupport.getClassLoader().getResource("wordlists/adjectives.txt").readLines()
	}
	
	@Memoized
	private static List<String> getLipsum() {
		return E2ETestSupport.getClassLoader().getResource("lipsum.txt").readLines()
	}
	
	private ThreadLocalRandom getRandom() {
		return ThreadLocalRandom.current()
	}

}
