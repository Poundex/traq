package net.poundex.traq.e2e

import java.time.Duration
import java.time.format.DateTimeParseException

trait TraqContent {

	Duration durationToDuration(String duration) {
		if ( ! duration) return null
		try {
			return Duration.parse("PT" + duration
					.replaceAll("[()]", "")
					.replace('′', "H")
					.replace('″', "M"))
		}
		catch (DateTimeParseException ignored) {
			return null
		}
	}
}