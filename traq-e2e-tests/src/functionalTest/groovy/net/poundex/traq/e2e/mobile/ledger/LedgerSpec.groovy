package net.poundex.traq.e2e.mobile.ledger

import net.poundex.traq.e2e.FlashStyle
import net.poundex.traq.e2e.mobile.AbstractMobileE2ESpec
import net.poundex.traq.ledger.LedgerDto

import java.time.Duration

class LedgerSpec extends AbstractMobileE2ESpec {
	
	void "Creates new Ledgers"() {
		given:
		String ledgerName = uniqueName()
		to LedgerIndexScreen
		
		when:
		newLedgerButton.click()

		then:
		at LedgerCreateScreen
		
		when:
		nameField << ledgerName
		saveButton.click()
		
		then:
		at LedgerViewScreen
		heading == ledgerName
		nameLabel.text() == ledgerName
		
		and:
		flash.items.size() == 1
		with(flash.items.first) {
			style == FlashStyle.SUCCESS
			message == "${ledgerName} saved"
		}
	}

	void "Edits ledgers"() {
		given:
		LedgerDto ledger = createLedger(uniqueName(), Duration.ofHours(7))
		String newLedgerName = uniqueName()
		to LedgerIndexScreen
		waitFor {
			findIndexItem(ledger.name())
		}.click()
		at LedgerViewScreen

		when:
		editButton.click()

		then:
		at LedgerEditScreen
		heading == "Edit ${ledger.name()}"
		nameField.text() == ledger.name()
		targetDurationField.text() == "7.0"

		when:
		nameField.value(newLedgerName)
		targetDurationField.value("7.5")
		saveButton.click()

		then:
		at LedgerViewScreen
		waitFor { heading == newLedgerName }
		nameLabel.text() == newLedgerName
		targetDurationLabel.text() == "7′30″"
	}
}
