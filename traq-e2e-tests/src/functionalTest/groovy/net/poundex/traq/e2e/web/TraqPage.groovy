package net.poundex.traq.e2e.web

import geb.Page

class TraqPage extends Page implements TraqWebContent {
	static content = {
		heading { $("h1").text() }
		ledgerChooser { $("select.ledger-chooser").module(LedgerChooserModule) }
		flash { $(".flash-container").module(FlashModule) }
	}
}
