package net.poundex.traq.e2e.web

import geb.Module
import net.poundex.traq.e2e.FlashStyle

class FlashModule extends Module {
	static content = {
		items { $("div.alert").moduleList(FlashItemModule) }
	}
}

class FlashItemModule extends Module {
	static content = {
		style { FlashStyle.valueOf((attr("class")
				.split(" ")
				.find { it.startsWith("alert-") } - "alert-").toUpperCase()) }
		message { text() }
	}
}