package net.poundex.traq.e2e.web.ledger

import groovy.transform.PackageScope
import net.poundex.traq.e2e.web.TraqPage

@PackageScope
class LedgerViewPage extends TraqPage {
	
	static at = {
		editButton
	}
	
	static content = {
		editButton { $("#ledgerEditButton") }
		nameLabel { $("#ledgerViewNameLabel") }
		targetDurationLabel { $("#ledgerViewTargetDurationLabel") }
	}
}
