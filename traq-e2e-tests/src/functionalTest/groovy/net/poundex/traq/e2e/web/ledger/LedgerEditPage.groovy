package net.poundex.traq.e2e.web.ledger

import groovy.transform.PackageScope

@PackageScope
class LedgerEditPage extends LedgerFormPage {
	static at = {
		(browser.currentUrl =~ '/ledger/[0-9a-z]+$').find()
			&& nameField
	}
}
