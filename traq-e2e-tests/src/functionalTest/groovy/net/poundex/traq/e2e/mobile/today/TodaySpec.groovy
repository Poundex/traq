package net.poundex.traq.e2e.mobile.today

import net.poundex.traq.e2e.mobile.AbstractMobileE2ESpec
import net.poundex.traq.e2e.mobile.TimelineEventModule
import net.poundex.traq.e2e.mobile.TraqScreen
import net.poundex.traq.ledger.LedgerDto
import net.poundex.traq.marker.MarkerDto
import net.poundex.traq.timeline.TimelineSpanDto

import java.time.*

class TodaySpec extends AbstractMobileE2ESpec {

	void "Default state on Now component is correct"() {
		when:
		to TodayScreen

		then:
		nowDescriptionField.attr("hint") == "Nothing active"
		nowStopActiveSpanButton.attr('clickable') == "false"
	}

	void "Creates points"() {
		given:
		String description = longText()
		to TodayScreen

		when:
		nowDescriptionField << description
		nowAddPointButton.click()

		then:
		at TodayScreen

		and:
		with(timelineEvents.findByDescription(description).first()) {
			eventType == "point"
			time in LocalTime.now()
			it.description == description
		}
	}

	void "Starts and stops spans"() {
		given:
		page(TraqScreen)
		String ledgerId = ledgerChooser.currentLedgerId

		and:
		String description = longText()
		to TodayScreen

		when:
		scrollToTop()
		nowDescriptionField << description
		nowStartSpanButton.click()

		then:
		waitFor {
			nowDescriptionField.attr("hint") == description
		}

		and:
		with(timelineEvents.findByDescription(description).first()) {
			eventType == "span"
			startTime in LocalTime.now()
			! endTime
			activeIndicator
			duration in Duration.ZERO
		}

		when:
		scrollToTop()
		waitFor { nowStopActiveSpanButton }
		nowStopActiveSpanButton.click()

		then:
		waitFor {
			nowStopActiveSpanButton.click()
			nowDescriptionField.attr("hint") == "Nothing active"
		}

		and:
		with(timelineEvents.findByDescription(description).first()) {
			startTime
			endTime in LocalTime.now()
			! activeIndicator
		}

		cleanup:
		stopActiveSpan(ledgerId)
	}

	void "Repeats last span"() {
		given: "There is at least already one span (ie a span which can be repeated)"
		to TodayScreen
		String ledgerId = ledgerChooser.currentLedgerId
		String description = longText()
		createSpan(ledgerId, LocalDate.now(), Instant.now(), Instant.now(), description)

		when:
		scrollToTop()
		device.slideDown()

		then:
		waitFor {
			nowRepeatLastSpanButton.click()
			nowDescriptionField.attr("hint") == description
		}

		List<TimelineEventModule> events = timelineEvents.findByDescription(description)
		events.size() == 2

		and:
		with(events.last()) {
			startTime in LocalTime.now()
			! endTime
			activeIndicator
			duration in Duration.ZERO
		}

		cleanup:
		stopActiveSpan(ledgerId)
	}

	void "Starts span from history"() {
		given: "9 spans in the most recent history, the last of which is kept active"
		to TodayScreen
		String ledgerId = ledgerChooser.currentLedgerId
		String descriptionPrefix = longText()
		List<TimelineSpanDto> spans = (0..9).collect {
			startSpan(ledgerId, "${descriptionPrefix} ${it}").span()
		}

		when:
		scrollToTop()
		device.slideDown()
		nowSpanFromHistoryButton.click()

		then:
		at SpanFromHistoryScreen
		recentSpans.size() == 7
		recentSpans*.description == (8..2).collect { "${descriptionPrefix} ${it}" }

		when:
		device.back()
		at TodayScreen
		nowStopActiveSpanButton.click()
		nowSpanFromHistoryButton.click()

		then:
		at SpanFromHistoryScreen
		recentSpans.size() == 8
		recentSpans*.description == (9..2).collect { "${descriptionPrefix} ${it}" }

		when:
		recentSpans.first().click()

		then:
		at TodayScreen
		nowDescriptionField.attr("hint") == "${descriptionPrefix} 9"

		cleanup:
		stopActiveSpan(ledgerId)
	}

	void "Amends event descriptions"() {
		given: 
		to TodayScreen
		String ledgerId = ledgerChooser.currentLedgerId
		String description = longText()
		createPoint(ledgerId, description)

		when:
		scrollToTop()
		device.slideDown()
		TimelineEventModule event = timelineEvents.findByDescription(description).first()
		event.longPress()

		then:
		at AmendEventDescriptionScreen
		descriptionField.text() == description

		when:
		descriptionField.value(description + " Edit")
		saveButton.click()

		then:
		at TodayScreen
		timelineEvents.findByDescription(description + " Edit").first()
	}


	void "Day summary is displayed correctly - open time, no splits, no target"() {
		given:
		LedgerDto ledger = createLedger(uniqueName(), null)
		MarkerDto parentMarker = modifyMarker(ledger.id(), "+" + titleToName(uniqueName()), [])
		MarkerDto childMarker = modifyMarker(ledger.id(), "+" + titleToName(uniqueName()), ["+" + parentMarker.name()])
		ZonedDateTime startOfDay = LocalDate.now().atTime(1, 0).atZone(ZoneId.systemDefault())

		when:
		to TodayScreen
		ledgerChooser.ledgerChooserButton.click()
		ledgerChooser.ledgerField.click()
		findPickerItem(ledger.name()).click()
		device.back()
		
		and:
		showDaySummaryButton.click()

		then:
		daySummary.time == "N/A"
		! daySummary.activeIndicator
		! daySummary.projectedEndTime
		daySummary.duration == Duration.ZERO
		daySummary.length == Duration.ZERO

		when:
		createPoint(ledger.id(), "+${childMarker.name()}")
		startSpan(
				ledger.id(),
				longText() + "+one @two :three=four :three=five +${childMarker.name()}",
				startOfDay.toLocalDate(),
				startOfDay.toInstant())

		and:
		device.back()
		device.slideDown()
		showDaySummaryButton.click()
		Duration durationUntilNow = Duration.between(startOfDay, ZonedDateTime.now())

		then:
		daySummary.time == "01:00"
		daySummary.activeIndicator
		! daySummary.projectedEndTime
		daySummary.length in durationUntilNow
		daySummary.duration in durationUntilNow

		and:
		["+one", "@two", ":three=four", ":three=five"].each { mt ->
			owner.with(daySummary.markerSummary.findDirectByTagString(mt)) {
				values.size() == 1
				durationToDuration(values.first()) in durationUntilNow
			}
		}
		with(daySummary.markerSummary.findDirectByTagString("+${childMarker.name()}")) {
			values.size() == 2
			values.find { durationToDuration(it) in durationUntilNow }
			values.find { it == "+1" }
		}
		with(daySummary.markerSummary.findDirectByTagString(":three")) {
			values == ["four", "five"].toSet()
		}

		when:
		waitFor {
			daySummary.markerSummary.showAllButton.click()
		}

		then:
		["+one", "@two", ":three=four", ":three=five"].each { mt ->
			owner.with(daySummary.markerSummary.findEffectiveByTagString(mt)) {
				values.size() == 1
				durationToDuration(values.first()) in durationUntilNow
			}
		}
		with(daySummary.markerSummary.findEffectiveByTagString(":three")) {
			values == ["four", "five"].toSet()
		}

		and:
		with(daySummary.markerSummary.findEffectiveByTagString("+${parentMarker.name()}")) {
			values.size() == 2
			values.find { durationToDuration(it) in durationUntilNow }
			values.find { it == "+1" }
		}

		cleanup:
		stopActiveSpan(ledger.id())
		if(daySummary.showing) device.back()
	}

	void "Day summary is displayed correctly - closed time, with split and target"() {
		given:
		LedgerDto ledger = createLedger(uniqueName(), Duration.ofHours(10))
		ZonedDateTime startOfDay = LocalDate.now().atTime(1, 0).atZone(ZoneId.systemDefault())

		and:
		createSpan(
				ledger.id(),
				startOfDay.toLocalDate(),
				startOfDay.toInstant(),
				startOfDay.plusHours(1).toInstant(),
				longText())
		createSpan(
				ledger.id(),
				startOfDay.toLocalDate(),
				startOfDay.plusHours(2).toInstant(),
				startOfDay.plusHours(3).toInstant(),
				longText())

		when:
		to TodayScreen
		ledgerChooser.ledgerChooserButton.click()
		ledgerChooser.ledgerField.click()
		findPickerItem(ledger.name()).click()
		device.back()
		
		and:
		showDaySummaryButton.click()

		then:
		daySummary.time == "01:00 - 04:00"
		daySummary.projectedEndTime in LocalTime.now().plusHours(8)
		daySummary.duration in Duration.ofHours(2)
		daySummary.length in Duration.ofHours(3)
		
		cleanup:
		if (daySummary.showing) device.back()
	}

	private void scrollToTop() {
		scrollTo("resourceId(\"net.poundex.traq:id/todayNowDescriptionField\")")
	}
}