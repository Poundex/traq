package net.poundex.traq.e2e.web

import java.time.LocalTime

class DaySummaryModule extends TraqWebModule {
	
	static content = {
		time { $("#daySummaryTime").text() }
		activeIndicator { $("i.bi-record-fill").displayed }
		projectedEndTime { $("#daySummaryProjectedEndTime")
				.with { displayed ? LocalTime.parse(text()) : null } }
		length { durationToDuration($("#daySummaryLength").text()) }
		duration { durationToDuration($("#daySummaryDuration").text()) }
		acctDuration { throw new UnsupportedOperationException() }
		prodDuration { throw new UnsupportedOperationException() }
		
		directMarkerSummaries { $("tr.markerSummaryDirectMarkerRow").moduleList(MarkerSummaryRowModule) }
		showAllButton { $("#markerSummaryShowAllButton") }
		effectiveSummaries { $("tr.markerSummaryEffectiveMarkerRow").moduleList(MarkerSummaryRowModule) }
	}
}
