package net.poundex.traq.e2e.web.day


import net.poundex.traq.e2e.web.AbstractWebE2ESpec
import net.poundex.traq.e2e.web.AmendEventDescriptionInlineFormModule
import net.poundex.traq.e2e.web.TimelineEventModule
import net.poundex.traq.ledger.LedgerDto
import net.poundex.traq.timeline.TimelinePointDto
import org.openqa.selenium.Keys

import java.time.LocalDate
import java.time.Month
import java.time.Year

class DaySpec extends AbstractWebE2ESpec {
	
	void "Shows correct timeline for date"() {
		given:
		to DayPage
		String ledgerId = getCurrentLedgerId()
		TimelinePointDto todayPoint = createPoint(ledgerId, longText())
		LocalDate old1Date = LocalDate.of(Year.now().minusYears(1).getValue(), Month.FEBRUARY, 12)
		LocalDate old2Date = LocalDate.of(Year.now().minusYears(2).getValue(), Month.AUGUST, 20)
		TimelinePointDto old1Point = createPoint(old1Date, ledgerId, longText())
		TimelinePointDto old2Point = createPoint(old2Date, ledgerId, longText())

		when:
		to DayPage
		
		then:
		timeline.events.find { it.description == todayPoint.description() }
		
		when:
		calendar.year(old1Date.year).click()
		calendar.month(old1Date.monthValue).click()
		calendar.day(old1Date.dayOfMonth).click()
		
		then:
		timeline.events.find { it.description == old1Point.description() }
		
		when:
		calendar.year(old2Date.year).click()
		calendar.month(old2Date.monthValue).click()
		calendar.day(old2Date.dayOfMonth).click()

		then:
		timeline.events.find { it.description == old2Point.description() }
	}
	
	void "Amends event descriptions"() {
		given:
		LocalDate yesterday = LocalDate.now().minusDays(1)
		to DayPage
		String ledgerId = getCurrentLedgerId()
		String description = longText()
		createPoint(yesterday, ledgerId, description)

		Closure<TimelineEventModule> findEvent = {
			return timeline.events.find {
				it.description.startsWith(description)
			}
		}

		when:
		to DayPage, yesterday
		TimelineEventModule event = findEvent()

		and:
		waitFor {
			event.editButton.click()
		}
		AmendEventDescriptionInlineFormModule form = $("td[data-role='description'] form", dynamic: true)
				.module(AmendEventDescriptionInlineFormModule)

		then:
		with(form) {
			textarea.text() == description
			textarea.focused
			commitButton.displayed
			cancelButton.displayed
		}

		when:
		waitFor {
			form.cancelButton.click()
		}

		then: 
		! form

		when:
		waitFor {
			findEvent().editButton.click()
		}
		interact {
			sendKeys(Keys.ESCAPE)
		}

		then:
		waitFor { ! form }

		when:
		findEvent().editButton.click()
		form.textarea.value(description + " Edit 1")
		form.commitButton.click()

		then:
		waitFor { ! form }
		with(findEvent()) {
			it.description == description + " Edit 1"
		}

		when:
		findEvent().editButton.click()
		form.textarea.value(description + " Edit 2")
		interact {
			keyDown(Keys.CONTROL)
			sendKeys(Keys.ENTER)
			keyUp(Keys.CONTROL)
		}

		then:
		waitFor { ! form }
		with(findEvent()) {
			it.description == description + " Edit 2"
		}
	}
	
	void "Displays events for current ledger"() {
		given:
		to DayPage
		String thisLedgerId = getCurrentLedgerId()
		LedgerDto otherLedger = createLedger(uniqueName(), null)
		LocalDate yesterday = LocalDate.now().minusDays(1)

		TimelinePointDto thisLedgerPoint = createPoint(yesterday, thisLedgerId, longText())
		TimelinePointDto otherLedgerPoint = createPoint(yesterday, otherLedger.id(), longText())

		when:
		to DayPage
		calendar.date(yesterday)

		then:
		timeline.events.find { it.description == thisLedgerPoint.description() }

		when:
		ledgerChooser.value(otherLedger.id())

		then:
		timeline.events.find { it.description == otherLedgerPoint.description() }
	}
}
