package net.poundex.traq.e2e.mobile.day

import net.poundex.traq.e2e.mobile.AbstractMobileE2ESpec
import net.poundex.traq.e2e.mobile.TimelineEventModule
import net.poundex.traq.e2e.mobile.TraqScreen
import net.poundex.traq.e2e.mobile.today.AmendEventDescriptionScreen
import net.poundex.traq.ledger.LedgerDto
import net.poundex.traq.timeline.TimelinePointDto

import java.time.LocalDate
import java.time.YearMonth

class DaySpec extends AbstractMobileE2ESpec {

	void "Shows correct timeline for date"() {
		given:
		page(TraqScreen)
		String ledgerId = ledgerChooser.currentLedgerId
		TimelinePointDto todayPoint = createPoint(ledgerId, longText())
		LocalDate old1Date = LocalDate.now().minusMonths(6)
		TimelinePointDto old1Point = createPoint(old1Date, ledgerId, longText())
		
		when:
		to DayScreen
		
		then:
		timelineEvents.findByDescription(todayPoint.description())
		
		when:
		scrollToTop()
		while(calendar.showingMonth.isAfter(YearMonth.of(old1Date.year, old1Date.month))) {
			calendar.scrollLeftButton.click()
		}
		calendar.date(old1Date).click()
		
		then:
		at DayScreen
		waitFor {
			date == old1Date
		}
		timelineEvents.findByDescription(old1Point.description())
	}
	
	void "Amends event descriptions"() {
		given: 
		page(TraqScreen)
		String ledgerId = ledgerChooser.currentLedgerId
		LocalDate yesterday = LocalDate.now().minusDays(1)
		String description = longText()
		createPoint(yesterday, ledgerId, description)

		when:
		to DayScreen
		calendar.date(yesterday).click()
		TimelineEventModule event = timelineEvents.findByDescription(description).first()
		event.longPress()

		then:
		at AmendEventDescriptionScreen
		descriptionField.text() == description

		when:
		descriptionField.value(description + " Edit")
		saveButton.click()

		then:
		at DayScreen
		date == yesterday
		timelineEvents.findByDescription(description + " Edit").first()
	}

	
	void "Displays events for current ledger"() {
		given:
		page(TraqScreen)
		String thisLedgerId = ledgerChooser.currentLedgerId
		LedgerDto otherLedger = createLedger(uniqueName(), null)
		LocalDate yesterday = LocalDate.now().minusDays(1)
		
		TimelinePointDto thisLedgerPoint = createPoint(yesterday, thisLedgerId, longText())
		TimelinePointDto otherLedgerPoint = createPoint(yesterday, otherLedger.id(), longText())

		when:
		to DayScreen
		calendar.date(yesterday).click()

		then:
		timelineEvents.findByDescription(thisLedgerPoint.description())

		when:
		ledgerChooser.ledgerChooserButton.click()
		ledgerChooser.ledgerField.click()
		findPickerItem(otherLedger.name()).click()
		device.back()

		then:
		date == yesterday
		timelineEvents.findByDescription(otherLedgerPoint.description())
	}

	private void scrollToTop() {
		scrollTo("resourceId(\"net.poundex.traq:id/dayCalendar.header.title\")")
	}
}