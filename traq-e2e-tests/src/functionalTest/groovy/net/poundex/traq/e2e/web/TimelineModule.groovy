package net.poundex.traq.e2e.web


import geb.Module

import java.time.Duration
import java.time.LocalTime

class TimelineModule extends Module {
	static content = {
		events { $("tr[data-event-type]").moduleList(TimelineEventModule) }
	}
}

class TimelineEventModule extends Module {
	static content = {
		description { $("td[data-role='description']").text() }
		eventType { attr("data-event-type") }
		sigil {
			$("td[data-role='sigil']")
					.find("i")
					.attr("class")
					.replaceAll("bi", "")
					.split(" ").find { it.startsWith("-") }
					.substring(1)  
		}
		time { LocalTime.parse($("td[data-role='time']").text()) }
		startTime { LocalTime.parse($("td[data-role='time-range']").text().split(" ‐ ")[0]) }
		endTime {
			$("td[data-role='time-range']").text().contains('‐')
					? LocalTime.parse($("td[data-role='time-range']").text().split(" ‐ ")[1])
					: null
		}
		activeIndicator { $("td[data-role='time-range'] i.bi-record-fill").displayed }
		duration {
			Duration.parse("PT" +
					$("td[data-role='duration']").text()
							.replace('′', "H")
							.replace('″', "M"))
		}
		editButton { $("a[class~='bi-pencil-fill']") }
	}
}