package net.poundex.traq.e2e.mobile.log

import net.poundex.traq.e2e.mobile.AbstractMobileE2ESpec
import net.poundex.traq.ledger.LedgerDto
import net.poundex.traq.marker.MarkerDto
import net.poundex.traq.timeline.TimelinePointDto
import net.poundex.traq.timeline.TimelineSpanDto

import java.time.Duration
import java.time.LocalDate
import java.time.ZoneId
import java.time.ZonedDateTime

class LogSpec extends AbstractMobileE2ESpec {

	void "Shows correct error on query parse failure"() {
		when:
		to LogScreen

		and:
		queryField << "junk"
		queryButton.click()

		then:
		waitFor {
			queryField.value() == "Enter Query"
		}
		queryError.text() == "Unexpected input: junk"
	}

	void "Shows correct log and summary for query (nothing)"() {
		given:
		to LogScreen
		String currentLedgerId = ledgerChooser.currentLedgerId
		modifyMarker(currentLedgerId, "+junkjunkjunk", [])

		when:
		queryField << "+junkjunkjunk"
		queryButton.click()

		then:
		! topSpans.showing
		! recentPoints.showing

		when:
		showLogSummaryButton.click()
		
		then:
		logSummary.duration == Duration.ZERO
		
		cleanup:
		device.back()
	}

	void "Shows correct log and summary for query"() {
		given:
		LedgerDto ledger = createLedger(uniqueName(), null)
		MarkerDto parentMarker = modifyMarker(ledger.id(), "+" + titleToName(uniqueName()), [])
		MarkerDto childMarker = modifyMarker(ledger.id(), "+" + titleToName(uniqueName()), ["+" + parentMarker.name()])
		ZonedDateTime startOfDay = LocalDate.now().atTime(1, 0).atZone(ZoneId.systemDefault())

		and:
		TimelinePointDto point = createPoint(ledger.id(), "+common +${childMarker.name()}")
		TimelineSpanDto tagSpan = createSpan(
				ledger.id(),
				startOfDay.toLocalDate(),
				startOfDay.plusHours(0).toInstant(),
				startOfDay.plusHours(1).toInstant(),
				longText() + " +common +one @two :three=four :three=five +${childMarker.name()}")

		String twoSpansDesc = longText() + " +common +one @two :three=four :three=five +${childMarker.name()}"

		2.times {
			createSpan(
					ledger.id(),
					startOfDay.toLocalDate(),
					startOfDay.plusHours(it + 2).toInstant(),
					startOfDay.plusHours(it + 2 + 1).toInstant(),
					twoSpansDesc)
		}

		and:
		to LogScreen
		ledgerChooser.ledgerChooserButton.click()
		waitFor {
			ledgerChooser.ledgerField.click()
		}
		findPickerItem(ledger.name()).click()
		device.back()
		
		when:
		queryField << "+common"
		queryButton.click()

		then:
		with(topSpans.findByDescription(tagSpan.description())) {
			time == startOfDay.toLocalDateTime()
			duration == Duration.ofHours(1)
		}
		with(topSpans.findByDescription(twoSpansDesc)) {
			count == 2
			duration == Duration.ofHours(2)
		}
		with(recentPoints.findByDescription(point.description()).first()) {
			time in point.time().atZone(ZoneId.systemDefault()).toLocalDateTime()
		}

		when:
		showLogSummaryButton.click()
		
		then:
		logSummary.duration in Duration.ofHours(3)

		and:
		["+one", "@two", ":three=four", ":three=five"].each { mt ->
			owner.with(logSummary.markerSummary.findDirectByTagString(mt)) {
				values.size() == 1
				durationToDuration(values.first()) in Duration.ofHours(3)
			}
		}
		with(logSummary.markerSummary.findDirectByTagString("+${childMarker.name()}")) {
			values.size() == 2
			values.find { durationToDuration(it) in Duration.ofHours(3) }
			values.find { it == "+1" }
		}
		with(logSummary.markerSummary.findDirectByTagString(":three")) {
			values == ["four", "five"].toSet()
		}

		when:
		waitFor {
			logSummary.markerSummary.showAllButton.click()
		}

		then:
		["+one", "@two", ":three=four", ":three=five"].each { mt ->
			owner.with(logSummary.markerSummary.findEffectiveByTagString(mt)) {
				values.size() == 1
				durationToDuration(values.first()) in Duration.ofHours(3)
			}
		}
		with(logSummary.markerSummary.findEffectiveByTagString(":three")) {
			values == ["four", "five"].toSet()
		}

		and:
		with(logSummary.markerSummary.findEffectiveByTagString("+${parentMarker.name()}")) {
			values.size() == 2
			values.find { durationToDuration(it) in Duration.ofHours(3) }
			values.find { it == "+1" }
		}
		
		cleanup:
		if (logSummary.showing) device.back()
	}
}
