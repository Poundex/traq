package net.poundex.traq.e2e.web.today

import net.poundex.traq.e2e.web.AbstractWebE2ESpec
import net.poundex.traq.e2e.web.AmendEventDescriptionInlineFormModule
import net.poundex.traq.e2e.web.TimelineEventModule
import net.poundex.traq.ledger.LedgerDto
import net.poundex.traq.marker.MarkerDto
import net.poundex.traq.timeline.TimelinePointDto
import net.poundex.traq.timeline.TimelineSpanDto
import org.openqa.selenium.Keys

import java.time.*

class TodaySpec extends AbstractWebE2ESpec {
	
	void "Default state on Now component is correct"() {
		when:
		to TodayPage
		
		then:
		nowDescriptionField.attr("placeholder") == "Nothing active"
		
		and:
		nowStartSpanButton.attr("class").contains("btn-primary")
		[nowRepeatLastSpanButton, nowSpanFromHistoryButton]
				.any { it.attr("class")?.contains("btn-primary") }
		! nowStopActiveSpanButton
	}
	
	void "Creates points"() {
		given:
		String description = longText()
		to TodayPage

		when:
		nowDescriptionField << description
		nowAddPointButton.click()

		then:
		with(timeline.events.find {  it.description == description }) {
			eventType == "point"
			sigil == "pin-angle-fill"
			time in LocalTime.now()
		}
	}
	
	void "Starts and stops spans"() {
		given:
		String description = longText()
		to TodayPage

		when:
		nowDescriptionField << description
		nowStartSpanButton.click()

		then:
		with(timeline.events.find {it.description == description }) {
			eventType == "span"
			sigil == "stopwatch"
			startTime in LocalTime.now()
			! endTime
			activeIndicator
			duration in Duration.ZERO
		}

		and:
		nowDescriptionField.attr("placeholder") == description

		and:
		nowStartSpanButton.attr("class").contains("btn-warning")
		nowSpanFromHistoryButton.displayed
		nowSpanFromHistoryButton.attr("class").contains("btn-warning")
		! nowRepeatLastSpanButton.displayed
		nowStopActiveSpanButton.displayed

		when:
		nowStopActiveSpanButton.click()

		then:
		nowDescriptionField.attr("placeholder") == "Nothing active"

		and:
		with(timeline.events.find {it.description == description }) {
			eventType == "span"
			sigil == "stopwatch"
			startTime 
			endTime in LocalTime.now()
			! activeIndicator
		}

		and:
		nowStartSpanButton.attr("class").contains("btn-primary")
		nowRepeatLastSpanButton.attr("class")?.contains("btn-primary")
		! nowStopActiveSpanButton
	}

	void "Repeats last span"() {
		given: "There is at least already one span (ie a span which can be repeated)"
		to TodayPage
		String ledgerId = getCurrentLedgerId()
		String description = longText()
		createSpan(ledgerId, LocalDate.now(), Instant.now(), Instant.now(), description)

		when:
		to TodayPage
		nowRepeatLastSpanButton.click()

		then:
		List<TimelineEventModule> events = 
				timeline.events.findAll { it.description == description }
		with(events.find { it.activeIndicator }) {
			startTime in LocalTime.now()
			! endTime
		}

		cleanup:
		stopActiveSpan(ledgerId)
	}

	void "Starts span from history"() {
		given: "9 spans in the most recent history, the last of which is kept active"
		to TodayPage
		String ledgerId = getCurrentLedgerId()
		String descriptionPrefix = longText()
		List<TimelineSpanDto> spans = (0..9).collect {
			startSpan(ledgerId, "${descriptionPrefix} ${it}").span()
		}

		when:
		to TodayPage
		nowSpanFromHistoryButton.click()

		then:
		recentSpans.size() == 7 
		recentSpans*.text() == (8..2).collect { "${descriptionPrefix} ${it}" }

		when:
		nowSpanFromHistoryButton.click()
		nowStopActiveSpanButton.click()
		nowSpanFromHistoryButton.click()

		then:
		recentSpans.size() == 8
		recentSpans*.text() == (9..2).collect { "${descriptionPrefix} ${it}" }

		when:
		recentSpans.first().click()

		then:
		waitFor {
			nowDescriptionField.attr("placeholder") == "${descriptionPrefix} 9"
		}

		cleanup:
		stopActiveSpan(ledgerId)
	}

	void "Amends event descriptions"() {
		given:
		to TodayPage
		String ledgerId = getCurrentLedgerId()
		String description = longText()
		TimelinePointDto point = createPoint(ledgerId, description)
		
		Closure<TimelineEventModule> findEvent = {
			return timeline.events.find {
				it.description.startsWith(description)
			}
		}

		when:
		to TodayPage
		TimelineEventModule event = findEvent()

		and:
		waitFor {
			event.editButton.click()
		}
		AmendEventDescriptionInlineFormModule form = $("td[data-role='description'] form", dynamic: true)
				.module(AmendEventDescriptionInlineFormModule)

		then:
		with(form) {
			textarea.text() == description
			textarea.focused
			commitButton.displayed
			cancelButton.displayed
		}

		when:
		form.cancelButton.click()

		then: 
		! form

		when:
		findEvent().editButton.click()
		interact {
			sendKeys(Keys.ESCAPE)
		}

		then:
		waitFor { ! form }

		when:
		findEvent().editButton.click()
		form.textarea.value(description + " Edit 1")
		form.commitButton.click()

		then:
		waitFor { ! form }
		with(findEvent()) {
			it.description == description + " Edit 1"
		}

		when:
		findEvent().editButton.click()
		form.textarea.value(description + " Edit 2")
		interact {
			keyDown(Keys.CONTROL)
			sendKeys(Keys.ENTER)
			keyUp(Keys.CONTROL)
		}

		then:
		waitFor { ! form }
		with(findEvent()) {
			it.description == description + " Edit 2"
		}
	}
	
	void "Day summary is displayed correctly - open time, no splits, no target"() {
		given:
		LedgerDto ledger = createLedger(uniqueName(), null)
		MarkerDto parentMarker = modifyMarker(ledger.id(), "+" + titleToName(uniqueName()), [])
		MarkerDto childMarker = modifyMarker(ledger.id(), "+" + titleToName(uniqueName()), ["+" + parentMarker.name()])
		ZonedDateTime startOfDay = LocalDate.now().atTime(1, 0).atZone(ZoneId.systemDefault())
		
		when:
		to TodayPage
		ledgerChooser.value(ledger.id())
		
		then:
		daySummary.time == "N/A" 
		! daySummary.activeIndicator
		! daySummary.projectedEndTime
		daySummary.duration == Duration.ZERO
		daySummary.length == Duration.ZERO

		when:
		createPoint(ledger.id(), "+${childMarker.name()}")
		startSpan(
				ledger.id(),
				longText() + "+one @two :three=four :three=five +${childMarker.name()}",
				startOfDay.toLocalDate(),
				startOfDay.toInstant())
		
		and:
		to TodayPage
		Duration durationUntilNow = Duration.between(startOfDay, ZonedDateTime.now())

		then:
		daySummary.time == "01:00"
		daySummary.activeIndicator
		! daySummary.projectedEndTime
		daySummary.length in durationUntilNow
		daySummary.duration in durationUntilNow
		
		and:
		["+one", "@two", ":three=four", ":three=five"].each { mt ->
			owner.with(daySummary.directMarkerSummaries.find { it.marker.text() == mt }) {
				values.size() == 1
				durationToDuration(values.first()) in durationUntilNow
			}
		}
		with(daySummary.directMarkerSummaries.find { it.marker.text() == "+${childMarker.name()}"}) {
			values.size() == 2
			values.find { durationToDuration(it) in durationUntilNow }
			values.find { it == "+1" }
		}
		with(daySummary.directMarkerSummaries.find { it.marker.text() == ":three" }) {
			values == ["four", "five"].toSet()
		}
		
		when:
		waitFor {
			daySummary.showAllButton.click()
		}
		
		then:
		["+one", "@two", ":three=four", ":three=five"].each { mt ->
			owner.with(daySummary.effectiveSummaries.find { it.marker.text() == mt }) {
				values.size() == 1
				durationToDuration(values.first()) in durationUntilNow
			}
		}
		with(daySummary.effectiveSummaries.find { it.marker.text() == ":three" }) {
			values == ["four", "five"].toSet()
		}
		
		and:
		with(daySummary.effectiveSummaries.find { it.marker.text() == "+${parentMarker.name()}" }) {
			values.size() == 2
			values.find { durationToDuration(it) in durationUntilNow }
			values.find { it == "+1" }
		}
		
		cleanup:
		stopActiveSpan(ledger.id())
	}

	void "Day summary is displayed correctly - closed time, with split and target"() {
		given:
		LedgerDto ledger = createLedger(uniqueName(), Duration.ofHours(10))
		ZonedDateTime startOfDay = LocalDate.now().atTime(1, 0).atZone(ZoneId.systemDefault())

		and:
		createSpan(
				ledger.id(),
				startOfDay.toLocalDate(),
				startOfDay.toInstant(), 
				startOfDay.plusHours(1).toInstant(),
				longText())
		createSpan(
				ledger.id(),
				startOfDay.toLocalDate(),
				startOfDay.plusHours(2).toInstant(),
				startOfDay.plusHours(3).toInstant(),
				longText())

		when:
		to TodayPage
		ledgerChooser.value(ledger.id())
		
		then:
		daySummary.time == "01:00 ‐ 04:00"
		daySummary.projectedEndTime in LocalTime.now().plusHours(8)
		daySummary.duration in Duration.ofHours(2)
		daySummary.length in Duration.ofHours(3)
	}
}
