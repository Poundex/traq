package net.poundex.traq.e2e.mobile.ledger

import net.poundex.traq.e2e.mobile.AbstractMobileE2ESpec
import net.poundex.traq.e2e.mobile.today.TodayScreen
import net.poundex.traq.ledger.LedgerDto
import net.poundex.traq.timeline.TimelinePointDto

class LedgerChooserSpec extends AbstractMobileE2ESpec {
	
	void "Changes active ledger"() {
		given:
		String ledger1Name = uniqueName()
		String ledger2Name = uniqueName()
		LedgerDto ledger1 = createLedger(ledger1Name, null)
		LedgerDto ledger2 = createLedger(ledger2Name, null)
		
		and:
		String pointOnLedger1Description = longText()
		String pointOnLedger2Description = longText()
		TimelinePointDto point1 = createPoint(ledger1.id(), pointOnLedger1Description)
		TimelinePointDto point2 = createPoint(ledger2.id(), pointOnLedger2Description)
		
		when:
		to TodayScreen
		ledgerChooser.ledgerChooserButton.click()
		ledgerChooser.ledgerField.click()
		
		then:
		findPickerItem(ledger1Name).click()
		
		when:
		device.back()
		
		then:
		ledgerChooser.ledgerChooserButton.text() == ledger1Name
		timelineEvents.findByDescription(pointOnLedger1Description)

		when:
		ledgerChooser.ledgerChooserButton.click()
		ledgerChooser.ledgerField.click()

		then:
		findPickerItem(ledger2Name).click()

		when:
		device.back()

		then:
		ledgerChooser.ledgerChooserButton.text() == ledger2Name
		timelineEvents.findByDescription(pointOnLedger2Description)
	}
	
	void "Updates ledger chooser when ledgers changed"() {
		given:
		String ledgerName = uniqueName()

		when:
		to LedgerCreateScreen
		nameField << ledgerName
		saveButton.click()
		ledgerChooser.ledgerChooserButton.click()
		waitFor {
			ledgerChooser.ledgerField.click()
		}

		then:
		findPickerItem(ledgerName).click()

		when:
		device.back()
		at LedgerViewScreen
		editButton.click()
		at LedgerEditScreen
		nameField.value("${ledgerName} Edited")
		saveButton.click()
		
		then:
		waitFor {
			ledgerChooser.ledgerChooserButton.text() == "${ledgerName} Edited"
		}

		when:
		ledgerChooser.ledgerChooserButton.click()
		waitFor {
			ledgerChooser.ledgerField.click()
		}

		then:
		findPickerItem("${ledgerName} Edited").click()
	}
}
