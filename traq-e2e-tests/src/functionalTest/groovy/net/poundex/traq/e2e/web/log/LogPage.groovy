package net.poundex.traq.e2e.web.log

import net.poundex.traq.e2e.web.MarkerSummaryRowModule
import net.poundex.traq.e2e.web.TraqPage
import net.poundex.traq.e2e.web.TraqWebModule

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class LogPage extends TraqPage {
	static url = "/log"
	
	static at = {
		heading == "Timeline Log"
	}
	
	static content = {
		queryField { $("#logQueryField") }
		queryButton { $("#logQueryButton") }
		queryError { $("#logQueryErrorContainer") }

		topSpans { $("#topspans").module(TopSpansModule) }
		recentPoints { $("#recentpoints").module(RecentPointsModule) }
		
		summary { $().module(LogSummaryWebModule) }
	}
}

class LogSummaryWebModule extends TraqWebModule {
	static content = {
		duration { durationToDuration($("#logSummaryDuration").text()) }

		directMarkerSummaries { $("tr.markerSummaryDirectMarkerRow").moduleList(MarkerSummaryRowModule) }
		showAllButton { $("#markerSummaryShowAllButton") }
		effectiveSummaries { $("tr.markerSummaryEffectiveMarkerRow").moduleList(MarkerSummaryRowModule) }
	}
}

class TopSpansModule extends TraqWebModule {
	static content = {
		spans { $("tbody tr").moduleList(TopSpansSpanModule) }
	}
}

class TopSpansSpanModule extends TraqWebModule {
	static content = {
		time {
			$(".topSpansSpanTime").with {
				displayed ? LocalDateTime.parse(text(), DateTimeFormatter.ofPattern("yyyy-MM-dd @ HH:mm")) : null
			}
		}
		count {
			$(".topSpansSpanCount").with {
				displayed ? text().replaceAll(/[^0-9]/, '').toInteger() : null
			}
		}
		description { $(".topSpansSpanDescription").text() }
		duration { durationToDuration($(".topSpansSpanDuration").text()) }
	}
}

class RecentPointsModule extends TraqWebModule {
	static content = {
		points { $("tbody tr").moduleList(RecentPointsPointModule) }
	}
}

class RecentPointsPointModule extends TraqWebModule {
	static content = {
		time {
			$(".recentPointsPointTime").with {
				displayed ? LocalDateTime.parse(text(), DateTimeFormatter.ofPattern("yyyy-MM-dd @ HH:mm")) : null
			}
		}
		description { $(".recentPointsPointDescription").text() }
	}
}