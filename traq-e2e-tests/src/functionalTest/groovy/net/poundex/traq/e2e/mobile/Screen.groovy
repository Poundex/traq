package net.poundex.traq.e2e.mobile

import geb.Page
import geb.url.UrlFragment

class Screen extends Page {
	
	@Override
	void to(Map params, UrlFragment fragment, Object... args) {
		Closure<?> c = steps
		c.delegate = this
		c.resolveStrategy = Closure.DELEGATE_FIRST
		c()
		browser.page(this)
	}
}
