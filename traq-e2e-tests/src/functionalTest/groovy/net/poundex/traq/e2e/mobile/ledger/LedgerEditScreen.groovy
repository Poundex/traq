package net.poundex.traq.e2e.mobile.ledger

import groovy.transform.PackageScope

@PackageScope
class LedgerEditScreen extends LedgerFormScreen {
	static at = {
		heading.startsWith("Edit") && nameField
	}
}
