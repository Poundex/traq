package net.poundex.traq.e2e.mobile.today

import net.poundex.traq.e2e.mobile.DaySummaryModule
import net.poundex.traq.e2e.mobile.TimelineModule
import net.poundex.traq.e2e.mobile.TraqScreen

class TodayScreen extends TraqScreen {
	
	static at = {
		$("#screenHeading").text() == "Today"
	}
	
	static steps = findMainMenu >> { device.back() }
	
	static content = {
		nowDescriptionField { $("#todayNowDescriptionField") }
		nowAddPointButton { $("#todayNowAddPointButton") }
		nowStartSpanButton { $("#todayNowStartSpanButton") }
		nowSpanFromHistoryButton { $("#todayNowSpanFromHistoryButton") }
		nowRepeatLastSpanButton { $("#todayNowRepeatLastSpanButton") }
		nowStopActiveSpanButton { $("#todayNowStopActiveSpanButton") }
		
		timelineEvents { module(TimelineModule) }
		
		showDaySummaryButton { $("#showDaySummaryButton") }
		daySummary { module(DaySummaryModule) }
	}
	
}
