package net.poundex.traq.e2e.mobile

import geb.Browser
import geb.Module
import io.appium.java_client.AppiumBy

class MarkerSummaryModule extends Module {
	static content = {
		findDirectByTagString { String tagString -> getMarkerRowByTagString(browser, "directMarkerSummaryRow", tagString) }
		findEffectiveByTagString { String tagString -> getMarkerRowByTagString(browser, "effectiveMarkerSummaryRow", tagString) }
		showAllButton { $("#markerSummaryShowAllButton") }
	}
	
	private static MarkerSummaryRowModule getMarkerRowByTagString(Browser browser, String prefixPrefix, String tagString) {

		Closure<MarkerSummaryRowModule> findIt = {
			return browser
					.find(AppiumBy.xpath("//*[starts-with(@resource-id, 'net.poundex.traq:id/${prefixPrefix}_marker_') and @text='${tagString}']"))
					.module(MarkerSummaryRowModule)
		}

		MarkerSummaryRowModule r = findIt()
		if ( ! r.empty) return r

		browser.find(AppiumBy.androidUIAutomator(
				"new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().className(\"android.widget.TextView\").text(\"${tagString}\").instance(${prefixPrefix.contains("direct") ? 0 : 1}))"))

		Thread.sleep(100)
		r = findIt()
		return r.empty ? null : r
	}
}

class MarkerSummaryRowModule extends Module {
	static content = {
		marker { text() }
		values {
			browser.find("#${shortIdStub}_values_${markerId}")
					.text()
					.split("\\s") as Set<String>
		}
	}

	private String getShortIdStub() {
		(attr("resource-id") - "net.poundex.traq:id/").split("_")[0]
	}

	private String getMarkerId() {
		(attr("resource-id") - "net.poundex.traq:id/").split("_").last()
	}
}
