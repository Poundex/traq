package net.poundex.traq.e2e.mobile

import geb.navigator.Navigator
import io.appium.java_client.AppiumBy
import io.appium.java_client.android.AndroidDriver
import io.appium.java_client.android.options.UiAutomator2Options
import net.poundex.traq.e2e.AbstractTraqE2ESpec

import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

class AbstractMobileE2ESpec extends AbstractTraqE2ESpec implements TraqMobileContent {
	
	private static driverCreated = false 
	
	void setupSpec() {
		if(driverCreated) return
		UiAutomator2Options options = new UiAutomator2Options()
			.tap {
				String apk = System.getenv("APK_PATH")
				String apkDir = System.getenv("APK_DIRECTORY")
				if (apk || apkDir) {
					if (apk) {
						setApp(apk)
					}
					else {
						Path apkDirPath = Paths.get(apkDir).toAbsolutePath()
						setApp(Files.walk(apkDirPath)
							.filter { Files.isRegularFile(it) }
							.findFirst()
							.orElseThrow()
							.toAbsolutePath()
							.toString())
					}
				}
				else noReset()
			}
			.setAppPackage("net.poundex.traq")
			.setAppActivity(".MainActivity")
			.tap {
				setCapability("forceAppLaunch", true)

				// https://github.com/appium/appium-uiautomator2-server/issues/475
				setCapability("appium:settings[enforceXPath1]", true)
			}
		
		browser.driver = new AndroidDriver("http://127.0.0.1:4723".toURL(), options)
		driverCreated = true
		
		waitFor(noException: true) {
			$("#uiServerPromptIsShowing")
		}
		
		if ($("#uiServerPromptIsShowing")) {
			$("#uiUrlPromptField").singleElement().clear()
			$("#uiUrlPromptField") << "http://10.0.2.2:18383"
			$("#saveUiUrlButton").click()
		}
	}
	
	Closure<Navigator> findTextByScrolling(String text, int position = 0) {
		return { 
			browser.find(AppiumBy.androidUIAutomator(
				"new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().text(\"${text}\").instance(${position}))"))
		}
	}

	Navigator findPickerItem() {
		return findPickerItem(" ")
	}
	
	Navigator findPickerItem(String text) {
		return scrollAndWait(findTextByScrolling(text)) {
			return browser.find(AppiumBy.xpath(
				"//android.widget.CheckedTextView[@text=\"${text}\"]"))
		}
	}
	
	Navigator findIndexItem(String text) {
		scrollAndWait(findTextByScrolling(text)) {
			return browser.find(AppiumBy.xpath(
				"//android.widget.TextView[@text='${text}']"))
		}
		return scrollAndWait(findTextByScrolling(text)) {
			return browser.find(AppiumBy.xpath(
					"//android.widget.TextView[@text='${text}']"))
		}
	}
	
	void scrollTo(String uiAutomatorUiSelectorExpression) {
		Closure<Navigator> findIt = {
			return browser.find(AppiumBy.androidUIAutomator("new UiSelector().${uiAutomatorUiSelectorExpression}"))
		}
		Navigator r = findIt()
		if(r) return
		
		browser.find(AppiumBy.androidUIAutomator(
				"new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().${uiAutomatorUiSelectorExpression})"))
	}
}
