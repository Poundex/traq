package net.poundex.traq.e2e.web.ledger

import groovy.transform.PackageScope
import net.poundex.traq.e2e.web.TraqPage

@PackageScope
class LedgerIndexPage extends TraqPage {
	
	static url = "/ledger"
	
	static at = {
		heading == "Ledgers"
	}
	
	static content = {
		newLedgerButton { $("#newLedgerButton") }
	}
}
