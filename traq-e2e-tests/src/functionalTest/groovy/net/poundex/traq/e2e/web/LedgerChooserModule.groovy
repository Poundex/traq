package net.poundex.traq.e2e.web

import geb.module.Select

class LedgerChooserModule extends Select {

	static content = {
		options { $("option") }
	}
}
