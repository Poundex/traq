package net.poundex.traq.e2e.mobile


import geb.Module
import io.appium.java_client.AppiumBy
import net.poundex.traq.e2e.FlashStyle

class FlashModule extends Module {

	static content = {
		items {
			$(AppiumBy.xpath(
					"//android.widget.TextView[starts-with(@resource-id, 'net.poundex.traq:id/flashItem')]"))
					.moduleList(FlashItemModule) 
		}
	}
}

class FlashItemModule extends Module {
	static content = {
		style { FlashStyle.valueOf(attr("resource-id").split("_")[2].toUpperCase()) }
		message { text() }
	}
}
