package net.poundex.traq.e2e.web.marker

import net.poundex.traq.e2e.FlashStyle
import net.poundex.traq.e2e.web.AbstractWebE2ESpec
import net.poundex.traq.e2e.web.today.TodayPage
import net.poundex.traq.marker.MarkerDto
import org.openqa.selenium.By

import java.nio.charset.Charset

class MarkerSpec extends AbstractWebE2ESpec {
	
	void "Displays markers as titles when available"() {
		given:
		to TodayPage
		String ledgerId = getCurrentLedgerId()
		MarkerDto marker = modifyMarker(ledgerId, "+titledmarker", [])
		updateMarker(marker.id(), "Marker Title")
		String descPrefix = longText()

		when:
		createPoint(ledgerId, "${descPrefix} +titledmarker")
		to TodayPage

		then: "Markers are rendered with title in timeline, link goes to correct place"
		with(timeline.events.find { it.description.startsWith(descPrefix) }) {
			with(it.find(By.xpath(".//a[text() = '+Marker Title']"))) {
				attr("href").contains("q=" + URLEncoder.encode("+titledmarker", Charset.defaultCharset()))
			}
		}

		and: "Markers are rendered with title in marker summary table, link goes to correct place"
		with(daySummary.directMarkerSummaries.find { it.marker.text() == "+Marker Title" }) {
			it.marker
					.find("a")
					.attr("href")
					.contains("q=" + URLEncoder.encode("+titledmarker", Charset.defaultCharset()))
		}
	}
	
	void "Displays markers on marker index"() {
		given:
		to MarkerIndexPage
		String ledgerId = getCurrentLedgerId()
		
		String marker1Title = uniqueName()
		String marker1Name = titleToName(marker1Title)
		
		String marker2FakeTitle = uniqueName()
		String marker2Name = titleToName(marker2FakeTitle)
		
		String otherRawName = titleToName(uniqueName())
		
		when:
		MarkerDto marker = modifyMarker(ledgerId, "+" + marker1Name, [])
		updateMarker(marker.id(), marker1Title)
		modifyMarker(ledgerId, "+" + marker2Name,
				["+${otherRawName}", "@${otherRawName}", ":${otherRawName}=${otherRawName}", "+${marker1Name}"]*.toString())
		
		and:
		to MarkerIndexPage
		
		then: 
		markerTable.rows.find { it.displayString == "+" + marker1Title }

		and:
		with(markerTable.rows.find { it.displayString == "+" + marker2Name }) {
			tags.size() == 2
			tags.markerLink*.text().containsAll(["+" + marker1Title, "+" + otherRawName])

			mentions.size() == 1
			mentions.first().markerLink.text() == "@" + otherRawName

			props.size() == 1
			props.first().markerLink.text() == ":${otherRawName}=${otherRawName}"
		}
	}

	void "Removes single markers"() {
		given:
		to MarkerIndexPage
		String ledgerId = getCurrentLedgerId()

		String markerName = titleToName(uniqueName())

		String otherRawName1 = titleToName(uniqueName())
		String otherRawName2 = titleToName(uniqueName())
		String otherRawName3 = titleToName(uniqueName())

		and:
		modifyMarker(ledgerId, "+" + markerName, ["+" + otherRawName1, "+" + otherRawName2, "+" + otherRawName3])

		and:
		to MarkerIndexPage

		when:
		waitFor(10) {
			markerTable.rows.find { it.displayString == "+" + markerName }
					.tags
					.find { it.markerLink.text() == "+" + otherRawName2 }
					.removeButton
					.click()
		}

		then:
		with(markerTable.rows.find { it.displayString == "+" + markerName}) {
			tags.size() == 2
			tags*.text().containsAll(["+" + otherRawName1, "+" + otherRawName3])
		}
	}

	void "Updates marker titles"() {
		given:
		to MarkerIndexPage
		String ledgerId = getCurrentLedgerId()
		String markerTitle = uniqueName()
		String markerName = titleToName(markerTitle)
		modifyMarker(ledgerId, "+" + markerName, [])

		String otherName = titleToName(uniqueName())
		modifyMarker(ledgerId, "+" + otherName, ["+" + markerName])

		when:
		to MarkerIndexPage
		MarkerIndexTableRowModule markerRow = markerTable.rows.find { 
			it.displayString == "+" + markerName 
		}
		
		waitFor(10) {
			markerRow.editTitleButton.click()
		}
		markerRow.titleField.value(markerTitle)
		markerRow.commitTitleButton.click()
		
		then:
		markerTable.rows.find { it.displayString == "+" + markerTitle }
		with(markerTable.rows.find { it.displayString == "+" + otherName }) {
			tags.find { it.markerLink.text() == "+" + markerTitle }
		}
	}
	
	void "Cancels editing marker titles"() {
		given:
		to MarkerIndexPage
		String ledgerId = getCurrentLedgerId()
		String markerName = titleToName(uniqueName())
		modifyMarker(ledgerId, "+" + markerName, [])

		when:
		to MarkerIndexPage
		MarkerIndexTableRowModule markerRow = markerTable.rows.find {
			it.displayString == "+" + markerName
		}

		waitFor(10) {
			markerRow.editTitleButton.click()
		}
		markerRow.cancelEditTitleButton.click()

		then:
		with(markerTable.rows.find { it.displayString == "+" + markerName }) {
			it
			editTitleButton
		}
	}
	
	void "Adds new markers to marker"() {
		given:
		to MarkerIndexPage
		String ledgerId = getCurrentLedgerId()
		String markerName = titleToName(uniqueName())
		String otherName = titleToName(uniqueName())

		and:
		modifyMarker(ledgerId, "+" + markerName, [])

		when:
		to MarkerIndexPage
		MarkerIndexTableRowModule markerRow = markerTable.rows.find {
			it.displayString == "+" + markerName
		}
		markerRow.addMarkersForm.markersField << "+" + otherName
		waitFor(10) {
			markerRow.addMarkersForm.submitButton.click()
		}

		then:
		markerTable.rows.find { it.displayString == "+" + markerName }
				.tags.find { it.markerLink.text() == "+" + otherName }
		
		and:
		markerTable.rows.find { it.displayString == "+" + otherName }
		
		and:
		flash.items.size() == 1
		with(flash.items.first()) {
			style == FlashStyle.SUCCESS
			message == "Marker updated"
		}
	}
	
	void "Displays markers for current ledger"() {
		given:
		to MarkerIndexPage
		String thisLedgerId = getCurrentLedgerId()
		String otherLedgerId = createLedger(uniqueName(), null).id()
		
		String thisLedgerMarkerName = titleToName(uniqueName())
		String otherLedgerMarkerName = titleToName(uniqueName())

		and:
		modifyMarker(thisLedgerId, "+" + thisLedgerMarkerName, [])
		modifyMarker(otherLedgerId, "+" + otherLedgerMarkerName, [])
		
		when:
		to MarkerIndexPage
		
		then:
		markerTable.rows.find { it.displayString == "+" + thisLedgerMarkerName }
		! markerTable.rows.find { it.displayString == "+" + otherLedgerMarkerName }
		
		when:
		ledgerChooser.value(otherLedgerId)
		
		then:
		! markerTable.rows.find { it.displayString == "+" + thisLedgerMarkerName }
		markerTable.rows.find { it.displayString == "+" + otherLedgerMarkerName }
	}
}
