package net.poundex.traq.e2e.mobile.today


import net.poundex.traq.e2e.mobile.TraqScreen

class AmendEventDescriptionScreen extends TraqScreen {
	
	static at = {
		$("#screenHeading").text() == "Amend Event"
	}
	
	static content = {
		descriptionField { $("#amendEventDescriptionField") }
		saveButton { $("#amendEventSaveButton") }
	}

}
