package net.poundex.traq.e2e.web

import geb.navigator.Navigator
import net.poundex.traq.e2e.AbstractTraqE2ESpec
import org.openqa.selenium.By
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.firefox.FirefoxOptions

class AbstractWebE2ESpec extends AbstractTraqE2ESpec implements TraqWebContent {
	
	private static boolean driverCreated = false
	
	void setupSpec() {
		if(driverCreated) return
		browser.baseUrl = System.getenv("E2E_BASE_URL")
		browser.driver = new FirefoxDriver(new FirefoxOptions().tap {
			it.addArguments("--headless")
		}).tap { d ->
			Runtime.getRuntime().addShutdownHook {
				d.quit()
			}
		}

		driverCreated = true
	}

	Navigator findIndexItem(String text) {
		return scrollAndWait {
			browser.find(By.xpath(
				"//a[contains(@class, 'indexItem') and text()='${text}']"))
		}
	}
	
	String getCurrentLedgerId() {
		return browser.find(".ledger-chooser").value()
	}
}
