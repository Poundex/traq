package net.poundex.traq.e2e.mobile

import geb.Browser
import geb.Module
import geb.navigator.Navigator
import io.appium.java_client.AppiumBy

import java.time.Duration
import java.time.LocalTime

class TimelineModule extends Module {

	static content = {
		findByDescription { String desc -> getTimelineEventByDescription(browser, desc) }
	}

	private static List<TimelineEventModule> getTimelineEventByDescription(Browser browser, String description) {
		Closure<List<TimelineEventModule>> findIt = {
			return browser
					.find(AppiumBy.xpath("//android.widget.TextView[@text=\"${description}\"]/parent::*"))
					.moduleList(TimelineEventModule)
		}

		List<TimelineEventModule> r = findIt()
		if(r) return r
		
		browser.find(AppiumBy.androidUIAutomator(
				"new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().className(\"android.widget.TextView\").text(\"${description}\").instance(0))"))
		
		Thread.sleep(100)
		return findIt()

	}
}

class TimelineEventModule extends Module {
	
	private static final String DESCRIPTION_TEXT_CHILD_SELECTOR = 
			".//android.widget.TextView[contains(@resource-id, 'description_')]"
	
	static content = {
		eventType { getDescriptionId(navigator).contains("point") ? "point" : "span" }
		description { $(AppiumBy.xpath(DESCRIPTION_TEXT_CHILD_SELECTOR)).text() }
		eventId { $(AppiumBy.xpath(DESCRIPTION_TEXT_CHILD_SELECTOR)).attr("resource-id").split("_").last() }
		time { LocalTime.parse(browser.find("#timeline_event_time_${eventId}").text()) }
		startTime { LocalTime.parse(browser.find("#timeline_event_timeRange_${eventId}").text().split(" - ")[0]) }
		endTime {
			$("#timeline_event_timeRange_${eventId}").text().with {
				it.contains("-")
						? LocalTime.parse(it.split(" - ")[1])
						: null
			}
		}
		activeIndicator { browser.find("#timeline_event_activeIndicator_${eventId}").displayed }
		duration {
			Duration.parse("PT" + browser.find("#timeline_event_duration_${eventId}").text()
								.replaceAll("[()]", "")
								.replace('′', "H")
								.replace('″', "M"))
		}
	}

	private static String getDescriptionId(Navigator navigator) {
		navigator.find(AppiumBy.xpath(DESCRIPTION_TEXT_CHILD_SELECTOR)).attr("resource-id")
	}
}