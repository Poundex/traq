package net.poundex.traq.e2e.web.log

import net.poundex.traq.e2e.web.AbstractWebE2ESpec
import net.poundex.traq.ledger.LedgerDto
import net.poundex.traq.marker.MarkerDto
import net.poundex.traq.timeline.TimelinePointDto
import net.poundex.traq.timeline.TimelineSpanDto

import java.time.Duration
import java.time.LocalDate
import java.time.ZoneId
import java.time.ZonedDateTime

class LogSpec extends AbstractWebE2ESpec {
	void "Shows correct error on query parse failure"() {
		when:
		to LogPage
		
		and:
		queryField << "junk"
		queryButton.click()
		
		then:
		! queryField.value()
		queryError.text() == "Unexpected input: junk"
	}
	
	void "Shows correct log and summary for query (nothing)"() {
		given:
		to LogPage
		modifyMarker(getCurrentLedgerId(), "+junkjunkjunk", [])
		
		when:
		queryField << "+junkjunkjunk"
		queryButton.click()
		
		then:
		! topSpans
		! recentPoints
		
		and:
		summary.duration == Duration.ZERO
		summary.directMarkerSummaries.empty
	}
	
	void "Shows correct log and summary for query"() {
		given:
		LedgerDto ledger = createLedger(uniqueName(), null)
		MarkerDto parentMarker = modifyMarker(ledger.id(), "+" + titleToName(uniqueName()), [])
		MarkerDto childMarker = modifyMarker(ledger.id(), "+" + titleToName(uniqueName()), ["+" + parentMarker.name()])
		ZonedDateTime startOfDay = LocalDate.now().atTime(1, 0).atZone(ZoneId.systemDefault())

		and:
		TimelinePointDto point = createPoint(ledger.id(), "+common +${childMarker.name()}")
		TimelineSpanDto tagSpan = createSpan(
				ledger.id(),
				startOfDay.toLocalDate(),
				startOfDay.plusHours(0).toInstant(),
				startOfDay.plusHours(1).toInstant(),
				longText() + " +common +one @two :three=four :three=five +${childMarker.name()}")

		String twoSpansDesc = longText() + " +common +one @two :three=four :three=five +${childMarker.name()}"

		2.times {
			createSpan(
					ledger.id(),
					startOfDay.toLocalDate(),
					startOfDay.plusHours(it + 2).toInstant(),
					startOfDay.plusHours(it + 2 + 1).toInstant(),
					twoSpansDesc)
		}

		when:
		to LogPage
		ledgerChooser.value(ledger.id())
		queryField << "+common"
		queryButton.click()

		then:
		with(topSpans.spans.find { it.description == tagSpan.description() }) {
			time == startOfDay.toLocalDateTime()
			duration == Duration.ofHours(1)
		}
		with(topSpans.spans.find { it.description == twoSpansDesc }) {
			count == 2
			duration == Duration.ofHours(2)
		}
		with(recentPoints.points.find { it.description == point.description() }) {
			time in point.time().atZone(ZoneId.systemDefault()).toLocalDateTime()
		}
		
		and:
		summary.duration in Duration.ofHours(3)

		and:
		["+one", "@two", ":three=four", ":three=five"].each { mt ->
			owner.with(summary.directMarkerSummaries.find { it.marker.text() == mt }) {
				values.size() == 1
				durationToDuration(values.first()) in Duration.ofHours(3)
			}
		}
		with(summary.directMarkerSummaries.find { it.marker.text() == "+${childMarker.name()}" }) {
			values.size() == 2
			values.find { durationToDuration(it) in Duration.ofHours(3) }
			values.find { it == "+1" }
		}
		with(summary.directMarkerSummaries.find { it.marker.text() == ":three" }) {
			values == ["four", "five"].toSet()
		}

		when:
		waitFor {
			summary.showAllButton.click()
		}

		then:
		["+one", "@two", ":three=four", ":three=five"].each { mt ->
			owner.with(summary.effectiveSummaries.find { it.marker.text() == mt }) {
				values.size() == 1
				durationToDuration(values.first()) in Duration.ofHours(3)
			}
		}
		with(summary.effectiveSummaries.find { it.marker.text() == ":three" }) {
			values == ["four", "five"].toSet()
		}

		and:
		with(summary.effectiveSummaries.find { it.marker.text() == "+${parentMarker.name()}" }) {
			values.size() == 2
			values.find { durationToDuration(it) in Duration.ofHours(3) }
			values.find { it == "+1" }
		}
	}
}
