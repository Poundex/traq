package net.poundex.traq.e2e.web.ledger

import groovy.transform.PackageScope

@PackageScope
class LedgerCreatePage extends LedgerFormPage {
	
	static url = "/ledger/create"
	
	static at = {
		heading == "New Ledger"
	}
}
