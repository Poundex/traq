package net.poundex.traq.e2e.mobile.day

import geb.Module
import groovy.transform.PackageScope
import io.appium.java_client.AppiumBy
import net.poundex.traq.e2e.mobile.TimelineModule
import net.poundex.traq.e2e.mobile.TraqScreen

import java.time.LocalDate
import java.time.YearMonth
import java.time.format.DateTimeFormatter

@PackageScope
class DayScreen extends TraqScreen {
	
	static at = {
		$("#screenHeading").text().startsWith("Day:")
	}

	static steps = findMainMenu >> {
		waitFor { $("#menu_days") }.click()
	}
	
	static content = {
		timelineEvents { module(TimelineModule) }
		calendar { module(CalendarModule) }
		date { LocalDate.parse($("#screenHeading").text() - "Day: ") }
	}
}

class CalendarModule extends Module {
	static content = {
		scrollLeftButton { $(AppiumBy.id("dayCalendar.header.leftArrow")) }
		scrollRightButton { $(AppiumBy.id("dayCalendar.header.rightArrow")) }
		showingMonth {
			YearMonth.parse(
					$(AppiumBy.id("dayCalendar.header.title")).text(),
					DateTimeFormatter.ofPattern("MMMM yyyy"))
		}
		date { date -> $(AppiumBy.id("dayCalendar.day_${date}")) }
	}
}
