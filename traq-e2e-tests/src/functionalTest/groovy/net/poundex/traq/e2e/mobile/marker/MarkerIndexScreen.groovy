package net.poundex.traq.e2e.mobile.marker


import groovy.transform.PackageScope
import net.poundex.traq.e2e.mobile.TraqScreen

@PackageScope
class MarkerIndexScreen extends TraqScreen {
	static at = {
		heading == "Markers"
	}
	
	static content = {
	}
	
	static steps = findMainMenu >> {
		waitFor { $("#menu_markers") }.click()
	}
}