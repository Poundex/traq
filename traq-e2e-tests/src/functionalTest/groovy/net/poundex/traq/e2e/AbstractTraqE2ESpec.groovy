package net.poundex.traq.e2e

import com.fasterxml.jackson.databind.ObjectMapper
import geb.navigator.Navigator
import geb.spock.GebReportingSpec
import graphql.schema.Coercing
import net.poundex.testing.spock.functional.ApplicationClassFinder
import net.poundex.testing.spock.functional.GraphQl
import net.poundex.testing.spock.functional.GraphQlApplicationResourceProvider
import net.poundex.traq.test.TestDataClient
import net.poundex.xmachinery.spring.graphql.client.ScalarUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.server.LocalServerPort
import org.springframework.context.ApplicationContextInitializer
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.context.annotation.AnnotationConfigRegistry
import org.springframework.graphql.execution.GraphQlSource
import org.springframework.test.context.ContextConfiguration
import org.springframework.web.reactive.function.client.WebClient

@SpringBootTest(
	webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(
	classes = ContextInitializer,
	initializers = ContextInitializer)
// TODO: Most of this is dumped in from AbstractFunctionalSpec because need to extend Geb's spec
abstract class AbstractTraqE2ESpec extends GebReportingSpec implements GraphQl, GraphQlApplicationResourceProvider {

	private final static ThreadLocal<Class<? super AbstractTraqE2ESpec>> thisSpec =
		new ThreadLocal<>()

	private static Optional<Class<?>> foundApplicationClass = Optional.empty();

	{
		thisSpec.set(this.class)
	}
	
	private String webSocketEndpoint
	private String httpEndpoint
	
	@LocalServerPort
	int randomServerPort
	
	@Autowired
	ObjectMapper objectMapper

	@Autowired
	WebClient.Builder webClientBuilder
	
	@Delegate
	E2ETestSupport testSupport = new E2ETestSupport(this)

	String getWebSocketEndpoint() {
		return webSocketEndpoint
	}

	String getHttpEndpoint() {
		return httpEndpoint
	}

	@Delegate
	protected TestDataClient testDataClient

	@Autowired
	GraphQlSource graphQlSource

	void setupSpec() {
	}

	void setup() {
		testDataClient = new TestDataClient(this)
		String endpoint = System.getenv("APP_SERVER_ENDPOINT") ?: "localhost:8282"
		webSocketEndpoint = "ws://${endpoint}" 
		httpEndpoint = "http://${endpoint}"
	}

	static class ContextInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
		@Override
		void initialize(ConfigurableApplicationContext applicationContext) {
			foundApplicationClass.or(() ->
				new ApplicationClassFinder().findApplicationClass(thisSpec.get()))
				.ifPresent {
					((AnnotationConfigRegistry) applicationContext).scan(it.getPackageName())
					foundApplicationClass = Optional.of(it)
				}
		}
	}
	
	Navigator scrollAndWait(Closure<Navigator> finder) {
		waitFor(finder, noException: true)
		return finder()
	}

	Navigator scrollAndWait(Closure<Navigator> scroller, Closure<Navigator> finder) {
		scroller()
		return scrollAndWait(finder)
	}

	@Override
	Map<Class<?>, ? extends Coercing<?, ?>> getScalars() {
		return ScalarUtils.getScalarsByInputType(graphQlSource)
	}

	static String titleToName(String markerTitle) {
		return markerTitle.toLowerCase().replace(' ', '')
	}
}
