package net.poundex.traq.e2e.web.ledger

import net.poundex.traq.e2e.FlashStyle
import net.poundex.traq.e2e.web.AbstractWebE2ESpec
import net.poundex.traq.ledger.LedgerDto

import java.time.Duration

class LedgerSpec extends AbstractWebE2ESpec {
	
	void "Creates new Ledgers"() {
		given:
		String ledgerName = uniqueName()
		to LedgerIndexPage

		when:
		newLedgerButton.click()

		then:
		at LedgerCreatePage

		when:
		nameField << ledgerName
		saveButton.click()

		then:
		at LedgerViewPage
		heading == ledgerName
		nameLabel.text() == ledgerName
		
		and:
		flash.items.size() == 1
		with(flash.items.first) {
			style == FlashStyle.SUCCESS
			message == "${ledgerName} saved"
		}
	}

	void "Edits ledgers"() {
		given:
		LedgerDto ledger = createLedger(uniqueName(), Duration.ofHours(7))
		String newLedgerName = uniqueName()
		to LedgerIndexPage
		waitFor { findIndexItem(ledger.name()).click() }
		at LedgerViewPage
		
		when:
		editButton.click()
		
		then:
		at LedgerEditPage
		heading == "Edit ${ledger.name()}"
		nameField.value() == ledger.name()
		targetDurationField.value() == "7.0"

		when:
		nameField.value(newLedgerName)
		targetDurationField.value(7.5)
		saveButton.click()

		then:
		at LedgerViewPage
		waitFor { heading == newLedgerName }
		nameLabel.text() == newLedgerName
		targetDurationLabel.text() == "7′30″"
	}
}
