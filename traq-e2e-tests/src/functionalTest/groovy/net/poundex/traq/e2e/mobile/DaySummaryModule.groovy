package net.poundex.traq.e2e.mobile

import java.time.LocalTime

class DaySummaryModule extends TraqMobileModule {
	static content = {
		time { $("#daySummaryTime").text() }
		activeIndicator { $("#daySummaryActiveIndicator").displayed }
		projectedEndTime {
			$("#daySummaryProjectedEndTime")
					.with { displayed ? LocalTime.parse(text().replaceAll(/[()]/, '')) : null }
		}
		length { durationToDuration($("#daySummaryLength").text()) }
		duration { durationToDuration($("#daySummaryDuration").text()) }
		acctDuration { throw new UnsupportedOperationException() }
		prodDuration { throw new UnsupportedOperationException() }

		markerSummary { module(MarkerSummaryModule) }
		showing { $("#daySummaryHeader").displayed }
	}
}