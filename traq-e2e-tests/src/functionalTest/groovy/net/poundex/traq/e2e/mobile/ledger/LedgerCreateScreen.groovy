package net.poundex.traq.e2e.mobile.ledger

import groovy.transform.PackageScope

@PackageScope
class LedgerCreateScreen extends LedgerFormScreen {
	
	static steps = findMainMenu >> {
		waitFor { $("#menu_ledgers").click() }
		waitFor {
			browser.page(LedgerIndexScreen).newLedgerButton
		}.click()
	}
	
	static at = {
		heading == "New Ledger"
	}
}
