package net.poundex.traq.e2e.mobile.log

import geb.Browser
import io.appium.java_client.AppiumBy
import net.poundex.traq.e2e.mobile.MarkerSummaryModule
import net.poundex.traq.e2e.mobile.TraqMobileModule
import net.poundex.traq.e2e.mobile.TraqScreen

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class LogScreen extends TraqScreen {

	static at = {
		heading == "Timeline Log"
	}
	
	static steps = findMainMenu >> {
		waitFor { $("#menu_log") }.click()
	}
	
	static content = {
		queryField { $("#logQueryField") }
		queryButton { $("#logQueryButton") }
		queryError { $("#logQueryErrorContainer") }

		topSpans { module(TopSpansModule) }
		recentPoints { module(RecentPointsModule) }

		showLogSummaryButton { $("#showLogSummaryButton") }
		logSummary { module(LogSummaryModule) }
	}
}

class LogSummaryModule extends TraqMobileModule {
	static content = {
		duration { durationToDuration($("#logSummaryDuration").text()) }

		markerSummary { module(MarkerSummaryModule) }
		showing { $("#logSummaryHeader").displayed }
	}
}

class TopSpansModule extends TraqMobileModule {
	static content = {
		showing { $("#topSpans").displayed }
		findByDescription { String desc -> getTimelineEventByDescription(browser, desc) }
	}

	private static TopSpanModule getTimelineEventByDescription(Browser browser, String description) {
		Closure<TopSpanModule> findIt = {
			return browser
					.find(AppiumBy.xpath("//android.widget.TextView[@text=\"${description}\"]/parent::*"))
					.module(TopSpanModule)
		}

		TopSpanModule r = findIt()
		if (r) return r

		browser.find(AppiumBy.androidUIAutomator(
				"new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().className(\"android.widget.TextView\").text(\"${description}\").instance(0))"))

		Thread.sleep(100)
		return findIt()
	}
}

class RecentPointsModule extends TraqMobileModule {

	static content = {
		showing { $("#recentPoints").displayed }
		findByDescription { String desc -> getTimelineEventByDescription(browser, desc) }
	}

	private static List<RecentPointModule> getTimelineEventByDescription(Browser browser, String description) {
		Closure<List<RecentPointModule>> findIt = {
			return browser
					.find(AppiumBy.xpath("//android.widget.TextView[@text=\"${description}\"]/parent::*"))
					.moduleList(RecentPointModule)
		}

		List<RecentPointModule> r = findIt()
		if (r) return r

		browser.find(AppiumBy.androidUIAutomator(
				"new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().className(\"android.widget.TextView\").text(\"${description}\").instance(0))"))

		Thread.sleep(100)
		return findIt()
	}
}

class TopSpanModule extends TraqMobileModule {

	private static final String DESCRIPTION_TEXT_CHILD_SELECTOR =
			".//android.widget.TextView[contains(@resource-id, 'description_')]"
	
	static content = {
		eventOrGroupId { $(AppiumBy.xpath(DESCRIPTION_TEXT_CHILD_SELECTOR)).attr("resource-id").split("_").last() }
		time {
			$("#logTopSpan_time_${eventOrGroupId}").with {
				displayed ? LocalDateTime.parse(text(), DateTimeFormatter.ofPattern("yyyy-MM-dd @ HH:mm")) : null
			}
		}
		count {
		$("#logTopSpan_count_${eventOrGroupId}").with {
				displayed ? text().replaceAll(/[^0-9]/, '').toInteger() : null
			}
		}
		description { $(AppiumBy.xpath(DESCRIPTION_TEXT_CHILD_SELECTOR)).text() }
		duration { durationToDuration($("#logTopSpan_duration_${eventOrGroupId}").text()) }
	}
}

class RecentPointModule extends TraqMobileModule {
	
	private static final String DESCRIPTION_TEXT_CHILD_SELECTOR =
			".//android.widget.TextView[contains(@resource-id, 'description_')]"

	static content = {
		eventId { $(AppiumBy.xpath(DESCRIPTION_TEXT_CHILD_SELECTOR)).attr("resource-id").split("_").last() }
		description { $(AppiumBy.xpath(DESCRIPTION_TEXT_CHILD_SELECTOR)).text() }
		time {
			$("#logRecentPoint_time_${eventId}").with {
				displayed ? LocalDateTime.parse(text(), DateTimeFormatter.ofPattern("yyyy-MM-dd @ HH:mm")) : null
			}
		}
	}

}
