package net.poundex.traq.e2e.mobile.ledger

import groovy.transform.PackageScope
import net.poundex.traq.e2e.mobile.TraqScreen

@PackageScope
abstract class LedgerFormScreen extends TraqScreen {
	static content = {
		cancelButton { $("#formCancelButton") }
		saveButton { $("#formSaveButton") }
		nameField { $("#ledgerFormNameField") }
		targetDurationField { $("#ledgerFormTargetDurationField") }
	}
}
