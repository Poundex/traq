package net.poundex.traq.e2e.mobile.ledger

import groovy.transform.PackageScope
import net.poundex.traq.e2e.mobile.TraqScreen

@PackageScope
class LedgerViewScreen extends TraqScreen {
	
	static at = {
		editButton
	}
	
	static content = {
		editButton { $("#ledgerEditButton") }
		nameLabel { $("#ledgerViewNameLabel") }
		targetDurationLabel { $("#ledgerViewTargetDurationLabel") } 
	}
}
