package net.poundex.traq.e2e.mobile.marker

import net.poundex.traq.e2e.FlashStyle
import net.poundex.traq.e2e.mobile.AbstractMobileE2ESpec
import net.poundex.traq.e2e.mobile.TraqScreen
import net.poundex.traq.e2e.mobile.today.TodayScreen
import net.poundex.traq.ledger.LedgerDto
import net.poundex.traq.marker.MarkerDto

class MarkerSpec extends AbstractMobileE2ESpec {
	void "Displays markers as titles when available"() {
		given:
		to TodayScreen
		String ledgerId = ledgerChooser.currentLedgerId
		MarkerDto marker = modifyMarker(ledgerId, "+titledmarker", [])
		updateMarker(marker.id(), "Marker Title")
		String descPrefix = longText()

		when:
		createPoint(ledgerId, "${descPrefix} +titledmarker")
		to TodayScreen

		then: 
		timelineEvents.findByDescription("${descPrefix} +Marker Title")
	}
	
	void "Displays markers on marker index"() {
		given:
		page(TraqScreen)
		String ledgerId = ledgerChooser.currentLedgerId
		
		String marker1Title = uniqueName()
		String marker1Name = titleToName(marker1Title)

		String marker2FakeTitle = uniqueName()
		String marker2Name = titleToName(marker2FakeTitle)

		String otherRawName = titleToName(uniqueName().toLowerCase().replace(' ', ''))

		when:
		MarkerDto marker = modifyMarker(ledgerId, "+" + marker1Name, [])
		updateMarker(marker.id(), marker1Title)
		modifyMarker(ledgerId, "+" + marker2Name,
				["+${otherRawName}", "@${otherRawName}", ":${otherRawName}=${otherRawName}", "+${marker1Name}"]*.toString())

		and:
		to MarkerIndexScreen
		
		then:
		findIndexItem("+" + marker1Title)
		findIndexItem("+" + otherRawName)
		findIndexItem("@" + otherRawName)
		findIndexItem(":${otherRawName}=${otherRawName}")
	}

	void "Views individual markers"() {
		given:
		page(TraqScreen)
		String ledgerId = ledgerChooser.currentLedgerId

		String markerTitle = uniqueName()
		String markerName = titleToName(markerTitle)

		String otherRawName = titleToName(uniqueName())

		when:
		MarkerDto marker = modifyMarker(ledgerId, "+" + markerName, [])
		updateMarker(marker.id(), markerTitle)
		modifyMarker(ledgerId, "+" + markerName, 
				["+${otherRawName}", "@${otherRawName}", ":${otherRawName}=${otherRawName}"]*.toString())

		and:
		to MarkerIndexScreen
		findIndexItem("+" + markerTitle).click()

		then:
		at MarkerViewScreen
		heading == "+" + markerTitle
		nameLabel.text() == "+" + markerName
		titleLabel.text() == markerTitle
		
		and:
		findIndexItem("+" + otherRawName)
		findIndexItem("@" + otherRawName)
		findIndexItem(":${otherRawName}=${otherRawName}")
	}

	void "Removes single markers"() {
		given:
		page(TraqScreen)
		String ledgerId = ledgerChooser.currentLedgerId

		String markerName = titleToName(uniqueName())

		String otherRawName1 = titleToName(uniqueName())
		String otherRawName2 = titleToName(uniqueName())
		String otherRawName3 = titleToName(uniqueName())

		and:
		modifyMarker(ledgerId, "+" + markerName, ["+" + otherRawName1, "+" + otherRawName2, "+" + otherRawName3])

		when:
		to MarkerIndexScreen
		findIndexItem("+" + markerName).click()
		at MarkerViewScreen

		and:
		findIndexItem("+" + otherRawName2)
		removeMarkerButtonForMarkerText("+" + otherRawName2).click()

		then:
		findIndexItem("+" + otherRawName1)
		findIndexItem("+" + otherRawName3)
		! findIndexItem("+" + otherRawName2)
	}
	
	void "Updates marker titles"() {
		given:
		page(TraqScreen)
		String ledgerId = ledgerChooser.currentLedgerId
		
		String markerTitle = uniqueName()
		String markerName = titleToName(markerTitle)

		and:
		modifyMarker(ledgerId, "+" + markerName, [])

		and:
		to MarkerIndexScreen
		findIndexItem("+" + markerName).click()
		at MarkerViewScreen
		
		when:
		editButton.click()
		at MarkerEditScreen
		titleField << markerTitle
		saveButton.click()
		
		then:
		at MarkerViewScreen
		heading == "+" + markerTitle
		titleLabel.text() == markerTitle
		
		when:
		device.back()
		
		then:
		findIndexItem("+" + markerTitle)
	}
	
	void "Adds new markers to marker"() {
		given:
		page(TraqScreen)
		String ledgerId = ledgerChooser.currentLedgerId
		String markerName = titleToName(uniqueName())
		String otherName = titleToName(uniqueName())

		and:
		modifyMarker(ledgerId, "+" + markerName, [])
		
		and:
		to MarkerIndexScreen
		findIndexItem("+" + markerName).click()
		at MarkerViewScreen
		
		when:
		addMarkersField << "+" + otherName
		addMarkersButton.click()
		
		then:
		at MarkerViewScreen
		waitFor {
			flash.items.size() == 1
		}
		with(flash.items.first()) {
			style == FlashStyle.SUCCESS
			message == "Marker updated"
		}
		
		and:
		findIndexItem("+" + otherName)
	}
	
	void "Displays markers for current ledger"() {
		given:
		page(TraqScreen)
		String thisLedgerId = ledgerChooser.currentLedgerId
		LedgerDto otherLedger = createLedger(uniqueName(), null)

		String thisLedgerMarkerName = titleToName(uniqueName())
		String otherLedgerMarkerName = titleToName(uniqueName())

		and:
		modifyMarker(thisLedgerId, "+" + thisLedgerMarkerName, [])
		modifyMarker(otherLedger.id(), "+" + otherLedgerMarkerName, [])

		when:
		to MarkerIndexScreen
		
		then:
		findIndexItem("+" + thisLedgerMarkerName)
		! findIndexItem("+" + otherLedgerMarkerName)
		
		when:
		ledgerChooser.ledgerChooserButton.click()
		ledgerChooser.ledgerField.click()
		findPickerItem(otherLedger.name()).click()
		device.back()
		
		then:
		! findIndexItem("+" + thisLedgerMarkerName)
		findIndexItem("+" + otherLedgerMarkerName)
	}
}
