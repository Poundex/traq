package net.poundex.traq.e2e.web;

import geb.Module;

class AmendEventDescriptionInlineFormModule extends Module {
	static content = {
		textarea { $("textarea") }
		commitButton { $("button[class~='bi-check']") }
		cancelButton { $("button[class~='bi-x']") }
	}
}
