package net.poundex.traq.e2e.mobile

import geb.Module

class LedgerChooserModule extends Module {

	static content = {
		ledgerChooserButton { $("#ledgerChooserButton") }
		currentLedgerIdLabel { $("#ledgerChooserLedgerLabel") }
		currentLedgerId { 
			ledgerChooserButton.click()
			String lid = waitFor { currentLedgerIdLabel.text() }
			device.back()
			return lid
		}
		
		ledgerField { $("#ledgerChooserLedgerField") }
	}
}
