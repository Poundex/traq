package net.poundex.traq.e2e.web

import geb.Module

class MarkerSummaryRowModule extends Module {
	static content = {
		marker { $("td")[0] }
		values { $("td")[1].children().allElements()*.text.toSet() }
	}
}