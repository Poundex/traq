package net.poundex.traq.e2e.mobile.marker

import groovy.transform.PackageScope
import io.appium.java_client.AppiumBy
import net.poundex.traq.e2e.mobile.TraqScreen

@PackageScope
class MarkerViewScreen extends TraqScreen {
	static at = {
		editButton
	}
	
	static content = {
		editButton { $("#markerEditButton") }
		nameLabel { $("#markerViewNameLabel") }
		titleLabel { $("#markerViewTitleLabel") }
		
		removeMarkerButtonForMarkerText { String text -> 
			$(AppiumBy.xpath("//android.widget.TextView[@text=\"${text}\"]/following::*[starts-with(@resource-id, \"net.poundex.traq:id/markerViewMarkersRemoveMarker_\")]")).first()
		}
		
		addMarkersField { $("#markerViewAddMarkersField") }
		addMarkersButton { $("#markerViewAddMarkersButton") }
	}
	
	static steps = findMainMenu >> {
		waitFor { $("#menu_markers") }.click()
	}
}