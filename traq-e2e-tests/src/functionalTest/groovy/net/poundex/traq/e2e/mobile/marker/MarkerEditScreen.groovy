package net.poundex.traq.e2e.mobile.marker

import groovy.transform.PackageScope
import net.poundex.traq.e2e.mobile.TraqScreen

@PackageScope
class MarkerEditScreen extends TraqScreen {
	static at = {
		heading.startsWith("Edit") && nameLabel
	}
	
	static content = {
		cancelButton { $("#formCancelButton") }
		saveButton { $("#formSaveButton") }
		nameLabel { $("#markerFormNameLabel") }
		titleField { $("#markerFormTitleField") }
	}
}
