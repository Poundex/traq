package net.poundex.traq.e2e.web.today

import net.poundex.traq.e2e.web.DaySummaryModule
import net.poundex.traq.e2e.web.TimelineModule
import net.poundex.traq.e2e.web.TraqPage

class TodayPage extends TraqPage {
	static url = "/today"
	
	static content = {
		nowDescriptionField { $("#todayNowDescriptionField") }
		nowAddPointButton { $("#todayNowAddPointButton") }
		nowStartSpanButton { $("#todayNowStartSpanButton") }
		nowSpanFromHistoryButton { $("#todayNowSpanFromHistoryButton") }
		nowRepeatLastSpanButton { $("#todayNowRepeatLastSpanButton") }
		nowStopActiveSpanButton { $("#todayNowStopActiveSpanButton") }
		
		timeline { $("table.timeline").module(TimelineModule) }
		
		recentSpans { $("li.recentSpanHistoryItem") }
		
		daySummary { $("#daysummary").module(DaySummaryModule) }
	}
	
}
