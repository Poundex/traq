package net.poundex.traq.e2e.web.ledger

import net.poundex.traq.e2e.web.AbstractWebE2ESpec
import net.poundex.traq.e2e.web.today.TodayPage
import net.poundex.traq.ledger.LedgerDto
import net.poundex.traq.timeline.TimelinePointDto

class LedgerChooserSpec extends AbstractWebE2ESpec {
	
	void "Changes active ledger"() {
		given:
		String ledger1Name = uniqueName()
		String ledger2Name = uniqueName()
		LedgerDto ledger1 = createLedger(ledger1Name, null)
		LedgerDto ledger2 = createLedger(ledger2Name, null)
		
		and:
		String pointOnLedger1Description = longText()
		String pointOnLedger2Description = longText()
		TimelinePointDto point1 = createPoint(ledger1.id(), pointOnLedger1Description)
		TimelinePointDto point2 = createPoint(ledger2.id(), pointOnLedger2Description)
		
		when:
		to TodayPage
		
		then:
		ledgerChooser.options*.value().containsAll([ledger1.id(), ledger2.id()])
		ledgerChooser.options*.text().containsAll([ledger1.name(), ledger2.name()])
		
		when:
		ledgerChooser.value(ledger1.id())
		
		then:
		waitFor {
			timeline.events.size() == 1
			timeline.events.find { it.description == point1.description() }
		}
		
		when:
		ledgerChooser.value(ledger2.id())

		then:
		waitFor {
			timeline.events.size() == 1
			timeline.events.find { it.description == point2.description() }
		}
	}
	
	void "Updates ledger chooser when ledgers changed"() {
		given:
		String ledgerName = uniqueName()
		
		when:
		to LedgerCreatePage
		nameField << ledgerName
		saveButton.click()
		
		then:
		waitFor {
			ledgerChooser.options*.text().contains(ledgerName)
		}
		
		when:
		to LedgerIndexPage
		waitFor { findIndexItem(ledgerName).click() }
		at LedgerViewPage
		editButton.click()
		at LedgerEditPage
		nameField.value("${ledgerName} Edited")
		saveButton.click()
		
		then:
		waitFor {
			ledgerChooser.options*.text().contains("${ledgerName} Edited".toString())
		}
	}
}
