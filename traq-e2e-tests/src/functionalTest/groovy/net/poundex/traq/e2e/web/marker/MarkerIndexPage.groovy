package net.poundex.traq.e2e.web.marker


import geb.Module
import net.poundex.traq.e2e.web.TraqPage

class MarkerIndexPage extends TraqPage {
	static url = "/marker"
	
	static content = {
		markerTable { $("div#markerlist").module(MarkerIndexTableModule) }
	}
}

class MarkerIndexTableModule extends Module {
	static content = {
		rows { $("tbody tr").moduleList(MarkerIndexTableRowModule) }
	}
}

class MarkerIndexTableRowModule extends Module {
	static content = {
		displayString { $("td").first().text() }
		
		editTitleButton { $("a.inlineEditLink") }
		
		titleField { $("input#editMarkerTitleField") }
		commitTitleButton { $("button.bi-check") }
		cancelEditTitleButton { $("button.bi-x") }
		
		tags { $("td")[1].find("p")[0]
				.find("p > span")
				.moduleList(MarkerIndexTableMarkerMarkerModule) }
		
		mentions { $("td")[1].find("p")[1]
				.find("p > span")
				.moduleList(MarkerIndexTableMarkerMarkerModule) }
		
		props { $("td")[1].find("p")[2]
				.find("p > span")
				.moduleList(MarkerIndexTableMarkerMarkerModule) }
		
		addMarkersForm { $("form").module(MarkerIndexTableMarkerAddMarkersFormModule) }
	}
}

class MarkerIndexTableMarkerMarkerModule extends Module {
	static content = {
		markerLink { $("a") }
		removeButton { $("button") }
	}
}

class MarkerIndexTableMarkerAddMarkersFormModule extends Module {
	static content = {
		markersField { $("input") }
		submitButton { $("button") }
	}
}