package net.poundex.traq.e2e.web.ledger

import groovy.transform.PackageScope
import net.poundex.traq.e2e.web.TraqPage

@PackageScope
abstract class LedgerFormPage extends TraqPage {
	static content = {
		cancelButton { $("#formCancelButton") }
		saveButton { $("#formSaveButton") }
		nameField { $("#ledgerFormNameField") }
		targetDurationField { $("#ledgerFormDurationField") }
	}
}
