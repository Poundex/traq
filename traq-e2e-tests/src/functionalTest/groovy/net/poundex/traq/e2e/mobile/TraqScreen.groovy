package net.poundex.traq.e2e.mobile

class TraqScreen extends Screen implements TraqMobileContent {
	static content = {
		heading { $("#screenHeading").text() }
		ledgerChooser { module LedgerChooserModule }
		flash { module FlashModule }
	}
	
	static findMainMenu = {
		while(true) {
			waitFor { heading }
			if (heading == "Traq") return
			if (heading == "Today") break
			device.back()
		}
		browser.find("#todayMainMenuButton").click()
		waitFor {
			heading == "Traq"
		}
	}
}
