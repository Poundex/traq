package net.poundex.traq.marker;

import net.poundex.traq.ledger.LedgerDto;
import net.poundex.traq.ledger.LedgerItemDto;

public sealed interface MarkerDto extends MarkableDto, LedgerItemDto
        permits BasePropertyDto, MentionDto, PropertyDto, TagDto {
    String id();
    String name();
    String asString();
    String title();
    LedgerDto ledger();
}
