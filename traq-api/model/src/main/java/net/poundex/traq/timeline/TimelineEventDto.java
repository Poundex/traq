package net.poundex.traq.timeline;

import net.poundex.traq.ledger.LedgerDto;
import net.poundex.traq.ledger.LedgerItemDto;
import net.poundex.traq.marker.MarkableDto;
import net.poundex.traq.marker.MarkerDto;

import java.time.Instant;
import java.time.LocalDate;
import java.util.Set;

public sealed interface TimelineEventDto extends MarkableDto, LedgerItemDto permits TimelinePointDto, TimelineSpanDto {
    String id();
    LocalDate day();
    String description();
    Instant time();
    Set<? extends MarkerDto> markers();
    LedgerDto ledger();
}
