package net.poundex.traq.importexport;

public record ImportResultDto(int eventsCreated) {
}
