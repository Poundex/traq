package net.poundex.traq;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import net.poundex.traq.marker.BasePropertyDto;
import net.poundex.traq.marker.MarkerDto;
import net.poundex.traq.marker.MentionDto;
import net.poundex.traq.marker.PropertyDto;
import net.poundex.traq.marker.TagDto;
import net.poundex.traq.timeline.TimelineEventDto;
import net.poundex.traq.timeline.TimelinePointDto;
import net.poundex.traq.timeline.TimelineSpanDto;

import java.io.IOException;
import java.util.Map;

public class TraqModule extends SimpleModule {
	public TraqModule() {
		super("TraqModule");
		
		addAbstractDeserializer(TimelineEventDto.class, Map.of(
				"TimelinePoint", TimelinePointDto.class,
				"TimelineSpan", TimelineSpanDto.class));
		
		addAbstractDeserializer(MarkerDto.class, Map.of(
				"Tag", TagDto.class,
				"Mention", MentionDto.class,
				"BaseProperty", BasePropertyDto.class,
				"Property", PropertyDto.class));
	}

	private <Dto> void addAbstractDeserializer(Class<Dto> abstractDtoType, Map<String, Class<? extends Dto>> mappings) {
		addDeserializer(abstractDtoType, new JsonDeserializer<>() {
			@Override
			public Dto deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JacksonException {
				ObjectMapper mapper = (ObjectMapper) p.getCodec();
				JsonNode root = mapper.readTree(p);

				JsonNode typeNode = root.get("__typename");
				if (typeNode == null || ! typeNode.isTextual())
					throw new JsonMappingException(p, "No __typename for object");

				String targetTypename = typeNode.textValue();
				Class<? extends Dto> targetDtoType = mappings.get(targetTypename);
				if(targetDtoType == null) 
					throw new JsonMappingException(p, String.format(
						"No sub-type of %s matches typename %s", abstractDtoType.getSimpleName(), targetTypename));
				
				return mapper.treeToValue(root, targetDtoType);
			}
		});
	}
}
