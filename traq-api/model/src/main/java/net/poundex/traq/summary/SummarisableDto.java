package net.poundex.traq.summary;

import net.poundex.traq.timeline.TimelineEventDto;

import java.util.List;

public interface SummarisableDto {
	List<TimelineEventDto> events();
}
