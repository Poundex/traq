package net.poundex.traq.summary;

import net.poundex.traq.marker.BasePropertyDto;
import net.poundex.traq.marker.MarkerDto;

import java.time.Duration;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public record MarkerSummaryDto(
		Set<MarkerCountDto> count,
		Set<MarkerDurationDto> duration,
		Set<MarkerPropertyValuesDto> propertyValues) {
	
	public Map<MarkerDto, Integer> countAsMap() {
		return count().stream().collect(
				Collectors.toMap(MarkerCountDto::marker, MarkerCountDto::value));
	}
	
	public Map<MarkerDto, Duration> durationAsMap() {
		return duration().stream().collect(
				Collectors.toMap(MarkerDurationDto::marker, MarkerDurationDto::value));
	}
	
	public Map<BasePropertyDto, Set<String>> propertyValuesAsMap() {
		return propertyValues().stream().collect(
				Collectors.toMap(MarkerPropertyValuesDto::marker, MarkerPropertyValuesDto::value));
	}
}
