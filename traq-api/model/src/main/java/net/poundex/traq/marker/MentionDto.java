package net.poundex.traq.marker;

import net.poundex.traq.ledger.LedgerDto;

import java.util.Set;

public record MentionDto(
		String id,
		String name,
		String title,
		Set<? extends MarkerDto> markers,
		Set<? extends MarkerDto> effectiveMarkers, 
		LedgerDto ledger) implements MarkerDto {

	@Override
	public String asString() {
		return "@" + name();
	}
}
