package net.poundex.traq.summary;

import net.poundex.traq.marker.MarkerDto;

public record MarkerCountDto(
		MarkerDto marker, 
		Integer value) implements MarkerSummaryDataDto<Integer> {
}
