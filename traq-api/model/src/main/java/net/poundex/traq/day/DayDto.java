package net.poundex.traq.day;

import net.poundex.traq.ledger.LedgerDto;
import net.poundex.traq.summary.MarkerSummaryDto;
import net.poundex.traq.summary.SummarisableDto;
import net.poundex.traq.timeline.TimelineEventDto;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.util.List;

public record DayDto(
		LocalDate day,
		List<TimelineEventDto> events,
		Instant startTime,
		Instant endTime,
		Instant projectedEndTime,
		Duration duration,
		LedgerDto ledger,
		Duration length, 
		MarkerSummaryDto markerSummary, 
		MarkerSummaryDto effectiveMarkerSummary) implements SummarisableDto {
}
