package net.poundex.traq.ledger;

public interface LedgerItemDto {
    String id();
    LedgerDto ledger();
}
