package net.poundex.traq.summary;

import net.poundex.traq.marker.BasePropertyDto;

import java.util.Set;

public record MarkerPropertyValuesDto(
		BasePropertyDto marker, 
		Set<String> value) implements MarkerSummaryDataDto<Set<String>> {
}
