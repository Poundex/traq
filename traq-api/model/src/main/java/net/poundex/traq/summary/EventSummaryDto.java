package net.poundex.traq.summary;

import net.poundex.traq.ledger.LedgerDto;
import net.poundex.traq.timeline.TimelineEventDto;

import java.time.Duration;
import java.time.LocalDate;
import java.util.List;

public record EventSummaryDto(
		LedgerDto ledger,
		String parsedQuery,
		LocalDate since,
		List<TimelineEventDto> events,
		Duration duration,
		MarkerSummaryDto markerSummary,
		MarkerSummaryDto effectiveMarkerSummary) implements SummarisableDto {
	
}
