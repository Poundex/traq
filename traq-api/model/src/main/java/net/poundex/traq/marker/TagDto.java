package net.poundex.traq.marker;

import lombok.Builder;
import net.poundex.traq.ledger.LedgerDto;

import java.util.Set;

@Builder
public record TagDto (
		String id,
		String name,
		String title,
		Set<? extends MarkerDto> markers,
		Set<? extends MarkerDto> effectiveMarkers, 
		LedgerDto ledger) implements MarkerDto {

	@Override
	public String asString() {
		return "+" + name();
	}
}
