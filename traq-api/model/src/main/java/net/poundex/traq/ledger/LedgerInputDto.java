package net.poundex.traq.ledger;

import jakarta.validation.constraints.NotBlank;

import java.time.Duration;

public record LedgerInputDto(@NotBlank String name, Duration targetDuration) { }
