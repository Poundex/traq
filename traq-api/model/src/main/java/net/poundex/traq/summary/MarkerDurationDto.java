package net.poundex.traq.summary;

import net.poundex.traq.marker.MarkerDto;

import java.time.Duration;

public record MarkerDurationDto(
		MarkerDto marker, 
		Duration value) implements MarkerSummaryDataDto<Duration> {
}
