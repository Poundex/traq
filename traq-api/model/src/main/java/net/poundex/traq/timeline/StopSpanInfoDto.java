package net.poundex.traq.timeline;

@Deprecated(forRemoval = true)
public record StopSpanInfoDto(TimelineSpanDto span) {
}
