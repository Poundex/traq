package net.poundex.traq.timeline;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

import java.time.Instant;
import java.time.LocalDate;

public record CreateSpanInputDto(
		@NotNull Instant time, 
		Instant endTime, 
		LocalDate day, 
		@NotBlank String description) {
}
