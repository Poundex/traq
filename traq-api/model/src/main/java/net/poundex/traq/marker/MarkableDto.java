package net.poundex.traq.marker;

import java.util.Set;

public interface MarkableDto {
    String id();
    Set<? extends MarkerDto> markers();
    Set<? extends MarkerDto> effectiveMarkers();
}
