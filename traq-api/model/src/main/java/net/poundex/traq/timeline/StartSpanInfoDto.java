package net.poundex.traq.timeline;

public record StartSpanInfoDto(TimelineSpanDto span, TimelineSpanDto previousSpan) {
}
