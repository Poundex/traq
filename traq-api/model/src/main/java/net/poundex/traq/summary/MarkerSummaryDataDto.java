package net.poundex.traq.summary;

import net.poundex.traq.marker.MarkerDto;

public sealed interface MarkerSummaryDataDto<T> 
		permits MarkerCountDto, 
		MarkerDurationDto, 
		MarkerPropertyValuesDto {
	
	MarkerDto marker();
	T value();
}
