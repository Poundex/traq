package net.poundex.traq.ledger;

import net.poundex.traq.marker.MarkerDto;

import java.time.Duration;
import java.util.Set;

public record LedgerDto(
		String id,
		String name, 
		Duration targetDuration, 
		Set<? extends MarkerDto> markers) {
}
