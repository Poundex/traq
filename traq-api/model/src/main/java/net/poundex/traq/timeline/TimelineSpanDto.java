package net.poundex.traq.timeline;

import net.poundex.traq.ledger.LedgerDto;
import net.poundex.traq.marker.MarkerDto;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Set;

public record TimelineSpanDto(
		String id,
		LocalDate day,
		String description,
		Instant time,
		Instant endTime,
		boolean active,
		Set<? extends MarkerDto> markers,
		LedgerDto ledger,
		Duration duration, 
		Set<? extends MarkerDto> effectiveMarkers) implements TimelineEventDto {
}
