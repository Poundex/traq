package net.poundex.traq.timeline;

import java.time.Instant;
import java.time.LocalDate;

public record RepeatSpanInputDto(Instant time, LocalDate day) { }
