package net.poundex.traq.timeline;

import jakarta.validation.constraints.NotBlank;

import java.time.Instant;
import java.time.LocalDate;

public record StartSpanInputDto(Instant time, LocalDate day, @NotBlank String description) {
}
