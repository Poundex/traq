import React from "react";
import { Text } from "react-native";

export interface LabelProps {
	value: string;
}

const Label = (props: LabelProps) => {
	return (<Text {...props}>{props.value}</Text>);
}

export default Label;

