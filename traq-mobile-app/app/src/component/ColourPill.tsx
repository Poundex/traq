import React from "react";
import { StyleSheet, View, ViewProps } from "react-native";
import { LinearGradient } from "react-native-linear-gradient";

export interface ColourPillProps extends ViewProps {
	colour: Colour;
}

export type ColourStrings = keyof typeof Colour;

// noinspection JSUnusedGlobalSymbols
export enum Colour {
	BLACK = "000000",
	BLUE = "0062f4",
	RED = "f90000",
	GREY = "a49ea2",
	YELLOW = "fbe400",
	GREEN = "007c3b",
	BROWN = "863501",
	PURPLE = "36346c",
	ORANGE = "ff7300",
	PINK = "ff2ed7",
	EARTH = "028c04"
}

const ColourPill = (props: ColourPillProps) => {

	const newStyles = StyleSheet.compose(props.style, {
		"backgroundColor": "#" + props.colour
	});
	
	if (props.colour !== Colour.EARTH)
		return React.createElement(View, {style: newStyles});

	return (<LinearGradient
		colors={["#028c04", "#f9db03", "#f9db03", "#028c04", "#028c04", "#f9db03", "#f9db03", "#028c04", "#028c04"]}
		angle={90}
		useAngle={true}
		locations={[0.2, 0.2, 0.4, 0.4, 0.6, 0.6, 0.8, 0.8, 1]}
		style={newStyles}/>)
}

export default ColourPill;

