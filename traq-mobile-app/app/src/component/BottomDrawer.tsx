import React, { RefObject, useRef } from "react";
import Modal, { ModalProps } from 'react-native-modalbox';
import * as Events from 'hyperview/src/services/events';

export interface BottomDrawerProps extends ModalProps {
	openerFnConsumer: (openerFn: () => void) => void;
	closerFnConsumer?: (closerFn: () => void) => void;
	id: string;
	swipeToClose: boolean;
}

export const ON_DISMISS_EVENT_NAME_PREFIX = "bottom-drawer-dismissed-";
export const ON_REQUEST_EVENT_NAME_PREFIX = "bottom-drawer-requested-";

const BottomDrawer = (props: BottomDrawerProps) => {
	
	const elementRef: RefObject<Modal> = useRef(null);
	props.openerFnConsumer(() => {
		if(elementRef.current) elementRef.current.open()
	});
	if(props.closerFnConsumer) props.closerFnConsumer(() => {
		if (elementRef.current) elementRef.current.close()
	});
	
	const onClose = () => Events.dispatch(ON_DISMISS_EVENT_NAME_PREFIX + props.id);

	return (
		<Modal {...props} position={"bottom"} ref={elementRef} onClosed={onClose} backButtonClose={true}>
			{props.children}
		</Modal>);
}

export default BottomDrawer;

