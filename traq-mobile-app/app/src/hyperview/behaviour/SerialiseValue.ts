import * as Dom from 'hyperview/src/services/dom';
import * as Logging from 'hyperview/src/services/logging';
import type { HvComponentOnUpdate, HvGetRoot, HvUpdateRoot, } from 'hyperview/src/types';
import { shallowCloneToRoot } from 'hyperview/src/services';

export default {
	action: 'serialise-value',
	callback: (element: Element, onUpdate: HvComponentOnUpdate, getRoot: HvGetRoot, updateRoot: HvUpdateRoot) => {

		const targetId = element.getAttribute('target');
		if ( ! targetId) {
			Logging.warn('[behaviors/serialise-value]: missing "target" attribute');
			return;
		}

		const doc = getRoot();
		const targetElement = Dom.getElementById(doc, targetId);
		if ( ! targetElement) return;
		
		const myId = element.getAttribute("id")!;
		// TODO: This is the old value???, hence the !
		const myValue: boolean = ! (element.getAttribute("value") === "true");
		const collectedValue = targetElement.getAttribute("value")
				?.split(",")
				.filter(it => it !== myId)
			?? [];
		if(myValue) collectedValue.push(myId);
		targetElement.setAttribute("value", collectedValue.join(","));
		
		let newRoot: Document = shallowCloneToRoot(targetElement);
		updateRoot(newRoot);
	}
};
