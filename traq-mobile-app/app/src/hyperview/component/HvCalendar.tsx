import Hyperview, { HvComponentProps, LocalName } from 'hyperview';
import React from "react";
import { TRAQ_UI_XMLNS } from "@/app/src/constants";
import { getNameValueFormInputValues } from "hyperview/src/services";
import { Calendar, CalendarProps, DateData } from "react-native-calendars";
import * as Behaviors from 'hyperview/src/services/behaviors';

export interface HvCalendarProps extends CalendarProps {}

const deviceOffset = new Date().getTimezoneOffset() * -1;

const HvCalendar = (props: HvComponentProps) => {
	
	const hvProps = Hyperview.createProps(
		props.element,
		props.stylesheets,
		props.options) as HvCalendarProps;
	
	const onDayPress = (date: DateData) => {
		const newElement = props.element.cloneNode(true) as Element;
		newElement.setAttribute('value', date.dateString);
		props.onUpdate(null, 'swap', props.element, {newElement});
		Behaviors.trigger('change', newElement, props.onUpdate);
	};
	
	return (
		<Calendar {...hvProps} firstDay={1} onDayPress={onDayPress}></Calendar>
	);
}

HvCalendar.namespaceURI = TRAQ_UI_XMLNS;
HvCalendar.localName = 'calendar' as LocalName;
HvCalendar.localNameAliases = [] as LocalName[];

HvCalendar.getFormInputValues = (element: Element): Array<[string, string]> => {
	return getNameValueFormInputValues(element);
};

export default HvCalendar;
