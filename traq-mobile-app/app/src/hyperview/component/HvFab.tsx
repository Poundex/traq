import Hyperview, { HvComponentProps, LocalName } from 'hyperview';
import React from "react";
import * as Events from 'hyperview/src/services/events';
import BsIcon from "@/app/src/component/BsIcon";
import FAB, { Props as FabProps } from "react-native-fab";
import { TRAQ_UI_XMLNS } from "@/app/src/constants";

export interface HvFabProps extends FabProps {
	eventName: string,
	icon: string
}

const HvFab = (props: HvComponentProps) => {

	const hvProps = Hyperview.createProps(
		props.element, 
		props.stylesheets, 
		props.options) as HvFabProps;
	
	const onClick = () => Events.dispatch(hvProps.eventName);

	return (
		<FAB
			buttonColor="#6c757d"
			iconTextColor="#000000"
			onClickAction={onClick}
			visible={true}
			iconTextComponent={<BsIcon fill={"#fff"} width={24} height={24} {...hvProps}/>}/>);
}

HvFab.namespaceURI = TRAQ_UI_XMLNS;
HvFab.localName = 'fab' as LocalName;
HvFab.localNameAliases = [] as LocalName[];

export default HvFab;

