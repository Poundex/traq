import Hyperview, { HvComponentProps, LocalName } from 'hyperview';
import React from "react";
import { TRAQ_UI_XMLNS } from "@/app/src/constants";
import Label, { LabelProps } from "@/app/src/component/Label";

const HvLabel = (props: HvComponentProps) => {
	const config: LabelProps = Hyperview.createProps(
		props.element,
		props.stylesheets,
		props.options) as LabelProps;

	return (<Label {...config} />);
}

HvLabel.namespaceURI = TRAQ_UI_XMLNS;
HvLabel.localName = 'label' as LocalName;
HvLabel.localNameAliases = [] as LocalName[];

export default HvLabel;
