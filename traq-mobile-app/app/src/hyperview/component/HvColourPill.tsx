import Hyperview, { HvComponentProps, LocalName } from 'hyperview';
import React from "react";
import { TRAQ_UI_XMLNS } from "@/app/src/constants";
import ColourPill, { Colour, ColourPillProps, ColourStrings } from "@/app/src/component/ColourPill";

const HvColourPill = (props: HvComponentProps) => {
	const rawConfig = Hyperview.createProps(
		props.element,
		props.stylesheets,
		props.options);
	
	const config: ColourPillProps = {
		...rawConfig, 
		colour: Colour[rawConfig.colour as ColourStrings]
	}
	return (<ColourPill {...config} />);
}

HvColourPill.namespaceURI = TRAQ_UI_XMLNS;
HvColourPill.localName = 'colour-pill' as LocalName;
HvColourPill.localNameAliases = [] as LocalName[];

export default HvColourPill;
