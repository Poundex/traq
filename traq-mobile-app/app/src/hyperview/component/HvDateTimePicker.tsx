import Hyperview, { HvComponentProps, LocalName } from 'hyperview';
import React, { useState } from "react";
import { TRAQ_UI_XMLNS } from "@/app/src/constants";
import DatePicker, { DatePickerProps } from "react-native-date-picker";
import { Text } from "react-native";
import { createStyleProp, getNameValueFormInputValues } from "hyperview/src/services";

export interface HvDatePickerProps extends DatePickerProps {
	datevalue: string;
}

const toLocalDate = (date: Date) => {
	return date.toISOString().substring(0, 19);
}

const fromLocalDate = (str: string) => {
	return new Date(str);
}

const deviceOffset = new Date().getTimezoneOffset() * -1;

const HvDateTimePicker = (props: HvComponentProps) => {
	
	const hvProps = Hyperview.createProps(
		props.element,
		props.stylesheets,
		props.options) as HvDatePickerProps;
	
	const updateElementValue = (newDate: Date) => {
		const localDate: string = toLocalDate(new Date(newDate.getTime() + (deviceOffset * 60 * 1000)));
		props.element.setAttribute("value", localDate);
	}

	const [date, setDate] = useState(hvProps.datevalue ? fromLocalDate(hvProps.datevalue) : new Date())
	const [open, setOpen] = useState(false)
	hvProps.date = date;
	updateElementValue(date);

	const onConfirm = (newDate: Date) => {
		setOpen(false);
		setDate(newDate);
		updateElementValue(newDate);
	}
	
	const styles = createStyleProp(props.element, props.stylesheets, props.options);
	return (<>
		<Text style={styles} onPress={() => setOpen(true)}>{date.toLocaleString()}</Text>
		<DatePicker modal
		            {...hvProps}
		            date={date}
		            open={open}
		            timeZoneOffsetInMinutes={deviceOffset}
		            onConfirm={onConfirm}
		            onCancel={() => setOpen(false)}/>
		</>);
}

HvDateTimePicker.namespaceURI = TRAQ_UI_XMLNS;
HvDateTimePicker.localName = 'datetime-picker' as LocalName;
HvDateTimePicker.localNameAliases = [] as LocalName[];

HvDateTimePicker.getFormInputValues = (element: Element): Array<[string, string]> => {
	return getNameValueFormInputValues(element);
};

export default HvDateTimePicker;
