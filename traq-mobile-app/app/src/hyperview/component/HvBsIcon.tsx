import Hyperview, { HvComponentProps, LocalName } from 'hyperview';
import React, { PureComponent } from "react";
import BsIcon, { BsIconProps } from "@/app/src/component/BsIcon";
import { TRAQ_UI_XMLNS } from "@/app/src/constants";

export default class HvBsIcon extends PureComponent<HvComponentProps> {
	static namespaceURI = TRAQ_UI_XMLNS;
	static localName = 'icon' as LocalName;
	static localNameAliases = [];

	render() {
		const config: BsIconProps = Hyperview.createProps(
				this.props.element,
				this.props.stylesheets,
				this.props.options) as BsIconProps;

		return (<BsIcon {...config} />);
	}
}
