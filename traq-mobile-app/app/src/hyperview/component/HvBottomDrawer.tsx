import Hyperview, { Events, HvComponentProps, LocalName } from 'hyperview';
import React, { useEffect } from "react";
import BottomDrawer, { BottomDrawerProps, ON_REQUEST_EVENT_NAME_PREFIX } from "@/app/src/component/BottomDrawer";
import { TRAQ_UI_XMLNS } from "@/app/src/constants";

export const DO_DISMISS_EVENT_NAME_PREFIX = "bottom-drawer-dismiss-";

const HvBottomDrawer = (props: HvComponentProps) => {

	const hvConfig = Hyperview.createProps(
			props.element,
			props.stylesheets,
			props.options) as BottomDrawerProps;
	
	hvConfig.swipeToClose = props.element.getAttribute("swipeToClose") !== "false"

	const children: any[] = Hyperview.renderChildren(
		props.element,
		props.stylesheets,
		props.onUpdate,
		props.options);
	
	const openerFnConsumer = function (openerFn: () => void) {
		const hvEventHandler = (e: string) => {
			if (e === ON_REQUEST_EVENT_NAME_PREFIX + hvConfig.id) openerFn();
		};

		useEffect(() => {
			Events.subscribe(hvEventHandler);
			return () => void Events.unsubscribe(hvEventHandler);
		});
	}
	const closerFnConsumer = function (closerFn: () => void) {
		const hvEventHandler = (e: string) => {
			if (e === DO_DISMISS_EVENT_NAME_PREFIX + hvConfig.id) closerFn();
		};

		useEffect(() => {
			Events.subscribe(hvEventHandler);
			return () => void Events.unsubscribe(hvEventHandler);
		});
	}
	return React.createElement(BottomDrawer, { ...hvConfig,
			openerFnConsumer: openerFnConsumer,
			closerFnConsumer: closerFnConsumer },
		...children);
}

HvBottomDrawer.namespaceURI = TRAQ_UI_XMLNS;
HvBottomDrawer.localName = 'bottom-drawer' as LocalName;
HvBottomDrawer.localNameAliases = [] as LocalName[];

export default HvBottomDrawer;
