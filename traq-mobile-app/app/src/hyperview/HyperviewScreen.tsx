import Hyperview from 'hyperview';
import React, { useEffect, useState } from 'react';
import { fetchWrapper, formatDate } from './helpers';
import HyperviewSvg from './HyperviewSvg';
import HvBsIcon from "@/app/src/hyperview/component/HvBsIcon";
import HvFab from "@/app/src/hyperview/component/HvFab";
import HvBottomDrawer from "@/app/src/hyperview/component/HvBottomDrawer";
import HvBoolSwitch from "@/app/src/hyperview/component/HvBoolSwitch";
import { BackHandler, Text, View, ViewProps } from "react-native";
import HvDateTimePicker from "@/app/src/hyperview/component/HvDateTimePicker";
import HvLabel from "@/app/src/hyperview/component/HvLabel";
import HvColourPill from "@/app/src/hyperview/component/HvColourPill";
import SerialiseValue from "@/app/src/hyperview/behaviour/SerialiseValue";
import AsyncStorage from '@react-native-async-storage/async-storage';
import DialogContainer from "react-native-dialog/lib/Container";
import DialogTitle from "react-native-dialog/lib/Title";
import DialogDescription from "react-native-dialog/lib/Description";
import DialogInput from "react-native-dialog/lib/Input";
import DialogButton from "react-native-dialog/lib/Button";
import HvCalendar from "@/app/src/hyperview/component/HvCalendar";

const Hyperviewcreen = (props: ViewProps) => {
	const [uiServerUrl, setUiServerUrl] = useState<string | null>(null);
	const [loading, setLoading] = useState(true);
	const [promptValue, setPromptValue] = useState('https://');

	const lookupUiServerUrl = async () => {
		try {
			const url = await AsyncStorage.getItem('traq.ui.server.url');
			if(url) setUiServerUrl(url);
			setLoading(false);
		} catch (e) {
			console.error(e);
		}
	};
	
	const saveUiServerUrl = async () => {
		try {
			await AsyncStorage.setItem('traq.ui.server.url', promptValue);
			setUiServerUrl(promptValue);
		} catch (e) {
			console.error(e);
		}
	}

	useEffect(() => {
		lookupUiServerUrl().catch(e => console.error(e));
	}, []); 
	
	if(loading)
		return <Text>Loading</Text>;

	if (uiServerUrl)
		return (
			<Hyperview
				behaviors={[SerialiseValue]}
				components={[
					HyperviewSvg,
					HvBsIcon,
					HvFab,
					HvBottomDrawer,
					HvBoolSwitch,
					HvDateTimePicker,
					HvLabel,
					HvColourPill, 
					HvCalendar]}
				entrypointUrl={uiServerUrl}
				fetch={fetchWrapper}
				formatDate={formatDate} />
		);

	else
		return (
		<View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}} testID="THEVIEW">
			<DialogContainer visible={true}>
				<DialogTitle>Sentinel</DialogTitle>
				<DialogDescription>
					Enter UI Server URL
				</DialogDescription>
				<DialogInput
					value={promptValue}
					testID="net.poundex.traq:id/uiUrlPromptField"
					onChangeText={setPromptValue}/>
				<DialogButton label="Quit" onPress={() => BackHandler.exitApp()} />
				<DialogButton label="Save" testID="net.poundex.traq:id/saveUiUrlButton" onPress={() => saveUiServerUrl()}/>
				<Text testID="net.poundex.traq:id/uiServerPromptIsShowing" style={{height: 1}}></Text>
			</DialogContainer>
		</View>
	);
}

export default Hyperviewcreen;
