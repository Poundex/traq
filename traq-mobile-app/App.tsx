import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { SafeAreaInsetsContext, SafeAreaProvider } from "react-native-safe-area-context";
import { View } from "react-native";
import HyperviewScreen from "@/app/src/hyperview/HyperviewScreen";

// noinspection JSUnusedGlobalSymbols
export default () => {
	return (
		<SafeAreaProvider>
			<SafeAreaInsetsContext.Consumer>
				{insets => (
					<View
						style={{
							flex: 1,
							paddingTop: insets?.top,
							paddingBottom: insets?.bottom,
							paddingLeft: insets?.left,
							paddingRight: insets?.right,
						}}>
						<NavigationContainer>
							<HyperviewScreen />
						</NavigationContainer>
					</View>
				)}
			</SafeAreaInsetsContext.Consumer>
		</SafeAreaProvider>);
};
