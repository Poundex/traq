package net.poundex.traq.marker;

import java.util.Comparator;
import java.util.Map;

public enum MarkerSummaryComparator 
		implements Comparator<Map.Entry<MarkerDto, MarkableService.MarkerSummary>> {
	
	INSTANCE;

	@Override
	public int compare(
			Map.Entry<MarkerDto, MarkableService.MarkerSummary> left, 
			Map.Entry<MarkerDto, MarkableService.MarkerSummary> right) {
		
		if(left.getKey() instanceof BasePropertyDto &&
				right.getKey() instanceof BasePropertyDto)
			return left.getKey().name().compareTo(right.getKey().name());
		else if(left.getKey() instanceof BasePropertyDto)
			return 1;
		else if(right.getKey() instanceof BasePropertyDto)
			return -1;

		if(left.getValue().duration() != null &&
				right.getValue().duration() != null)
			return right.getValue().duration().value()
					.compareTo(left.getValue().duration().value());
		else if(left.getValue().duration() != null)
			return -1;
		else if(right.getValue().duration() != null)
			return 1;

		if(left.getValue().count() != null &&
				right.getValue().count() != null)
			return right.getValue().count().value()
					.compareTo(left.getValue().count().value());
		else if (left.getValue().count() != null)
			return -1;
		else if(right.getValue().count() != null)
			return 1;

		return 0;
	}
}
