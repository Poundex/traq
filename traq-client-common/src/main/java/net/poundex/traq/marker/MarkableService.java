package net.poundex.traq.marker;

import lombok.Builder;
import lombok.RequiredArgsConstructor;
import net.poundex.traq.summary.MarkerCountDto;
import net.poundex.traq.summary.MarkerDurationDto;
import net.poundex.traq.summary.MarkerPropertyValuesDto;
import net.poundex.traq.summary.MarkerSummaryDataDto;
import net.poundex.traq.summary.MarkerSummaryDto;
import net.poundex.xmachinery.spring.graphql.client.GraphQlClient;
import org.springframework.aot.hint.annotation.RegisterReflectionForBinding;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
@RegisterReflectionForBinding({
		MarkerDto.class, 
		TagDto.class,
		MentionDto.class,
		BasePropertyDto.class,
		PropertyDto.class
})
public class MarkableService {

	private final GraphQlClient graphQlClient;

	@Builder(toBuilder = true)
	public record MarkerSummary(MarkerDto marker, MarkerDurationDto duration, MarkerCountDto count,
	                            MarkerPropertyValuesDto values) { }

	public Map<MarkerDto, MarkerSummary> getMarkerSummaries(MarkerSummaryDto markerSummaryDto) {
		return Stream.<MarkerSummaryDataDto<?>>concat(
						markerSummaryDto.duration().stream(), Stream.concat(
								markerSummaryDto.count().stream(),
								markerSummaryDto.propertyValues().stream()))
				.collect(Collectors.groupingBy(
						MarkerSummaryDataDto::marker,
						Collectors.reducing(MarkerSummary.builder().build(), dto -> switch (dto) {
							case MarkerCountDto c -> MarkerSummary.builder().count(c).build();
							case MarkerDurationDto du -> MarkerSummary.builder().duration(du).build();
							case MarkerPropertyValuesDto pv -> MarkerSummary.builder().values(pv).build();
						}, (l, r) -> {
							MarkerSummary.MarkerSummaryBuilder builder = l.toBuilder();
							if (r.count() != null) builder.count(r.count());
							if (r.duration() != null) builder.duration(r.duration());
							if (r.values() != null) builder.values(r.values());
							return builder.build();
						})));
	}
}