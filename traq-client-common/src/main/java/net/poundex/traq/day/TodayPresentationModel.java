package net.poundex.traq.day;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import net.poundex.traq.marker.MarkableService;
import net.poundex.traq.marker.MarkerDto;
import net.poundex.traq.timeline.TimelineSpanDto;

import java.util.Map;

@Getter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
public class TodayPresentationModel extends DayPresentationModel {

	private final TimelineSpanDto activeSpan;
	
	public TodayPresentationModel(DayDto day, TimelineSpanDto activeSpan, Map<MarkerDto, MarkableService.MarkerSummary> markerSummaries) {
		super(day, markerSummaries);
		this.activeSpan = activeSpan;
	}

	public static TodayPresentationModelBuilder<TodayPresentationModel, ?> newTodayPresentationModelBuilder() {
		return new TodayPresentationModelBuilderImpl();
	}
}
