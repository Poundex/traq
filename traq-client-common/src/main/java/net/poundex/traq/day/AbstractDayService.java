package net.poundex.traq.day;

import io.vavr.Function3;
import lombok.RequiredArgsConstructor;
import net.poundex.traq.api.client.DayGraphQLQuery;
import net.poundex.traq.api.client.DayProjectionRoot;
import net.poundex.traq.marker.MarkableService;
import net.poundex.traq.timeline.TimelineSpanDto;
import net.poundex.xmachinery.spring.graphql.client.GraphQlClient;
import net.poundex.xmachinery.spring.graphql.client.QueryRequest;
import net.poundex.xmachinery.spring.graphql.client.QueryResponse;
import org.springframework.aot.hint.annotation.RegisterReflectionForBinding;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.time.LocalDate;
import java.util.function.Supplier;

@Service
@RequiredArgsConstructor
@RegisterReflectionForBinding(DayDto.class)
abstract class AbstractDayService<
		Today extends TodayPresentationModel,
		Day extends DayPresentationModel,
		TodayBuilder extends TodayPresentationModel.TodayPresentationModelBuilder<? extends Today, ? extends TodayBuilder>,
		DayBuilder extends DayPresentationModel.DayPresentationModelBuilder<? extends Day, ? extends DayBuilder>> {

	protected final GraphQlClient graphQlClient;
	protected final MarkableService markableService;

	protected abstract TodayBuilder newTodayPresentationModel();
	protected abstract DayBuilder newDayPresentationModel();

	public Mono<Today> getDayModelForToday(String ledgerId) {
		return this.doGetDayModel(
				todayQueryWithExtra(ledgerId, getTodayQuery(LocalDate.now(), ledgerId)), 
				this::newTodayPresentationModel, 
				this::doWithTodayQueryResponse);
	}
	
	public Mono<Day> getDayModel(String ledgerId, LocalDate day) {
		return doGetDayModel(
				dayQueryWithExtra(ledgerId, getQuery(day, ledgerId)),
				this::newDayPresentationModel,
				this::doWithDayQueryResponse);
	}
	
	private <PM extends DayPresentationModel, B extends DayPresentationModel.DayPresentationModelBuilder<? extends PM, ? extends B>> 
	Mono<PM> doGetDayModel(
			QueryRequest query,
			Supplier<B> newPmFactory,
			Function3<DayDto, QueryResponse, B, B> doWithFn) {
		return graphQlClient.query(query)
				.map(resp -> buildDayPresentationModel(newPmFactory, doWithFn, resp));
	}

	private <PM extends DayPresentationModel, B extends DayPresentationModel.DayPresentationModelBuilder<? extends PM, ? extends B>>
	PM buildDayPresentationModel(
			Supplier<B> newPmFactory, 
			Function3<DayDto, QueryResponse, B, B> doWithFn,
			QueryResponse resp) {
		B b = newPmFactory.get();
		DayDto day = resp.extractValue("day", DayDto.class);
		b.day(day);
		b.markerSummaries(markableService.getMarkerSummaries(day.markerSummary()));
		return doWithFn.apply(day, resp, b).build();
	}
	
	protected QueryRequest getTodayQuery(LocalDate day, String ledgerId) {
		return QueryRequest.of(DayGraphQLQuery.newRequest()
						.day(day)
						.ledger(ledgerId)
				.build(), new DayProjectionRoot<>()
				.day()
				.events()
					.id()
					.__typename()
					.time()
					.description()
					.markers()
						.__typename()
						.id()
						.name()
						.title()
						.onProperty().value().parent()
						.parent()
					.onTimelineSpan()
						.endTime()
						.active()
						.duration()
					.root()
				.startTime()
				.endTime()
				.projectedEndTime()
				.duration()
				.length()
				.markerSummary()
					.duration()
					.marker()
						.__typename()
						.id()
						.name()
						.title()
						.onProperty()
							.value()
							.parent()
							.parent()
						.value()
						.parent()
					.count()
					.marker()
						.__typename()
						.id()
						.name()
						.title()
						.onProperty()
							.value()
							.parent()
						.parent()
					.value()
					.parent()
				.propertyValues()
					.marker()
						.__typename()
						.id()
						.name()
						.title()
						.parent()
					.value());
	}

	protected QueryRequest todayQueryWithExtra(String ledgerId, QueryRequest todayQueryRequest) {
		return todayQueryRequest;
	}

	protected QueryRequest getQuery(LocalDate day, String ledgerId) {
		return QueryRequest.of(DayGraphQLQuery.newRequest()
						.day(day)
						.ledger(ledgerId)
				.build(), new DayProjectionRoot<>()
				.day()
				.events()
					.id()
					.__typename()
					.time()
					.description()
					.markers()
						.__typename()
						.id()
						.name()
						.title()
						.onProperty().value().parent()
						.parent()
				.onTimelineSpan()
						.endTime()
						.active()
						.duration()
						.parent()
					.parent()
				.startTime()
				.endTime()
				.duration()
				.length()
				.markerSummary()
					.duration()
						.marker()
							.__typename()
							.id()
							.name()
							.title()
							.onProperty()
								.value()
								.parent()
							.parent()
						.value()
						.parent()
					.count()
						.marker()
						.__typename()
						.id()
						.name()
						.title()
						.onProperty()
							.value()
							.parent()
						.parent()
					.value()
					.parent()
				.propertyValues()
					.marker()
						.__typename()
						.id()
						.name()
						.title()
						.parent()
					.value());
	}
	
	protected QueryRequest dayQueryWithExtra(String ledgerId, QueryRequest dayQueryRequest) {
		return dayQueryRequest;
	}

	protected TimelineSpanDto findActiveSpan(DayDto day) {
		return day.events().stream()
				.filter(x -> x instanceof TimelineSpanDto)
				.map(x -> ((TimelineSpanDto) x))
				.filter(TimelineSpanDto::active)
				.findFirst()
				.orElse(null);
	}
	
	protected TodayBuilder doWithTodayQueryResponse(DayDto day, QueryResponse queryResponse, TodayBuilder builder) {
		builder.activeSpan(findActiveSpan(day));
		return builder;
	}

	protected <B extends DayBuilder> B doWithDayQueryResponse(DayDto day, QueryResponse queryResponse, B builder) {
		return builder;
	}
}
