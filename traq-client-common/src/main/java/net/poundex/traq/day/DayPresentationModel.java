package net.poundex.traq.day;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.SuperBuilder;
import net.poundex.traq.marker.MarkableService;
import net.poundex.traq.marker.MarkerDto;
import net.poundex.traq.marker.MarkerSummaryComparator;

import java.util.List;
import java.util.Map;

@Data
@RequiredArgsConstructor
@SuperBuilder
public class DayPresentationModel {
	private final DayDto day;
	private final Map<MarkerDto, MarkableService.MarkerSummary> markerSummaries;

	public static DayPresentationModelBuilder<DayPresentationModel, ?> newDayPresentationModelBuilder() {
		return new DayPresentationModelBuilderImpl();
	}
	
	public List<Map.Entry<MarkerDto, MarkableService.MarkerSummary>> getSortedMarkerSummaries() {
		return markerSummaries.entrySet().stream()
				.sorted(MarkerSummaryComparator.INSTANCE)
				.toList();
	}
}
