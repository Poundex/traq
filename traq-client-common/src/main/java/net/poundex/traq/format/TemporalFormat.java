package net.poundex.traq.format;

public class TemporalFormat {

	public static String getOrd(Integer day) {
		return switch (day.toString()) {
			case "11", "12", "13" -> "th";
			default -> switch (day.toString().substring(day.toString().length() - 1)) {
				case "1" -> "st";
				case "2" -> "nd";
				case "3" -> "rd";
				default -> "th";
			};
		};
	}
}
