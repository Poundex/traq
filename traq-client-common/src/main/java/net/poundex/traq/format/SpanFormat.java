package net.poundex.traq.format;

import java.time.Duration;

public class SpanFormat {
	public static String toElapsedTime(Duration duration) {
		return String.format("%s′%02d″", duration.toHours(), duration.toMinutesPart());
	}
}
