package net.poundex.traq.format;

import net.poundex.traq.marker.BasePropertyDto;
import net.poundex.traq.marker.MarkerDto;
import net.poundex.traq.marker.MentionDto;
import net.poundex.traq.marker.PropertyDto;
import net.poundex.traq.marker.TagDto;
import org.springframework.util.StringUtils;
import org.unbescape.html.HtmlEscape;

import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class MarkerFormat {

	private static final Pattern tagPattern = Pattern.compile("\\B(\\+[\\w-]+)");
	private static final Pattern mentionPattern = Pattern.compile("\\B(@[\\w-]+)");
	private static final Pattern propPattern = Pattern.compile("\\B(:[\\w-]+(=[\\w-]+)?)");

	@Deprecated
	public static String marker(MarkerDto markerDto) {
		return switch (markerDto) {
			case BasePropertyDto dto -> String.format(":%s", dto.name());
			case MentionDto dto -> String.format("@%s", dto.name());
			case PropertyDto dto -> String.format(":%s=%s", dto.name(), dto.value());
			case TagDto dto -> String.format("+%s", dto.name());
		};
	}

	public interface MarkerFormatter {
		String formatBaseProperty(String markerWithSigil, BasePropertyDto dto);
		String formatMention(String markerWithSigil, MentionDto dto);
		String formatPropertyDto(String markerWithSigil, PropertyDto dto);
		String formatTag(String markerWithSigil, TagDto dto);
	}

	public static String format(MarkerDto markerDto, MarkerFormatter formatter, String original) {
		String originalTrimmed = original != null ? original.substring(1) : null;
		return switch (markerDto) {
			case BasePropertyDto dto -> formatter.formatBaseProperty(":%s".formatted(titleOrNameEscaped(dto, originalTrimmed)), dto);
			case MentionDto dto -> formatter.formatMention("@%s".formatted(titleOrNameEscaped(dto, originalTrimmed)), dto);
			case PropertyDto dto -> formatter.formatPropertyDto(StringUtils.hasText(dto.title())
					? ":%s".formatted(dto.title())
					: ":%s=%s".formatted(dto.name(), dto.value()), dto);
			case TagDto dto -> formatter.formatTag("+%s".formatted(titleOrNameEscaped(dto, originalTrimmed)), dto);
		};
	}

	private static String titleOrNameEscaped(MarkerDto markerDto, String original) {
		return Optional.ofNullable(markerDto.title())
				.or(() -> Optional.ofNullable(StringUtils.hasText(original) ? original : null))
				.or(() -> Optional.of(markerDto.name()))
				.map(HtmlEscape::escapeHtml4Xml)
				.orElseThrow();
	}

	public static String formatAll(String stringWithMarkers, Set<? extends MarkerDto> markers, BiFunction<MarkerDto, String, String> formatter) {
		Map<String, MarkerDto> markersByString = markers.stream().collect(Collectors.toMap(
				markerDto -> markerDto.asString().toLowerCase(), kv -> kv));

		String s = tagPattern.matcher(stringWithMarkers).replaceAll(mr -> formatter.apply(markersByString.get(mr.group().toLowerCase()), mr.group()));
		s = mentionPattern.matcher(s).replaceAll(mr -> formatter.apply(markersByString.get(mr.group().toLowerCase()), mr.group()));
		return propPattern.matcher(s).replaceAll(mr -> formatter.apply(markersByString.get(mr.group().toLowerCase()), mr.group()));
	}
}
