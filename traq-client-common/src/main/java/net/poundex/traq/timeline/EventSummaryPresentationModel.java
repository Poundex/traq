package net.poundex.traq.timeline;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import net.poundex.traq.marker.MarkableService;
import net.poundex.traq.marker.MarkerDto;
import net.poundex.traq.marker.MarkerSummaryComparator;
import net.poundex.traq.summary.EventSummaryDto;

import java.time.Duration;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Data
@RequiredArgsConstructor
public class EventSummaryPresentationModel {
	private final EventSummaryDto eventSummary;
	private final Map<MarkerDto, MarkableService.MarkerSummary> markerSummaries;
	
	public record GroupedSpans(String description, List<TimelineSpanDto> spans, Duration durationSum, int count) {}
	
	public List<GroupedSpans> getGroupedSpans() {
		return eventSummary.events().stream()
				.filter(e -> e instanceof TimelineSpanDto)
				.map(e -> ((TimelineSpanDto) e))
				.collect(Collectors.groupingBy(TimelineSpanDto::description,
						Collectors.collectingAndThen(Collectors.toList(), spans ->
								new GroupedSpans(
										spans.get(0).description(),
										spans,
										spans.stream()
												.map(TimelineSpanDto::duration)
												.reduce(Duration.ZERO, Duration::plus),
										spans.size()))))
				.values()
				.stream()
				.sorted(Comparator.comparing(GroupedSpans::durationSum))
				.toList();
	}
	
	public Stream<GroupedSpans> getSortedGroupedSpans() {
		return getGroupedSpans().stream()
				.sorted((l, r) -> r.durationSum().compareTo(l.durationSum()));
	}

	public List<Map.Entry<MarkerDto, MarkableService.MarkerSummary>> getSortedMarkerSummaries() {
		return markerSummaries.entrySet().stream()
				.sorted(MarkerSummaryComparator.INSTANCE)
				.toList();
	}
}
