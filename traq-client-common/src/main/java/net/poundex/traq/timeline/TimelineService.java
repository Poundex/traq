package net.poundex.traq.timeline;

import lombok.RequiredArgsConstructor;
import net.poundex.traq.api.client.SummaryGraphQLQuery;
import net.poundex.traq.api.client.SummaryProjectionRoot;
import net.poundex.traq.ledger.LedgerDto;
import net.poundex.traq.marker.MarkableService;
import net.poundex.traq.summary.EventSummaryDto;
import net.poundex.xmachinery.spring.graphql.client.GraphQlClient;
import net.poundex.xmachinery.spring.graphql.client.QueryRequest;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
abstract class AbstractTimelineService {

	protected final GraphQlClient graphQlClient;
	protected final MarkableService markableService;

	protected abstract Mono<LedgerDto> getLedgerByName(String ledgerName);

	public Mono<EventSummaryPresentationModel> getEventSummary(String ledgerName, String timelineQuery) {
		return getLedgerByName(ledgerName).flatMap(l -> doGetEventSummary(l.id(), null, timelineQuery));
	}

	// TODO: Not this
	public Mono<EventSummaryPresentationModel> getEventSummary2(String ledgerId, String timelineQuery) {
		return doGetEventSummary(ledgerId, null, timelineQuery);
	}

	private Mono<EventSummaryPresentationModel> doGetEventSummary(
			String ledgerId,
			LocalDate since,
			String timelineQuery) {
		return graphQlClient.query(withExtraQuery(getQuery(ledgerId, timelineQuery)))
				.map(resp -> buildPresentationModel(resp.extractValue("summary", EventSummaryDto.class)));
	}

	private EventSummaryPresentationModel buildPresentationModel(EventSummaryDto summary) {
		return new EventSummaryPresentationModel(summary, markableService.getMarkerSummaries(summary.markerSummary()));
	}

	protected QueryRequest getQuery(String ledgedId, String timelineQuery) {
		return QueryRequest.of(SummaryGraphQLQuery.newRequest()
						.ledger(ledgedId)
						.timelineQuery(timelineQuery)
				.build(), new SummaryProjectionRoot<>()
				.ledger()
					.name()
					.parent()
				.parsedQuery()
				.events()
					.id()
					.day()
					.__typename()
					.time()
					.description()
					.markers()
						.__typename()
						.id()
						.name()
						.title()
						.onProperty().value().parent()
						.parent()
					.onTimelineSpan()
						.endTime()
						.active()
						.duration()
						.parent()
					.parent()
				.duration()
				.markerSummary()
					.duration()
						.marker()
							.__typename()
							.id()
							.name()
							.title()
							.onProperty()
								.value()
								.parent()
							.parent()
						.value()
						.parent()
					.count()
						.marker()
							.__typename()
							.id()
							.name()
							.title()
							.onProperty()
								.value()
								.parent()
							.parent()
						.value()
						.parent()
					.propertyValues()
						.marker()
							.__typename()
							.id()
							.name()
							.title()
						.parent()
					.value());
	}

	protected QueryRequest withExtraQuery(QueryRequest queryRequest) {
		return queryRequest;
	}
}
