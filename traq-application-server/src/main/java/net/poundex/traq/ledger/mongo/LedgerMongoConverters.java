package net.poundex.traq.ledger.mongo;

import net.poundex.autoconverters.convert.Converters;
import net.poundex.autoconverters.convert.PropertyResolver;
import net.poundex.traq.ledger.Ledger;

import java.time.Duration;

@Converters
interface LedgerMongoConverters {
    Ledger toDomain(MongoLedger ledger);
    MongoLedger toDbo(Ledger ledger);

    @PropertyResolver(MongoLedger.class)
    default Long targetDuration(Ledger ledger) {
        if(ledger.getTargetDuration() == null) return null;
        return ledger.getTargetDuration().toSeconds();
    }
    
    @PropertyResolver(Ledger.class)
    default Duration targetDuration(MongoLedger ledger) {
        if(ledger.getTargetDuration() == null) return null;
        return Duration.ofSeconds(ledger.getTargetDuration());
    }
}