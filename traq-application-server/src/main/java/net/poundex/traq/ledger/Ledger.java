package net.poundex.traq.ledger;

import com.querydsl.core.annotations.QueryEntity;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import net.poundex.traq.TraqObject;

import java.time.Duration;

@Data
@RequiredArgsConstructor
@Builder(toBuilder = true)
@QueryEntity
public class Ledger implements TraqObject {
    private final String id;
    private final String name;
    private final Duration targetDuration;
    private final String noProdMarker;
    private final String noAccountMarker;
}
