package net.poundex.traq.ledger.api;

import lombok.RequiredArgsConstructor;
import net.poundex.traq.ledger.Ledger;
import net.poundex.traq.ledger.LedgerInputDto;
import net.poundex.traq.ledger.LedgerRepository;
import net.poundex.traq.ledger.LedgerService;
import net.poundex.traq.marker.Marker;
import net.poundex.traq.marker.MarkerRepository;
import net.poundex.xmachinery.spring.graphql.batch.BatchLoader;
import net.poundex.xmachinery.spring.graphql.batch.ObjectLoader;
import net.poundex.xmachinery.spring.graphql.batch.RegisterBatchLoaders;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.graphql.data.method.annotation.SchemaMapping;
import org.springframework.stereotype.Controller;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static net.poundex.traq.api.ControllerUtils.required;
import static net.poundex.traq.ledger.QLedger.ledger;

@Controller
@RequiredArgsConstructor
@SchemaMapping(typeName = "Ledger")
@RegisterBatchLoaders
class LedgerController {
	
	private final LedgerRepository ledgerRepository;
	private final LedgerService ledgerService;
	private final LedgerApiConverters converters;
	private final MarkerRepository markerRepository;
	
	@QueryMapping
	public Flux<Ledger> ledgers() {
		return ledgerRepository.findAll(); 
	}
	
	@MutationMapping
	public Mono<Ledger> createLedger(@Argument LedgerInputDto ledger) {
		return ledgerService.createLedger(ledger.name(), ledger.targetDuration());
	}

	@QueryMapping
	public Mono<Ledger> ledger(@Argument String id, ObjectLoader<String, Ledger> dataLoader) {
		return required(dataLoader.load(id), Ledger.class, id);
	}
	
	@QueryMapping
	public Mono<Ledger> ledgerByName(@Argument String name) {
		return required(
				ledgerRepository.findOne(ledger.name.equalsIgnoreCase(name)),
				Ledger.class,
				name);
	}
	
	@MutationMapping
	public Mono<Ledger> updateLedger(
			@Argument String id,
			@Argument LedgerInputDto ledger,
			ObjectLoader<String, Ledger> loader) {

		return required(loader, Ledger.class, id)
				.flatMap(existing ->
						ledgerService.updateLedger(existing, converters.fromDto(ledger)));
	}

	@BatchLoader
	Mono<Map<String, Ledger>> ledgerLoader(Set<String> ids) {
		return ledgerRepository.findAllById(ids)
				.collect(Collectors.toMap(Ledger::getId, m -> m));
	}

	@SchemaMapping
	public Mono<Set<Marker>> markers(
			Ledger ledger,
			ObjectLoader<String, Set<Marker>> markersLoader) {
		return markersLoader.load(ledger.getId());
	}
	
	@BatchLoader
	Mono<Map<String, Set<Marker>>> markersByLedgerLoader(Set<String> ledgerIds) {
		return markerRepository.findAll()
				.collect(Collectors.groupingBy(Marker::getLedger, Collectors.toSet()));
	}
}
