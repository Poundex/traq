package net.poundex.traq.ledger;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Order(Ordered.HIGHEST_PRECEDENCE)
class LedgerEnsurer implements ApplicationListener<ApplicationReadyEvent> {
	
	private final LedgerRepository ledgerRepository;
	private final LedgerService ledgerService;
	
	@Override
	public void onApplicationEvent(ApplicationReadyEvent event) {
		ledgerRepository.count()
				.filter(n -> n == 0)
				.flatMap(ignored -> ledgerService.createLedger("default", null))
				.block();
	}
}
