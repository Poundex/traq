package net.poundex.traq.ledger.api;

import graphql.schema.DataFetchingFieldSelectionSet;
import lombok.RequiredArgsConstructor;
import net.poundex.traq.ledger.Ledger;
import net.poundex.traq.ledger.LedgerItem;
import net.poundex.xmachinery.spring.graphql.batch.ObjectLoader;
import org.springframework.graphql.data.method.annotation.SchemaMapping;
import org.springframework.stereotype.Controller;
import reactor.core.publisher.Mono;

import static net.poundex.xmachinery.spring.graphql.batch.BatchLoadingHelper.loadIfNecessary;

@Controller
@RequiredArgsConstructor
class LedgerItemController {
	
	@SchemaMapping(field = "ledger", typeName = "LedgerItem")
	public Mono<Ledger> ledger(LedgerItem ledgerItem,
	                              ObjectLoader<String, Ledger> loader,
	                              DataFetchingFieldSelectionSet fieldSelectionSet) {

		return loadIfNecessary(ledgerItem.getLedger(), fieldSelectionSet, loader)
				.andGet()
				.orElse(() -> Ledger.builder().id(ledgerItem.getLedger()).build());
	}
}
