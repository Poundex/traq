package net.poundex.traq.ledger;

import org.springframework.data.querydsl.ReactiveQuerydslPredicateExecutor;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Component;

@NoRepositoryBean
@Component
public interface LedgerRepository extends
		ReactiveCrudRepository<Ledger, String>,
        ReactiveQuerydslPredicateExecutor<Ledger> {
}
