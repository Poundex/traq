package net.poundex.traq.ledger.mongo;

import net.poundex.autoconverters.repositories.DaoFor;
import net.poundex.traq.ledger.LedgerRepository;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.data.querydsl.ReactiveQuerydslPredicateExecutor;

@DaoFor(LedgerRepository.class)
interface MongoLedgerRepository extends 
        ReactiveMongoRepository<MongoLedger, ObjectId>,
        ReactiveQuerydslPredicateExecutor<MongoLedger> {
}
