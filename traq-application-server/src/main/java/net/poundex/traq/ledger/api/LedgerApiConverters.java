package net.poundex.traq.ledger.api;

import net.poundex.autoconverters.convert.Converters;
import net.poundex.traq.ledger.Ledger;
import net.poundex.traq.ledger.LedgerDto;
import net.poundex.traq.ledger.LedgerInputDto;

@Converters
public interface LedgerApiConverters {
    LedgerDto toDto(Ledger ledger);
    Ledger fromDto(LedgerInputDto dto);
}
