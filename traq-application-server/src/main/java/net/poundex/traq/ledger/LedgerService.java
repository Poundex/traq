package net.poundex.traq.ledger;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import net.poundex.traq.marker.MarkerService;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.Collections;
import java.util.List;

@Service
@RequiredArgsConstructor
@Validated
public class LedgerService {
	private final LedgerRepository ledgerRepository;
	private final MarkerService markerService;
	
	public Mono<Ledger> createLedger(String name, Duration targetDuration) {
		return ledgerRepository.save(Ledger.builder().name(name).targetDuration(targetDuration).build())
				.flatMap(l -> installDefaultMarkers(l)
						.flatMap(ledgerRepository::save));
	}
	
	private Mono<Ledger> installDefaultMarkers(Ledger ledger) {
		return Mono.just(ledger.toBuilder())
				
				.flatMap(builder -> markerService.modifyMarker(ledger, "+noprod", "No Prod", Collections.emptyList())
						.map(m -> builder.noProdMarker(m.getId())))

				.flatMap(builder -> markerService.modifyMarker(ledger, "+noacct", "No Acct", List.of("+noprod"))
						.map(m -> builder.noAccountMarker(m.getId())))
	
				.map(Ledger.LedgerBuilder::build);
	}

	@Validated
	public Mono<Ledger> updateLedger(Ledger existing, @Valid Ledger updated) {
		return ledgerRepository.save(updated.toBuilder().id(existing.getId()).build());
	}
}
