package net.poundex.traq.ledger;

import net.poundex.traq.TraqObject;

public interface LedgerItem extends TraqObject {
	String getLedger();
}
