package net.poundex.traq.ledger.mongo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("ledgers")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
class MongoLedger {
    @Id
    private ObjectId id;
    private String name;
    private Long targetDuration;
    private String noProdMarker;
    private String noAccountMarker;
}
