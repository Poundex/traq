package net.poundex.traq.timeline;

import org.springframework.data.querydsl.ReactiveQuerydslPredicateExecutor;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Component;

@NoRepositoryBean
@Component
public interface EventRepository extends
		ReactiveCrudRepository<TimelineEvent, String>,
        ReactiveQuerydslPredicateExecutor<TimelineEvent> {
}
