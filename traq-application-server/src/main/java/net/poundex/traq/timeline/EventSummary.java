package net.poundex.traq.timeline;

import net.poundex.traq.ledger.Ledger;
import net.poundex.traq.summary.Summarisable;

import java.time.Duration;
import java.time.LocalDate;
import java.util.List;

public record EventSummary(
		Ledger ledger,
		String parsedQuery,
		LocalDate since,
		List<TimelineEvent> events,
		Duration duration) implements Summarisable {

	@Override
	public List<TimelineEvent> getEvents() {
		return events();
	}
}
