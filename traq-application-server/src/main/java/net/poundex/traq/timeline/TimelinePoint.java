package net.poundex.traq.timeline;

import com.querydsl.core.annotations.QueryEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import java.time.Instant;
import java.time.LocalDate;
import java.util.Set;

@Getter
@EqualsAndHashCode(callSuper = true)
@ToString
@SuperBuilder(toBuilder = true)
@QueryEntity
public class TimelinePoint extends TimelineEvent {
    public TimelinePoint(
            String id, 
            LocalDate day, 
            String description, 
            Instant time, 
            Set<String> markers, 
            String ledger) {
        super(id, day, description, time, markers, ledger);
    }
}