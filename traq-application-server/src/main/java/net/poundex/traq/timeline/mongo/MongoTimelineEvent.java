package net.poundex.traq.timeline.mongo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;
import java.time.LocalDate;
import java.util.Set;

@Document("events")
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
abstract class MongoTimelineEvent {
    @Id
    private ObjectId id;
    private LocalDate day;
    private String description;
    private Instant time;
    private Set<String> markers;
    private String ledger;
}
