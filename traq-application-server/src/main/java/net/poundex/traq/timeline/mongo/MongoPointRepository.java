package net.poundex.traq.timeline.mongo;

import net.poundex.autoconverters.repositories.DaoFor;
import net.poundex.traq.timeline.PointRepository;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.data.querydsl.ReactiveQuerydslPredicateExecutor;

@DaoFor(PointRepository.class)
interface MongoPointRepository extends 
        ReactiveMongoRepository<MongoTimelinePoint, ObjectId>,
        ReactiveQuerydslPredicateExecutor<MongoTimelinePoint> {
}
