package net.poundex.traq.timeline;

import com.querydsl.core.annotations.QueryEntity;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.SuperBuilder;
import net.poundex.traq.TraqObject;
import net.poundex.traq.ledger.LedgerItem;
import net.poundex.traq.marker.Markable;

import java.time.Instant;
import java.time.LocalDate;
import java.util.Collections;
import java.util.Set;

@Data
@RequiredArgsConstructor
@SuperBuilder(toBuilder = true)
@QueryEntity
public abstract class TimelineEvent implements TraqObject, Markable, LedgerItem {
    private final String id;
    private final LocalDate day;
    private final String description;
    private final Instant time;
    @Builder.Default
    private final Set<String> markers = Collections.emptySet();
    private final String ledger;

    public abstract <C extends TimelineEvent, B extends TimelineEventBuilder<C, B>> TimelineEventBuilder<C, B> toBuilder();
}
