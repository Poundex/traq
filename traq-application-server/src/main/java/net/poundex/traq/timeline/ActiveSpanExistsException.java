package net.poundex.traq.timeline;

import lombok.experimental.StandardException;

@StandardException
public class ActiveSpanExistsException extends RuntimeException { }
