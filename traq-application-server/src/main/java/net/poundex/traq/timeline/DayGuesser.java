package net.poundex.traq.timeline;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.time.LocalDate;
import java.util.NoSuchElementException;

import static net.poundex.traq.timeline.QTimelineSpan.timelineSpan;

@Service
@RequiredArgsConstructor
public class DayGuesser {
	
	private final SpanRepository spanRepository;
	
	public Mono<LocalDate> getCurrentDayBestGuess(LocalDate day) {
		return Mono.justOrEmpty(day)
				.switchIfEmpty(Mono.defer(() -> spanRepository.findAll(timelineSpan.active.isTrue())
						.map(TimelineEvent::getDay)
						.single()))
				.onErrorComplete(IndexOutOfBoundsException.class)
				.onErrorComplete(NoSuchElementException.class)
				.switchIfEmpty(Mono.just(LocalDate.now()));
	}
}
