package net.poundex.traq.timeline.api;

import net.poundex.autoconverters.convert.Abstract;
import net.poundex.autoconverters.convert.Converters;
import net.poundex.autoconverters.convert.IgnoreTargetProperty;
import net.poundex.traq.timeline.StartSpanInfo;
import net.poundex.traq.timeline.StartSpanInfoDto;
import net.poundex.traq.timeline.TimelineEvent;
import net.poundex.traq.timeline.TimelineEventDto;
import net.poundex.traq.timeline.TimelinePoint;
import net.poundex.traq.timeline.TimelinePointDto;
import net.poundex.traq.timeline.TimelineSpan;
import net.poundex.traq.timeline.TimelineSpanDto;

@Converters
public interface TimelineEventApiConverters {
    @Abstract
    @IgnoreTargetProperty("markers")
    @IgnoreTargetProperty("ledger")
    TimelineEventDto toDto(TimelineEvent timelineEvent);
    TimelinePointDto toDto(TimelinePoint timelinePoint);
    TimelineSpanDto toDto(TimelineSpan timelineSpan);
    
    StartSpanInfoDto toDto(StartSpanInfo info);
    
//    @PropertyResolver(TimelineSpanDto.class)
//    default long duration(TimelineSpan timelineSpan) {
//        return timelineSpan.getDuration().toMinutes();
//    }
}
