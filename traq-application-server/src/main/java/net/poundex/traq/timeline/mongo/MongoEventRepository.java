package net.poundex.traq.timeline.mongo;

import net.poundex.autoconverters.repositories.DaoFor;
import net.poundex.traq.timeline.EventRepository;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.data.querydsl.ReactiveQuerydslPredicateExecutor;

@DaoFor(EventRepository.class)
interface MongoEventRepository extends 
        ReactiveMongoRepository<MongoTimelineEvent, ObjectId>,
        ReactiveQuerydslPredicateExecutor<MongoTimelineEvent> {
}
