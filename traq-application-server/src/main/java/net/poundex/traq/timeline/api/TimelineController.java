package net.poundex.traq.timeline.api;

import com.querydsl.core.types.Predicate;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.poundex.traq.api.ControllerUtils;
import net.poundex.traq.ledger.Ledger;
import net.poundex.traq.marker.Marker;
import net.poundex.traq.timeline.CreatePointInputDto;
import net.poundex.traq.timeline.CreateSpanInputDto;
import net.poundex.traq.timeline.EventRepository;
import net.poundex.traq.timeline.EventSummary;
import net.poundex.traq.timeline.PointRepository;
import net.poundex.traq.timeline.RepeatSpanInputDto;
import net.poundex.traq.timeline.SpanRepository;
import net.poundex.traq.timeline.StartSpanInfo;
import net.poundex.traq.timeline.StartSpanInputDto;
import net.poundex.traq.timeline.TimelineEvent;
import net.poundex.traq.timeline.TimelinePoint;
import net.poundex.traq.timeline.TimelineService;
import net.poundex.traq.timeline.TimelineSpan;
import net.poundex.traq.timeline.query.TimelineQueryParser;
import net.poundex.xmachinery.spring.graphql.batch.LoadContextObjects;
import net.poundex.xmachinery.spring.graphql.batch.LoadedContextObject;
import net.poundex.xmachinery.spring.graphql.batch.UniverseLoader;
import net.poundex.xmachinery.spring.querydsl.graphql.argumentresolver.Root;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.ContextValue;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;
import java.util.Map;

import static net.poundex.traq.api.ControllerUtils.validQuery;
import static net.poundex.traq.timeline.QTimelineSpan.timelineSpan;

@Controller
@RequiredArgsConstructor
@LoadContextObjects
@Slf4j
@Validated
class TimelineController {

	private final TimelineService timelineService;
	private final EventRepository eventRepository;
	private final SpanRepository spanRepository;
	private final PointRepository pointRepository;
	private final TimelineQueryParser queryParser;

	@MutationMapping
	@LoadedContextObject(Ledger.class)
	public Mono<TimelinePoint> createPoint(@Argument @Valid CreatePointInputDto point, @ContextValue Mono<Ledger> ledger) {
		return ledger.flatMap(l -> 
				timelineService.createPoint(point.time(), point.day(), point.description(), l));
	}

	@QueryMapping
	public Flux<TimelineEvent> events(@Root(TimelineEvent.class) Predicate predicate) {
		return eventRepository.findAll(predicate); 
	}

	@QueryMapping
	public Flux<TimelinePoint> points(@Root(TimelinePoint.class) Predicate predicate) {
		return pointRepository.findAll(predicate);
	}

	@QueryMapping
	public Flux<TimelineSpan> spans(
			@Root(TimelineSpan.class) Predicate predicate, 
			@Argument Integer last) {
		
		Flux<TimelineSpan> spans = spanRepository
				.findAll(predicate, /* TODO: See below */ last == null
						? timelineSpan.time.asc()
						: timelineSpan.time.desc());

		// TODO: Don't shove this into spans() (add something else like distinctRecentSpanHistory() )
		return last != null
				? spans
					.distinct(TimelineSpan::getDescription)
					.take(last, true)
				: spans;
	}

	@MutationMapping
	@LoadedContextObject(Ledger.class)
	public Mono<StartSpanInfo> startSpan(@Argument @Valid StartSpanInputDto span, @ContextValue Mono<Ledger> ledger) {
		return ledger.flatMap(l ->
				timelineService.startSpan(span.time(), span.day(), span.description(), l));
	}

	@MutationMapping
	@LoadedContextObject(Ledger.class)
	public Mono<TimelineSpan> stopActiveSpan(@ContextValue Mono<Ledger> ledger) {
		return ledger.flatMap(timelineService::stopActiveSpan);
	}

	@MutationMapping
	@LoadedContextObject(Ledger.class)
	public Mono<TimelineSpan> createSpan(@Argument @Valid CreateSpanInputDto span, @ContextValue Mono<Ledger> ledger) {
		return ledger.flatMap(l ->
				timelineService.createSpan(span.day(), span.time(), span.endTime(), span.description(), l));
	}
	
	@MutationMapping
	public Mono<TimelineEvent> updateDescription(@Argument String event, @Argument @NotBlank String description) {
		return ControllerUtils.required(eventRepository.findById(event), TimelineEvent.class, event)
				.flatMap(te -> timelineService.updateDescription(te, description));
	}
	
	@QueryMapping
	public Mono<TimelineEvent> event(@Argument String id) {
		return eventRepository.findById(id);
	}
	
	@MutationMapping
	@LoadedContextObject(Ledger.class)
	public Mono<TimelineSpan> repeatLastSpan(@Argument RepeatSpanInputDto span, @ContextValue Mono<Ledger> ledger) {
		return ledger.flatMap(l -> timelineService.repeatLastSpan(span.time(), span.day(), l));
	}
	
	@QueryMapping
	@LoadedContextObject(Ledger.class)
	public Mono<EventSummary> summary(
			@ContextValue Mono<Ledger> ledger,
			@Argument String timelineQuery,
			@Argument LocalDate since,
			UniverseLoader<String, Marker> markersLoader) {

		Mono<Map<String, ? extends Marker>> muLoaded = markersLoader.load();
		return ledger.flatMap(
				l -> muLoaded.flatMap(
						mu -> timelineService.summary(l,
								validQuery(() -> queryParser.parse(timelineQuery, mu)), since)));
	}
}
