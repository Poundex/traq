package net.poundex.traq.timeline;

import net.poundex.xmachinery.spring.ReadOnlyRepository;
import org.springframework.data.querydsl.ReactiveQuerydslPredicateExecutor;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.stereotype.Component;

@NoRepositoryBean
@Component
public interface PointRepository extends
		ReadOnlyRepository<TimelinePoint, String>,
        ReactiveQuerydslPredicateExecutor<TimelinePoint> {
}
