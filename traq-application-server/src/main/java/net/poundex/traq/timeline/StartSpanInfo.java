package net.poundex.traq.timeline;

public record StartSpanInfo(TimelineSpan span, TimelineSpan previousSpan) {
}
