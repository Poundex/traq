package net.poundex.traq.timeline.api;

import graphql.ErrorClassification;
import lombok.Getter;
import net.poundex.traq.timeline.query.QueryException;
import net.poundex.xmachinery.spring.graphql.error.GraphQlRequestException;

@Getter
public class TimelineQueryException extends GraphQlRequestException {
	
	private final QueryException queryException;

	public TimelineQueryException(QueryException queryException) {
		super(queryException.getMessage());
		this.queryException = queryException;
	}
	
	@Override
	public ErrorClassification getErrorType() {
		return ErrorClassification.errorClassification("TimelineQueryError");
	}
}
