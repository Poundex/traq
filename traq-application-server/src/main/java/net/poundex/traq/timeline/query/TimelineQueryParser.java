package net.poundex.traq.timeline.query;

import com.querydsl.core.types.Predicate;
import lombok.RequiredArgsConstructor;
import net.poundex.traq.marker.BaseProperty;
import net.poundex.traq.marker.Marker;
import net.poundex.traq.marker.MarkerService;
import net.poundex.traq.marker.Mention;
import net.poundex.traq.marker.Property;
import net.poundex.traq.marker.Tag;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static net.poundex.traq.timeline.QTimelineEvent.timelineEvent;

@Service
@RequiredArgsConstructor
public class TimelineQueryParser {
	
	private final MarkerService markerService;
	
	public TimelineQuery parse(String input, Map<String, ? extends Marker> markerUniverse) {
		record InputResult(String input, Optional<? extends Marker> result) {}

		Set<? extends Marker> markers = Arrays.stream(input.split(" "))
				.filter(it -> ! it.equals("OR"))
				.map(token -> {
					if ( ! token.startsWith("+")
							&& ! token.startsWith("@")
							&& ! token.startsWith(":"))
						throw new QueryException("Unexpected input: " + token);

					Optional<? extends Marker> m = Optional.empty();
					if (token.startsWith("+"))
						m = markerUniverse.values().stream()
								.filter(it -> it instanceof Tag && it.getName().equalsIgnoreCase(token.substring(1)))
								.findFirst();

					if (token.startsWith("@"))
						m = markerUniverse.values().stream()
								.filter(it -> it instanceof Mention && it.getName().equalsIgnoreCase(token.substring(1)))
								.findFirst();

					if (token.startsWith(":")) {
						if (token.contains("="))
							m = markerUniverse.values().stream()
									.filter(it -> it instanceof Property p && findProperty(p, token))
									.findFirst();
						else
							m = markerUniverse.values().stream()
									.filter(it -> it instanceof BaseProperty && it.getName().equalsIgnoreCase(token.substring(1)))
									.findFirst();
					}
						
					return new InputResult(token, m);
				})
				.map(o -> o.result().orElseThrow(() ->
						new QueryException("Could not find Marker for " + o.input())))
				.collect(Collectors.toSet());
	
		return new TimelineQuery() {
			@Override
			public Predicate getEventPredicate() {
				Map<Marker, Set<Marker>> allRoutes = markerService.getAllRoutes(markerUniverse);
				return timelineEvent.markers.any().in(
						markers.stream()
								.flatMap(m -> allRoutes.getOrDefault(m, Collections.emptySet()).stream())
								.map(Marker::getId)
								.collect(Collectors.toSet()));
			}

			@Override
			public String asString() {
				return markers.stream().map(Marker::asString)
						.collect(Collectors.joining(" OR "));
			}
		};
	}

	private boolean findProperty(Property marker, String token) {
		String[] bits = token.split("=", 2);
		return marker.getName().equalsIgnoreCase(bits[0].substring(1))
				&& marker.getValue().equalsIgnoreCase(bits[1]);
	}
}
