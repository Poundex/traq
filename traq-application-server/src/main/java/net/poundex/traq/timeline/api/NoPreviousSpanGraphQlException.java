package net.poundex.traq.timeline.api;

import graphql.ErrorClassification;
import lombok.Getter;
import net.poundex.traq.timeline.NoPreviousSpanException;
import net.poundex.xmachinery.spring.graphql.error.GraphQlRequestException;

@Getter
public class NoPreviousSpanGraphQlException extends GraphQlRequestException {
	
	private final NoPreviousSpanException noPreviousSpanException;

	public NoPreviousSpanGraphQlException(NoPreviousSpanException noPreviousSpanException) {
		super(noPreviousSpanException.getMessage());
		this.noPreviousSpanException = noPreviousSpanException;
	}
	
	@Override
	public ErrorClassification getErrorType() {
		return ErrorClassification.errorClassification("NoPreviousSpanError");
	}
}
