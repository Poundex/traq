package net.poundex.traq.timeline.mongo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.bson.types.ObjectId;

import java.time.Instant;
import java.time.LocalDate;
import java.util.Set;

@Data
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@NoArgsConstructor
class MongoTimelinePoint extends MongoTimelineEvent {
	public MongoTimelinePoint(
			ObjectId id, 
			LocalDate day, 
			String description, 
			Instant time, 
			Set<String> markers, 
			String ledger) {
		super(id, day, description, time, markers, ledger);
	}
}
