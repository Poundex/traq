package net.poundex.traq.timeline.mongo;

import com.querydsl.core.types.Predicate;
import net.poundex.autoconverters.repositories.DaoFor;
import net.poundex.traq.timeline.SpanRepository;
import net.poundex.xmachinery.spring.querydsl.predicatemapper.PredicateMapper;
import net.poundex.xmachinery.spring.querydsl.predicatemapper.QueryDslPredicateMapper;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.data.querydsl.ReactiveQuerydslPredicateExecutor;

import static net.poundex.traq.timeline.mongo.QMongoTimelineSpan.mongoTimelineSpan;

@DaoFor(SpanRepository.class)
@QueryDslPredicateMapper
interface MongoSpanRepository extends 
        ReactiveMongoRepository<MongoTimelineSpan, ObjectId>,
        ReactiveQuerydslPredicateExecutor<MongoTimelineSpan> {
	
	@PredicateMapper
	default Predicate active(boolean active) {
		return active
				? mongoTimelineSpan.endTime.isNull()
				: mongoTimelineSpan.endTime.isNotNull();
	}
}
