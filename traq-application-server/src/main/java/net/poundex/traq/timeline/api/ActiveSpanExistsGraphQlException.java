package net.poundex.traq.timeline.api;

import graphql.ErrorClassification;
import lombok.Getter;
import net.poundex.xmachinery.spring.graphql.error.GraphQlRequestException;

@Getter
public class ActiveSpanExistsGraphQlException extends GraphQlRequestException {
	
	private final ActiveSpanExistsGraphQlException activeSpanExistsGraphQlException;

	public ActiveSpanExistsGraphQlException(ActiveSpanExistsGraphQlException activeSpanExistsGraphQlException) {
		super(activeSpanExistsGraphQlException.getMessage());
		this.activeSpanExistsGraphQlException = activeSpanExistsGraphQlException;
	}
	
	@Override
	public ErrorClassification getErrorType() {
		return ErrorClassification.errorClassification("ActiveSpanExistsError");
	}
}
