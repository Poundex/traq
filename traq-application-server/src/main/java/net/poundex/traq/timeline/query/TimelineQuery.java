package net.poundex.traq.timeline.query;

import com.querydsl.core.types.Predicate;

public interface TimelineQuery {
	Predicate getEventPredicate();
	String asString();
}
