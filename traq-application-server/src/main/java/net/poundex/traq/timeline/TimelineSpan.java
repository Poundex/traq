package net.poundex.traq.timeline;

import com.querydsl.core.annotations.QueryEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Set;

@Data
@EqualsAndHashCode(callSuper = true)
@SuperBuilder(toBuilder = true)
@QueryEntity
@ToString(callSuper = true)
public class TimelineSpan extends TimelineEvent {
    
    private final Instant endTime;
    
    public TimelineSpan(
            String id, 
            LocalDate day, 
            String description, 
            Instant time, 
            Instant endTime, 
            Set<String> markers, 
            String ledger) {
        super(id, day, description, time, markers, ledger);
        this.endTime = endTime;
    }
    
    public boolean isActive() {
        return endTime == null;
    }
    
    public Duration getDuration() {
        return Duration.between(getTime(), getEndTime() != null ? getEndTime() : Instant.now());
    }

    public Instant getLatestTime() {
        return isActive() ? Instant.now() : endTime;
    }
}
