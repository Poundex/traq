package net.poundex.traq.timeline;

import lombok.RequiredArgsConstructor;
import net.poundex.traq.ledger.Ledger;
import net.poundex.traq.marker.Marker;
import net.poundex.traq.marker.realiser.MarkerRealiserService;
import net.poundex.traq.timeline.query.TimelineQuery;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

import static net.poundex.traq.timeline.QTimelineEvent.timelineEvent;
import static net.poundex.traq.timeline.QTimelineSpan.timelineSpan;

@Service
@RequiredArgsConstructor
public class TimelineService {

    private final EventRepository eventRepository;
	private final DayGuesser dayGuesser;
	private final MarkerRealiserService markerRealiserService;
	private final SpanRepository spanRepository;

	public Mono<TimelinePoint> createPoint(Instant time, LocalDate day, String description, Ledger ledger) {
		return createTimelineEvent(ledger, day, description, (d, m) ->
				TimelinePoint.builder()
						.day(d)
						.description(description)
						.time(time == null ? Instant.now() : time)
						.markers(m.stream().map(Marker::getId).collect(Collectors.toSet()))
						.ledger(ledger.getId())
						.build());
	}

	public Mono<StartSpanInfo> startSpan(Instant time, LocalDate day, String description, Ledger ledger) {
		Instant stopTime = time == null ? Instant.now() : time;
		return stopActiveSpan(ledger, stopTime)
				.singleOptional()
				.flatMap(stopped -> createTimelineEvent(ledger, day, description,
						(d, m) -> TimelineSpan.builder()
								.day(d)
								.description(description)
								.time(stopTime)
								.markers(m.stream().map(Marker::getId).collect(Collectors.toSet()))
								.ledger(ledger.getId())
								.build())
						.map(started -> new StartSpanInfo(started, stopped.orElse(null))));
	}

	public Mono<TimelineSpan> stopActiveSpan(Ledger ledger) {
		return stopActiveSpan(ledger, Instant.now());
	}

	public Mono<TimelineSpan> stopActiveSpan(Ledger ledger, Instant time) {
		return findActiveSpan(ledger)
				.flatMap(prevSpan -> eventRepository.save(prevSpan.toBuilder()
						.endTime(time)
						.build()));
	}

	@NotNull
	private Mono<TimelineSpan> findActiveSpan(Ledger ledger) {
		return spanRepository.findOne(timelineSpan.active.isTrue().and(timelineSpan.ledger.eq(ledger.getId())));
	}

	private <T extends TimelineEvent> Mono<T> createTimelineEvent(
			Ledger ledger, LocalDate day, String description, BiFunction<LocalDate, Set<? extends Marker>, T> eventFactory) {
		
		return dayGuesser.getCurrentDayBestGuess(day).flatMap(newDay ->
				markerRealiserService.realiseMarkersInString(ledger.getId(), description)
						.collect(Collectors.toSet())
						.flatMap(markers -> eventRepository.save(eventFactory.apply(newDay, markers))));
	}

	public Mono<TimelineSpan> createSpan(LocalDate day, Instant time, Instant endTime, String description, Ledger ledger) {
		return createTimelineEvent(ledger, day, description, (d, m) ->
				TimelineSpan.builder()
						.day(d)
						.description(description)
						.time(time)
						.endTime(endTime)
						.markers(m.stream().map(Marker::getId).collect(Collectors.toSet()))
						.ledger(ledger.getId())
						.build());
	}

	@SuppressWarnings("unchecked")
	public <T extends TimelineEvent> Mono<T> updateDescription(T event, String newDescription) {
		return markerRealiserService.realiseMarkersInString(event.getLedger(), newDescription)
				.collect(Collectors.toSet())
				.flatMap(markers -> eventRepository.save(((T)event
						.toBuilder()
						.description(newDescription)
						.markers((markers.stream().map(Marker::getId).collect(Collectors.toSet())))
						.build())));
	}

	public Mono<EventSummary> summary(Ledger ledger, TimelineQuery timelineQuery, LocalDate since) {
		return eventRepository.findAll(timelineQuery.getEventPredicate(), timelineEvent.time.asc())
				.collectList()
				.map(events -> new EventSummary(
						ledger,
						timelineQuery.asString(),
						null,
						events,
						events.stream()
								.filter(e -> e instanceof TimelineSpan)
								.map(e -> ((TimelineSpan) e).getDuration())
								.reduce(Duration.ZERO, Duration::plus)));
	}

	public Mono<TimelineSpan> repeatLastSpan(Instant time, LocalDate day, Ledger ledger) {
		return findActiveSpan(ledger)
				.flatMap(_ -> Mono.<TimelineSpan>error(new ActiveSpanExistsException()))
				.switchIfEmpty(Mono.defer(() -> doRepeatLastSpan(time, day, ledger)));
	}

	@NotNull
	private Mono<TimelineSpan> doRepeatLastSpan(Instant time, LocalDate day, Ledger ledger) {
		return spanRepository.findAll(timelineSpan.ledger.eq(ledger.getId()), timelineSpan.time.desc())
				.take(1)
				.singleOrEmpty()
				.switchIfEmpty(Mono.error(new NoPreviousSpanException()))
				.flatMap(last -> startSpan(time, day, last.getDescription(), ledger))
				.map(StartSpanInfo::span);
	}

}
