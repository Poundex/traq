package net.poundex.traq.timeline.query;

import lombok.experimental.StandardException;

@StandardException
public class QueryException extends RuntimeException {
}
