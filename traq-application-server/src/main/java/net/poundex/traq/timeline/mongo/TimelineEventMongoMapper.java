package net.poundex.traq.timeline.mongo;

import net.poundex.autoconverters.convert.Abstract;
import net.poundex.autoconverters.convert.Converters;
import net.poundex.traq.timeline.TimelineEvent;
import net.poundex.traq.timeline.TimelinePoint;
import net.poundex.traq.timeline.TimelineSpan;

@Converters
interface TimelineEventMongoMapper {
    @Abstract
    TimelineEvent toDomain(MongoTimelineEvent timelineEvent);
    TimelinePoint toDomain(MongoTimelinePoint timelinePoint);
    TimelineSpan toDomain(MongoTimelineSpan timelineSpan);
    
    @Abstract
    MongoTimelineEvent toDbo(TimelineEvent timelineEvent);
    MongoTimelinePoint toDbo(TimelinePoint timelinePoint);
    MongoTimelineSpan toDbo(TimelineSpan timelineSpan);
}