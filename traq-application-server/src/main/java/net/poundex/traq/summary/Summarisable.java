package net.poundex.traq.summary;

import net.poundex.traq.timeline.TimelineEvent;

import java.util.List;

public interface Summarisable {
	List<TimelineEvent> getEvents();
}
