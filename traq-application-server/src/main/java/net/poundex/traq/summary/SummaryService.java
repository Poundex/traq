package net.poundex.traq.summary;

import lombok.RequiredArgsConstructor;
import net.poundex.traq.marker.BaseProperty;
import net.poundex.traq.marker.Markable;
import net.poundex.traq.marker.Marker;
import net.poundex.traq.marker.MarkerService;
import net.poundex.traq.marker.Property;
import net.poundex.traq.timeline.TimelinePoint;
import net.poundex.traq.timeline.TimelineSpan;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class SummaryService {
	
	private final MarkerService markerService;

	public Map<Marker, Integer> getMarkerCount(Summarisable summarisable, Map<String, ? extends Marker> markerUniverse) {
		return doGetMarkerCount(summarisable, getDirectMarkers(markerUniverse));
	}

	public Map<Marker, Integer> getEffectiveMarkerCount(Summarisable summarisable, boolean excludeDirect, Map<String, ? extends Marker> markerUniverse) {
		return doGetMarkerCount(summarisable, getEffectiveMarkers(excludeDirect, markerUniverse));
	}

	private Map<Marker, Integer> doGetMarkerCount(Summarisable summarisable, Function<Markable, Stream<Marker>> getMarkersFn) {
		return summarisable.getEvents().stream()
				.filter(it -> it instanceof TimelinePoint)
				.flatMap(getMarkersFn)
				.collect(Collectors.groupingBy(m -> m, Collectors.summingInt(ignored -> 1)));
	}

	public Map<BaseProperty, Set<String>> getPropertyValues(Summarisable summarisable, Map<String, ? extends Marker> markerUniverse) {
		return doGetPropertyValues(summarisable, getDirectMarkers(markerUniverse), markerUniverse);
	}

	public Map<BaseProperty, Set<String>> getEffectivePropertyValues(Summarisable summarisable, boolean excludeDirect, Map<String, ? extends Marker> markerUniverse) {
		return doGetPropertyValues(summarisable, getEffectiveMarkers(excludeDirect, markerUniverse), markerUniverse);
	}

	private Map<BaseProperty, Set<String>> doGetPropertyValues(Summarisable summarisable, Function<Markable, Stream<Marker>> getMarkersFn, Map<String, ? extends Marker> markerUniverse) {
		return summarisable.getEvents().stream()
				.flatMap(getMarkersFn)
				.filter(m -> m instanceof Property)
				.map(m -> (Property) m)
				.collect(Collectors.collectingAndThen(
						Collectors.groupingBy(
								Marker::getName,
								Collectors.mapping(Property::getValue, Collectors.toSet())),
						nameToValues -> nameToValues.entrySet().stream()
								.collect(Collectors.toMap(kv -> markerUniverse.values().stream()
												.filter(m -> m instanceof BaseProperty bp && bp.getName().equals(kv.getKey()))
												.map(m -> (BaseProperty) m)
												.findFirst()
												.orElseThrow(),
										Map.Entry::getValue))));
	}

	public Map<Marker, Duration> getMarkerDurations(Summarisable summarisable, Map<String, ? extends Marker> markerUniverse) {
		return doGetMarkerDurations(summarisable, getDirectMarkers(markerUniverse));
	}

	public Map<Marker, Duration> getEffectiveMarkerDurations(Summarisable summarisable, boolean excludeDirect, Map<String, ? extends Marker> markerUniverse) {
		return doGetMarkerDurations(summarisable, getEffectiveMarkers(excludeDirect, markerUniverse));
	}

	private Map<Marker, Duration> doGetMarkerDurations(Summarisable summarisable, Function<Markable, Stream<Marker>> getMarkersFn) {
		return summarisable.getEvents().stream()
				.filter(it -> it instanceof TimelineSpan)
				.map(s -> (TimelineSpan) s)
				.flatMap(s -> getMarkersFn.apply(s)
						.filter(m -> ! (m instanceof BaseProperty))
						.map(m -> Map.entry(m, s.getDuration())))
				.collect(Collectors.groupingBy(Map.Entry::getKey,
						Collectors.reducing(Duration.ZERO, Map.Entry::getValue, Duration::plus)));
	}

	private static Function<Markable, Stream<Marker>> getDirectMarkers(Map<String, ? extends Marker> markerUniverse) {
		return markable -> markable.getMarkers().stream().map(markerUniverse::get);
	}

	private Function<Markable, Stream<Marker>> getEffectiveMarkers(boolean excludeDirect, Map<String, ? extends Marker> markerUniverse) {
		return markable -> markerService.getEffectiveMarkers(markable, excludeDirect, markerUniverse).stream();
	}
}