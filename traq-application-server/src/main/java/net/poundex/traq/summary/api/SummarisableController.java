package net.poundex.traq.summary.api;

import graphql.GraphQLContext;
import lombok.RequiredArgsConstructor;
import net.poundex.traq.marker.BaseProperty;
import net.poundex.traq.marker.Marker;
import net.poundex.traq.marker.MarkerDto;
import net.poundex.traq.marker.api.MarkerApiConverters;
import net.poundex.traq.summary.MarkerCountDto;
import net.poundex.traq.summary.MarkerDurationDto;
import net.poundex.traq.summary.MarkerPropertyValuesDto;
import net.poundex.traq.summary.MarkerSummaryDataDto;
import net.poundex.traq.summary.MarkerSummaryDto;
import net.poundex.traq.summary.Summarisable;
import net.poundex.traq.summary.SummaryService;
import net.poundex.xmachinery.spring.graphql.batch.BatchLoader;
import net.poundex.xmachinery.spring.graphql.batch.ObjectLoader;
import net.poundex.xmachinery.spring.graphql.batch.RegisterBatchLoaders;
import net.poundex.xmachinery.spring.graphql.batch.UniverseLoader;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.ContextValue;
import org.springframework.graphql.data.method.annotation.SchemaMapping;
import org.springframework.stereotype.Controller;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.Map;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

@Controller
@RequiredArgsConstructor
@RegisterBatchLoaders
class SummarisableController {
	
	private final SummaryService summaryService;
	private final MarkerApiConverters markerConverters;
	
	record MarkerSummaryContext(
			Summarisable summarisable,
			Mono<? extends Map<String, ? extends Marker>> markerUniverse,
			BiFunction<Summarisable, Map<String, ? extends Marker>, Map<Marker, Integer>> countFn,
			BiFunction<Summarisable, Map<String, ? extends Marker>, Map<BaseProperty, Set<String>>> propertyValuesFn,
			BiFunction<Summarisable, Map<String, ? extends Marker>, Map<Marker, Duration>> durationFn) {}
	
	@SchemaMapping(field = "markerSummary", typeName = "Summarisable")
	public Mono<MarkerSummaryDto> markerSummary(
			Summarisable summarisable, 
			ObjectLoader<String, ? extends Marker> markersLoader, 
			GraphQLContext ctx) {
		
		ctx.put("markerSummaryContext", new MarkerSummaryContext(
				summarisable,
				markersLoader.loadMany(summarisable.getEvents().stream()
								.flatMap(e -> e.getMarkers().stream())
								.collect(Collectors.toSet()))
						.flatMapIterable(t -> t)
						.collect(Collectors.toMap(Marker::getId, m -> m))
						.cache(),
				summaryService::getMarkerCount,
				summaryService::getPropertyValues,
				summaryService::getMarkerDurations));

		return Mono.just(new MarkerSummaryDto(null, null, null));
	}

	@SchemaMapping(field = "effectiveMarkerSummary", typeName = "Summarisable")
	public Mono<MarkerSummaryDto> effectiveMarkerSummary(
			Summarisable summarisable,
			@Argument Boolean excludeDirect,
			UniverseLoader<String, ? extends Marker> markersLoader,
			GraphQLContext ctx) {
		
		boolean doExcludeDirect = excludeDirect != null && excludeDirect;

		ctx.put("markerSummaryContext", new MarkerSummaryContext(
				summarisable,
				markersLoader.load().cache(),
				(szl, mu) -> summaryService.getEffectiveMarkerCount(szl, doExcludeDirect, mu),
				(szl, mu) -> summaryService.getEffectivePropertyValues(szl, doExcludeDirect, mu),
				(szl, mu) -> summaryService.getEffectiveMarkerDurations(szl, doExcludeDirect, mu)));

		return Mono.just(new MarkerSummaryDto(null, null, null));
	}
	
	@SchemaMapping(field = "count", typeName = "MarkerSummary")
	public Mono<Set<MarkerCountDto>> count(
			@ContextValue MarkerSummaryContext markerSummaryContext, 
			ObjectLoader<Summarisable, Set<MarkerCountDto>> loader) {
		
		return loader.load(markerSummaryContext.summarisable());
	}
	
	@SchemaMapping(field = "propertyValues", typeName = "MarkerSummary")
	public Mono<Set<MarkerPropertyValuesDto>> propertyValues(
			@ContextValue MarkerSummaryContext markerSummaryContext,
			ObjectLoader<Summarisable, Set<MarkerPropertyValuesDto>> loader) {

		return loader.load(markerSummaryContext.summarisable());
	}
	
	@SchemaMapping(field = "duration", typeName = "MarkerSummary")
	public Mono<Set<MarkerDurationDto>> duration(
			@ContextValue MarkerSummaryContext markerSummaryContext,
			ObjectLoader<Summarisable, Set<MarkerDurationDto>> loader) {

		return loader.load(markerSummaryContext.summarisable());
	}
	
	@BatchLoader
	Mono<Map<Summarisable, Set<MarkerCountDto>>> countSummaryLoader(
			Set<Summarisable> summarisables,
			@ContextValue MarkerSummaryContext markerSummaryContext) {

		return summarise(
				summarisables,
				markerSummaryContext.markerUniverse(),
				markerSummaryContext.countFn(),
				MarkerCountDto::new, 
				markerConverters::toDto);
	}

	@BatchLoader
	Mono<Map<Summarisable, Set<MarkerPropertyValuesDto>>> propertyValuesLoader(
			Set<Summarisable> summarisables,
			@ContextValue MarkerSummaryContext markerSummaryContext) {

		return this.summarise(
				summarisables,
				markerSummaryContext.markerUniverse(),
				markerSummaryContext.propertyValuesFn(),
				MarkerPropertyValuesDto::new, 
				markerConverters::toDto);
	}

	@BatchLoader
	Mono<Map<Summarisable, Set<MarkerDurationDto>>> markerDurations(
			Set<Summarisable> summarisables,
			@ContextValue MarkerSummaryContext markerSummaryContext) {

		return this.summarise(
				summarisables, 
				markerSummaryContext.markerUniverse(),
				markerSummaryContext.durationFn(),
				MarkerDurationDto::new, 
				markerConverters::toDto);
	}
	
	private <V, D extends MarkerSummaryDataDto<V>, MD extends MarkerDto, M extends Marker> Mono<Map<Summarisable, Set<D>>> 
	summarise(
			Set<Summarisable> summarisables,
			Mono<? extends Map<String, ? extends Marker>> markerUniverse,
			BiFunction<Summarisable, Map<String, ? extends Marker>, Map<M, V>> fn,
			BiFunction<MD, V, D> dataDtoFn,
			Function<M, MD> markerDtoFn) {

		return Flux.fromIterable(summarisables)
				.flatMap(szl -> markerUniverse
						.map(mu -> Map.entry(szl, mapToTupleSet(fn.apply(szl, mu),
								(k, v) -> dataDtoFn.apply(markerDtoFn.apply(k), v)))))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
	}
	
	private static <K, V, T> Set<T> mapToTupleSet(Map<K, V> map, BiFunction<K, V, T> tupleFactory) {
		return map.entrySet().stream()
				.map(kv -> tupleFactory.apply(kv.getKey(), kv.getValue()))
				.collect(Collectors.toSet());
	}
}
