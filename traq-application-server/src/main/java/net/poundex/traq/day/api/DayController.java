package net.poundex.traq.day.api;

import graphql.schema.DataFetchingFieldSelectionSet;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.poundex.traq.day.Day;
import net.poundex.traq.day.DayService;
import net.poundex.traq.ledger.Ledger;
import net.poundex.traq.timeline.DayGuesser;
import net.poundex.xmachinery.spring.graphql.batch.BatchLoader;
import net.poundex.xmachinery.spring.graphql.batch.LoadContextObjects;
import net.poundex.xmachinery.spring.graphql.batch.LoadedContextObject;
import net.poundex.xmachinery.spring.graphql.batch.ObjectLoader;
import net.poundex.xmachinery.spring.graphql.batch.RegisterBatchLoaders;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.graphql.data.method.annotation.SchemaMapping;
import org.springframework.stereotype.Controller;
import reactor.core.publisher.Mono;

import java.time.LocalDate;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static net.poundex.xmachinery.spring.graphql.batch.BatchLoadingHelper.loadIfNecessary;

@Controller
@RegisterBatchLoaders
@RequiredArgsConstructor
@SchemaMapping(typeName = "Day")
@LoadContextObjects
@Slf4j
class DayController {
	
	private final DayService dayService;
	private final DayGuesser dayGuesser;
	
	@QueryMapping
	@LoadedContextObject(Ledger.class)
	public Mono<Day> day(
			@Argument LocalDate day,
			DataFetchingFieldSelectionSet fields,
			ObjectLoader<LocalDate, Day> dayLoader) {
		
		return dayGuesser.getCurrentDayBestGuess(day)
				.flatMap(guessed -> loadIfNecessary("day", guessed, fields, dayLoader)
						.andGet()
						.orElse(() -> Day.builder().day(guessed).build()));
	}
	
	@BatchLoader
	Mono<Map<LocalDate, Day>> dayLoader(Set<LocalDate> days, Mono<Ledger> ledger) {
		return ledger
				.flatMapMany(l -> dayService.getDays(l, days))
				.collect(Collectors.toMap(Day::getDay, d -> d));
	}
}
