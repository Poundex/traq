package net.poundex.traq.day;

import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import net.poundex.traq.ledger.Ledger;
import net.poundex.traq.summary.Summarisable;
import net.poundex.traq.timeline.TimelineEvent;
import net.poundex.traq.timeline.TimelineSpan;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Stream;

@RequiredArgsConstructor
@Data
@Builder
public class Day implements Summarisable {
	private final LocalDate day;
	private final Ledger ledger;
	private final List<TimelineEvent> events;

	public Instant getStartTime() {
		return getSpans().findFirst()
				.map(TimelineEvent::getTime)
				.orElse(null);
	}
	
	public Instant getEndTime() {
		List<TimelineSpan> spans = getSpans().toList();
		if(spans.isEmpty()) return null;
		return spans.get(spans.size() - 1).getEndTime();
	}
	
	public Duration getDuration() {
		return getDayDuration();
	}
	
	public Instant getProjectedEndTime() {
		if(ledger.getTargetDuration() == null)
			return null;

		Duration remaining = ledger.getTargetDuration().minus(getDuration());
		return remaining.isPositive()
				? Instant.now().plus(remaining) 
				: null;
	}
	
	public Duration getLength() {
		List<TimelineSpan> spanList = getSpans().toList();
		if(spanList.isEmpty())
			return Duration.ZERO;
		
		return Duration.between(spanList.get(0).getTime(), spanList.get(spanList.size() - 1).getLatestTime());
	}
	
	private Stream<TimelineSpan> getSpans() {
		return events.stream()
				.filter(it -> it instanceof TimelineSpan)
				.map(it -> (TimelineSpan) it);
	}
	
	private Duration getDayDuration() {
		return getSpans()
				.filter(span -> ! span.getMarkers().contains(ledger.getNoAccountMarker()))
				.reduce(Duration.ZERO, (d, s) -> d.plus(s.getDuration()), Duration::plus);
	}
}
