package net.poundex.traq.day;

import lombok.RequiredArgsConstructor;
import net.poundex.traq.ledger.Ledger;
import net.poundex.traq.timeline.EventRepository;
import net.poundex.traq.timeline.TimelineEvent;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static net.poundex.traq.timeline.QTimelineEvent.timelineEvent;

@Service
@RequiredArgsConstructor
public class DayService {
	
	private final EventRepository eventRepository;

	public Flux<Day> getDays(Ledger ledger, Set<LocalDate> days) {
		return eventRepository.findAll(
						timelineEvent.day.in(days).and(timelineEvent.ledger.eq(ledger.getId())),
						timelineEvent.time.asc())
				.collect(Collectors.groupingBy(
						TimelineEvent::getDay,
						Collectors.toList()))
				.flatMapIterable(Map::entrySet)
				.switchIfEmpty(Flux.defer(() -> 
						Flux.fromIterable(days).map(d -> Map.entry(d, Collections.emptyList()))))
				.map(dayToEvents -> new Day(dayToEvents.getKey(), ledger, dayToEvents.getValue()));
	}
}
