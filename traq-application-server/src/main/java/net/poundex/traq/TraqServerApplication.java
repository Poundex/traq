package net.poundex.traq;

import de.flapdoodle.embed.mongo.spring.autoconfigure.EmbeddedMongoAutoConfiguration;
import net.poundex.autoconverters.convert.EnableAutoConverters;
import net.poundex.autoconverters.repositories.EnableConvertingRepositories;
import net.poundex.xmachinery.fio.io.DefaultIo;
import net.poundex.xmachinery.fio.io.IO;
import net.poundex.xmachinery.spring.querydsl.predicatemapper.EnableQueryDslPredicateMapping;
import net.poundex.xmachinery.spring.typedmongo.EnableTypeAwareMongoRepositories;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.EnableLoadTimeWeaving;

@SpringBootApplication(exclude = EmbeddedMongoAutoConfiguration.class)
@EnableAutoConverters
@EnableConvertingRepositories
@EnableQueryDslPredicateMapping
@ConfigurationPropertiesScan(basePackages = "net.poundex.traq")
@EnableTypeAwareMongoRepositories
@EnableLoadTimeWeaving(aspectjWeaving = EnableLoadTimeWeaving.AspectJWeaving.ENABLED)
public class TraqServerApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(TraqServerApplication.class, args);
	}
	
	@Bean
	public IO io() {
		return new DefaultIo();
	}
}
