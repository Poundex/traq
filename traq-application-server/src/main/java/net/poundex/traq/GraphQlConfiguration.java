package net.poundex.traq;

import lombok.extern.slf4j.Slf4j;
import net.poundex.xmachinery.spring.graphql.batch.BatchLoadingConfiguration;
import net.poundex.xmachinery.spring.graphql.error.ExceptionResolverConfiguration;
import net.poundex.xmachinery.spring.graphql.handlerfix.InterfaceHandlerFixConfiguration;
import org.springframework.boot.autoconfigure.graphql.GraphQlSourceBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.graphql.execution.ClassNameTypeResolver;

@Configuration(proxyBeanMethods = false)
@Slf4j
@Import({BatchLoadingConfiguration.class, ExceptionResolverConfiguration.class, InterfaceHandlerFixConfiguration.class})
class GraphQlConfiguration {

	@Bean
	public GraphQlSourceBuilderCustomizer sourceBuilderCustomizer() {
		return builder -> {
			ClassNameTypeResolver resolver = new ClassNameTypeResolver();
			resolver.setClassNameExtractor(klass -> klass.getSimpleName().replace("Dto", ""));
			builder.defaultTypeResolver(resolver);
		};
	}
}
