package net.poundex.traq.marker.api;

import net.poundex.autoconverters.convert.Abstract;
import net.poundex.autoconverters.convert.Converters;
import net.poundex.autoconverters.convert.IgnoreTargetProperty;
import net.poundex.traq.marker.BaseProperty;
import net.poundex.traq.marker.BasePropertyDto;
import net.poundex.traq.marker.Marker;
import net.poundex.traq.marker.MarkerDto;
import net.poundex.traq.marker.Mention;
import net.poundex.traq.marker.MentionDto;
import net.poundex.traq.marker.Property;
import net.poundex.traq.marker.PropertyDto;
import net.poundex.traq.marker.Tag;
import net.poundex.traq.marker.TagDto;

@Converters
public interface MarkerApiConverters {
	
	@Abstract
	@IgnoreTargetProperty("markers")
	@IgnoreTargetProperty("ledger")
	MarkerDto toDto(Marker marker);
	
	TagDto toDto(Tag tag);
	MentionDto toDto(Mention mention);
	BasePropertyDto toDto(BaseProperty baseProperty);
	PropertyDto toDto(Property property);
}
