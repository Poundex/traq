package net.poundex.traq.marker.realiser;

import net.poundex.traq.marker.Marker;
import org.reactivestreams.Publisher;

interface MarkerRealiser {
	Publisher<? extends Marker> realiseMarkers(String ledgerId, String token);
}
