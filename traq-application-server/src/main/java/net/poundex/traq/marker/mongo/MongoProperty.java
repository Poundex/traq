package net.poundex.traq.marker.mongo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.bson.types.ObjectId;

import java.util.Set;

@Data
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@NoArgsConstructor
class MongoProperty extends MongoMarker {
	
	private String value;
	
	public MongoProperty(ObjectId id, String name, String title, Set<String> markers, String value, String ledger) {
		super(id, name, name.toLowerCase(), title, markers, ledger);
		this.value = value;
	}
}
