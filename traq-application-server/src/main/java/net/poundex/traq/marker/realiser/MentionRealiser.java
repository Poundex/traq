package net.poundex.traq.marker.realiser;

import lombok.RequiredArgsConstructor;
import net.poundex.traq.marker.Marker;
import net.poundex.traq.marker.Mention;
import net.poundex.traq.marker.MentionRepository;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static net.poundex.traq.marker.QMention.mention;

@RequiredArgsConstructor
@Component
class MentionRealiser extends AbstractMarkerRealiser {
	
	private static final Pattern pattern = Pattern.compile("\\B@[\\w-]+");
	
	private final MentionRepository mentionRepository;

	@Override
	public Flux<Mention> realiseMarkers(String ledgerId, String input) {
		Set<String> foundMarkers = findMarkers(input, pattern);

		if (foundMarkers.isEmpty())
			return Flux.empty();

		return getOrCreateAllFoundMarkers(
				ledgerId,
				mentionRepository,
				mention.nameLower.in(foundMarkers.stream().map(String::toLowerCase).collect(Collectors.toSet())),
				foundMarkers,
				Marker::getNameLower,
				n -> Mention.builder().name(n).ledger(ledgerId).build());
	}
}
