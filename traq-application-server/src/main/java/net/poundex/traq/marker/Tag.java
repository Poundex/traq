package net.poundex.traq.marker;

import com.querydsl.core.annotations.QueryEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import java.util.Set;

@QueryEntity
@SuperBuilder(toBuilder = true)
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class Tag extends Marker {
	public Tag(String id, String name, String title, Set<String> markers, String ledger) {
		super(id, name, title, markers, ledger);
	}

	@Override
	public String asString() {
		return "+" + getName();
	}
}
