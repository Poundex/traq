package net.poundex.traq.marker.api;

import graphql.GraphQLContext;
import lombok.RequiredArgsConstructor;
import net.poundex.traq.ledger.Ledger;
import net.poundex.traq.marker.Marker;
import net.poundex.traq.marker.MarkerRepository;
import net.poundex.traq.marker.MarkerService;
import net.poundex.xmachinery.spring.graphql.batch.LoadContextObjects;
import net.poundex.xmachinery.spring.graphql.batch.LoadedContextObject;
import net.poundex.xmachinery.spring.graphql.batch.ObjectLoader;
import net.poundex.xmachinery.spring.graphql.error.NotFoundException;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.ContextValue;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.graphql.data.method.annotation.SchemaMapping;
import org.springframework.stereotype.Controller;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

import static net.poundex.traq.api.ControllerUtils.required;
import static net.poundex.traq.marker.QMarker.marker;

@Controller
@RequiredArgsConstructor
@SchemaMapping(typeName = "Marker")
@LoadContextObjects
class MarkerController {

	private final MarkerRepository markerRepository;
	private final MarkerService markerService;

	@MutationMapping
	@LoadedContextObject(Ledger.class)
	public Mono<? extends Marker> modifyMarker(
			@ContextValue Mono<Ledger> ledger, 
			@Argument String marker, 
			@Argument String title, 
			@Argument List<String> markers) {
		
		return ledger.flatMap(l -> markerService.modifyMarker(l, marker, title, markers));
	}

	@QueryMapping
	@LoadedContextObject(Ledger.class)
	public Flux<? extends Marker> markers(@ContextValue Mono<Ledger> ledger) {
		return ledger.flatMapMany(l -> markerRepository.findAll(marker.ledger.eq(l.getId())));
	}

	@QueryMapping
	public Mono<? extends Marker> marker(
			@Argument String id,
			ObjectLoader<String, Marker> markerLoader,
			ObjectLoader<String, Ledger> ledgerLoader,
			GraphQLContext context) {
		
		return markerLoader.load(id)
				.doOnNext(m -> context.put("ledger", ledgerLoader.load(m.getLedger())))
				.switchIfEmpty(Mono.error(() -> new NotFoundException(Marker.class, id)));
	}

	@MutationMapping
	public Mono<? extends Marker> updateMarker(
			@Argument String marker,
			@Argument String title,
			ObjectLoader<String, Marker> loader) {

		return required(loader, Marker.class, marker)
				.flatMap(existing -> markerService.updateMarker(existing, title));
	}

	@MutationMapping
	public Mono<? extends Marker> setMarkers(
			@Argument String marker,
			@Argument List<String> markers,
			ObjectLoader<String, Marker> markerLoader) {
		return required(markerLoader, Marker.class, marker)
				.flatMap(m -> markerService.setMarkers(m, markers));
	}
}
