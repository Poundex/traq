package net.poundex.traq.marker;

import org.springframework.data.querydsl.ReactiveQuerydslPredicateExecutor;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Component;

@NoRepositoryBean
@Component
public interface MentionRepository extends
		ReactiveCrudRepository<Mention, String>,
        ReactiveQuerydslPredicateExecutor<Mention> {
}
