package net.poundex.traq.marker.mongo;

import net.poundex.autoconverters.repositories.DaoFor;
import net.poundex.traq.marker.BasePropertyRepository;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.data.querydsl.ReactiveQuerydslPredicateExecutor;

@DaoFor(BasePropertyRepository.class)
interface MongoBasePropertyRepository extends 
        ReactiveMongoRepository<MongoBaseProperty, ObjectId>,
        ReactiveQuerydslPredicateExecutor<MongoBaseProperty> {
}
