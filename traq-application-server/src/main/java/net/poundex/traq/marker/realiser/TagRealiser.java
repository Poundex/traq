package net.poundex.traq.marker.realiser;

import lombok.RequiredArgsConstructor;
import net.poundex.traq.marker.Marker;
import net.poundex.traq.marker.Tag;
import net.poundex.traq.marker.TagRepository;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static net.poundex.traq.marker.QTag.tag;

@RequiredArgsConstructor
@Component
class TagRealiser extends AbstractMarkerRealiser {
	
	private static final Pattern pattern = Pattern.compile("\\B\\+[\\w-]+");
	
	private final TagRepository tagRepository;

	@Override
	public Flux<Tag> realiseMarkers(String ledgerId, String input) {
		Set<String> foundMarkers = findMarkers(input, pattern);

		if (foundMarkers.isEmpty())
			return Flux.empty();

		return getOrCreateAllFoundMarkers(
				ledgerId,
				tagRepository,
				tag.nameLower.in(foundMarkers.stream().map(String::toLowerCase).collect(Collectors.toSet())),
				foundMarkers,
				Marker::getNameLower,
				n -> Tag.builder().name(n).ledger(ledgerId).build());
	}
}
