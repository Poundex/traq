package net.poundex.traq.marker.mongo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.bson.types.ObjectId;

import java.util.Set;

@Data
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@NoArgsConstructor
class MongoBaseProperty extends MongoMarker {
	public MongoBaseProperty(ObjectId id, String name, String title, Set<String> markers, String ledger) {
		super(id, name, name.toLowerCase(), title, markers, ledger);
	}
}
