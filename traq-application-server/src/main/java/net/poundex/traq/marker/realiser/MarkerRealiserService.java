package net.poundex.traq.marker.realiser;

import lombok.RequiredArgsConstructor;
import net.poundex.traq.marker.Marker;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import java.util.Collection;

@Service
@RequiredArgsConstructor
public class MarkerRealiserService {
	
	private final ObjectProvider<MarkerRealiser> realisers;

	public Flux<? extends Marker> realiseMarkersInString(String ledgerId, String input) {
		return Flux.fromStream(realisers.stream())
				.flatMap(r -> r.realiseMarkers(ledgerId, input));
	}

	public Flux<? extends Marker> realiseMarkers(String ledgerId, Collection<String> strings) {
		return realiseMarkersInString(ledgerId, String.join(" ", strings));
	}
}
