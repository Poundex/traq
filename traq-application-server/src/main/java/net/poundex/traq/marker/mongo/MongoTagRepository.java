package net.poundex.traq.marker.mongo;

import net.poundex.autoconverters.repositories.DaoFor;
import net.poundex.traq.marker.TagRepository;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.data.querydsl.ReactiveQuerydslPredicateExecutor;

@DaoFor(TagRepository.class)
interface MongoTagRepository extends 
        ReactiveMongoRepository<MongoTag, ObjectId>,
        ReactiveQuerydslPredicateExecutor<MongoTag> {
}
