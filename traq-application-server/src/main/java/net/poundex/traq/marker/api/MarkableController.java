package net.poundex.traq.marker.api;

import lombok.RequiredArgsConstructor;
import net.poundex.traq.ledger.Ledger;
import net.poundex.traq.marker.Markable;
import net.poundex.traq.marker.Marker;
import net.poundex.traq.marker.MarkerRepository;
import net.poundex.traq.marker.MarkerService;
import net.poundex.traq.marker.QMarker;
import net.poundex.xmachinery.spring.graphql.batch.BatchLoader;
import net.poundex.xmachinery.spring.graphql.batch.ObjectLoader;
import net.poundex.xmachinery.spring.graphql.batch.RegisterBatchLoaders;
import net.poundex.xmachinery.spring.graphql.batch.UniverseLoader;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.SchemaMapping;
import org.springframework.stereotype.Controller;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Controller
@RequiredArgsConstructor
@RegisterBatchLoaders
class MarkableController {
	
	private final MarkerRepository markerRepository;
	private final MarkerService markerService;
	
	@SchemaMapping(field = "markers", typeName = "Markable")
	public Mono<List<? extends Marker>> markers(
			Markable markable,
			ObjectLoader<String, Marker> loader) {
		return loader.loadMany(markable.getMarkers());
	}

	@BatchLoader
	Mono<Map<String, ? extends Marker>> markersLoader(Set<String> ids) {
		return markerRepository.findAllById(ids)
				.collect(Collectors.toMap(Marker::getId, m -> m));
	}

	@SchemaMapping(field = "effectiveMarkers", typeName = "Markable")
	public Mono<Set<? extends Marker>> effectiveMarkers(
			Markable markable, 
			@Argument Boolean excludeDirect,
			UniverseLoader<String, Marker> markersUniverseLoader) {
		return markersUniverseLoader.load()
				.map(markerUniverse -> markerService.getEffectiveMarkers(
						markable, 
						Optional.ofNullable(excludeDirect).orElse(false),
						markerUniverse));
	}

	@BatchLoader
	Mono<Map<String, ? extends Marker>> markerUniverseLoader(Mono<Ledger> ledger) {
		return ledger.flatMap(l ->
				markerRepository.findAll(QMarker.marker.ledger.eq(l.getId()))
						.collect(Collectors.toMap(Marker::getId, m -> m)));
	}
}
