package net.poundex.traq.marker.mongo;

import net.poundex.autoconverters.convert.Abstract;
import net.poundex.autoconverters.convert.Converters;
import net.poundex.traq.marker.BaseProperty;
import net.poundex.traq.marker.Marker;
import net.poundex.traq.marker.Mention;
import net.poundex.traq.marker.Property;
import net.poundex.traq.marker.Tag;

@Converters
interface MarkerMongoMapper {
    @Abstract
    Marker toDomain(MongoMarker marker);
    Tag toDomain(MongoTag tag);
    Mention toDomain(MongoMention mention);
    BaseProperty toDomain(MongoBaseProperty baseProperty);
    Property toDomain(MongoProperty property);
    
    @Abstract
    MongoMarker toDbo(Marker marker);
    MongoTag toDbo(Tag tag);
    MongoMention toDbo(Mention mention);
    MongoBaseProperty toDbo(BaseProperty baseProperty);
    MongoProperty toDbo(Property property);
}