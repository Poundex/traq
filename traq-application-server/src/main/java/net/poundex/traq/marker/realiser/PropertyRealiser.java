package net.poundex.traq.marker.realiser;

import com.querydsl.core.types.Predicate;
import lombok.RequiredArgsConstructor;
import net.poundex.traq.marker.BaseProperty;
import net.poundex.traq.marker.BasePropertyRepository;
import net.poundex.traq.marker.Marker;
import net.poundex.traq.marker.Property;
import net.poundex.traq.marker.PropertyRepository;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Gatherer;

import static com.querydsl.core.types.ExpressionUtils.anyOf;
import static net.poundex.traq.marker.QBaseProperty.baseProperty;
import static net.poundex.traq.marker.QProperty.property;

@RequiredArgsConstructor
@Component
class PropertyRealiser extends AbstractMarkerRealiser {
	
	private static final Pattern pattern = Pattern.compile("\\B:[\\w-]+=[\\w-]+");
	
	private final BasePropertyRepository basePropertyRepository;
	private final PropertyRepository propertyRepository;

	@Override
	public Flux<? extends Marker> realiseMarkers(String ledgerId, String input) {

		record PropertyKV(String key, String value) {

			PropertyKV asKey() {
				return new PropertyKV(key.toLowerCase(), value.toLowerCase());
			}

			@Override
			public boolean equals(Object other) {
				if (! (other instanceof PropertyKV(String otherKey, String otherValue))) return false;
				return key().equalsIgnoreCase(otherKey)
						&& value().equalsIgnoreCase(otherValue);
			}
		}

		Set<PropertyKV> foundMarkers = findMarkers(input, pattern).stream()
				.map(s -> s.split("=", 2))
				.map(kv -> new PropertyKV(kv[0], kv[1]))
				.collect(Collectors.toSet());

		if (foundMarkers.isEmpty())
			return Flux.empty();

		Set<String> propertyNames = foundMarkers.stream()
				.map(PropertyKV::key)
				.collect(Collectors.toSet());

		Flux<BaseProperty> baseProperties = getOrCreateAllFoundMarkers(
				ledgerId,
				basePropertyRepository,
				baseProperty.nameLower.in(propertyNames.stream().map(String::toLowerCase).collect(Collectors.toSet())),
				propertyNames,
				Marker::getNameLower,
				n -> BaseProperty.builder().name(n).ledger(ledgerId).build());

		Flux<Property> properties = getOrCreateAllFoundMarkers(
				ledgerId,
				propertyRepository,
				anyOf(foundMarkers.stream()
						.gather(Gatherer.of(
								HashSet::new,
								(state, item, sink) -> {
									if (state.add(item.asKey())) sink.push(item);
									return true;
								},
								(left, right) -> {
									left.addAll(right);
									return left;
								},
								Gatherer.<HashSet<PropertyKV>, PropertyKV>defaultFinisher()))
						.<Predicate>map(m -> property.name.equalsIgnoreCase(m.key())
								.and(property.value.equalsIgnoreCase(m.value())))
						.toList()),
				foundMarkers,
				m -> new PropertyKV(m.getName(), m.getValue()).asKey(),
				desc -> Property.builder().name(desc.key()).value(desc.value()).ledger(ledgerId).build());

		return Flux.concat(baseProperties, properties);
	}
}
