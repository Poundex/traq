package net.poundex.traq.marker;

import java.util.Set;

public interface Markable {
	Set<String> getMarkers();
}
