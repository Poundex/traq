package net.poundex.traq.marker;

import lombok.RequiredArgsConstructor;
import net.poundex.traq.ledger.Ledger;
import net.poundex.traq.marker.realiser.MarkerRealiserService;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import reactor.core.publisher.Mono;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class MarkerService {

	private final MarkerRealiserService markerRealiserService;
	private final MarkerRepository markerRepository;

	public Mono<? extends Marker> modifyMarker(Ledger ledger, String marker, String title, List<String> markers) {
		return markerRealiserService.realiseMarkers(ledger.getId(), markers)
				.map(Marker::getId)
				.collect(Collectors.toSet())
				.flatMap(markerIds -> markerRealiserService.realiseMarkersInString(ledger.getId(), marker)
						.single()
						.flatMap(modified -> doModifyMarker(title, markerIds, modified)));
	}

	private Mono<Marker> doModifyMarker(String title, Set<String> markerIds, Marker modified) {
		Marker.MarkerBuilder<?, ?> b = modified.toBuilder().markers(markerIds);
		if (StringUtils.hasText(title))
			b = b.title(title);
		return markerRepository.save(b.build());
	}

	public Set<Marker> getEffectiveMarkers(Markable markable, boolean excludeDirect, Map<String, ? extends Marker> markerUniverse) {
		Stream<Marker> indirectMarkers = markable.getMarkers().stream()
				.flatMap(m -> getEffectiveMarkers(markerUniverse.get(m), markerUniverse));

		return (excludeDirect
				? indirectMarkers
				: Stream.concat(markable.getMarkers().stream().map(markerUniverse::get), indirectMarkers))
				.collect(Collectors.toSet());
	}

	private Stream<Marker> getEffectiveMarkers(Marker marker, Map<String, ? extends Marker> markersById) {
		return getEffectiveMarkers(marker, markersById, new HashSet<>());
	}
	
	private Stream<Marker> getEffectiveMarkers(Marker marker, Map<String, ? extends Marker> markersById, Set<Marker> seenMutableSet) {
		seenMutableSet.add(marker);
		return Stream.concat(
				marker.getMarkers().stream().map(markersById::get),
				marker.getMarkers().stream()
						.map(markersById::get)
						.filter(m -> ! seenMutableSet.contains(m))
						.flatMap(m -> getEffectiveMarkers(m, markersById, seenMutableSet)));
	}
	
	public Map<Marker, Set<Marker>> getAllRoutes(Map<String, ? extends Marker> markerUniverse) {
		return markerUniverse.values().stream()
				.collect(Collectors.toMap(
						m -> m, 
						m -> getRoutes(m, markerUniverse).collect(Collectors.toSet())));
	}
	
	private static Stream<? extends Marker> getRoutes(Marker marker, Map<String, ? extends Marker> markerUniverse) {
		return Stream.concat(Stream.of(marker), markerUniverse.values().stream()
				.filter(m -> m.getMarkers().contains(marker.getId()))
				.flatMap(m -> Stream.concat(Stream.of(m), getRoutes(m, markerUniverse))));
	}

	public Mono<? extends Marker> updateMarker(Marker existing, String title) {
		return markerRepository.save(existing.toBuilder().title(StringUtils.hasText(title) ? title : null).build());
	}

	public Mono<? extends Marker> setMarkers(Marker marker, List<String> markers) {
		return markerRealiserService.realiseMarkers(marker.getLedger(), markers)
				.map(Marker::getId)
				.collect(Collectors.toSet())
				.flatMap(markerIds ->
						markerRepository.save(marker.toBuilder()
								.markers(markerIds)
								.build()));
	}
}
