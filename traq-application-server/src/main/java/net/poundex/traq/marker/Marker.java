package net.poundex.traq.marker;

import com.querydsl.core.annotations.QueryEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.SuperBuilder;
import net.poundex.traq.TraqObject;
import net.poundex.traq.ledger.LedgerItem;

import java.util.Collections;
import java.util.Set;

@QueryEntity
@AllArgsConstructor
@SuperBuilder(toBuilder = true)
@Data
public abstract class Marker implements TraqObject, Markable, LedgerItem {
	private final String id;
	private final String name;
	private final String title;
	@Builder.Default
	private final Set<String> markers = Collections.emptySet();
	private final String ledger;
	
	public abstract <C extends Marker, B extends MarkerBuilder<C, B>> Marker.MarkerBuilder<C, B> toBuilder();
	public abstract String asString();
	
	public String getNameLower() {
		return name.toLowerCase();
	}
}
