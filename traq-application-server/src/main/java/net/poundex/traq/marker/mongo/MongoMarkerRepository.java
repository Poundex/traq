package net.poundex.traq.marker.mongo;

import net.poundex.autoconverters.repositories.DaoFor;
import net.poundex.traq.marker.MarkerRepository;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.data.querydsl.ReactiveQuerydslPredicateExecutor;

@DaoFor(MarkerRepository.class)
interface MongoMarkerRepository extends 
        ReactiveMongoRepository<MongoMarker, ObjectId>,
        ReactiveQuerydslPredicateExecutor<MongoMarker> {
}
