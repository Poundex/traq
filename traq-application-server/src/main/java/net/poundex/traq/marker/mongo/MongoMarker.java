package net.poundex.traq.marker.mongo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Set;

@Document("markers")
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
abstract class MongoMarker {
    @Id
    private ObjectId id;
    private String name;
    private String nameLower;
    private String title;
    private Set<String> markers;
    private String ledger;
}
