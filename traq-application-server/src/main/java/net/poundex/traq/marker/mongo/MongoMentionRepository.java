package net.poundex.traq.marker.mongo;

import net.poundex.autoconverters.repositories.DaoFor;
import net.poundex.traq.marker.MentionRepository;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.data.querydsl.ReactiveQuerydslPredicateExecutor;

@DaoFor(MentionRepository.class)
interface MongoMentionRepository extends 
        ReactiveMongoRepository<MongoMention, ObjectId>,
        ReactiveQuerydslPredicateExecutor<MongoMention> {
}
