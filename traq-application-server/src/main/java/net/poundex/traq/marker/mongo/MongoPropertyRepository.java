package net.poundex.traq.marker.mongo;

import net.poundex.autoconverters.repositories.DaoFor;
import net.poundex.traq.marker.PropertyRepository;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.data.querydsl.ReactiveQuerydslPredicateExecutor;

@DaoFor(PropertyRepository.class)
interface MongoPropertyRepository extends 
        ReactiveMongoRepository<MongoProperty, ObjectId>,
        ReactiveQuerydslPredicateExecutor<MongoProperty> {
}
