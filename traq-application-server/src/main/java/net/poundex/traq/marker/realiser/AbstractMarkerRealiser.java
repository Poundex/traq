package net.poundex.traq.marker.realiser;

import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Predicate;
import org.springframework.data.querydsl.ReactiveQuerydslPredicateExecutor;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static net.poundex.traq.marker.QMarker.marker;

abstract class AbstractMarkerRealiser implements MarkerRealiser {
	
	protected Set<String> findMarkers(String input, Pattern pattern) {
		return pattern.matcher(input).results()
				.map(MatchResult::group)
				.map(s -> s.substring(1))
				.collect(Collectors.toSet());
	}

	protected <T, K, R extends ReactiveCrudRepository<T, String> & ReactiveQuerydslPredicateExecutor<T>>
	Flux<T> getOrCreateAllFoundMarkers(
			String ledgerId,
			R repository, 
			Predicate predicate, 
			Set<K> foundKeys, 
			Function<T, K> keyExtractor,
			Function<K, T> factory) {

		return repository.findAll(ExpressionUtils.and(predicate, marker.ledger.eq(ledgerId)))
				.collect(Collectors.toSet())
				.flatMapMany(existing -> Flux.concat(
						Flux.fromIterable(existing),
						repository.saveAll(foundKeys
								.stream()
								.map(factory)
								.filter(k ->
										existing.stream().noneMatch(m -> 
										keyExtractor.apply(m).equals(keyExtractor.apply(k))))
								.reduce(new HashSet<>(), (acc, m) ->{
									if(acc.stream().noneMatch(k -> keyExtractor.apply(m).equals(keyExtractor.apply((k)))))
										acc.add(m);
									return acc;
								}, (l, r) -> {
									l.addAll(r);
									return l;
								}))));
	}
}
