package net.poundex.traq.importexport;

import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import net.poundex.traq.ledger.Ledger;
import net.poundex.traq.timeline.TimelineEvent;
import net.poundex.traq.timeline.TimelineService;
import net.poundex.xmachinery.fio.io.IO;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.Reader;
import java.nio.file.Path;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;

@RequiredArgsConstructor
@Service
public class ImportService {
	
	private final IO io;
	private final TimelineService timelineService;

	public Mono<? extends List<? extends TimelineEvent>> importFile(Ledger ledger, Path file, ZoneId zoneId) {
		Reader reader = io.newBufferedReader(file);
		CSVParser parser = Try.of(() -> CSVFormat.RFC4180.builder()
//					.setSkipHeaderRecord(true)
				.setIgnoreEmptyLines(true)
				.build()
				.withFirstRecordAsHeader()
				.parse(reader)).get();

		return Flux.concat(parser.stream()
						.filter(row -> Arrays.stream(row.values()).allMatch(StringUtils::hasText))
						.map(row -> createEvent(row, ledger, zoneId))
						.toList())
				.collectList()
				.doFinally(ignored -> io.close(reader));
	}

	private Mono<? extends TimelineEvent> createEvent(CSVRecord record, Ledger ledger, ZoneId zoneId) {
		LocalDate date = LocalDate.parse(record.get("date"));
		LocalTime startTime = LocalTime.parse(record.get("time"));
		String endTimeOrSigil = record.get("endTime");
		String description = record.get("description");
		Instant time = ZonedDateTime.of(date.atTime(startTime), zoneId).toInstant();

		if (endTimeOrSigil.equals("*"))
			return timelineService.createPoint(time, date, description, ledger);

		LocalTime endTime = LocalTime.parse(endTimeOrSigil);
		return timelineService.createSpan(date, time, ZonedDateTime.of(date.atTime(endTime),
				zoneId).toInstant(), description, ledger);
	}
}
