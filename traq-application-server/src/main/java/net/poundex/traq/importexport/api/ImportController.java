package net.poundex.traq.importexport.api;

import lombok.RequiredArgsConstructor;
import net.poundex.traq.importexport.ImportResultDto;
import net.poundex.traq.importexport.ImportService;
import net.poundex.traq.ledger.Ledger;
import net.poundex.traq.ledger.LedgerRepository;
import net.poundex.xmachinery.fio.io.IO;
import net.poundex.xmachinery.spring.graphql.error.NotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.nio.file.Path;
import java.time.ZoneId;

import static net.poundex.traq.ledger.QLedger.ledger;

@RestController
@RequestMapping("/import")
@RequiredArgsConstructor
class ImportController {
	
	private final IO io;
	private final ImportService importService;
	private final LedgerRepository ledgerRepository;
	
	@PostMapping("/{ledgerName}")
	Mono<ResponseEntity<ImportResultDto>> importFile(@PathVariable("ledgerName") String ledgerName,
	                                                 @RequestPart("importFile") FilePart importFilePart,
	                                                 @RequestParam("zoneId") String zoneId) {
		
		return ledgerRepository.findOne(ledger.name.equalsIgnoreCase(ledgerName))
				.switchIfEmpty(Mono.error(() -> new NotFoundException(Ledger.class, ledgerName)))
				.flatMap(ledger -> {
					Path tmpFile = io.createTempFile("import", ".csv");
					return importFilePart.transferTo(tmpFile)
							.then(Mono.defer(() -> importService.importFile(ledger, tmpFile, ZoneId.of(zoneId))))
							.map(created -> ResponseEntity.ok(new ImportResultDto(created.size())));
				});
	}
}
