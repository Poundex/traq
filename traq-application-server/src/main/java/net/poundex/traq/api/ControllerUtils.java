package net.poundex.traq.api;

import net.poundex.traq.timeline.api.TimelineQueryException;
import net.poundex.traq.timeline.query.QueryException;
import net.poundex.traq.timeline.query.TimelineQuery;
import net.poundex.xmachinery.spring.graphql.batch.ObjectLoader;
import net.poundex.xmachinery.spring.graphql.error.NotFoundException;
import reactor.core.publisher.Mono;

import java.util.function.Supplier;

public class ControllerUtils {
	private ControllerUtils() { }
	
	public static <V> Mono<V> required(Mono<V> source, Class<V> klass, Object key) {
		return source.switchIfEmpty(Mono.error(() -> 
				new NotFoundException(klass, key.toString())));
	}
	
	public static <K, V> Mono<V> required(ObjectLoader<K, V> loader, Class<V> klass, K key) {
		return required(loader.load(key), klass, key);
	}
	
	public static TimelineQuery validQuery(Supplier<TimelineQuery> fn) {
		try {
			return fn.get();
		}
		catch (QueryException qex) {
			throw new TimelineQueryException(qex);
		}
	}
}
