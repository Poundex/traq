package net.poundex.traq;

import de.flapdoodle.embed.mongo.spring.autoconfigure.EmbeddedMongoAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.mongodb.core.convert.MongoCustomConversions;

import java.time.LocalDate;
import java.util.List;

@SuppressWarnings({"Convert2Lambda", "Anonymous2MethodRef"})
@Configuration
class MongoConfiguration {
    
    @Bean
    public MongoCustomConversions mongoCustomConversions() {
        return new MongoCustomConversions(List.of(
                new Converter<LocalDate, String>() {
                    @Override
                    public String convert(LocalDate localDate) {
                        return localDate.toString();
                    }
                },

                new Converter<String, LocalDate>() {
                    @Override
                    public LocalDate convert(String text) {
                        return LocalDate.parse(text);
                    }
                }));
    }
    
    @Configuration
    @Profile("embedded")
    @Import(EmbeddedMongoAutoConfiguration.class)
    public static class EmbeddedMongoConfiguration { }
}
