package net.poundex.traq.testdata;

import lombok.RequiredArgsConstructor;
import net.poundex.traq.ledger.LedgerRepository;
import net.poundex.traq.timeline.EventRepository;
import net.poundex.traq.timeline.TimelinePoint;
import net.poundex.traq.timeline.TimelineSpan;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.List;

@RequiredArgsConstructor
@Component
@Profile("testdata")
class TestEvents implements ApplicationListener<ApplicationReadyEvent> {
    
    private final EventRepository eventRepository;
    private final LedgerRepository ledgerRepository;
    
    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        ledgerRepository.findAll().take(1).single().flatMapMany(ledger ->
                eventRepository.saveAll(List.of(
                        TimelinePoint.builder()
                                .ledger(ledger.getId())
                                .day(LocalDate.now().minusDays(2))
                                .description("Point 2 ago")
                                .time(ZonedDateTime.now().minusDays(2).toInstant())
                                .build(),
                        TimelinePoint.builder()
                                .ledger(ledger.getId())
                                .day(LocalDate.now().minusDays(1))
                                .description("Point 1 ago")
                                .time(ZonedDateTime.now().minusDays(1).toInstant())
                                .build(),
                        TimelinePoint.builder()
                                .ledger(ledger.getId())
                                .day(LocalDate.now().minusDays(1))
                                .description("Point another 1 ago")
                                .time(ZonedDateTime.now().minusDays(1).toInstant())
                                .build(),
                        TimelinePoint.builder()
                                .ledger(ledger.getId())
                                .day(LocalDate.now())
                                .description("Point today")
                                .time(ZonedDateTime.now().toInstant())
                                .build(),
                        TimelineSpan.builder()
                                .ledger(ledger.getId())
                                .day(LocalDate.now())
                                .description("span today")
                                .time(ZonedDateTime.now().minusMinutes(30).toInstant())
                                .endTime(ZonedDateTime.now().minusMinutes(2).toInstant())
                                .build(),
                        TimelineSpan.builder()
                                .ledger(ledger.getId())
                                .day(LocalDate.now())
                                .description("span today again")
                                .time(ZonedDateTime.now().minusMinutes(2).toInstant())
                                .endTime(ZonedDateTime.now().toInstant())
                                .build()
                ))).subscribe();
    }
}
