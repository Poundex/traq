package net.poundex.traq.ledger

import reactor.core.publisher.Mono
import spock.lang.Specification
import spock.lang.Subject

class LedgerEnsurerSpec extends Specification {
	
	LedgerRepository ledgerRepository = Mock()
	LedgerService ledgerService = Mock()
	
	@Subject
	LedgerEnsurer ensurer = new LedgerEnsurer(ledgerRepository, ledgerService)
	
	void "Does nothing if any ledgers exist"() {
		when:
		ensurer.onApplicationEvent(null)
		
		then:
		1 * ledgerRepository.count() >> Mono.just(3L)
		0 * ledgerRepository._(*_)
		0 * ledgerService._(*_)
	}

	void "Creates a default ledger if no ledgers"() {
		given:
		Ledger ledger = Ledger.builder().name("default").build()
		
		when:
		ensurer.onApplicationEvent(null)

		then:
		1 * ledgerRepository.count() >> Mono.just(0L)
		1 * ledgerService.createLedger("default", null) >> Mono.just(ledger)
	}

}
