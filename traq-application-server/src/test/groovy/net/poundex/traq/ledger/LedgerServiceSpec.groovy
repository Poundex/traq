package net.poundex.traq.ledger

import net.poundex.traq.marker.MarkerService
import net.poundex.traq.marker.Tag
import net.poundex.xmachinery.test.PublisherUtils
import reactor.core.publisher.Mono
import spock.lang.Specification
import spock.lang.Subject

class LedgerServiceSpec extends Specification implements PublisherUtils {
	
	LedgerRepository ledgerRepository = Mock()
	int mid = 0
	MarkerService markerService = Mock()
	
	@Subject
	LedgerService service = new LedgerService(ledgerRepository, markerService)
	
	void "Creates new ledgers and default markers"() {
		given:
		Ledger unsaved = Ledger.builder().name("ledger-name").build()
		Ledger saved = unsaved.toBuilder().id("id").build()
		Ledger resaved = saved.toBuilder().noProdMarker("0").noAccountMarker("1").build()
		
		and:
		ledgerRepository.save(unsaved) >> Mono.just(saved)
		ledgerRepository.save(resaved) >> Mono.just(resaved)
		
		when:
		Ledger ledger = block service.createLedger("ledger-name", null)
		
		then:
		1 * markerService.modifyMarker(saved, "+noprod", _, Collections.emptyList()) >> { Ledger l, String name, String title, List<String> ms ->
			Mono.just(Tag.builder().id((mid++).toString()).name(name).title(title).ledger(l.id).markers(ms.toSet()).build())
		}
		1 * markerService.modifyMarker(saved, "+noacct", _, ["+noprod"]) >> { Ledger l, String name, String title, List<String> ms ->
			Mono.just(Tag.builder().id((mid++).toString()).name(name).title(title).markers(ms.toSet()).ledger(l.id).build())
		}
		0 * markerService._(*_)
		
		and:
		ledger.name == "ledger-name"
		! ledger.targetDuration
		ledger.noProdMarker == "0"
		ledger.noAccountMarker == "1"
	}

	void "Updates existing"() {
		given:
		Ledger existing = Ledger.builder().id("id").name("old-name").build()
		Ledger updated = Ledger.builder().name("new-name").build()
		Ledger expected = updated.toBuilder().id("id").build()

		when:
		Ledger r = block service.updateLedger(existing, updated)

		then:
		1 * ledgerRepository.save(expected) >> Mono.just(expected)
		r == expected
	}
}

