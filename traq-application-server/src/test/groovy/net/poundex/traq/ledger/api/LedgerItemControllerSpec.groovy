package net.poundex.traq.ledger.api

import net.poundex.traq.ledger.Ledger
import net.poundex.traq.ledger.LedgerItem
import net.poundex.xmachinery.spring.graphql.batch.ObjectLoader
import net.poundex.xmachinery.test.BatchLoadingUtils
import net.poundex.xmachinery.test.PublisherUtils
import reactor.core.publisher.Mono
import spock.lang.Specification
import spock.lang.Subject

class LedgerItemControllerSpec extends Specification implements BatchLoadingUtils, PublisherUtils {
	
	LedgerItem ledgerItem = Stub() {
		getLedger() >> "some-ledger-id"
	}
	
	@Subject
	LedgerItemController controller = new LedgerItemController()
	
	void "Returns just ledger id without loading"() {
		when:
		Ledger ledger = block controller.ledger(ledgerItem, null, justId())
		
		then:
		ledger.id == "some-ledger-id"
	}
	
	void "Returns ledger"() {
		given:
		Ledger ledger = Ledger.builder().id(ledgerItem.ledger).name("some-ledger").build()
		ObjectLoader<String, Ledger> ledgerLoader = Stub() {
			load(ledgerItem.ledger) >> Mono.just(ledger)
		}

		when:
		Ledger r = block controller.ledger(ledgerItem, ledgerLoader, fields("id", "name"))
		
		then:
		r == ledger
	}
}