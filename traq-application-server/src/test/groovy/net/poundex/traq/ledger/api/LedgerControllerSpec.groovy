package net.poundex.traq.ledger.api

import net.poundex.traq.ledger.Ledger
import net.poundex.traq.ledger.LedgerInputDto
import net.poundex.traq.ledger.LedgerRepository
import net.poundex.traq.ledger.LedgerService
import net.poundex.traq.marker.Marker
import net.poundex.traq.marker.MarkerRepository
import net.poundex.traq.marker.Tag
import net.poundex.xmachinery.spring.graphql.batch.ObjectLoader
import net.poundex.xmachinery.test.PublisherUtils
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import spock.lang.Specification
import spock.lang.Subject

import java.time.Duration

import static net.poundex.traq.ledger.QLedger.ledger

class LedgerControllerSpec extends Specification implements PublisherUtils {

	LedgerRepository ledgerRepository = Stub()
	LedgerService ledgerService = Stub()
	LedgerApiConverters apiConverters = Stub()
	MarkerRepository markerRepository = Stub()
	
	@Subject
	LedgerController controller = new LedgerController(ledgerRepository, ledgerService, apiConverters, markerRepository)

	void "Creates new Ledgers"() {
		given:
		Ledger saved = Ledger.builder().name("new-ledger").id("id").build()
		ledgerService.createLedger("new-ledger", null) >> Mono.just(saved)

		when:
		Ledger ledger = block controller.createLedger(new LedgerInputDto("new-ledger", null))

		then:
		ledger.id == "id"
		ledger.name == "new-ledger"
		! ledger.targetDuration
	}

	void "Returns ledger by ID"() {
		given:
		Ledger dto = Ledger.builder().id("id").name("new-ledger").build()
		ObjectLoader<String, Ledger> dataLoader = Stub() {
			load("id") >> Mono.just(dto)
		}

		when:
		Ledger ledger = block controller.ledger("id", dataLoader)

		then:
		ledger.id == "id"
		ledger.name == "new-ledger"
		! ledger.targetDuration
	}

	void "Creates new Ledger with target duration"() {
		Duration d7hours = Duration.ofHours(7)
		given:
		Ledger saved = Ledger.builder().name("new-ledger").targetDuration(d7hours).id("id").build()
		ledgerService.createLedger("new-ledger", d7hours) >> Mono.just(saved)

		when:
		Ledger ledger = block controller.createLedger(new LedgerInputDto("new-ledger", d7hours))

		then:
		ledger.id == "id"
		ledger.name == "new-ledger"
		ledger.targetDuration == d7hours
	}

	void "Returns ledger by Name"() {
		given:
		Ledger l = Ledger.builder().id("id").name("new-ledger").build()
		ledgerRepository.findOne(ledger.name.equalsIgnoreCase("new-ledger")) >> Mono.just(l)

		when:
		Ledger r = block controller.ledgerByName("new-ledger")

		then:
		r.id == "id"
		r.name == "new-ledger"
		! r.targetDuration
	}

	void "Loads ledgers"() {
		given:
		String id1 = "id-1"
		String id2 = "id-2"
		String id3 = "id-3"
		Set<String> ids = [id1, id2, id3].toSet()

		and:
		Ledger l1 = Ledger.builder().id(id1).build()
		Ledger l2 = Ledger.builder().id(id2).build()
		Ledger l3 = Ledger.builder().id(id3).build()
		ledgerRepository.findAllById(ids) >> Flux.just(l1, l2, l3)

		when:
		Map<String, Ledger> loaded = block controller.ledgerLoader(ids)

		then:
		loaded[id1] == l1
		loaded[id2] == l2
		loaded[id3] == l3
	}
	
	void "Updates ledgers"() {
		given:
		LedgerInputDto dto = new LedgerInputDto("new-name", Duration.ofHours(7))
		Ledger existing = Ledger.builder().id("id").name("old-name").build()
		Ledger updated =  Ledger.builder().name("new-name").build()
		Ledger expected = existing.toBuilder().name("new-name").build()
		apiConverters.fromDto(dto) >> updated
		ObjectLoader<String, Ledger> loader = Stub() {
			load("id") >> Mono.just(existing)
		}
		ledgerService.updateLedger(existing, updated) >> Mono.just(expected)
		
		when:
		Ledger ret = block controller.updateLedger("id", dto, loader)

		then:
		ret == expected
	}
	
	void "Returns markers for ledger"() {
		given:
		Ledger existing = Ledger.builder().id("ledger-id").name("name").build()
		Set<Marker> markers = [Stub(Marker), Stub(Marker)].toSet()
		ObjectLoader<String, Set<Marker>> loader = Stub() {
			load("ledger-id") >> Mono.just(markers)
		}

		when:
		Set<Marker> r = block controller.markers(existing, loader)
		
		then:
		r == markers
	}
	
	void "Loads markers by ledger"() {
		given:
		Set<String> ledgerIds = ["l1", "l2"].toSet()
		Marker m1 = Tag.builder().ledger("l1").id("m1").build()
		Marker m2 = Tag.builder().ledger("l1").id("m2").build()
		Marker m3 = Tag.builder().ledger("l2").id("m3").build()
		
		and:
		markerRepository.findAll() >> Flux.just(m1, m2, m3)

		when:
		Map<String, Set<Marker>> r = block controller.markersByLedgerLoader(ledgerIds)
		
		then:
		r.keySet() == ledgerIds
		r['l1'] == [m1, m2].toSet()
		r['l2'] == [m3].toSet()
	}
}
