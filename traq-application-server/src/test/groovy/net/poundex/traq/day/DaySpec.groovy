package net.poundex.traq.day

import net.poundex.traq.ledger.Ledger
import net.poundex.traq.timeline.TimelinePoint
import net.poundex.traq.timeline.TimelineSpan
import net.poundex.xmachinery.test.PublisherUtils
import spock.lang.Specification

import java.time.*
import java.time.temporal.ChronoUnit

class DaySpec extends Specification implements PublisherUtils {

	private static final LocalDate today = LocalDate.now()
	private static final Instant t1000 = ZonedDateTime.of(today, LocalTime.of(10, 0), ZoneId.systemDefault()).toInstant()
	private static final Instant t1200 = ZonedDateTime.of(today, LocalTime.of(12, 0), ZoneId.systemDefault()).toInstant()
	private static final Instant t1400 = ZonedDateTime.of(today, LocalTime.of(14, 0), ZoneId.systemDefault()).toInstant()
	private static final Instant t1700 = ZonedDateTime.of(today, LocalTime.of(17, 0), ZoneId.systemDefault()).toInstant()
	private static final TimelinePoint earlyPoint = TimelinePoint.builder().time(ZonedDateTime.of(today, LocalTime.of(0, 1), ZoneId.systemDefault()).toInstant()).build()
	
	Ledger ledger = Stub()

	void "Returns null start time for no spans"() {
		given:
		Day day = new Day(today, null, [earlyPoint])

		expect:
		! day.startTime
	}

	void "Returns correct start time"() {
		given:
		TimelineSpan span = TimelineSpan.builder().time(t1200).build()
		Day day = new Day(today, null, List.of(earlyPoint, span))

		expect:
		day.startTime == t1200
	}
	
	void "Returns null end time for no spans"() {
		given:
		Day day = new Day(today, null, [earlyPoint])
		
		expect:
		! day.endTime
	}

	void "Returns correct end time"() {
		given:
		TimelineSpan span = TimelineSpan.builder().endTime(t1200).build()
		Day day = new Day(today, null, List.of(earlyPoint, span))
		
		expect:
		day.endTime == t1200
	}

	void "Returns zero duration for no spans"() {
		given:
		Day day = new Day(today, null, [earlyPoint])
		
		expect:
		day.duration.isZero()
	}

	void "Returns correct duration"() {
		given:
		TimelineSpan span = TimelineSpan.builder().time(t1200).endTime(t1400).build()
		Day day = new Day(today, ledger, List.of(earlyPoint, span))
		
		expect:
		day.duration == Duration.ofMinutes(120)
	}

	void "Returns correct duration with unaccountable spans"() {
		given:
		ledger.getNoAccountMarker() >> "no-acct"
		
		TimelineSpan span1 = TimelineSpan.builder().time(t1200).endTime(t1400).build()
		TimelineSpan span2 = TimelineSpan.builder().time(t1400).endTime(t1700).markers(["no-acct"].toSet()).build()
		Day day = new Day(today, ledger, List.of(earlyPoint, span1, span2))

		expect:
		day.duration == Duration.ofMinutes(120)
	}

	void "Return null projected end time for no target time"() {
		given:
		TimelineSpan span = TimelineSpan.builder().time(t1200).endTime(t1400).build()
		Ledger someLedger = Ledger.builder().id("ledger-id").name("Ledger").build()
		Day day = new Day(today, someLedger, List.of(earlyPoint, span))

		expect:
		! day.projectedEndTime
	}

	void "Return correct projected end time"() {
		given:
		TimelineSpan span = TimelineSpan.builder().time(t1200).endTime(t1400).build()
		Ledger someLedger = Ledger.builder().id("ledger-id").name("Ledger").targetDuration(Duration.ofHours(4)).build()
		Day day = new Day(today, someLedger, List.of(earlyPoint, span))

		expect:
		day.projectedEndTime.truncatedTo(ChronoUnit.MINUTES) ==
				Instant.now().plus(2, ChronoUnit.HOURS).truncatedTo(ChronoUnit.MINUTES)
	}
	
	void "Returns zero length for no spans"() {
		given:
		Day day = new Day(today, null, [earlyPoint])

		expect:
		day.length.isZero()
	}

	void "Returns correct length"() {
		given:
		TimelineSpan span1 = TimelineSpan.builder().time(t1000).endTime(t1200).build()
		TimelineSpan span2 = TimelineSpan.builder().time(t1400).endTime(t1700).build()
		Day day = new Day(today, null, List.of(earlyPoint, span1, span2))

		expect:
		day.length == Duration.ofMinutes(7 * 60)
	}
	
}
