package net.poundex.traq.day

import com.querydsl.core.types.Constant
import com.querydsl.core.types.Predicate
import net.poundex.traq.ledger.Ledger
import net.poundex.traq.timeline.EventRepository
import net.poundex.traq.timeline.TimelinePoint
import net.poundex.xmachinery.test.PublisherUtils
import reactor.core.publisher.Flux
import spock.lang.Specification
import spock.lang.Subject

import java.time.LocalDate

import static net.poundex.traq.timeline.QTimelineEvent.timelineEvent

class DayServiceSpec extends Specification implements PublisherUtils {
	
	LocalDate today = LocalDate.now()
	LocalDate yesterday = today.minusDays(1)

	EventRepository eventRepository = Mock()
	
	@Subject
	DayService service = new DayService(eventRepository)
	
	void "Returns Days with events"() {
		given:
		Ledger ledger = Ledger.builder().id("some-ledger").build()
		eventRepository.findAll(
				timelineEvent.day.in(Set.of(today, yesterday)).and(timelineEvent.ledger.eq("some-ledger")), 
				timelineEvent.time.asc()) >> { Predicate p, o ->
			Set<LocalDate> dates = (p.getArg(0).getArg(1) as Constant).getConstant() as Set<LocalDate>
			Flux.fromIterable(dates.collect { dd ->
				new TimelinePoint("id", dd, "description", null, Collections.emptySet(), "defl")
			})
		}
		
		when:
		List<Day> r = block service.getDays(ledger, Set.of(today, yesterday))
		
		then:
		r.size() == 2
		r.every { d -> d.events.first().day == d.day }
	}
}
