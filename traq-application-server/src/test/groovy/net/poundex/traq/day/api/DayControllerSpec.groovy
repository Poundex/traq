package net.poundex.traq.day.api


import net.poundex.traq.day.Day
import net.poundex.traq.day.DayService
import net.poundex.traq.ledger.Ledger
import net.poundex.traq.timeline.DayGuesser
import net.poundex.traq.timeline.TimelinePoint
import net.poundex.traq.timeline.api.TimelineEventApiConverters
import net.poundex.xmachinery.spring.graphql.batch.ObjectLoader
import net.poundex.xmachinery.test.BatchLoadingUtils
import net.poundex.xmachinery.test.PublisherUtils
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import spock.lang.Specification
import spock.lang.Subject

import java.time.*

class DayControllerSpec extends Specification implements PublisherUtils, BatchLoadingUtils {

	private static final LocalDate today = LocalDate.now()
	private static final Instant t1000 = ZonedDateTime.of(today, LocalTime.of(10, 0), ZoneId.systemDefault()).toInstant()
	private static final Instant t1200 = ZonedDateTime.of(today, LocalTime.of(12, 0), ZoneId.systemDefault()).toInstant()
	private static final Instant t1400 = ZonedDateTime.of(today, LocalTime.of(14, 0), ZoneId.systemDefault()).toInstant()
	private static final Instant t1700 = ZonedDateTime.of(today, LocalTime.of(17, 0), ZoneId.systemDefault()).toInstant()
	private static final TimelinePoint earlyPoint = TimelinePoint.builder().time(ZonedDateTime.of(today, LocalTime.of(0, 1), ZoneId.systemDefault()).toInstant()).build()
	private static final Day todayEmpty = Day.builder().day(today).build()

	DayService dayService = Stub()
	TimelineEventApiConverters eventConverters = Stub()
	DayGuesser dayGuesser = Stub() {
		getCurrentDayBestGuess(_) >> { LocalDate d -> Mono.just(d ?: LocalDate.now()) }
	}
	
	@Subject
	DayController dayController = new DayController(dayService, dayGuesser)

	void "Returns empty day"() {
		when:
		Day day = block dayController.day(today, fields("day"), null)

		then:
		day.day == today
	}
	
	void "Returns day"() {
		given:
		Day day = new Day(today, null, [])
		ObjectLoader<LocalDate, Day> dayLoader = Stub() {
			load(today) >> Mono.just(day)
		}
		
		expect:
		block(dayController.day(today, fields("day", "events"), dayLoader)) == day
	}
	
	void "Loads Days by day "() {
		given:
		LocalDate one = LocalDate.now()
		LocalDate two = LocalDate.now().minusDays(1)
		LocalDate three = LocalDate.now().minusDays(2)
		Set<LocalDate> days = [one, two, three].toSet()
		Ledger someLedger = Stub()
		Mono<Ledger> ledgerMono = Mono.just(someLedger)
		Day d1 = Day.builder().day(one).build()
		Day d2 = Day.builder().day(two).build()
		Day d3 = Day.builder().day(three).build()
		
		and:
		dayService.getDays(someLedger, days) >> Flux.just(d1, d2, d3)

		when:
		Map<LocalDate, Day> r = block dayController.dayLoader(days, ledgerMono)
		
		then:
		r[one] == d1
		r[two] == d2
		r[three] == d3
	}
}
