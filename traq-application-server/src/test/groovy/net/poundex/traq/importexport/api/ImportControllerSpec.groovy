package net.poundex.traq.importexport.api


import net.poundex.traq.importexport.ImportResultDto
import net.poundex.traq.importexport.ImportService
import net.poundex.traq.ledger.Ledger
import net.poundex.traq.ledger.LedgerRepository
import net.poundex.traq.ledger.QLedger
import net.poundex.traq.timeline.TimelineEvent
import net.poundex.xmachinery.fio.io.IO
import net.poundex.xmachinery.test.PublisherUtils
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.http.codec.multipart.FilePart
import reactor.core.publisher.Mono
import spock.lang.Specification
import spock.lang.Subject

import java.nio.file.Path
import java.time.ZoneId

class ImportControllerSpec extends Specification implements PublisherUtils {
	
	IO io = Stub()
	ImportService importService = Stub()
	FilePart filePart = Mock()
	Ledger ledger = Stub()
	LedgerRepository ledgerRepository = Stub() {
		findOne(QLedger.ledger.name.equalsIgnoreCase("default")) >> Mono.just(ledger)
	}
	
	@Subject
	ImportController controller = new ImportController(io, importService, ledgerRepository)
	
	void "Dumps upload file to temp file and calls ImportService"() {
		given:
		Path tempFile = Stub()
		io.createTempFile(_, _) >> tempFile
		importService.importFile(ledger, tempFile, ZoneId.systemDefault()) >> 
				Mono.just([Stub(TimelineEvent), Stub(TimelineEvent), Stub(TimelineEvent)])
		
		when:
		ResponseEntity<ImportResultDto> r = block controller.importFile("default", filePart, ZoneId.systemDefault().getId())
		
		then:
		1 * filePart.transferTo(tempFile) >> Mono.empty()
		
		and:
		r.statusCode == HttpStatus.OK
		r.body.eventsCreated() == 3
	}
}
