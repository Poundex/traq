package net.poundex.traq.importexport

import net.poundex.traq.ledger.Ledger
import net.poundex.traq.timeline.TimelineEvent
import net.poundex.traq.timeline.TimelinePoint
import net.poundex.traq.timeline.TimelineService
import net.poundex.traq.timeline.TimelineSpan
import net.poundex.xmachinery.fio.io.IO
import net.poundex.xmachinery.test.PublisherUtils
import reactor.core.publisher.Mono
import spock.lang.Specification
import spock.lang.Subject

import java.nio.file.Path
import java.time.*

class ImportServiceSpec extends Specification implements PublisherUtils {
	
	Ledger ledger = Stub()
	Path importFile = Stub()
	IO io = Stub()
	TimelineService timelineService = Stub()
	
	@Subject
	ImportService service = new ImportService(io, timelineService)
	
	void "Creates TimelineEvents from import data"() {
		given:
		io.newBufferedReader(importFile) >> getClass().classLoader.getResource("importdata.csv").newReader()
		timelineService.createSpan(_, _, _, _, _) >> { LocalDate day, Instant time, Instant endTime, String description, Ledger ledger ->
			Mono.just(TimelineSpan.builder().day(day).time(time).endTime(endTime).description(description).ledger(ledger.id).build())
		}
		timelineService.createPoint(_, _, _, _) >> { Instant time, LocalDate day, String description, Ledger ledger ->
			Mono.just(TimelinePoint.builder().day(day).time(time).description(description).ledger(ledger.id).build())
		}
		LocalDate date = LocalDate.of(2023, 9, 5)
		Instant t1 = ZonedDateTime.of(2023, 9, 5, 8, 30, 0, 0, ZoneOffset.UTC).toInstant()
		Instant t2 = ZonedDateTime.of(2023, 9, 5, 9, 30, 0, 0, ZoneOffset.UTC).toInstant()
		Instant t3 = ZonedDateTime.of(2023, 9, 5, 10, 30, 0, 0, ZoneOffset.UTC).toInstant()
		
		when:
		List<? extends TimelineEvent> r = block service.importFile(ledger, importFile, ZoneId.of("Europe/London"))
		
		then:
		r.size() == 3
		r.find {
			it instanceof TimelineSpan
					&& it.day == date
					&& it.time == t1
					&& it.endTime == t2
					&& it.description == "1 Hour Span"
		}
		r.find {
			it instanceof TimelinePoint
					&& it.day == date
					&& it.time == t2
					&& it.description == "A Point"
		}
		r.find {
			it instanceof TimelinePoint
					&& it.day == date
					&& it.time == t3
					&& it.description == "Another Point"
		}
	}
}
