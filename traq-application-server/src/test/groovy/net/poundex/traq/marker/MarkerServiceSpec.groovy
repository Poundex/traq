package net.poundex.traq.marker

import net.poundex.traq.ledger.Ledger
import net.poundex.traq.marker.realiser.MarkerRealiserService
import net.poundex.xmachinery.test.PublisherUtils
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import spock.lang.Specification
import spock.lang.Subject

class MarkerServiceSpec extends Specification implements PublisherUtils {

	MarkerRealiserService markerRealiserService = Mock()
	MarkerRepository markerRepository = Mock()
	Ledger ledger = Ledger.builder().id("lid").name("some-ledger").build()

	@Subject
	MarkerService markerService = new MarkerService(markerRealiserService, markerRepository)

	void "Creates new Markers"() {
		given:
		markerRealiserService.realiseMarkers(ledger.id, []) >> Flux.empty()
		Tag expected = Tag.builder()
				.id("id")
				.name("newmarker")
				.markers(Collections.emptySet())
				.ledger(ledger.id)
				.build()
		markerRepository.save(expected) >> Mono.just(expected)

		when:
		Marker marker = block markerService.modifyMarker(ledger, "+newmarker", null, Collections.emptyList())

		then:
		1 * markerRealiserService.realiseMarkersInString(ledger.getId(), "+newmarker") >> Flux.just(expected)

		and:
		marker == expected
	}

	void "Modifies existing marker with new marker"() {
		given:
		Tag otherMarker = Tag.builder()
				.id("oth")
				.name("othermarker")
				.markers(Collections.emptySet())
				.build()
		Tag newMarker = Tag.builder()
				.id("id")
				.name("newmarker")
				.markers(Set.of(otherMarker.id))
				.ledger(ledger.id)
				.build()

		when:
		Marker marker = block markerService.modifyMarker(ledger, "+newmarker", null, ["+othermarker"])

		then:
		1 * markerRealiserService.realiseMarkers(ledger.id, ["+othermarker"]) >> Flux.just(otherMarker)
		1 * markerRealiserService.realiseMarkersInString(ledger.id, "+newmarker") >> Flux.just(newMarker)
		1 * markerRepository.save(newMarker) >> Mono.just(newMarker)

		and:
		marker == newMarker
	}

	void "Updates Marker title"() {
		given:
		markerRealiserService.realiseMarkers(ledger.id, []) >> Flux.empty()

		and:
		Tag original = Tag.builder()
				.id("id")
				.name("newmarker")
				.markers(Collections.emptySet())
				.ledger(ledger.id)
				.build()
		Tag withTitle = original.toBuilder().title("New Marker").build()

		when:
		Marker marker = block markerService.modifyMarker(ledger, "+newmarker", "New Marker", [])

		then:
		1 * markerRealiserService.realiseMarkersInString(ledger.id, "+newmarker") >> Flux.just(original)
		1 * markerRepository.save(withTitle) >> Mono.just(withTitle)

		and:
		marker == withTitle
	}

	void "Keeps existing title"() {
		given:
		markerRealiserService.realiseMarkers(ledger.id, []) >> Flux.empty()

		and:
		Tag original = Tag.builder()
				.id("id")
				.name("newmarker")
				.title("New Marker")
				.markers(Collections.emptySet())
				.build()

		when:
		Marker marker = block markerService.modifyMarker(ledger, "+newmarker", null, [])

		then:
		1 * markerRealiserService.realiseMarkersInString(ledger.id, "+newmarker") >> Flux.just(original)
		1 * markerRepository.save(original) >> Mono.just(original)

		and:
		marker == original
	}

	void "Returns all effective markers"() {
		given:
		Tag one = Tag.builder().id("1").name("one").markers(Collections.emptySet()).build()
		Tag two = Tag.builder().id("2").name("two").markers(Set.of("1")).build()
		Tag three = Tag.builder().id("3").name("three").markers(Set.of("2", "5")).build()
		Tag four = Tag.builder().id("4").name("four").markers(Set.of("3")).build()
		Tag five = Tag.builder().id("5").name("five").markers(Set.of("4")).build()
		Map<String, Tag> mu = [one, two, three, four, five].collectEntries { [it.id, it] }

		and:
		Markable markable = Stub() {
			getMarkers() >> Set.of("5")
		}

		when:
		Set<Marker> markers = markerService.getEffectiveMarkers(markable, false, mu)

		then:
		markers.size() == 5
		markers.containsAll(one, two, three, four, five)
	}

	void "Returns all effective markers excluding direct"() {
		given:
		Tag one = Tag.builder().id("1").name("one").markers(Collections.emptySet()).build()
		Tag two = Tag.builder().id("2").name("two").markers(Set.of("1")).build()
		Tag three = Tag.builder().id("3").name("three").markers(Set.of("2")).build()
		Tag four = Tag.builder().id("4").name("four").markers(Set.of("3")).build()
		Tag five = Tag.builder().id("5").name("five").markers(Set.of("4")).build()
		Map<String, Tag> mu = [one, two, three, four, five].collectEntries { [it.id, it] }

		and:
		Markable markable = Stub() {
			getMarkers() >> Set.of("5")
		}

		when:
		Set<Marker> markers = markerService.getEffectiveMarkers(markable, true, mu)

		then:
		markers.size() == 4
		markers.containsAll(one, two, three, four)
	}

	void "Gets all routes to markers"() {
		given:
		Tag food = Tag.builder().id("food").name("Food").markers(Collections.emptySet()).build()
		Tag cake = Tag.builder().id("cake").name("Cake").markers(Set.of("food")).build()
		Tag cup = Tag.builder().id("cup").name("Cup").markers(Set.of("cake")).build()
		Tag birthday = Tag.builder().id("birthday").name("Birthday").markers(Set.of("cake")).build()
		Tag free = Tag.builder().id("free").name("Free").markers(Set.of("cake")).build()
		Map<String, Marker> muMap = [food, cake, cup, birthday, free].collectEntries { [it.id, it] }

		when:
		Map<Marker, Set<Marker>> routes = markerService.getAllRoutes(muMap)

		then:
		routes[food] == [food, cake, cup, birthday, free].toSet()
		routes[cake] == [cake, cup, birthday, free].toSet()
		routes[cup] == [cup].toSet()
		routes[birthday] == [birthday].toSet()
		routes[free] == [free].toSet()
	}

	void "Updates existing"() {
		given:
		Marker existing = Tag.builder().id("id").name("name").build()
		Marker expected = existing.toBuilder().id("id").name("name").title("title").build()

		when:
		Marker r = block markerService.updateMarker(existing, "title")

		then:
		1 * markerRepository.save(expected) >> Mono.just(expected)
		r == expected
	}

	void "Sets markers"() {
		given:
		Tag toMark = Tag.builder()
				.id("id")
				.ledger(ledger.id)
				.name("markme")
				.markers(Collections.emptySet())
				.build()

		Tag markerMarker = Tag.builder()
				.id("id-new")
				.ledger(ledger.id)
				.name("bemarkedbyme")
				.markers(Collections.emptySet())
				.build()
		
		and:
		Tag expected = toMark.toBuilder()
			.markers(["id-new"].toSet())
				.build()

		when:
		Marker marker = block markerService.setMarkers(toMark, ["+bemarkedbyme"])

		then:
		1 * markerRealiserService.realiseMarkers(ledger.id, ["+bemarkedbyme"]) >> Flux.just(markerMarker)
		1 * markerRepository.save(expected) >> Mono.just(expected)

		and:
		marker == expected
	}
}