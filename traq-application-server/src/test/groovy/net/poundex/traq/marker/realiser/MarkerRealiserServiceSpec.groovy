package net.poundex.traq.marker.realiser

import net.poundex.traq.ledger.Ledger
import net.poundex.traq.marker.Marker
import net.poundex.traq.marker.Mention
import net.poundex.traq.marker.Tag
import net.poundex.xmachinery.test.PublisherUtils
import org.springframework.beans.factory.ObjectProvider
import reactor.core.publisher.Flux
import spock.lang.Specification

import java.util.stream.Stream

class MarkerRealiserServiceSpec extends Specification implements PublisherUtils {

	Ledger ledger = Ledger.builder().id("lid").name("some-ledger").build()
	
	Tag blue = Tag.builder().name("blue").build()
	Mention them = Mention.builder().name("them").build()
	
	MarkerRealiser realiser1 = Stub()
	MarkerRealiser realiser2 = Stub()

	ObjectProvider<MarkerRealiser> objectProvider = Stub() {
		stream() >> Stream.of(realiser1, realiser2)
	}

	MarkerRealiserService service = new MarkerRealiserService(objectProvider)
	
	void "Uses realisers to realise and return all markers"() {
		given:
		String input = "This is +blue string @them"
		
		and:
		realiser1.realiseMarkers(ledger.id, input) >> Flux.just(blue)
		realiser2.realiseMarkers(ledger.id, input) >> Flux.just(them)
		
		when:
		List<Marker> markers = block service.realiseMarkersInString(ledger.id, input).collectList()

		then:
		markers.containsAll([blue, them])
	}
}
