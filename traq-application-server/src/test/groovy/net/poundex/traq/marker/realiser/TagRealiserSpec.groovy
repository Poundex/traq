package net.poundex.traq.marker.realiser

import net.poundex.traq.ledger.Ledger
import net.poundex.traq.marker.QMarker
import net.poundex.traq.marker.Tag
import net.poundex.traq.marker.TagRepository
import reactor.core.publisher.Flux
import spock.lang.Specification
import spock.lang.Subject

import static net.poundex.traq.marker.QTag.tag

class TagRealiserSpec extends Specification {

	Ledger ledger = Ledger.builder().id("lid").name("some-ledger").build()
	TagRepository tagRepository = Mock() {
		saveAll(Collections.emptySet()) >> Flux.empty()
	}
	
	@Subject
	TagRealiser realiser = new TagRealiser(tagRepository)
	
	void "Returns empty for nothing recognised"() {
		when:
		List<Tag> markers = realiser.realiseMarkers(ledger.id, "one two three four five")
				.collectList().block()
		
		then:
		markers.empty
	}
	
	void "Returns existing marker"() {
		given:
		Tag marker = Tag.builder().name("blue").build()
		
		when:
		List<Tag> markers = realiser.realiseMarkers(ledger.id, "one two +blue")
				.collectList().block()

		then:
		1 * tagRepository.findAll(tag.nameLower.in(["blue"]).and(QMarker.marker.ledger.eq(ledger.id))) >> Flux.just(marker)

		and:
		markers.size() == 1
		markers.first() == marker
	}

	void "Creates when required and returns new marker"() {
		given:
		Tag marker = Tag.builder().name("blue").build()
		Tag marker2 = Tag.builder().name("green").ledger(ledger.id).build()

		when:
		List<Tag> markers = realiser.realiseMarkers(ledger.id, "one two +blue +green")
				.collectList().block()

		then:
		1 * tagRepository.findAll(tag.nameLower.in(["blue", "green"].toSet()).and(QMarker.marker.ledger.eq(ledger.id))) >> Flux.just(marker)
		1 * tagRepository.saveAll(Set.of(marker2)) >> Flux.just(marker2)

		and:
		markers.size() == 2
		markers.containsAll(marker, marker2)
	}

	void "Existing marker search is case-insensitive"() {
		given:
		Tag marker = Tag.builder().name("blue").build()

		when:
		List<Tag> markers = realiser.realiseMarkers(ledger.id, "one two +BLUE")
				.collectList().block()

		then:
		1 * tagRepository.findAll(tag.nameLower.in(["blue"]).and(QMarker.marker.ledger.eq(ledger.id))) >> Flux.just(marker)

		and:
		markers.size() == 1
		markers.first() == marker
	}

}
