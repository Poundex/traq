package net.poundex.traq.marker.api

import graphql.GraphQLContext
import net.poundex.traq.ledger.Ledger
import net.poundex.traq.marker.*
import net.poundex.xmachinery.spring.graphql.batch.ObjectLoader
import net.poundex.xmachinery.test.PublisherUtils
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import spock.lang.Specification
import spock.lang.Subject

class MarkerControllerSpec extends Specification implements PublisherUtils {

	MarkerRepository markerRepository = Stub()
	MarkerService markerService = Mock()
	Ledger ledger = Ledger.builder().id("lid").name("some-ledger").build()

	@Subject
	MarkerController controller = new MarkerController(markerRepository, markerService)

	void "Modifies markers"() {
		given:
		Tag saved = Tag.builder()
				.name("newtag")
				.id("id")
				.markers(Collections.emptySet())
				.ledger(ledger.id)
				.build()

		when:
		Marker marker = block controller.modifyMarker(Mono.just(ledger), "+newtag", "New Tag", Collections.emptyList())

		then:
		1 * markerService.modifyMarker(ledger, "+newtag", "New Tag", Collections.emptyList()) >> Mono.just(saved)
		marker == saved
	}
	
	void "Returns existing markers"() {
		given:
		Tag one = Tag.builder().id("1").name("one").title("One").markers(Collections.emptySet()).build()
		Tag two = Tag.builder().id("2").name("two").title("Two").markers(Set.of(one.id)).build()
		markerRepository.findAll(QMarker.marker.ledger.eq(ledger.id)) >> Flux.just(one, two)
		
		when:
		List<? extends Marker> markers = block controller.markers(Mono.just(ledger))
		
		then:
		markers.size() == 2
		markers.containsAll(one, two)
	}
	
	void "Returns Marker by ID"() {
		given:
		Tag tag = Tag.builder().id("1").name("one").ledger(ledger.id).build()
		ObjectLoader<String, Marker> markerLoader = Stub() {
			load("1") >> Mono.just(tag)
		}
		Mono<Ledger> ledgerMono = Mono.just(ledger)
		ObjectLoader<String, Ledger> ledgerLoader = Stub() {
			load(ledger.id) >> ledgerMono
		}
		GraphQLContext context = new GraphQLContext.Builder().build()

		when:
		Marker marker = block controller.marker("1", markerLoader, ledgerLoader, context)

		then:
		marker.id == "1"
		marker.name == "one"
		
		and:
		context.get("ledger").is(ledgerMono)
	}

	void "Updates marker titles"() {
		given:
		Marker existing = Tag.builder().id("id").name("name").build()
		Marker expected = existing.toBuilder().title("title").build()
		ObjectLoader<String, Marker> loader = Stub() {
			load("id") >> Mono.just(existing)
		}
		markerService.updateMarker(existing, "title") >> Mono.just(expected)

		when:
		Marker ret = block controller.updateMarker("id", "title", loader)

		then:
		ret == expected
	}

	void "Sets markers"() {
		given:
		Marker existing = Tag.builder().id("id").name("name").build()
		Marker expected = existing.toBuilder().title("title").build()
		ObjectLoader<String, Marker> loader = Stub() {
			load("id") >> Mono.just(existing)
		}
		markerService.setMarkers(existing, ["+one"]) >> Mono.just(expected)

		when:
		Marker ret = block controller.setMarkers("id", ["+one"], loader)

		then:
		ret == expected
	}

}
