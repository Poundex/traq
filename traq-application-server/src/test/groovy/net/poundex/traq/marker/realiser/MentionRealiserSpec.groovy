package net.poundex.traq.marker.realiser

import net.poundex.traq.ledger.Ledger
import net.poundex.traq.marker.Mention
import net.poundex.traq.marker.MentionRepository
import net.poundex.traq.marker.QMarker
import reactor.core.publisher.Flux
import spock.lang.Specification
import spock.lang.Subject

import static net.poundex.traq.marker.QMention.mention

class MentionRealiserSpec extends Specification {

	Ledger ledger = Ledger.builder().id("lid").name("some-ledger").build()
	MentionRepository mentionRepository = Mock() {
		saveAll(Collections.emptySet()) >> Flux.empty()
	}
	
	@Subject
	MentionRealiser realiser = new MentionRealiser(mentionRepository)
	
	void "Returns empty for nothing recognised"() {
		when:
		List<Mention> markers = realiser.realiseMarkers(ledger.id, "one two three four five")
				.collectList().block()
		
		then:
		markers.empty
	}
	
	void "Returns existing marker"() {
		given:
		Mention marker = Mention.builder().name("blue").build()
		
		when:
		List<Mention> markers = realiser.realiseMarkers(ledger.id, "one two @blue")
				.collectList().block()

		then:
		1 * mentionRepository.findAll(mention.nameLower.in(["blue"]).and(QMarker.marker.ledger.eq(ledger.id))) >> Flux.just(marker)

		and:
		markers.size() == 1
		markers.first() == marker
	}

	void "Creates when required and returns new marker"() {
		given:
		Mention marker = Mention.builder().name("blue").ledger(ledger.id).build()
		Mention marker2 = Mention.builder().name("green").ledger(ledger.id).build()

		when:
		List<Mention> markers = realiser.realiseMarkers(ledger.id, "one two @blue @green")
				.collectList().block()

		then:
		1 * mentionRepository.findAll(mention.nameLower.in(["blue", "green"].toSet()).and(QMarker.marker.ledger.eq(ledger.id))) >> Flux.just(marker)
		1 * mentionRepository.saveAll(Set.of(marker2)) >> Flux.just(marker2)

		and:
		markers.size() == 2
		markers.containsAll(marker, marker2)
	}

	void "Existing marker search is case-insensitive"() {
		given:
		Mention marker = Mention.builder().name("blue").build()

		when:
		List<Mention> markers = realiser.realiseMarkers(ledger.id, "one two @BLUE")
				.collectList().block()

		then:
		1 * mentionRepository.findAll(mention.nameLower.in(["blue"]).and(QMarker.marker.ledger.eq(ledger.id))) >> Flux.just(marker)

		and:
		markers.size() == 1
		markers.first() == marker
	}
}
