package net.poundex.traq.marker.api

import net.poundex.traq.ledger.Ledger
import net.poundex.traq.marker.*
import net.poundex.xmachinery.spring.graphql.batch.ObjectLoader
import net.poundex.xmachinery.spring.graphql.batch.UniverseLoader
import net.poundex.xmachinery.test.BatchLoadingUtils
import net.poundex.xmachinery.test.PublisherUtils
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import spock.lang.Specification
import spock.lang.Subject

class MarkableControllerSpec extends Specification implements BatchLoadingUtils, PublisherUtils {
	
	MarkerRepository repository = Stub()
	MarkerService markerService = Stub()
	
	Markable markable = Stub() {
		getMarkers() >> ["m1", "m2", "m3"].toSet()
	}
	
	Marker m1 = Stub() {
		getId() >> "m1"
		getMarkers() >> "m4"
	}
	Marker m2 = Stub() {
		getId() >> "m2"
	}
	Marker m3 = Stub() {
		getId() >> "m3"
	}
	Marker m4 = Stub() {
		getId() >> "m4"
	}


	@Subject
	MarkableController controller = new MarkableController(repository, markerService)
	
	void "Returns markers"() {
		given:
		ObjectLoader<String, Marker> markerLoader = Stub() {
			loadMany(markable.markers) >> Mono.just([m1, m2, m3])
		}

		when:
		List<? extends Marker> r = block controller.markers(markable, markerLoader)
		
		then:
		r == [m1, m2, m3]
	}
	
	void "Loads markers"() {
		given:
		String id1 = "m1"
		String id2 = "m2"
		String id3 = "m3"
		Set<String> ids = [id1, id2, id3].toSet()
		
		and:
		repository.findAllById(ids) >> Flux.just(m1, m2, m3)

		when:
		Map<String, Marker> loaded = block controller.markersLoader(ids)
		
		then:
		loaded[id1] == m1
		loaded[id2] == m2
		loaded[id3] == m3
	}
	
	void "Returns Markable's effective markers"() {
		given:
		UniverseLoader<String, Marker> markersLoader = Stub() {
			load() >> Mono.just([(m1.id): m1, (m2.id): m2, (m3.id): m3, (m4.id): m4])
		}
		markerService.getEffectiveMarkers(markable, false, _ as Map) >> [m1, m2, m3, m4].toSet()

		when:
		Set<? extends Marker> r = block controller.effectiveMarkers(markable, false, markersLoader)

		then:
		r == [m1, m2, m3, m4].toSet()
	}

	void "Returns Markable's effective markers excluding direct"() {
		given:
		UniverseLoader<String, Marker> markersLoader = Stub() {
			load() >> Mono.just([(m1.id): m1, (m2.id): m2, (m3.id): m3, (m4.id): m4])
		}
		markerService.getEffectiveMarkers(markable, true, _ as Map) >> [m4].toSet()

		when:
		Set<? extends Marker> r = block controller.effectiveMarkers(markable, true, markersLoader)

		then:
		r == [m4].toSet()
	}

	void "Loads all markers"() {
		given:
		String id1 = "m1"
		String id2 = "m2"
		String id3 = "m3"
		Set<String> ids = [id1, id2, id3].toSet()
		
		and:
		Ledger ledger = Ledger.builder().id("lid").build()

		and:
		repository.findAll(QMarker.marker.ledger.eq("lid")) >> Flux.just(m1, m2, m3)

		when:
		Map<String, Marker> loaded = block controller.markerUniverseLoader(Mono.just(ledger))

		then:
		loaded[id1] == m1
		loaded[id2] == m2
		loaded[id3] == m3
	}

}
