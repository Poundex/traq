package net.poundex.traq.marker.realiser

import com.querydsl.core.types.ExpressionUtils
import net.poundex.traq.ledger.Ledger
import net.poundex.traq.marker.*
import reactor.core.publisher.Flux
import spock.lang.Specification
import spock.lang.Subject

import static net.poundex.traq.marker.QBaseProperty.baseProperty
import static net.poundex.traq.marker.QProperty.property

class PropertyRealiserSpec extends Specification {

	Ledger ledger = Ledger.builder().id("lid").name("some-ledger").build()
	BasePropertyRepository basePropertyRepository = Mock() {
		saveAll(Collections.emptySet()) >> Flux.empty()
	}
	PropertyRepository propertyRepository = Mock() {
		saveAll(Collections.emptySet()) >> Flux.empty()
	}

	@Subject
	PropertyRealiser realiser = new PropertyRealiser(basePropertyRepository, propertyRepository)

	void "Returns empty for nothing recognised"() {
		when:
		List<Marker> markers = realiser.realiseMarkers(ledger.id, "one two three four five")
				.collectList().block()

		then:
		markers.empty
	}

	void "Returns existing marker"() {
		given:
		BaseProperty base = BaseProperty.builder().name("foo").ledger(ledger.id).build()
		Property prop = Property.builder().name("foo").value("bar").ledger(ledger.id).build()

		when:
		List<Marker> markers = realiser.realiseMarkers(ledger.id, "one two :foo=bar")
				.collectList().block()

		then:
		1 * basePropertyRepository.findAll(baseProperty.nameLower.in(["foo"]).and(QMarker.marker.ledger.eq(ledger.id))) >> Flux.just(base)
		1 * propertyRepository.findAll(
				property.name.equalsIgnoreCase("foo").and(property.value.equalsIgnoreCase("bar")).and(QMarker.marker.ledger.eq(ledger.id))) >> Flux.just(prop)

		and:
		markers.size() == 2
		markers.containsAll(base, prop)
	}

	void "Creates when required and returns new marker"() {
		given:
		BaseProperty base = BaseProperty.builder().name("foo").build()
		Property prop = Property.builder().name("foo").value("bar").build()

		BaseProperty base2 = BaseProperty.builder().name("baz").ledger(ledger.id).build()
		Property prop2 = Property.builder().name("baz").value("bat").ledger(ledger.id).build()

		BaseProperty base3 = BaseProperty.builder().name("blah").ledger(ledger.id).build()
		Property prop3 = Property.builder().name("blah").ledger(ledger.id).value("bang").build()

		when:
		List<Marker> markers = realiser.realiseMarkers(ledger.id, "one two :foo=bar :baz=bat :blah=bang")
				.collectList().block()

		then:
		1 * basePropertyRepository.findAll(baseProperty.nameLower.in(["foo", "baz", "blah"].toSet()).and(QMarker.marker.ledger.eq(ledger.id))) >> Flux.just(base, base2)
		1 * basePropertyRepository.saveAll(Set.of(base3)) >> Flux.just(base3)
		0 * basePropertyRepository._(*_)

		and:
		1 * propertyRepository.findAll(ExpressionUtils.and(ExpressionUtils.anyOf(
				property.name.equalsIgnoreCase("blah").and(property.value.equalsIgnoreCase("bang")),
				property.name.equalsIgnoreCase("baz").and(property.value.equalsIgnoreCase("bat")),
				property.name.equalsIgnoreCase("foo").and(property.value.equalsIgnoreCase("bar"))), QMarker.marker.ledger.eq(ledger.id))) >> Flux.just(prop)
		1 * propertyRepository.saveAll(Set.of(prop2, prop3)) >> Flux.just(prop2, prop3) 
		0 * propertyRepository._(*_)

		and:
		markers.size() == 6
		markers.containsAll(base, base2, base3, prop, prop2, prop3)
	}

	void "Existing BaseProperty search is case-insensitive when already exists"() {
		given:
		BaseProperty base = BaseProperty.builder().name("foo").ledger(ledger.id).build()
		Property prop = Property.builder().name("foo").value("bar").ledger(ledger.id).build()

		when:
		List<Marker> markers = realiser.realiseMarkers(ledger.id, "one two :Foo=bar")
				.collectList().block()

		then:
		1 * basePropertyRepository.findAll(baseProperty.nameLower.in(["foo"]).and(QMarker.marker.ledger.eq(ledger.id))) >> Flux.just(base)
		1 * propertyRepository.findAll(
				property.name.equalsIgnoreCase("Foo").and(property.value.equalsIgnoreCase("bar")).and(QMarker.marker.ledger.eq(ledger.id))) >> Flux.just(prop)

		and:
		markers.size() == 2
		markers.containsAll(base, prop)
	}
	
	void "Existing Property search is case-insensitive when already exists"() {
		given:
		BaseProperty base = BaseProperty.builder().name("foo").ledger(ledger.id).build()
		Property prop = Property.builder().name("foo").value("bar").ledger(ledger.id).build()

		when:
		List<Marker> markers = realiser.realiseMarkers(ledger.id, "one two :Foo=Bar")
				.collectList().block()

		then:
		1 * basePropertyRepository.findAll(baseProperty.nameLower.in(["foo"]).and(QMarker.marker.ledger.eq(ledger.id))) >> Flux.just(base)
		1 * propertyRepository.findAll(
				property.name.equalsIgnoreCase("Foo").and(property.value.equalsIgnoreCase("Bar")).and(QMarker.marker.ledger.eq(ledger.id))) >> Flux.just(prop)

		and:
		markers.size() == 2
		markers.containsAll(base, prop)
	}

	void "BaseProperties that do not already exist are case-insensitive within same input"() {
		given:
		BaseProperty base = BaseProperty.builder().name("baz").ledger(ledger.id).build()
		Property prop = Property.builder().name("baz").value("bat").ledger(ledger.id).build()

		when:
		List<Marker> markers = realiser.realiseMarkers(ledger.id, ":baz=bat :Baz=BAT")
				.collectList().block()

		then:
		1 * basePropertyRepository.findAll(baseProperty.nameLower.in(["baz"].toSet())
				.and(QMarker.marker.ledger.eq(ledger.id))) >> Flux.empty()
		1 * basePropertyRepository.saveAll(Set.of(base)) >> Flux.just(base)
		0 * basePropertyRepository._(*_)

		and:
		1 * propertyRepository.findAll(ExpressionUtils.and(ExpressionUtils.anyOf(
				property.name.equalsIgnoreCase("baz").and(property.value.equalsIgnoreCase("bat"))),
				QMarker.marker.ledger.eq(ledger.id))) >> Flux.empty()
		1 * propertyRepository.saveAll(Set.of(prop)) >> Flux.just(prop)
		0 * propertyRepository._(*_)

		and:
		markers.size() == 2
		markers.containsAll(base, prop)
	}
	
}
