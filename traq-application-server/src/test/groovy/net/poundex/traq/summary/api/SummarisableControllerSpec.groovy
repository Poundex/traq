package net.poundex.traq.summary.api

import graphql.GraphQLContext
import net.poundex.traq.marker.*
import net.poundex.traq.marker.api.MarkerApiConverters
import net.poundex.traq.summary.*
import net.poundex.traq.timeline.TimelinePoint
import net.poundex.traq.timeline.TimelineSpan
import net.poundex.xmachinery.spring.graphql.batch.ObjectLoader
import net.poundex.xmachinery.spring.graphql.batch.UniverseLoader
import net.poundex.xmachinery.test.PublisherUtils
import reactor.core.publisher.Mono
import spock.lang.Specification
import spock.lang.Subject

import java.time.Duration
import java.util.function.BiFunction

class SummarisableControllerSpec extends Specification implements PublisherUtils {
	
	TimelinePoint event1 = TimelinePoint.builder().markers(Set.of("1")).build()
	TimelineSpan event2 = TimelineSpan.builder().markers(Set.of("2")).build()
	Summarisable summarisable = Stub() {
		getEvents() >> [event1, event2]
	}
	GraphQLContext context = GraphQLContext.newContext().build()
	SummaryService summaryService = Mock()
	MarkerApiConverters markerApiConverters = Stub()
	
	@Subject
	SummarisableController controller = new SummarisableController(summaryService, markerApiConverters)
	
	void "Returns empty MarkerSummary and populates context correctly"() {
		given:
		List<? extends Marker> someTags = [Tag.builder().id("1").build(), Tag.builder().id("2").build()]
		ObjectLoader<String, ? extends Marker> markerLoader = Stub() {
			loadMany(Set.of("1", "2")) >> Mono.just(someTags)
		}
		
		when:
		MarkerSummaryDto summary = block controller.markerSummary(summarisable, markerLoader, context)

		then:
		! summary.count()
		! summary.duration()
		! summary.propertyValues()

		and:
		with(context.get("markerSummaryContext") as SummarisableController.MarkerSummaryContext) {
			it.summarisable() == summarisable
			block(it.markerUniverse()).values().containsAll(someTags)
			block(it.markerUniverse()).values().containsAll(someTags)
		}
		
		when:
		(context.get("markerSummaryContext") as SummarisableController.MarkerSummaryContext).with {
			it.countFn().apply(null, null)
			it.durationFn().apply(null, null)
			it.propertyValuesFn().apply(null, null)
		}
		
		then:
		1 * summaryService.getMarkerCount(_, _)
		1 * summaryService.getMarkerDurations(_, _)
		1 * summaryService.getPropertyValues(_, _)
	}
	
	void "Returns marker count and existing values"() {
		given:
		MarkerCountDto markerCountDto = new MarkerCountDto(null, 0)
		ObjectLoader<Summarisable, Set<MarkerCountDto>> countDtoLoader = Stub() {
			load(summarisable) >> Mono.just(Set.of(markerCountDto))
		}

		when:
		Set<MarkerCountDto> summary = block controller.count(new SummarisableController.MarkerSummaryContext(summarisable, null, null, null, null), countDtoLoader)
		
		then:
		summary.size() == 1
		summary.first() == markerCountDto
	}

	void "Loads count summaries"() {
		given:
		Marker marker = Tag.builder().build()
		Map<String, ? extends Marker> mu = ["1": marker]
		BiFunction<Summarisable, Map<String, ? extends Marker>, Map<Marker, Integer>> countFn = Mock()
		SummarisableController.MarkerSummaryContext context = new SummarisableController.MarkerSummaryContext(
				summarisable, Mono.just(mu), countFn, null, null)
		
		and:
		MarkerDto markerDto = TagDto.builder().build()
		markerApiConverters.toDto(marker) >> markerDto

		when:
		Map<Summarisable, Set<MarkerCountDto>> r = 
				block controller.countSummaryLoader(Set.of(summarisable), context)
		
		then:
		1 * countFn.apply(summarisable, mu) >> [(marker): 2]
		
		and:
		r.size() == 1
		r[summarisable].size() == 1
		with(r[summarisable].first()) {
			it.marker() == markerDto
			it.value() == 2
		}
	}
	
	void "Returns property values and existing values"() {
		given:
		MarkerPropertyValuesDto dto = new MarkerPropertyValuesDto(null, Collections.emptySet())
		ObjectLoader<Summarisable, Set<MarkerPropertyValuesDto>> countDtoLoader = Stub() {
			load(summarisable) >> Mono.just(Set.of(dto))
		}

		when:
		Set<MarkerPropertyValuesDto> summary = block controller.propertyValues(new SummarisableController.MarkerSummaryContext(summarisable, null, null, null, null), countDtoLoader)

		then:
		summary.size() == 1
		summary.first() == dto
	}
	
	void "Loads property values"() {
		given:
		BaseProperty marker = BaseProperty.builder().build()
		Map<String, ? extends Marker> mu = ["1": marker]
		BiFunction<Summarisable, Map<String, ? extends Marker>, Map<Marker, Set<String>>> valuesFn = Mock()
		SummarisableController.MarkerSummaryContext context = new SummarisableController.MarkerSummaryContext(
				summarisable, Mono.just(mu), null, valuesFn, null)

		and:
		BasePropertyDto markerDto = new BasePropertyDto(null, null, null, null, null, null)
		markerApiConverters.toDto(marker) >> markerDto

		when:
		Map<Summarisable, Set<MarkerPropertyValuesDto>> r =
				block controller.propertyValuesLoader(Set.of(summarisable), context)

		then:
		1 * valuesFn.apply(summarisable, mu) >> [(marker): Set.of("one", "two")]
		
		and:
		r.size() == 1
		r[summarisable].size() == 1
		with(r[summarisable].first()) {
			it.marker() == markerDto
			it.value() == Set.of("one", "two")
		}
	}
	
	void "Returns durations and existing values"() {
		given:
		MarkerDurationDto dto = new MarkerDurationDto(null, null)
		ObjectLoader<Summarisable, Set<MarkerDurationDto>> loader = Stub() {
			load(summarisable) >> Mono.just(Set.of(dto))
		}

		when:
		Set<MarkerDurationDto> summary = block controller.duration(new SummarisableController.MarkerSummaryContext(summarisable, null, null, null, null), loader)

		then:
		summary.size() == 1
		summary.first() == dto
	}
	
	void "Loads durations"() {
		given:
		Tag marker = Tag.builder().build()
		Map<String, ? extends Marker> mu = ["1": marker]
		BiFunction<Summarisable, Map<String, ? extends Marker>, Map<Marker, Duration>> durationFn = Mock()
		SummarisableController.MarkerSummaryContext context = new SummarisableController.MarkerSummaryContext(
				summarisable, Mono.just(mu), null, null, durationFn)

		and:
		BasePropertyDto markerDto = new BasePropertyDto(null, null, null, null, null, null)
		markerApiConverters.toDto(marker) >> markerDto

		when:
		Map<Summarisable, Set<MarkerDurationDto>> r =
				block controller.markerDurations(Set.of(summarisable), context)

		then:
		1 * durationFn.apply(summarisable, mu) >> [(marker): Duration.ofHours(2)]
		
		and:
		r.size() == 1
		r[summarisable].size() == 1
		with(r[summarisable].first()) {
			it.marker() == markerDto
			it.value() == Duration.ofHours(2)
		}
	}

	void "Returns empty MarkerSummary and populates context correctly for effective"(Boolean excludeDirectArg, boolean excludeDirect) {
		given:
		Map<String, Tag> someTags = ["1": Tag.builder().id("1").build(), "2": Tag.builder().id("2").build()]
		UniverseLoader<String, ? extends Marker> markerLoader = Stub() {
			load() >> Mono.just(someTags)
		}

		when:
		MarkerSummaryDto summary = block controller.effectiveMarkerSummary(summarisable, excludeDirectArg, markerLoader, context)

		then:
		! summary.count()
		! summary.duration()
		! summary.propertyValues()

		and:
		with(context.get("markerSummaryContext") as SummarisableController.MarkerSummaryContext) {
			it.summarisable() == summarisable
			block(it.markerUniverse()).values().containsAll(someTags.values())
			block(it.markerUniverse()).values().containsAll(someTags.values())
		}

		when:
		(context.get("markerSummaryContext") as SummarisableController.MarkerSummaryContext).with {
			it.countFn().apply(null, null)
			it.durationFn().apply(null, null)
			it.propertyValuesFn().apply(null, null)
		}

		then:
		1 * summaryService.getEffectiveMarkerCount(_, excludeDirect, _)
		1 * summaryService.getEffectiveMarkerDurations(_, excludeDirect, _)
		1 * summaryService.getEffectivePropertyValues(_, excludeDirect, _)

		where:
		excludeDirectArg || excludeDirect
		null             || false
		false            || false
		true             || true
	}
}
