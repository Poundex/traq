package net.poundex.traq.summary

import net.poundex.traq.marker.*
import net.poundex.traq.timeline.TimelinePoint
import net.poundex.traq.timeline.TimelineSpan
import spock.lang.Specification
import spock.lang.Subject

import java.time.Duration
import java.time.Instant

class SummaryServiceSpec extends Specification {
	
	Marker one = Tag.builder().id("1").name("one").build()
	Marker two = Tag.builder().id("2").name("two").build()
	Marker three = Tag.builder().id("3").name("three").build()
	
	BaseProperty four = BaseProperty.builder().id("4").name("four").build()
	Property fourOne = Property.builder().id("5").name("four").value("one").build()
	Property fourTwo = Property.builder().id("6").name("four").value("two").build()
	
	Marker five = Tag.builder().id("7").name("five").markers(Set.of(one.id)).build()
	Marker six = Tag.builder().id("8").name("six").markers(Set.of(one.id)).build()
	Marker seven = Tag.builder().id("9").name("seven").markers(Set.of(two.id)).build()
	
	Marker eight = Tag.builder().id("10").name("eight").markers(Set.of(fourOne.id)).build()
	

	Summarisable summarisable = Stub()
	MarkerService markerService = Stub()
	
	@Subject
	SummaryService service = new SummaryService(markerService)
	
	void "Returns correct marker counts"() {
		given:
		Map<String, ? extends Marker> mu = [(one.id): one, (two.id): two, (three.id): three]
		summarisable.getEvents() >> [
				TimelinePoint.builder().markers(Set.of(one.id)).build(),
				TimelinePoint.builder().markers(Set.of(one.id)).build(),
				TimelineSpan.builder().markers(Set.of(one.id)).build(),
				TimelinePoint.builder().markers(Set.of(two.id)).build(),
				TimelinePoint.builder().markers(Set.of(three.id, one.id)).build(),
		]
	
		when:
		Map<Marker, Integer> count = service.getMarkerCount(summarisable, mu)
		
		then:
		count[one] == 3
		count[two] == 1
		count[three] == 1
	}
	
	void "Returns property values"() {
		given:
		Map<String, ? extends Marker> mu = [(four.id): four, (fourOne.id): fourOne, (fourTwo.id): fourTwo]
		summarisable.getEvents() >> [
				TimelinePoint.builder().markers(Set.of(four.id, fourOne.id)).build(),
				TimelinePoint.builder().markers(Set.of(four.id, fourOne.id)).build(),
				TimelineSpan.builder().markers(Set.of(four.id, fourTwo.id)).build(),
				TimelineSpan.builder().markers(Set.of(four.id, fourTwo.id)).build()
		]
		
		when:
		Map<BaseProperty, Set<String>> values = service.getPropertyValues(summarisable, mu)
		
		then:
		values.size() == 1
		values[four] == ["one", "two"].toSet()
	}
	
	void "Returns marker durations"() {
		given:
		Map<String, ? extends Marker> mu = [
				(one.id): one, (two.id): two, (three.id): three, (four.id): four, (fourOne.id): fourOne, (fourTwo.id): fourTwo]
		Instant t1 = Instant.now().minusSeconds(60 * 60 * 24)
		Instant t2 = Instant.now().minusSeconds(60 * 60 * 24 * 2)
		Instant t3 = Instant.now().minusSeconds(60 * 60 * 24 * 3)
		
		summarisable.getEvents() >> [
				TimelineSpan.builder().time(t1).endTime(t1.plusSeconds(60 * 60 * 2)).markers(Set.of(one.id, two.id, four.id, fourOne.id)).build(),
				TimelineSpan.builder().time(t2).endTime(t2.plusSeconds(60 * 60 * 2)).markers(Set.of(one.id, three.id)).build(),
				TimelineSpan.builder().time(t3).endTime(t3.plusSeconds(60 * 60 * 2)).markers(Set.of(one.id, three.id, four.id, fourTwo.id)).build(),
		]

		when:
		Map<Marker, Duration> durations = service.getMarkerDurations(summarisable, mu)

		then:
		durations.size() == 5
		durations[one] == Duration.ofHours(6)
		durations[two] == Duration.ofHours(2)
		durations[three] == Duration.ofHours(4)
		durations[fourOne] == Duration.ofHours(2)
		durations[fourTwo] == Duration.ofHours(2)
	}

	void "Returns correct effective marker counts"() {
		given:
		Map<String, ? extends Marker> mu = [(one.id): one, (two.id): two, (three.id): three, (five.id): five, (six.id): six, (seven.id): seven]

		TimelinePoint point1 = TimelinePoint.builder().markers(Set.of(five.id)).build()
		TimelinePoint point2 = TimelinePoint.builder().markers(Set.of(six.id)).build()
		TimelinePoint point3 = TimelinePoint.builder().markers(Set.of(seven.id)).build()
		summarisable.getEvents() >> [point1, point2, point3]
		
		and:
		markerService.getEffectiveMarkers(point1, false, _) >> Set.of(five, one)
		markerService.getEffectiveMarkers(point2, false, _) >> Set.of(six, one)
		markerService.getEffectiveMarkers(point3, false, _) >> Set.of(seven, two)

		when:
		Map<Marker, Integer> count = service.getEffectiveMarkerCount(summarisable, false, mu)

		then:
		count[one] == 2
		count[two] == 1
		count[five] == 1
		count[six] == 1
		count[seven] == 1
	}

	void "Returns effective property values"() {
		given:
		Map<String, ? extends Marker> mu = [(four.id): four, (fourOne.id): fourOne, (fourTwo.id): fourTwo, (eight.id): eight]

		TimelinePoint point1 = TimelinePoint.builder().markers(Set.of(four.id, fourTwo.id)).build()

		TimelinePoint point2 = TimelinePoint.builder().markers(Set.of(eight.id)).build()
		summarisable.getEvents() >> [point1, point2]
		
		and:
		markerService.getEffectiveMarkers(point1, false, _) >> Set.of(four, fourTwo)
		markerService.getEffectiveMarkers(point2, false, _) >> Set.of(four, fourOne, eight)

		when:
		Map<BaseProperty, Set<String>> values = service.getEffectivePropertyValues(summarisable, false, mu)

		then:
		values.size() == 1
		values[four] == ["one", "two"].toSet()
	}

	void "Returns effective marker durations"() {
		given:
		Map<String, ? extends Marker> mu = [
				(one.id): one, 
				(two.id): two, 
				(three.id): three, 
				(four.id): four,
				(fourOne.id): fourOne, 
				(fourTwo.id): fourTwo,
				(five.id): five,
				(six.id): six,
				(seven.id): seven,
				(eight.id): eight]
				
		Instant t1 = Instant.now().minusSeconds(60 * 60 * 24)
		Instant t2 = Instant.now().minusSeconds(60 * 60 * 24 * 2)
		Instant t3 = Instant.now().minusSeconds(60 * 60 * 24 * 3)

		TimelineSpan span1 = TimelineSpan.builder().time(t1).endTime(t1.plusSeconds(60 * 60 * 2)).markers(Set.of(five.id, six.id)).build()
		TimelineSpan span2 = TimelineSpan.builder().time(t2).endTime(t2.plusSeconds(60 * 60 * 2)).markers(Set.of(seven.id, fourOne.id)).build()
		TimelineSpan span3 = TimelineSpan.builder().time(t3).endTime(t3.plusSeconds(60 * 60 * 2)).markers(Set.of(eight.id)).build()
		summarisable.getEvents() >> [span1, span2, span3]
		
		and:
		markerService.getEffectiveMarkers(span1, false, _) >> Set.of(five, six, one)
		markerService.getEffectiveMarkers(span2, false, _) >> Set.of(seven, two, fourOne)
		markerService.getEffectiveMarkers(span3, false, _) >> Set.of(eight, fourOne)

		when:
		Map<Marker, Duration> durations = service.getEffectiveMarkerDurations(summarisable, false, mu)

		then:
		durations.size() == 7
		durations[one] == Duration.ofHours(2)
		durations[two] == Duration.ofHours(2)
		durations[fourOne] == Duration.ofHours(4)
		durations[five] == Duration.ofHours(2)
		durations[six] == Duration.ofHours(2)
		durations[seven] == Duration.ofHours(2)
		durations[eight] == Duration.ofHours(2)
	}
}
