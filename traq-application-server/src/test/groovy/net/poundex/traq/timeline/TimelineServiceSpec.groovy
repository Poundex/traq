package net.poundex.traq.timeline

import com.querydsl.core.types.dsl.BooleanExpression
import net.poundex.traq.ledger.Ledger
import net.poundex.traq.marker.Marker
import net.poundex.traq.marker.Tag
import net.poundex.traq.marker.realiser.MarkerRealiserService
import net.poundex.traq.timeline.query.TimelineQuery
import net.poundex.xmachinery.test.PublisherUtils
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import spock.lang.Specification
import spock.lang.Subject

import java.time.Duration
import java.time.Instant
import java.time.LocalDate

import static net.poundex.traq.timeline.QTimelineEvent.timelineEvent
import static net.poundex.traq.timeline.QTimelineSpan.timelineSpan

class TimelineServiceSpec extends Specification implements PublisherUtils {

	static LocalDate today = LocalDate.now()
	static Instant now = Instant.now()

	Ledger ledger = Ledger.builder().id("lid").build()

	EventRepository eventRepository = Mock()
	DayGuesser dayGuesser = Mock()
	MarkerRealiserService markerRealiserService = Stub()
	SpanRepository spanRepository = Mock()

	@Subject
	TimelineService service = new TimelineService(eventRepository, dayGuesser, markerRealiserService, spanRepository)

	void "Creates TimelinePoints"() {
		given:
		TimelinePoint pointToCreate = new TimelinePoint(null, today, "some-description", now, Collections.emptySet(), ledger.id)

		and:
		markerRealiserService.realiseMarkersInString(ledger.id, "some-description") >> Flux.empty()

		when:
		TimelinePoint point = block service.createPoint(now, today, "some-description", ledger)

		then:
		1 * dayGuesser.getCurrentDayBestGuess(today) >> Mono.just(today)
		1 * eventRepository.save(pointToCreate) >> Mono.just(pointToCreate)

		and:
		point == pointToCreate
	}

	void "Creates TimelinePoints with markers"() {
		given:
		String description = "some-description +blue"
		Tag tag = Tag.builder().id("id").name("blue").build()
		TimelinePoint pointToCreate = new TimelinePoint(null, today, description, now, Set.of(tag.getId()), ledger.id)

		and:
		dayGuesser.getCurrentDayBestGuess(today) >> Mono.just(today)
		markerRealiserService.realiseMarkersInString(ledger.id, description) >> Flux.just(tag)

		when:
		TimelinePoint point = block service.createPoint(now, today, description, ledger)

		then:
		1 * eventRepository.save(pointToCreate) >> Mono.just(pointToCreate)

		and:
		point == pointToCreate
	}

	void "Starts new spans"() {
		given:
		String description = "some-description +blue"
		Tag tag = Tag.builder().id("id").name("blue").build()
		TimelineSpan spanToCreate = new TimelineSpan(null, today, description, now, null, Set.of(tag.getId()), ledger.id)

		and:
		dayGuesser.getCurrentDayBestGuess(today) >> Mono.just(today)
		markerRealiserService.realiseMarkersInString(ledger.id, description) >> Flux.just(tag)
		spanRepository.findOne(_) >> Mono.empty()

		when:
		StartSpanInfo ssi = block service.startSpan(now, today, description, ledger)

		then:
		1 * eventRepository.save(spanToCreate) >> Mono.just(spanToCreate)

		and:
		ssi.span() == spanToCreate
		!ssi.previousSpan()
	}

	void "Stops active span"() {
		given:
		TimelineSpan previouslyActiveSpan = TimelineSpan.builder().id("1").build()

		when:
		TimelineSpan stoppedSpan = block service.stopActiveSpan(ledger)

		then:
		1 * spanRepository.findOne(timelineSpan.active.isTrue().and(timelineSpan.ledger.eq(ledger.id))) >> Mono.just(previouslyActiveSpan)
		1 * eventRepository.save(_ as TimelineSpan) >> { TimelineSpan it -> Mono.just(it) } 

		and:
		stoppedSpan.id == previouslyActiveSpan.id
		stoppedSpan.endTime
		! stoppedSpan.active
	}
	
	void "Starts new span, stopping active span"() {
		given:
		TimelineSpan first = new TimelineSpan("1", today, "first", now, null, Collections.emptySet(), ledger.id)
		TimelineSpan second = new TimelineSpan("2", today, "second", now, null, Collections.emptySet(), ledger.id)
		TimelineSpan first2 = first.toBuilder().endTime(now).build()

		and:
		dayGuesser.getCurrentDayBestGuess(today) >> Mono.just(today)
		markerRealiserService.realiseMarkersInString(ledger.id, _) >> Flux.empty()

		when:
		StartSpanInfo ssi = block service.startSpan(now, today, "second", ledger)

		then:
		1 * spanRepository.findOne(timelineSpan.active.isTrue().and(timelineSpan.ledger.eq(ledger.id))) >> Mono.just(first)
		1 * eventRepository.save({ s -> s.id == "1" && s.endTime }) >> Mono.just(first2)
		1 * eventRepository.save({ s -> s.description == "second" }) >> Mono.just(second)

		and:
		ssi.span() == second
		ssi.previousSpan() == first2
	}

	void "Creates manual Timeline Spans"() {
		given:
		Instant then = now.minusSeconds(60 * 60 * 2)
		TimelineSpan spanToCreate = new TimelineSpan(
				null, 
				today, 
				"some-description", 
				then,
				now,
				Collections.emptySet(), 
				ledger.id)

		and:
		dayGuesser.getCurrentDayBestGuess(today) >> Mono.just(today)
		markerRealiserService.realiseMarkersInString(ledger.id, "some-description") >> Flux.empty()

		when:
		TimelineSpan span = block service.createSpan(today, then, now, "some-description", ledger)

		then:
		1 * eventRepository.save(spanToCreate) >> Mono.just(spanToCreate)

		and:
		span == spanToCreate
	}
	
	void "Updates descriptions"() {
		given:
		String newDescription = "new-description"
		Tag tag = Tag.builder().id("tag-id").name("tag").build()
		TimelineSpan original = TimelineSpan.builder().id("1").ledger("ledger-id").description("original-description").build()
		TimelineSpan updated = original.toBuilder().description(newDescription).markers(Set.of("tag-id")).build()
		
		and:
		markerRealiserService.realiseMarkersInString("ledger-id", newDescription) >> Flux.just(tag)

		when:
		TimelineSpan r = block service.updateDescription(original, newDescription)

		then:
		1 * eventRepository.save(updated) >> Mono.just(updated)

		and:
		r.description == newDescription
	}
	
	void "Calculates event summaries for markers"() {
		given:
		Instant now = Instant.now()
		Tag food = Tag.builder().id("food").name("Food").build()
		Tag cake = Tag.builder().id("cake").name("Cake").markers(Set.of("food")).build()
		Tag cup = Tag.builder().id("cup").name("Cup").markers(Set.of("cake")).build()
		Tag birthday = Tag.builder().id("birthday").name("Birthday").markers(Set.of("cake")).build()
		Tag free = Tag.builder().id("free").name("Free").markers(Set.of("cake")).build()
		Map<String, Marker> muMap = [food, cake, cup, birthday, free].collectEntries { [it.id, it] }

		and:
		TimelineSpan freeSpan = TimelineSpan.builder()
				.time(now - Duration.ofMinutes(30))
				.endTime(now)
				.build()
		TimelineSpan cupSpan = TimelineSpan.builder()
				.time(now - Duration.ofMinutes(90))
				.endTime(now - Duration.ofMinutes(30))
				.build()
		TimelineSpan cakeSpan = TimelineSpan.builder()
				.time(now - Duration.ofMinutes(120))
				.endTime(now - Duration.ofMinutes(90))
				.build()
		
		and:
		BooleanExpression freePredicate = timelineEvent.markers.any().in(Set.of(free.id))
		TimelineQuery freeQuery = Stub() {
			getEventPredicate() >> freePredicate
		}
		BooleanExpression allCakesPredicate = timelineEvent.markers.any().in(Set.of(free.id, cup.id, birthday.id, cake.id))
		TimelineQuery allCakesQuery = Stub() {
			getEventPredicate() >> allCakesPredicate
		}
		
		when:
		EventSummary summary = block service.summary(ledger, freeQuery, null)
		
		then: 
		1 * eventRepository.findAll(freePredicate, timelineEvent.time.asc()) >> Flux.just(freeSpan)
		
		and:
		summary.duration() == Duration.ofMinutes(30)
		summary.events() == [freeSpan]

		when:
		EventSummary summary2 = block service.summary(ledger, allCakesQuery, null)

		then:
		1 * eventRepository.findAll(allCakesPredicate, timelineEvent.time.asc()) >> 
				Flux.just(cakeSpan, cupSpan, freeSpan)

		and:
		summary2.duration() == Duration.ofMinutes(120)
		summary2.events() == [cakeSpan, cupSpan, freeSpan]
	}

	void "Repeats last span"() {
		given:
		String description = "some-description"
		TimelineSpan lastSpan = new TimelineSpan(null, today, description, now, now, Collections.emptySet(), ledger.id)
		TimelineSpan expected = new TimelineSpan(null, today, description, now, null, Collections.emptySet(), ledger.id)

		and:
		dayGuesser.getCurrentDayBestGuess(today) >> Mono.just(today)
		markerRealiserService.realiseMarkersInString(ledger.id, description) >> Flux.empty()
		
		spanRepository.findOne(timelineSpan.active.isTrue().and(timelineSpan.ledger.eq(ledger.getId()))) >> Mono.empty()
		spanRepository.findAll(timelineSpan.ledger.eq(ledger.getId()), timelineSpan.time.desc()) >> Flux.just(lastSpan)
		spanRepository.findOne(_) >> Mono.empty()

		when:
		TimelineSpan span = block service.repeatLastSpan(now, today, ledger)

		then:
		1 * eventRepository.save(expected) >> Mono.just(expected)

		and:
		span.day == expected.day
		span.time == expected.time
		span.description == expected.description
		! span.endTime
		span.active
	}
	
	void "Repeats last span - throws when no last span to repeat"() {
		given:
		spanRepository.findAll(timelineSpan.ledger.eq(ledger.getId()), timelineSpan.time.desc()) >> Flux.empty()

		when:
		spanRepository.findOne(timelineSpan.active.isTrue().and(timelineSpan.ledger.eq(ledger.getId()))) >> Mono.empty()
		block service.repeatLastSpan(now, today, ledger)

		then:
		thrown(NoPreviousSpanException)
		
		and:
		0 * eventRepository.save(_) 
	}

	void "Repeats last span - throws when there is already an active span"() {
		given:
		String description = "some-description"
		TimelineSpan someSpan = new TimelineSpan(null, today, description, now, now, Collections.emptySet(), ledger.id)

		and:
		spanRepository.findOne(timelineSpan.active.isTrue().and(timelineSpan.ledger.eq(ledger.getId()))) >> Mono.just(someSpan)

		when:
		block service.repeatLastSpan(now, today, ledger)

		then:
		thrown(ActiveSpanExistsException)

		and:
		0 * eventRepository.save(_)
	}
}
