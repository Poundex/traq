package net.poundex.traq.timeline.mongo

import spock.lang.Specification
import spock.lang.Subject

import static net.poundex.traq.timeline.mongo.QMongoTimelineSpan.mongoTimelineSpan

class MongoSpanRepositorySpec extends Specification {
	@Subject
	MongoSpanRepository repository = Spy(TestableMongoSpanRepository)
	
	void "Returns correct predicate for completion status"() {
		expect:
		repository.active(true) == mongoTimelineSpan.endTime.isNull()
		repository.active(false) == mongoTimelineSpan.endTime.isNotNull()
	}
}
