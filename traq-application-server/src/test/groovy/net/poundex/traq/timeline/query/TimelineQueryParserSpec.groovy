package net.poundex.traq.timeline.query

import net.poundex.traq.marker.*
import net.poundex.traq.timeline.QTimelineEvent
import spock.lang.Specification
import spock.lang.Subject

class TimelineQueryParserSpec extends Specification {
	
	Tag tag1 = Tag.builder().id("1").name("tag").build()
	Tag tag2 = Tag.builder().id("2").name("tagtwo").build()
	BaseProperty bp = BaseProperty.builder().id("3").name("foo").build()
	Property prop = Property.builder().id("4").name("foo").value("bar").build()
	Tag tag3 = Tag.builder().id("5").name("tagthree").build()
	Tag tag4 = Tag.builder().id("6").name("tagtagfour").markers(Set.of("5")).build()
	
	Mention mention = Mention.builder().id("7").name("mention").build()

	MarkerService markerService = new MarkerService(null, null)

	@Subject
	TimelineQueryParser parser = new TimelineQueryParser(markerService)
	
	void "Parses single marker"() {
		when:
		TimelineQuery query = parser.parse("+tag", ["1": tag1])
		
		then:
		query.eventPredicate == QTimelineEvent.timelineEvent.markers.any().in(Set.of("1"))
		
		and:
		query.asString() == "+tag"
		
		when:
		TimelineQuery query2 = parser.parse("@mention", ["7": mention])

		then:
		query2.eventPredicate == QTimelineEvent.timelineEvent.markers.any().in(Set.of("7"))

		and:
		query2.asString() == "@mention"
	}

	void "Parses multiple marker"() {
		when:
		TimelineQuery query = parser.parse("+tag +tagtwo", ["1": tag1, "2": tag2])

		then:
		query.eventPredicate == QTimelineEvent.timelineEvent.markers.any().in(Set.of("1", "2"))

		and:
		with(query.asString().split(" OR ")) {
			[it[0], it[1]].toSet() == Set.of("+tag", "+tagtwo")
		}
	}
	
	void "Parses single base property"() {
		when:
		TimelineQuery query = parser.parse(":foo", ["3": bp, "4": prop])

		then:
		query.eventPredicate == QTimelineEvent.timelineEvent.markers.any().in(Set.of("3"))

		and:
		query.asString() == ":foo"	
	}

	void "Parses base property and value"() {
		when:
		TimelineQuery query = parser.parse(":foo=bar", ["3": bp, "4": prop])

		then:
		query.eventPredicate == QTimelineEvent.timelineEvent.markers.any().in(Set.of("4"))

		and:
		query.asString() == ":foo=bar"
	}

	void "Parses separate base property and value"() {
		when:
		TimelineQuery query = parser.parse(":foo :foo=bar", ["3": bp, "4": prop])

		then:
		query.eventPredicate == QTimelineEvent.timelineEvent.markers.any().in(Set.of("3", "4"))

		and:
		with(query.asString().split(" OR ")) {
			[it[0], it[1]].toSet() == Set.of(":foo", ":foo=bar")
		}
	}
	
	void "Throws on unexpected input"() {
		when:
		parser.parse("+tag blah", ["1": tag1])
		
		then:
		QueryException qex = thrown()
		qex.message == "Unexpected input: blah"
	}
	
	void "Includes related markers in query"() {
		when:
		TimelineQuery query = parser.parse("+tagthree", ["5": tag3, "6": tag4])

		then:
		query.eventPredicate == QTimelineEvent.timelineEvent.markers.any().in(Set.of("5", "6"))

		and:
		query.asString() == "+tagthree"
	}

	void "Marker names are case-insensitive"() {
		when:
		TimelineQuery query = parser.parse("+Tag", ["1": tag1])

		then:
		query.eventPredicate == QTimelineEvent.timelineEvent.markers.any().in(Set.of("1"))

		and:
		query.asString() == "+tag"

		when:
		TimelineQuery query2 = parser.parse("@MENTION", ["7": mention])

		then:
		query2.eventPredicate == QTimelineEvent.timelineEvent.markers.any().in(Set.of("7"))

		and:
		query2.asString() == "@mention"

		when:
		TimelineQuery query3 = parser.parse(":Foo", ["3": bp, "4": prop])

		then:
		query3.eventPredicate == QTimelineEvent.timelineEvent.markers.any().in(Set.of("3"))

		and:
		query3.asString() == ":foo"
		
		when:
		TimelineQuery query4 = parser.parse(":Foo=Bar", ["3": bp, "4": prop])

		then:
		query4.eventPredicate == QTimelineEvent.timelineEvent.markers.any().in(Set.of("4"))

		and:
		query4.asString() == ":foo=bar"
	}
}
