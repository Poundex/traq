package net.poundex.traq.timeline


import reactor.core.publisher.Flux
import spock.lang.Specification
import spock.lang.Subject

import java.time.LocalDate

import static net.poundex.traq.timeline.QTimelineSpan.timelineSpan

class DayGuesserSpec extends Specification {
	
	static LocalDate today = LocalDate.now()
	
	SpanRepository spanRepository = Mock()
	
	@Subject
	DayGuesser dayGuesser = new DayGuesser(spanRepository)
	
	void "Returns passed in day if not null"() {
		given:
		LocalDate inDate = LocalDate.of(2000, 1, 1)

		when:
		LocalDate r = dayGuesser.getCurrentDayBestGuess(inDate).block()
		
		then: 
		r == inDate
	}
	
	void "Returns day of current open span if found and passed in day is null"() {
		given:
		LocalDate day = LocalDate.of(2000, 1, 1)
		
		when:
		LocalDate r = dayGuesser.getCurrentDayBestGuess(null).block()
		
		then:
		1 * spanRepository.findAll(timelineSpan.active.isTrue()) >> Flux.just(
				TimelineSpan.builder().day(day).build())
		
		and:
		r == day
	}

	void "Returns today if wrong number of open spans found"() {
		given:
		LocalDate day = LocalDate.of(2000, 1, 1)

		when:
		LocalDate r = dayGuesser.getCurrentDayBestGuess(null).block()

		then:
		1 * spanRepository.findAll(timelineSpan.active.isTrue()) >> Flux.just(
				TimelineSpan.builder().id("1").day(day).build(),
				TimelineSpan.builder().id("2").day(day).build());

		and:
		r == today
		
		when:
		r = dayGuesser.getCurrentDayBestGuess(null).block()

		then:
		1 * spanRepository.findAll(timelineSpan.active.isTrue()) >> Flux.empty()

		and:
		r == today
	}
}
