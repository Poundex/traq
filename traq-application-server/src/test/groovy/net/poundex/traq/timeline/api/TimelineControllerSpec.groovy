package net.poundex.traq.timeline.api

import com.querydsl.core.types.Predicate
import net.poundex.traq.ledger.Ledger
import net.poundex.traq.ledger.LedgerRepository
import net.poundex.traq.marker.Marker
import net.poundex.traq.marker.Tag
import net.poundex.traq.timeline.*
import net.poundex.traq.timeline.query.TimelineQuery
import net.poundex.traq.timeline.query.TimelineQueryParser
import net.poundex.xmachinery.spring.graphql.batch.UniverseLoader
import net.poundex.xmachinery.test.PublisherUtils
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import spock.lang.Specification
import spock.lang.Subject

import java.time.Instant
import java.time.LocalDate
import java.time.ZoneId
import java.time.temporal.ChronoUnit

import static net.poundex.traq.timeline.QTimelineSpan.timelineSpan

class TimelineControllerSpec extends Specification implements PublisherUtils {

	static LocalDate today = LocalDate.now()
	static Instant now = Instant.now()
	
	Ledger ledger = Ledger.builder().id("defl").build()

	TimelineService timelineService = Mock()
	EventRepository eventRepository = Mock()
	SpanRepository spanRepository = Mock()
	PointRepository pointRepository = Mock()
	LedgerRepository ledgerRepository = Stub() {
		findById(ledger.id) >> Mono.just(ledger)
		findById(_) >> Mono.empty()
	}
	TimelineQueryParser timelineQueryParser = Mock()

	@Subject
	TimelineController controller = new TimelineController(timelineService, eventRepository, spanRepository, pointRepository, timelineQueryParser)

	void "Creates Timeline Points"() {
		given:
		TimelinePoint timelinePoint = new TimelinePoint("some-id", today, "some-description", now, Collections.emptySet(), "defl")

		when:
		TimelinePoint point = block controller.createPoint(
				new CreatePointInputDto(null, null, "some-description"), Mono.just(ledger))

		then:
		1 * timelineService.createPoint(null, null, "some-description", ledger) >>
				Mono.just(timelinePoint)

		and:
		point.id == "some-id"
		point.time.truncatedTo(ChronoUnit.HOURS) == now.truncatedTo(ChronoUnit.HOURS)
		point.day == LocalDate.ofInstant(now, ZoneId.systemDefault())
		point.description == "some-description"
	}

	void "Returns events for Predicate"() {
		given:
		Predicate predicate = Stub()
		TimelineEvent event = Stub()

		when:
		List<TimelineEvent> events = controller.events(predicate).collectList().block()

		then:
		1 * eventRepository.findAll(predicate) >> Flux.just(event)
		events == [event]
	}

	void "Returns spans for Predicate"() {
		given:
		Predicate predicate = Stub()
		TimelineSpan span = new TimelineSpan("some-id", today, "some-description", now, null, Collections.emptySet(), "defl")

		when:
		List<TimelineSpan> spans = controller.spans(predicate, null).collectList().block()

		then:
		1 * spanRepository.findAll(predicate, _) >> Flux.just(span)
		spans == [span]
	}

	void "Returns points for Predicate"() {
		given:
		Predicate predicate = Stub()
		TimelinePoint point = new TimelinePoint("some-id", today, "some-description", now, Collections.emptySet(), "defl")

		when:
		List<TimelinePoint> points = controller.points(predicate).collectList().block()

		then:
		1 * pointRepository.findAll(predicate) >> Flux.just(point)
		points == [point]
	}

	void "Creates Timeline Points with markers"() {
		given:
		String description = "some-description +blue"
		Tag tag = Tag.builder().id("1").name("blue").build()
		TimelinePoint timelinePoint = new TimelinePoint("some-id", today, description, now, Set.of(tag.id), "defl")
		
		and:
		timelineService.createPoint(null, null, description, ledger) >> Mono.just(timelinePoint)

		when:
		TimelinePoint point = block controller.createPoint(new CreatePointInputDto(null, null, description), Mono.just(ledger))

		then:
		point.markers == [tag.id].toSet()
	}
	
	void "Starts new TimelineSpans"() {
		given:
		String description = "some-description +blue"
		Tag tag = Tag.builder().id("1").name("blue").build()
		TimelineSpan span = new TimelineSpan("some-id", today, description, now, null, Set.of(tag.id), "defl")
		StartSpanInfo startSpanInfo = new StartSpanInfo(span, null)
		
		and:
		timelineService.startSpan(null, null, description, ledger) >> Mono.just(startSpanInfo)

		when:
		StartSpanInfo infoDto = block controller.startSpan(new StartSpanInputDto(null, null, description), Mono.just(ledger))

		then:
		infoDto.span() == span
		! infoDto.previousSpan()
	}

	void "Stops active Span"() {
		given:
		String description = "some-description +blue"
		Tag tag = Tag.builder().id("1").name("blue").build()
		TimelineSpan span = new TimelineSpan("some-id", today, description, now, null, Set.of(tag.id), "defl")

		and:
		timelineService.stopActiveSpan(ledger) >> Mono.just(span)

		when:
		TimelineSpan r = controller.stopActiveSpan(Mono.just(ledger)).block()

		then:
		r == span
	}

	void "Returns recent spans"() {
		given:
		Predicate predicate = Stub()
		TimelineSpan span1 = new TimelineSpan("some-id", today, "some-description1", now, null, Collections.emptySet(), "defl")
		TimelineSpan span2 = new TimelineSpan("some-id", today, "some-description1", now, null, Collections.emptySet(), "defl")
		TimelineSpan span3 = new TimelineSpan("some-id", today, "some-description2", now, null, Collections.emptySet(), "defl")
		TimelineSpan span4 = new TimelineSpan("some-id", today, "some-description3", now, null, Collections.emptySet(), "defl")
		
		when:
		List<TimelineSpan> spans = block controller.spans(predicate, 2).collectList()

		then:
		1 * spanRepository.findAll(predicate, timelineSpan.time.desc()) >> Flux.just(span1, span2, span3, span4)
		spans == [span1, span3]
	}
	
	void "Creates manual Timeline Spans"() {
		given:
		Instant then = now.minusSeconds(60 * 60 * 2)
		TimelineSpan timelineSpan = new TimelineSpan(
				"some-id", 
				today, 
				"some-description", 
				then,
				now, 
				Collections.emptySet(), 
				"defl")
		
		when:
		TimelineSpan span = block controller.createSpan(
				new CreateSpanInputDto(then, now, today, "some-description"), Mono.just(ledger))

		then:
		1 * timelineService.createSpan(
				today, then, now, "some-description", ledger) >> Mono.just(timelineSpan)

		and:
		span.id == "some-id"
		span.time == then
		span.endTime == now
		span.day == today
		span.description == "some-description"
	}
	
	void "Updates descriptions"() {
		given:
		TimelineSpan original = TimelineSpan.builder()
				.id("some-id")
				.description("original-description")
				.build()
		TimelineSpan updated = original.toBuilder().description("the-new-description").build()
		
		when:
		TimelineEvent event = block controller.updateDescription("some-id", "the-new-description")
		
		then:
		1 * eventRepository.findById("some-id") >> Mono.just(original)
		1 * timelineService.updateDescription(original, "the-new-description") >> Mono.just(updated)
		
		and:
		event.description == "the-new-description"
	}
	
	void "Returns events by ID"() {
		given:
		TimelineEvent event = Stub()

		when:
		TimelineEvent r = block controller.event("some-id")

		then:
		1 * eventRepository.findById("some-id") >> Mono.just(event)

		and: 
		r == event
	}
	
	void "Returns event summary for markers"() {
		given:
		Tag one = Tag.builder().id("1").name("one").build()
		Tag two = Tag.builder().id("2").name("two").build()
		Tag three = Tag.builder().id("3").name("three").build()
		EventSummary expected = new EventSummary(null, null, null, null, null)
		
		and:
		Map<String, Tag> muMap = Map.of("1", one, "2", two, "3", three)
		UniverseLoader<String, Marker> markerUniverseLoader = Stub() {
			load() >> Mono.just(muMap)
		}
		
		and:
		TimelineQuery parsed = Stub()

		when:
		EventSummary summary = block controller.summary(
				Mono.just(ledger), "+one +two", null, markerUniverseLoader)
		
		then:
		1 * timelineQueryParser.parse("+one +two", muMap) >> parsed
		1 * timelineService.summary(ledger, parsed, null) >> Mono.just(expected)
		
		and: 
		summary == expected
	}
	
	void "Repeats last span"() {
		given:
		TimelineSpan result = Stub(TimelineSpan)
		
		when:
		TimelineSpan started = block controller.repeatLastSpan(new RepeatSpanInputDto(null, null), Mono.just(ledger))

		then:
		1 * timelineService.repeatLastSpan(null, null, ledger) >> Mono.just(result)
		started == result
	}

}
