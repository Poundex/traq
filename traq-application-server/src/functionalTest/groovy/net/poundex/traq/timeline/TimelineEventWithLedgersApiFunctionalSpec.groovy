package net.poundex.traq.timeline

import net.poundex.testing.spock.functional.GraphQlResponseHasErrors
import net.poundex.traq.AbstractTraqFunctionalSpec
import net.poundex.traq.api.client.*
import net.poundex.traq.day.DayDto
import net.poundex.traq.ledger.LedgerDto
import net.poundex.traq.ledger.LedgerInputDto
import org.bson.types.ObjectId
import spock.lang.Stepwise

import java.time.LocalDate

@Stepwise
class TimelineEventWithLedgersApiFunctionalSpec extends AbstractTraqFunctionalSpec {
	
	private static String ledger2id
	
	void "Create new Ledger"() {
		when:
		LedgerDto ledger = query(LedgerDto,
				CreateLedgerGraphQLQuery.newRequest()
						.ledger(new LedgerInputDto("Ledger2", null))
						.build(),
				new CreateLedgerProjectionRoot<>().id().name())

		then:
		ledger.id()
		ledger.name()
		
		cleanup:
		ledger2id = ledger.id()
	}
	
	void "Fetches events on correct ledger"() {
		given:
		createPoint(LocalDate.of(2020, 1, 1), defaultLedgerId, "on-default")
		createPoint(LocalDate.of(2020, 1, 1), ledger2id, "on-other")

		when:
		List<TimelinePointDto> pointsOnDefault = queryList(TimelinePointDto,
				PointsGraphQLQuery.newRequest()
						.day(LocalDate.of(2020, 1, 1))
						.ledger(defaultLedgerId)
						.build(),
				new PointsProjectionRoot<>().description())
		
		then:
		pointsOnDefault.size() == 1
		pointsOnDefault.first().description() == "on-default"

		when:
		List<TimelinePointDto> pointsOnOther = queryList(TimelinePointDto,
				PointsGraphQLQuery.newRequest()
						.day(LocalDate.of(2020, 1, 1))
						.ledger(ledger2id)
						.build(),
				new PointsProjectionRoot<>().description())

		then:
		pointsOnOther.size() == 1
		pointsOnOther.first().description() == "on-other"
	}

	void "Fetches events on correct ledger for Day"() {
		when:
		DayDto defaultDay = query(DayDto,
				DayGraphQLQuery.newRequest()
				.day(LocalDate.of(2020, 1, 1))
				.ledger(defaultLedgerId)
						.build(),
				new DayProjectionRoot<>().events().__typename().description())
		
		then:
		defaultDay.events().size() == 1
		defaultDay.events().first().description() == "on-default"

		when:
		DayDto otherDay = query(DayDto,
				DayGraphQLQuery.newRequest()
						.day(LocalDate.of(2020, 1, 1))
						.ledger(ledger2id)
						.build(),
				new DayProjectionRoot<>().events().__typename().description())
		
		then:
		otherDay.events().size() == 1
		otherDay.events().first().description() == "on-other"
	}
	
	void "Error when Ledger not found"() {
		when:
		query(TimelinePointDto,
				CreatePointGraphQLQuery.newRequest()
						.ledger(new ObjectId().toHexString())
						.point(new CreatePointInputDto(null, LocalDate.of(2020, 1, 1), "on-default"))
						.build(),
				new CreatePointProjectionRoot<>().id())

		then:
		GraphQlResponseHasErrors e = thrown(GraphQlResponseHasErrors)
		e.responseErrors.size() == 1
		with(e.responseErrors.first()) {
			it.extensions.classification == "NOT_FOUND"
		}
	}
	
	void "Stops spans on correct ledger"() {
		given:
		StartSpanInfoDto spanOnDefault = startSpan(defaultLedgerId, "span-on-default")

		when:
		StartSpanInfoDto spanOnOther = startSpan(ledger2id, "span-on-other")
		
		then:
		! spanOnOther.previousSpan()

		when:
		TimelineSpanDto deflSpan = query(TimelineSpanDto.class, "{ event(id: \"${spanOnDefault.span().id()}\") { ... on TimelineSpan { active  } } }")
		
		then:
		deflSpan.active()

		when:
		TimelineSpanDto stoppedOnDefault1 = stopActiveSpan(defaultLedgerId)
		TimelineSpanDto stoppedOnDefault2 = stopActiveSpan(defaultLedgerId)
		TimelineSpanDto stoppedOnOther1 = stopActiveSpan(ledger2id)
		TimelineSpanDto stoppedOnOther2 = stopActiveSpan(ledger2id)
		
		then:
		stoppedOnDefault1.id() == spanOnDefault.span().id()
		! stoppedOnDefault2
		stoppedOnOther1.id() == spanOnOther.span().id()
		! stoppedOnOther2
	}
}
