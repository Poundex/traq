package net.poundex.traq.timeline

import net.poundex.traq.AbstractTraqFunctionalSpec
import spock.lang.Stepwise

@Stepwise
class TimelineEventWithEffectiveMarkersFunctionalSpec extends AbstractTraqFunctionalSpec {
	
	void "Return effective markers for TimelineEvent"() {
		given:
		modifyMarker(defaultLedgerId, "+direct", ["+indirect"])

		when:
		TimelinePointDto point = createPoint(defaultLedgerId, "Tag this with +direct")
		
		then:
		point.markers().size() == 1
		point.markers().first().name() == "direct"
		
		and:
		point.effectiveMarkers().size() == 2
		point.effectiveMarkers()*.name().containsAll(["direct", "indirect"])
	}

	void "Return effective markers for TimelineEvent excluding direct"() {
		given:
		modifyMarker(defaultLedgerId, "+direct", ["+indirect"])

		when:
		TimelinePointDto point = createPoint(defaultLedgerId, "Tag this with +direct", true)

		then:
		point.markers().size() == 1
		point.markers().first().name() == "direct"

		and:
		point.effectiveMarkers().size() == 1
		point.effectiveMarkers()*.name().containsAll(["indirect"])
	}
	
}
