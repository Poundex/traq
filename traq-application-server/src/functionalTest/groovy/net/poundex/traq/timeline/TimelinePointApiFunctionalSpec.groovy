package net.poundex.traq.timeline

import net.poundex.traq.AbstractTraqFunctionalSpec
import net.poundex.traq.api.client.CreatePointGraphQLQuery
import net.poundex.traq.api.client.CreatePointProjectionRoot
import net.poundex.traq.api.client.PointsGraphQLQuery
import net.poundex.traq.api.client.PointsProjectionRoot
import net.poundex.traq.marker.BasePropertyDto
import net.poundex.traq.marker.MentionDto
import net.poundex.traq.marker.PropertyDto
import net.poundex.traq.marker.TagDto
import spock.lang.Stepwise

import java.time.Instant
import java.time.LocalDate
import java.time.ZoneId
import java.time.temporal.ChronoUnit

@Stepwise
class TimelinePointApiFunctionalSpec extends AbstractTraqFunctionalSpec {

	private static final String description = "some-description +blue +green @them :that=this"
	private static final Instant now = Instant.now()
	private static final LocalDate today = LocalDate.now()

	void "Create new Timeline Point"() {
		when:
		TimelinePointDto r = query(TimelinePointDto,
				CreatePointGraphQLQuery.newRequest()
						.point(new CreatePointInputDto(null, null, description))
						.ledger(defaultLedgerId)
						.build(),
				new CreatePointProjectionRoot<>()
						.id()
						.day()
						.time()
						.description()
						.markers()
							.__typename()
							.id()
							.name()
							.onProperty()
								.value()
								.parent()
							.markers()
								.id())

		then:
		r.id()
		r.day() == LocalDate.ofInstant(now, ZoneId.systemDefault())
		r.time().truncatedTo(ChronoUnit.HOURS) == now.truncatedTo(ChronoUnit.HOURS)
		r.description() == description
		with(r.markers()) { markers ->
			size() == 5
			markers.find {
				it instanceof TagDto
						&& it.id()
						&& it.name() == "blue"
						&& it.markers().empty
			}
			markers.find {
				it instanceof TagDto
						&& it.id()
						&& it.name() == "green"
						&& it.markers().empty
			}
			markers.find {
				it instanceof MentionDto
						&& it.id()
						&& it.name() == "them"
						&& it.markers().empty
			}
			markers.find {
				it instanceof BasePropertyDto
						&& it.id()
						&& it.name() == "that"
						&& it.markers().empty
			}
			markers.find {
				it instanceof PropertyDto
						&& it.id()
						&& it.name() == "that"
						&& it.value() == "this"
						&& it.markers().empty
			}
		}
	}

	void "Returns all points"() {
		when:
		List<TimelinePointDto> r = queryList(TimelinePointDto,
				PointsGraphQLQuery.newRequest().ledger(defaultLedgerId).build(),
				new PointsProjectionRoot<>().id().day().time().description())

		then:
		r.size() == 1
		r.find {
			it instanceof TimelinePointDto
					&& it.id()
					&& it.day() == today
					&& it.description() == description
					&& it.time()
		}
	}

	void "Returns points for predicate"() {
		when:
		List<TimelinePointDto> r = queryList(TimelinePointDto,
				PointsGraphQLQuery.newRequest().day(today).ledger(defaultLedgerId).build(),
				new PointsProjectionRoot().id().day().time().description())

		then:
		r.size() == 1
	}
}
