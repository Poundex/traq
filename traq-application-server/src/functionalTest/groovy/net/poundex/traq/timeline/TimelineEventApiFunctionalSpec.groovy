package net.poundex.traq.timeline

import net.poundex.traq.AbstractTraqFunctionalSpec
import net.poundex.traq.api.client.*
import spock.lang.Stepwise

import java.time.LocalDate

@Stepwise
class TimelineEventApiFunctionalSpec extends AbstractTraqFunctionalSpec {

	private static final LocalDate today = LocalDate.now()

	void "Returns all events"() {
		given:
		startSpan(defaultLedgerId, "Span 1")
		createPoint(defaultLedgerId, "Point 1")

		when:
		List<TimelineEventDto> r = queryList(TimelineEventDto,
				EventsGraphQLQuery.newRequest()
						.ledger(defaultLedgerId)
						.build(),
				new EventsProjectionRoot<>()
						.__typename()
						.id()
						.day()
						.time()
						.description()
						.onTimelineSpan()
							.endTime())
				
		then:
		r.size() == 2
		r.find {
			it instanceof TimelineSpanDto
					&& it.id()
					&& it.day() == today
					&& it.time()
					&& !it.endTime()
					&& it.description() == "Span 1"
		}
		r.find {
			it instanceof TimelinePointDto
					&& it.id()
					&& it.time()
					&& it.day() == today
					&& it.description() == "Point 1"
		}
	}

	void "Returns events for predicate"() {
		given:
		LocalDate yesterday = LocalDate.now().minusDays(1)
		createPoint(yesterday, defaultLedgerId, "Point 2")

		when:
		List<TimelineEventDto> r = queryList(TimelineEventDto,
				EventsGraphQLQuery.newRequest()
						.day(yesterday)
						.ledger(defaultLedgerId)
						.build(),
				new EventsProjectionRoot<>().
						__typename()
						.id()
						.day()
						.time()
						.description()
						.onTimelineSpan()
							.endTime())

		then:
		r.size() == 1
		r.find {
			it instanceof TimelinePointDto
					&& it.id()
					&& it.day() == yesterday
					&& it.description() == "Point 2"
					&& it.time()
		}
	}

	void "Updates descriptions"() {
		given:
		StartSpanInfoDto span = startSpan(defaultLedgerId, "Span 1")

		when:
		TimelineSpanDto r = query(TimelineSpanDto,
				UpdateDescriptionGraphQLQuery.newRequest()
						.event(span.span().id())
						.description("Span 1 EDITED")
						.build(),
				new UpdateDescriptionProjectionRoot<>().__typename().description())

		then:
		r.description() == "Span 1 EDITED"
	}

	void "Fetches events by ID"() {
		given:
		StartSpanInfoDto span = startSpan(defaultLedgerId, "Span 1")
		TimelinePointDto point = createPoint(defaultLedgerId, "Point 1")

		when:
		TimelineSpanDto spanResponse = query(TimelineSpanDto,
				EventGraphQLQuery.newRequest().id(span.span().id()).build(),
				new EventsProjectionRoot<>().__typename().id().description())
		
		then:
		spanResponse.description() == "Span 1"

		when:
		TimelinePointDto pointResponse = query(TimelinePointDto,
				EventGraphQLQuery.newRequest().id(point.id()).build(),
				new EventsProjectionRoot<>().__typename().id().description())

		then:
		pointResponse.description() == "Point 1"
	}
}