package net.poundex.traq.timeline

import net.poundex.traq.AbstractTraqFunctionalSpec
import net.poundex.traq.api.client.*
import spock.lang.Stepwise

import java.time.*
import java.time.temporal.ChronoUnit

@Stepwise
class TimelineSpanApiFunctionalSpec extends AbstractTraqFunctionalSpec {

	private static final Instant now = Instant.now()
	private static final LocalDate today = LocalDate.now()
	
	void "Start new Timeline Span"() {
		given:
		String description = "some-description +blue +green @them :that=this"
		
		when:
		StartSpanInfoDto r = query(StartSpanInfoDto,
				StartSpanGraphQLQuery.newRequest()
						.span(new StartSpanInputDto(null, null, description))
						.ledger(defaultLedgerId)
						.build(),
				new StartSpanProjectionRoot<>()
						.span()
							.id()
							.day()
							.time()
							.description()
							.active()
							.root()
						.previousSpan()
							.id())

		then:
		with(r.span()) { s ->
			s.id()
			s.day() == LocalDate.ofInstant(now, ZoneId.systemDefault())
			s.time().truncatedTo(ChronoUnit.HOURS) == now.truncatedTo(ChronoUnit.HOURS)
			s.description() == description
			s.active()
		}
		! r.previousSpan()
	}
	
	void "Stops active span"() {
		when:
		TimelineSpanDto r = query(TimelineSpanDto,
				StopActiveSpanGraphQLQuery.newRequest().ledger(defaultLedgerId).build(),
				new StopActiveSpanProjectionRoot<>()
						.id()
						.day()
						.time()
						.endTime()
						.description()
						.active())

		then:
		r.id()
		r.day() == LocalDate.ofInstant(now, ZoneId.systemDefault())
		r.time()
		r.endTime().truncatedTo(ChronoUnit.HOURS) == now.truncatedTo(ChronoUnit.HOURS)
		! r.active()
		r.description().startsWith("some-description")
	}
	
	void "Starts new span, stopping active span"() {
		given:
		query(StartSpanInfoDto,
				StartSpanGraphQLQuery.newRequest()
						.ledger(defaultLedgerId)
						.span(new StartSpanInputDto(null, null, "first"))
						.build(),
				new StartSpanProjectionRoot<>().span().id())
				
		when:
		StartSpanInfoDto r = query(StartSpanInfoDto, 
				StartSpanGraphQLQuery.newRequest()
						.ledger(defaultLedgerId)
						.span(new StartSpanInputDto(null, null, "second"))
						.build(),
				new StartSpanProjectionRoot<>()
						.span()
							.id()
							.day()
							.time()
							.endTime()
							.description()
							.active()
							.root()
						.previousSpan()
							.id()
							.day()
							.time()
							.endTime()
							.description()
							.active())
		
		then:
		with(r.span()) { s ->
			s.description() == "second"
			! s.endTime()
			s.active()
		}
		with(r.previousSpan()) { s ->
			s.description() == "first"
			s.endTime()
			! s.active()
		}
	}
	
	void "Returns all spans"() {
		when:
		List<TimelineSpanDto> r = queryList(TimelineSpanDto,
				SpansGraphQLQuery.newRequest()
						.ledger(defaultLedgerId)
						.build(),
				new SpansProjectionRoot<>()
						.id()
						.day()
						.time()
						.description()
						.endTime())

		then:
		r.size() == 3
		r.find {
			it instanceof TimelineSpanDto
					&& it.id()
					&& it.day() == today
					&& it.description() == "first"
					&& it.time()
					&& it.endTime()
		}
		r.find {
			it instanceof TimelineSpanDto
					&& it.id()
					&& it.day() == today
					&& it.description() == "second"
					&& it.time()
					&& ! it.endTime()
		}
	}

	void "Returns spans for predicate"() {
		when:
		List<TimelineSpanDto> r = queryList(TimelineSpanDto,
				SpansGraphQLQuery.newRequest()
						.day(today.minusDays(1))
						.ledger(defaultLedgerId)
						.build(),
				new SpansProjectionRoot<>()
						.id()
						.day()
						.time()
						.description()
						.endTime())

		then:
		r.empty
	}

	void "Create manual span"() {
		given:
		LocalDate today = LocalDate.now()
		Instant start = ZonedDateTime.of(today, LocalTime.now().minusHours(2), ZoneId.systemDefault()).toInstant()
		Instant end = ZonedDateTime.of(today, LocalTime.now(), ZoneId.systemDefault()).toInstant()
		
		String description = "some-description +blue +green @them :that=this"

		when:
		TimelineSpanDto r = query(TimelineSpanDto,
				CreateSpanGraphQLQuery.newRequest()
						.ledger(defaultLedgerId)
						.span(new CreateSpanInputDto(start, end, today, description))
						.build(),
				new CreateSpanProjectionRoot<>()
						.id()
						.day()
						.time()
						.endTime()
						.description()
						.active()
						.duration())

		then:
		with(r) { s ->
			s.id()
			s.day() == today
			s.time() == start
			s.endTime() == end
			s.description() == description
			! s.active()
			s.duration() == Duration.ofHours(2)
		}
	}

	void "Repeats last timeline span"() {
		given:
		startSpan(defaultLedgerId, "Span to repeat")
		stopActiveSpan(defaultLedgerId)
		String expected = "Span to repeat"

		when:
		TimelineSpanDto r = query(TimelineSpanDto,
				RepeatLastSpanGraphQLQuery.newRequest()
						.span(new RepeatSpanInputDto(null, null))
						.ledger(defaultLedgerId)
						.build(),
				new RepeatLastSpanProjectionRoot<>()
						.id()
						.day()
						.time()
						.endTime()
						.description()
						.active())

		then:
		with(r) { s ->
			s.id()
			s.day() == LocalDate.ofInstant(now, ZoneId.systemDefault())
			s.time().truncatedTo(ChronoUnit.HOURS) == now.truncatedTo(ChronoUnit.HOURS)
			s.description() == expected
			s.active()
			! s.endTime()
		}
	}
}