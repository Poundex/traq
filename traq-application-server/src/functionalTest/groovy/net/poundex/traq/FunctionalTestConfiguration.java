package net.poundex.traq;

import com.fasterxml.jackson.databind.Module;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.TestPropertySource;

@Configuration
@TestPropertySource(properties = "spring.data.mongodb.port=40000")
public class FunctionalTestConfiguration {

	@Bean
	public Module traqJacksonModule() {
		return new TraqModule();
	}
}
