package net.poundex.traq.importexport

import net.poundex.testing.spock.functional.Json
import net.poundex.traq.AbstractTraqFunctionalSpec
import net.poundex.traq.CleanData
import net.poundex.traq.FunctionalTestConfiguration
import net.poundex.traq.marker.MarkerDto
import net.poundex.traq.timeline.TimelineEventDto
import net.poundex.traq.timeline.TimelinePointDto
import net.poundex.traq.timeline.TimelineSpanDto
import org.springframework.context.annotation.Import
import org.springframework.core.io.ClassPathResource
import org.springframework.http.HttpStatus
import spock.lang.Stepwise

import java.time.Instant
import java.time.LocalDate
import java.time.ZoneOffset
import java.time.ZonedDateTime

@Stepwise
@Import(FunctionalTestConfiguration)
@CleanData
class ImportFunctionalSpec extends AbstractTraqFunctionalSpec implements Json {

	LocalDate date = LocalDate.of(2023, 9, 5)
	Instant t1 = ZonedDateTime.of(2023, 9, 5, 8, 30, 0, 0, ZoneOffset.UTC).toInstant()
	Instant t2 = ZonedDateTime.of(2023, 9, 5, 9, 30, 0, 0, ZoneOffset.UTC).toInstant()
	Instant t3 = ZonedDateTime.of(2023, 9, 5, 10, 30, 0, 0, ZoneOffset.UTC).toInstant()

	void "Imports data from CSV"() {
		when:
		ImportResultDto importResult = postFile(
				"/import/default?zoneId=Europe/London",
				"importFile",
				new ClassPathResource("importdata.csv"),
				ImportResultDto.class)
		
		then:
		status == HttpStatus.OK
		
		and:
		importResult.eventsCreated() == 3

		when:
		List<TimelineEventDto> r = queryList(TimelineEventDto, "{ events(ledger: \"${defaultLedgerId}\") { " +
				"__typename day time description ... on TimelineSpan { endTime } } }")
		
		then:
		r.find {
			it instanceof TimelineSpanDto
					&& it.day() == date
					&& it.time() == t1
					&& it.endTime() == t2
					&& it.description() == "1 Hour Span"
		}
		r.find {
			it instanceof TimelinePointDto
					&& it.day() == date
					&& it.time() == t2
					&& it.description() == "A Point"
		}
		r.find {
			it instanceof TimelinePointDto
					&& it.day() == date
					&& it.time() == t3
					&& it.description() == "Another Point"
		}
	}
	
	void "Doesn't duplicate marker creations"() {
		when:
		postFile(
				"/import/default?zoneId=Europe/London",
				"importFile",
				new ClassPathResource("manymarkers.csv"),
				ImportResultDto.class)

		then:
		status == HttpStatus.OK
		
		when:
		List<MarkerDto> r = queryList(MarkerDto, "{ markers(ledger: \"${defaultLedgerId}\") { " +
				"__typename id name } }")

		then:
		r.size() == 3
		r*.name().containsAll("one", "two", "three")
	}
}
