package net.poundex.traq.marker

import net.poundex.traq.AbstractTraqFunctionalSpec
import net.poundex.traq.CleanData
import net.poundex.traq.FunctionalTestConfiguration
import net.poundex.traq.api.client.SummaryGraphQLQuery
import net.poundex.traq.api.client.SummaryProjectionRoot
import net.poundex.traq.summary.EventSummaryDto
import org.springframework.context.annotation.Import
import spock.lang.Stepwise

import java.time.Duration

@Stepwise
@Import(FunctionalTestConfiguration)
@CleanData
class EventSummaryFunctionalSpec extends AbstractTraqFunctionalSpec {
	void "Returns event summary for marker"() {
		given:
		startSpan(defaultLedgerId, "a span +tag")
		Thread.sleep(3000)
		stopActiveSpan(defaultLedgerId)

		when:
		EventSummaryDto eventSummary = query(EventSummaryDto,
				SummaryGraphQLQuery.newRequest()
						.ledger(defaultLedgerId)
						.timelineQuery("+tag")
						.build(),
				new SummaryProjectionRoot<>()
						.ledger()
							.id()
							.parent()
						.duration()
						.events()
							.__typename()
							.id()
							.description()
							.parent()
						.parsedQuery())
		
		then:
		eventSummary.duration() >= Duration.ofSeconds(3)
		eventSummary.duration() <= Duration.ofSeconds(5)
		eventSummary.events().size() == 1
		eventSummary.parsedQuery() == "+tag"
	}
}
