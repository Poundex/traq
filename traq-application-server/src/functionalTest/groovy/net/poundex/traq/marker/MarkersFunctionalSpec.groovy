package net.poundex.traq.marker

import net.poundex.traq.AbstractTraqFunctionalSpec
import net.poundex.traq.CleanData
import net.poundex.traq.FunctionalTestConfiguration
import net.poundex.traq.api.client.*
import net.poundex.traq.ledger.LedgerDto
import net.poundex.traq.timeline.TimelinePointDto
import org.springframework.context.annotation.Import
import spock.lang.Stepwise

@Stepwise
@Import(FunctionalTestConfiguration)
@CleanData
class MarkersFunctionalSpec extends AbstractTraqFunctionalSpec {

	static String newMarkerId
	static String newMarkerAgainId

	void "Creates new markers"() {
		when:
		MarkerDto marker = query(MarkerDto, ModifyMarkerGraphQLQuery.newRequest()
				.ledger(defaultLedgerId)
				.marker("+newmarker")
				.markers([])
				.build(), new ModifyMarkerProjectionRoot<>()
				.__typename()
				.id()
				.name()
				.markers()
				.id()
				.parent()
				.ledger()
				.id())

		then:
		marker instanceof TagDto
		with(marker as TagDto) { tag ->
			tag.id()
			tag.name() == "newmarker"
			tag.markers().empty
			tag.ledger().id() == defaultLedgerId
		}

		cleanup:
		newMarkerId = marker.id()
	}

	void "Updates marker title"() {
		when:
		MarkerDto marker = query(MarkerDto, ModifyMarkerGraphQLQuery.newRequest()
				.ledger(defaultLedgerId)
				.marker("+newmarker")
				.title("New Marker")
				.markers([])
				.build(), new ModifyMarkerProjectionRoot<>()
				.__typename()
				.id()
				.name()
				.title()
				.markers()
				.id()
				.parent()
				.ledger()
				.id())

		then:
		marker instanceof TagDto
		with(marker as TagDto) { tag ->
			tag.id() == newMarkerId
			tag.name() == "newmarker"
			tag.title() == "New Marker"
			tag.markers().empty
			tag.ledger().id() == defaultLedgerId
		}
	}

	void "Modify existing marker with new marker"() {
		when:
		MarkerDto marker = query(MarkerDto, ModifyMarkerGraphQLQuery.newRequest()
				.ledger(defaultLedgerId)
				.marker("+newmarker")
				.markers(["+markertwo"])
				.build(), new ModifyMarkerProjectionRoot<>()
				.__typename()
				.id()
				.name()
				.title()
				.markers()
				.__typename()
				.id()
				.name()
				.markers()
				.id()
				.parent()
				.ledger()
				.id()
				.root()
				.ledger()
				.id())

		then:
		marker instanceof TagDto
		with(marker as TagDto) { tag ->
			tag.id() == newMarkerId
			tag.name() == "newmarker"
			tag.title() == "New Marker"
			tag.markers().size() == 1
			tag.markers().first() instanceof TagDto
			tag.ledger().id() == defaultLedgerId
			with(tag.markers().first() as TagDto) { created ->
				created.id()
				created.name() == "markertwo"
				created.markers().empty
				created.ledger().id() == defaultLedgerId
			}
		}
	}

	void "Create new marker with existing"() {
		when:
		MarkerDto marker = query(MarkerDto, ModifyMarkerGraphQLQuery.newRequest()
				.marker("+newmarkeragain")
				.markers(["+newmarker"])
				.ledger(defaultLedgerId)
				.build(), new ModifyMarkerProjectionRoot<>()
				.__typename()
				.id()
				.name()
				.markers()
				.__typename()
				.id()
				.name()
				.ledger()
				.id()
				.parent()
				.markers()
				.__typename()
				.id()
				.name()
				.ledger()
				.id()
				.root()
				.ledger()
				.id())

		then:
		marker instanceof TagDto
		with(marker as TagDto) { tag ->
			tag.id()
			tag.name() == "newmarkeragain"
			tag.markers().size() == 1
			tag.markers().first() instanceof TagDto
			tag.ledger().id() == defaultLedgerId
			with(tag.markers().first() as TagDto) { other ->
				other.id() == newMarkerId
				other.name() == "newmarker"
				other.markers().size() == 1
				other.ledger().id() == defaultLedgerId
			}
		}

		cleanup:
		newMarkerAgainId = marker.id()
	}

	void "List existing markers"() {
		when:
		List<MarkerDto> markers = queryList(MarkerDto, 
				MarkersGraphQLQuery.newRequest().ledger(defaultLedgerId).build(), 
		new MarkersProjectionRoot<>()
				.__typename()
				.id()
				.name()
				.title()
				.markers()
					.__typename()
					.id()
					.name()
					.title()
					.parent()
				.ledger()
				.id())
		
		then:
		markers.size() >= 3
		with(markers.find { it.name() == "newmarker" }) {
			it instanceof TagDto
			it.title() == "New Marker"
			it.markers().size() == 1
			it.markers().first().name() == "markertwo"
			it.ledger().id() == defaultLedgerId
		}
		markers.find { it.name() == "markertwo" }
		markers.find { it.name() == "newmarkeragain" }
	}
	
	void "Returns Marker by ID"() {
		when:
		MarkerDto marker = query(MarkerDto, 
				MarkerGraphQLQuery.newRequest().id(newMarkerAgainId).build(),
				new MarkerProjectionRoot<>().__typename().id().name())

		then:
		marker instanceof TagDto
		with(marker as TagDto) { tag ->
			tag.id() == newMarkerAgainId
			tag.name() == "newmarkeragain"
		}
	}
	
	void "Returns effective markers"() {
		given:
		modifyMarker(defaultLedgerId, "+markertwo", ["+markerthree"])

		when:
		MarkerDto marker = query(MarkerDto, 
				MarkerGraphQLQuery.newRequest().id(newMarkerAgainId).build(),
				new MarkerProjectionRoot<>()
						.__typename()
						.id()
						.name()
						.markers()
							.__typename()
							.name()
							.parent()
						.effectiveMarkers()
							.__typename()
							.name())

		then:
		marker instanceof TagDto
		with(marker as TagDto) { tag ->
			tag.id() == newMarkerAgainId
			tag.name() == "newmarkeragain"
			tag.markers().size() == 1
			tag.effectiveMarkers().size() == 3
			tag.effectiveMarkers()*.name().containsAll(["newmarker", "markertwo", "markerthree"])
		}
	}

	void "Returns effective markers excluding direct"() {
		when:
		MarkerDto marker = query(MarkerDto,
				MarkerGraphQLQuery.newRequest()
						.id(newMarkerAgainId)
						.build(),
				new MarkerProjectionRoot<>()
						.__typename()
						.id()
						.name()
						.markers()
							.__typename()
							.name()
							.parent()
						.effectiveMarkers(true)
							.__typename()
							.name())

		then:
		marker instanceof TagDto
		with(marker as TagDto) { tag ->
			tag.id() == newMarkerAgainId
			tag.name() == "newmarkeragain"
			tag.markers().size() == 1
			tag.effectiveMarkers().size() == 2
			tag.effectiveMarkers()*.name().containsAll(["markertwo", "markerthree"])
		}
	}
	
	void "Updates marker titles"() {
		given:
		MarkerDto marker = modifyMarker(defaultLedgerId, "+thismarker", [])
		
		when:
		MarkerDto updated = query(MarkerDto,
				UpdateMarkerGraphQLQuery.newRequest().marker(marker.id()).title("This Marker").build(),
				new UpdateMarkerProjectionRoot<>().__typename().id().name().title())
		
		then:
		updated.id() == marker.id()
		updated.name() == marker.name()
		updated.title() == "This Marker"
		
		when:
		MarkerDto updated2 = query(MarkerDto,
				UpdateMarkerGraphQLQuery.newRequest().marker(marker.id()).title("").build(),
				new UpdateMarkerProjectionRoot<>().__typename().id().name().title())

		then:
		! updated2.title()
	}
	
	void "Marker realisation is case-insensitive"() {
		given:
		LedgerDto ledger = createLedger("Case Test Ledger", null)
		
		when:
		TimelinePointDto point1 = createPoint(ledger.id(), 
				"+casetesttag +CaseTestTag @casetestmention :casetestprop=casetestvalue")
		
		then:
		point1.markers().size() == 4
		point1.markers()*.class.toSet() == [TagDto, MentionDto, BasePropertyDto, PropertyDto].toSet()
		
		when:
		TagDto tag = point1.markers().find {  it instanceof TagDto }
		MentionDto mention = point1.markers().find { it instanceof MentionDto }
		BasePropertyDto baseProperty = point1.markers().find { it instanceof BasePropertyDto }
		PropertyDto property = point1.markers().find { it instanceof PropertyDto }

		and:
		TimelinePointDto point2 = createPoint(ledger.id(), 
				"+CASETESTTAG @CaseTestMention :casetestprop=CaseTestValue")
		
		then:
		point2.markers().size() == 4
		point2.markers() == [tag, mention, baseProperty, property].toSet()
		
		when:
		TimelinePointDto point3 = createPoint(ledger.id(), 
		":CaseTestProp=CaseTestValue :CASETESTPROP=AnotherValue")
		
		then:
		point3.markers().size() == 3
		point3.markers().containsAll([baseProperty, property])
	}

	void "Marks marker with existing marker"() {
		given:
		MarkerDto toMark = modifyMarker(defaultLedgerId, "+markthis", [])

		when:
		MarkerDto marker = query(MarkerDto, SetMarkersGraphQLQuery.newRequest()
				.marker(toMark.id())
				.markers(["+createthis"])
				.build(), new SetMarkersProjectionRoot<>()
				.__typename()
				.id()
				.name()
				.title()
				.markers()
					.__typename()
					.id()
					.name()
					.markers()
						.id()
						.parent()
					.ledger()
						.id()
						.root()
				.ledger()
					.id())

		then:
		marker instanceof TagDto
		with(marker as TagDto) { tag ->
			tag.name() == "markthis"
			tag.markers().size() == 1
			tag.markers().first() instanceof TagDto
			with(tag.markers().first() as TagDto) { created ->
				created.name() == "createthis"
				created.markers().empty
			}
		}
	}
}