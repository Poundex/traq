package net.poundex.traq

import groovy.util.logging.Slf4j
import org.spockframework.runtime.IRunListener
import org.spockframework.runtime.extension.IAnnotationDrivenExtension
import org.spockframework.runtime.model.ErrorInfo
import org.spockframework.runtime.model.FeatureInfo
import org.spockframework.runtime.model.IterationInfo
import org.spockframework.runtime.model.SpecInfo
import org.spockframework.spring.SpringInterceptor
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import reactor.core.publisher.Flux

@Slf4j
class CleanDataExtension implements IAnnotationDrivenExtension<CleanData> {

	@Override
	void visitSpecAnnotation(CleanData annotation, SpecInfo spec) { }

	@Override
	void visitSpec(SpecInfo specz) {
		specz.addListener(new IRunListener() {
			@Override
			void beforeSpec(SpecInfo spec) {
			}

			@Override
			void beforeFeature(FeatureInfo feature) {

			}

			@Override
			void beforeIteration(IterationInfo iteration) {

			}

			@Override
			void afterIteration(IterationInfo iteration) {

			}

			@Override
			void afterFeature(FeatureInfo feature) {

			}

			@Override
			void afterSpec(SpecInfo spec) {
				ReactiveMongoTemplate mongoTemplate = spec.getSetupInterceptors()
						.find { it instanceof SpringInterceptor }
						.manager
						.delegate
						.testContext
						.applicationContext
						.getBean(ReactiveMongoTemplate.class)

				log.info("Cleaning database")

				mongoTemplate
						.getMongoDatabase()
						.flatMap(db -> Flux.fromIterable(["events", "markers"])
								.flatMap(coll -> db.getCollection(coll).drop()))
						.block()
			}

			@Override
			void error(ErrorInfo error) {

			}

			@Override
			void specSkipped(SpecInfo spec) {

			}

			@Override
			void featureSkipped(FeatureInfo feature) {

			}
		})
	}
}
