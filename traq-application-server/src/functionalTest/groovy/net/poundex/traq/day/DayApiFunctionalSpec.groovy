package net.poundex.traq.day

import net.poundex.traq.AbstractTraqFunctionalSpec
import net.poundex.traq.CleanData
import net.poundex.traq.FunctionalTestConfiguration
import net.poundex.traq.api.client.DayGraphQLQuery
import net.poundex.traq.api.client.DayProjectionRoot
import net.poundex.traq.ledger.LedgerDto
import net.poundex.traq.timeline.TimelineEventDto
import net.poundex.traq.timeline.TimelinePointDto
import net.poundex.traq.timeline.TimelineSpanDto
import org.springframework.context.annotation.Import
import spock.lang.Stepwise

import java.time.*
import java.time.temporal.ChronoUnit

@Import(FunctionalTestConfiguration)
@Stepwise
@CleanData
class DayApiFunctionalSpec extends AbstractTraqFunctionalSpec {

	private static LocalDate today = LocalDate.now()

	void "Return nothing for day"() {
		when:
		DayDto r = query(DayDto,
				DayGraphQLQuery.newRequest()
						.day(today)
						.ledger(defaultLedgerId)
						.build(),
				new DayProjectionRoot<>().day())

		then:
		r.day() == today
	}
	
	void "Return zero-stats for empty day"() {
		when:
		DayDto r = query(DayDto,
				DayGraphQLQuery.newRequest()
						.day(today)
						.ledger(defaultLedgerId).build(),
				new DayProjectionRoot<>()
						.day()
						.startTime()
						.endTime()
						.projectedEndTime()
						.duration())

		then:
		r.day() == today
		! r.startTime()
		! r.endTime()
		! r.projectedEndTime()
		r.duration() == Duration.ZERO
	}
	
	void "Return correct stats for day"() {
		given:
		LedgerDto ledger = createLedger("test-ledger", Duration.ofMinutes(450))
		createPoint(
				today,
				ledger.id(),
				"Some Point", 
				false,
				ZonedDateTime.of(today, LocalTime.of(0, 1), ZoneId.systemDefault()).toInstant())
		
		Instant startTime = ZonedDateTime.of(today, LocalTime.of(10, 0), ZoneId.systemDefault()).toInstant()
		Instant endTime = ZonedDateTime.of(today, LocalTime.of(17, 0), ZoneId.systemDefault()).toInstant()
		
		createSpan(
				ledger.id(),
				today,
				ZonedDateTime.of(today, LocalTime.of(14, 0), ZoneId.systemDefault()).toInstant(),
				endTime,
				"Span 2")

		createSpan(
				ledger.id(), 
				today,
				startTime,
				ZonedDateTime.of(today, LocalTime.of(12, 0), ZoneId.systemDefault()).toInstant(),
				"Span 1")


		when:
		DayDto r = query(DayDto,
				DayGraphQLQuery.newRequest()
						.day(today)
						.ledger(ledger.id())
						.build(),
				new DayProjectionRoot<>()
						.startTime()
						.endTime()
						.duration()
						.projectedEndTime()
						.length())

		then:
		r.startTime() == startTime
		r.endTime() == endTime
		r.duration() == Duration.ofHours(5)
		r.projectedEndTime().truncatedTo(ChronoUnit.MINUTES) == Instant.now()
				.plus(150, ChronoUnit.MINUTES)
				.truncatedTo(ChronoUnit.MINUTES)
		r.length() == Duration.ofHours(7)
	}

	void "Return day with events and correct TimelineEvent type"() {
		given:
		createPoint(defaultLedgerId, "Point 1")
		startSpan(defaultLedgerId, "Span 1")

		when:
		DayDto r = query(DayDto,
				DayGraphQLQuery.newRequest()
						.day(today)
						.ledger(defaultLedgerId)
						.build(),
				new DayProjectionRoot<>()
						.day()
						.events()
							.__typename()
							.id()
							.description()
							.day()
							.time()
							.onTimelineSpan()
								.endTime()
								.parent()
							.ledger()
								.id())

		then:
		r.day() == today
		r.events().size() == 2
		r.events().find { TimelineEventDto dto ->
			dto instanceof TimelinePointDto 
					&& dto.id() 
					&& dto.time() 
					&& dto.day() == today 
					&& dto.description() == "Point 1" 
					&& dto.ledger().id() == defaultLedgerId
		}
		r.events().find { TimelineEventDto dto ->
			dto instanceof TimelineSpanDto 
					&& dto.id() 
					&& dto.time() 
					&& dto.day() == today 
					&& dto.description() == "Span 1" 
					&& ! dto.endTime() 
					&& dto.ledger().id() == defaultLedgerId
		}
	}

	void "Return day empty events"() {
		when:
		DayDto r = query(DayDto,
				DayGraphQLQuery.newRequest()
						.day(today.minusDays(10))
						.ledger(defaultLedgerId)
						.build(),
				new DayProjectionRoot<>()
						.day()
						.events()
							.__typename()
							.id()
							.description()
							.day()
							.time()
							.onTimelineSpan()
								.endTime())
		
		then:
		r.day() == today.minusDays(10)
		r.events().empty
	}
}
