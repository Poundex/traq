package net.poundex.traq.day

import net.poundex.traq.AbstractTraqFunctionalSpec
import net.poundex.traq.CleanData
import net.poundex.traq.FunctionalTestConfiguration
import net.poundex.traq.api.client.DayGraphQLQuery
import net.poundex.traq.api.client.DayProjectionRoot
import net.poundex.traq.marker.BasePropertyDto
import net.poundex.traq.marker.PropertyDto
import net.poundex.traq.marker.TagDto
import org.springframework.context.annotation.Import
import spock.lang.Stepwise

import java.time.Duration
import java.time.Instant
import java.time.LocalDate

@Import(FunctionalTestConfiguration)
@Stepwise
@CleanData
class MarkerSummariesFunctionalSpec extends AbstractTraqFunctionalSpec {

	private static LocalDate today = LocalDate.now()

	void "Returns correct marker summaries"() {
		given:
		modifyMarker(defaultLedgerId, "+one", ["+two", ':prop=value2'])
		createPoint(defaultLedgerId, 'A point +one :prop=value1')
		createSpan(
				defaultLedgerId,
				LocalDate.now(),
				Instant.now().minusSeconds(3600 * 2),
				Instant.now(),
				'A span +one :prop=value3')

		when:
		DayDto r = query(DayDto,
				DayGraphQLQuery.newRequest()
						.day(today)
						.ledger(defaultLedgerId)
						.build(),
				new DayProjectionRoot<>()
						.markerSummary()
							.duration()
							.marker()
								.__typename()
								.name()
								.onProperty()
									.value()
									.parent()
								.parent()
							.value()
							.parent()
						.count()
							.marker()
								.__typename()
								.name()
								.onProperty()
									.value()
									.parent()
								.parent()
							.value()
							.parent()
						.propertyValues()
							.marker()
								.__typename()
								.name()
								.parent()
							.value())

		then:
		r.markerSummary().countAsMap().find { k, v ->
			k instanceof TagDto && k.name() == "one" }.value == 1

		r.markerSummary().countAsMap().find { k, v ->
			k instanceof PropertyDto && k.name() == 'prop' && k.value() == "value1" }.value == 1

		r.markerSummary().countAsMap().find { k, v ->
			k instanceof BasePropertyDto && k.name() == 'prop' }.value == 1

		! r.markerSummary().countAsMap().find { k, v ->
			k instanceof TagDto && k.name() == "two" }

		! r.markerSummary().countAsMap().find { k, v ->
			k instanceof PropertyDto && k.name() == 'prop' && k.value() == "value3" }

		! r.markerSummary().countAsMap().find { k, v ->
			k instanceof PropertyDto && k.name() == 'prop' && k.value() == "value2" }

		and:
		r.markerSummary().propertyValuesAsMap().size() == 1
		with(r.markerSummary().propertyValuesAsMap().find { k, v ->
			k instanceof BasePropertyDto
					&& k.name() == "prop" }) {

			it.value.size() == 2
			it.value.containsAll("value1", "value3")
		}
		
		and:
		r.markerSummary().durationAsMap().size() == 2
		r.markerSummary().durationAsMap().find { k, v -> 
			k instanceof TagDto && k.name() == "one"
		}.value == Duration.ofHours(2)
		
		r.markerSummary().durationAsMap().find { k, v ->
			k instanceof PropertyDto && k.name() == "prop" &&
					k.value() == "value3"
		}.value == Duration.ofHours(2)
	}

	void "Returns correct effective marker summaries"() {
		when:
		DayDto r = query(DayDto,
				DayGraphQLQuery.newRequest()
						.day(today)
						.ledger(defaultLedgerId)
						.build(),
				new DayProjectionRoot<>()
						.effectiveMarkerSummary()
							.duration()
								.marker()
									.__typename()
									.name()
									.onProperty()
										.value()
										.parent()
									.parent()
								.value()
								.parent()
							.count()
								.marker()
									.__typename()
									.name()
									.onProperty()
										.value()
										.parent()
									.parent()
								.value()
								.parent()
							.propertyValues()
								.marker()
									.__typename()
									.name()
									.parent()
								.value())

		then:
		r.effectiveMarkerSummary().countAsMap().find { k, v ->
			k instanceof TagDto && k.name() == "one" }.value == 1

		r.effectiveMarkerSummary().countAsMap().find { k, v ->
			k instanceof PropertyDto && k.name() == 'prop' && k.value() == "value1" }.value == 1

		r.effectiveMarkerSummary().countAsMap().find { k, v ->
			k instanceof BasePropertyDto && k.name() == 'prop' }.value == 1

		r.effectiveMarkerSummary().countAsMap().find { k, v ->
			k instanceof TagDto && k.name() == "two" }.value == 1

		! r.effectiveMarkerSummary().countAsMap().find { k, v ->
			k instanceof PropertyDto && k.name() == 'prop' && k.value() == "value3" }

		r.effectiveMarkerSummary().countAsMap().find { k, v ->
			k instanceof PropertyDto && k.name() == 'prop' && k.value() == "value2" }.value == 1

		and:
		r.effectiveMarkerSummary().propertyValuesAsMap().size() == 1
		with(r.effectiveMarkerSummary().propertyValuesAsMap().find { k, v ->
			k instanceof BasePropertyDto
					&& k.name() == "prop" }) {

			it.value.size() == 3
			it.value.containsAll("value1", "value2", "value3")
		}

		and:
		r.effectiveMarkerSummary().durationAsMap().size() == 4
		
		r.effectiveMarkerSummary().durationAsMap().find { k, v ->
			k instanceof TagDto && k.name() == "one"
		}.value == Duration.ofHours(2)
		
		r.effectiveMarkerSummary().durationAsMap().find { k, v ->
			k instanceof TagDto && k.name() == "two"
		}.value == Duration.ofHours(2)

		r.effectiveMarkerSummary().durationAsMap().find { k, v ->
			k instanceof PropertyDto && k.name() == "prop" &&
					k.value() == "value2"
		}.value == Duration.ofHours(2)
		
		r.effectiveMarkerSummary().durationAsMap().find { k, v ->
			k instanceof PropertyDto && k.name() == "prop" &&
					k.value() == "value3"
		}.value == Duration.ofHours(2)
	}
}

