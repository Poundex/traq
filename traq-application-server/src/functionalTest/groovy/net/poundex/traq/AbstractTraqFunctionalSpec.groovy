package net.poundex.traq

import graphql.schema.Coercing
import net.poundex.testing.spock.functional.AbstractFunctionalSpec
import net.poundex.testing.spock.functional.GraphQl
import net.poundex.testing.spock.functional.GraphQlApplicationResourceProvider
import net.poundex.traq.api.client.LedgersGraphQLQuery
import net.poundex.traq.api.client.LedgersProjectionRoot
import net.poundex.traq.ledger.LedgerDto
import net.poundex.traq.test.TestDataClient
import net.poundex.xmachinery.spring.graphql.client.ScalarUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Import
import org.springframework.graphql.execution.GraphQlSource
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.TestPropertySource

@Import(FunctionalTestConfiguration)
@TestPropertySource(properties = "json.logging.enabled=false")
@ActiveProfiles(value = [ "ftest", "embedded" ])
@CleanData
class AbstractTraqFunctionalSpec extends AbstractFunctionalSpec implements GraphQl, GraphQlApplicationResourceProvider {

	protected static String defaultLedgerId
	
	@Delegate
	protected TestDataClient dataHelper
	
	@Autowired
	GraphQlSource graphQlSource
	
	void setupSpec() {
		defaultLedgerId = null
	}
	
	void setup() {
		dataHelper = new TestDataClient(this)
		if ( ! defaultLedgerId)
			defaultLedgerId = queryList(LedgerDto, 
					LedgersGraphQLQuery.newRequest().build(), 
					new LedgersProjectionRoot<>().id())
					.first()
					.id()
	}

	@Override
	Map<Class<?>, ? extends Coercing<?, ?>> getScalars() {
		return ScalarUtils.getScalarsByInputType(graphQlSource)
	}
}
