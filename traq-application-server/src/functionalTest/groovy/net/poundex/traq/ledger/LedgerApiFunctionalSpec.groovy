package net.poundex.traq.ledger

import net.poundex.traq.AbstractTraqFunctionalSpec
import net.poundex.traq.CleanData
import net.poundex.traq.FunctionalTestConfiguration
import net.poundex.traq.api.client.*
import org.springframework.context.annotation.Import
import spock.lang.Stepwise

import java.time.Duration

@Import(FunctionalTestConfiguration)
@Stepwise
@CleanData
class LedgerApiFunctionalSpec extends AbstractTraqFunctionalSpec {
	
	static String lastSeenId
	
	void "Creates ledgers"() {
		when:
		LedgerDto created = query(LedgerDto, CreateLedgerGraphQLQuery.newRequest()
				.ledger(new LedgerInputDto("New Ledger", Duration.ofHours(7)))
				.build(), new CreateLedgerProjectionRoot<>().id().name().targetDuration())
		
		then:
		created.id()
		created.name() == "New Ledger"
		created.targetDuration() == Duration.ofHours(7)
		
		cleanup:
		lastSeenId = created.id()
	}
	
	void "Returns ledgers"() {
		when:
		List<LedgerDto> ledgers = queryList(LedgerDto, "{ ledgers { id name targetDuration } }")
		
		then:
		ledgers.size() >= 1
		ledgers.find {
			it.id()
					&& it.name() == "New Ledger"
					&& it.targetDuration() == Duration.ofHours(7)
		}
	}
	
	void "Returns single ledger by id"() {
		when:
		LedgerDto r = query(LedgerDto, LedgerGraphQLQuery.newRequest().id(lastSeenId).build(),
				new LedgerProjectionRoot<>()
						.id()
						.name()
						.targetDuration()
						.markers()
							.__typename()
							.name()
							.title()
							.onProperty().value().root())
		
		then:
		r.id() == lastSeenId
		r.name() == "New Ledger"
		r.targetDuration() == Duration.ofHours(7)
		r.markers().size() == 2
		r.markers().find { it.asString() == "+noprod" }
	}
	
	void "Returns single ledger by name"() {
		when:
		LedgerDto r = query(LedgerDto, '{ ledgerByName(name: "New Ledger") { id name targetDuration } }')

		then:
		r.id() == lastSeenId
		r.name() == "New Ledger"
		r.targetDuration() == Duration.ofHours(7)
	}
	
	void "Updates ledgers"() {
		given:
		LedgerDto ledger = createLedger("Old Name", null)
		
		when:
		LedgerDto updated = query(LedgerDto, UpdateLedgerGraphQLQuery.newRequest()
				.id(ledger.id())
				.ledger(new LedgerInputDto("New Name", Duration.ofHours(7)))
				.build(), new UpdateLedgerProjectionRoot<>().id().name().targetDuration())
		
		then:
		updated.id() == ledger.id()
		updated.name() == "New Name"
		updated.targetDuration() == Duration.ofHours(7)
	}
}
