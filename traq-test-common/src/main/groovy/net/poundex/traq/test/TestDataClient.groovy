package net.poundex.traq.test

import net.poundex.testing.spock.functional.GraphQl
import net.poundex.traq.api.client.*
import net.poundex.traq.ledger.LedgerDto
import net.poundex.traq.ledger.LedgerInputDto
import net.poundex.traq.marker.MarkerDto
import net.poundex.traq.timeline.*

import java.time.Duration
import java.time.Instant
import java.time.LocalDate

class TestDataClient {
	private final GraphQl graphQl

	TestDataClient(GraphQl graphQl) {
		this.graphQl = graphQl
	}
	
	TimelinePointDto createPoint(String ledger, String description, boolean excludeDirect = false) {
		createPoint(LocalDate.now(), ledger, description, excludeDirect)
	}
	
	TimelinePointDto createPoint(LocalDate day, String ledger, String description, boolean excludeDirect = false, Instant time = null) {
		return graphQl.query(TimelinePointDto,
				CreatePointGraphQLQuery.newRequest()
						.ledger(ledger)
						.point(new CreatePointInputDto(time, day, description))
						.build(),
				new CreatePointProjectionRoot<>()
						.id()
						.description()
						.time()
						.markers()
							.__typename()
							.name()
							.parent()
						.effectiveMarkers(excludeDirect)
							.__typename()
							.name())
	}
	
	StartSpanInfoDto startSpan(String ledger, String description, LocalDate day = null, Instant time = null) {
		return graphQl.query(StartSpanInfoDto,
				StartSpanGraphQLQuery.newRequest()
						.ledger(ledger)
						.span(new StartSpanInputDto(time, day, description))
						.build(),
				new StartSpanProjectionRoot<>()
						.span()
							.id()
							.description()
							.active().
							parent()
						.previousSpan()
							.id()
							.description()
							.active())
	}

	TimelineSpanDto createSpan(String ledger, LocalDate day, Instant time, Instant endTime, String description) {
		return graphQl.query(TimelineSpanDto,
				CreateSpanGraphQLQuery.newRequest()
						.ledger(ledger)
						.span(new CreateSpanInputDto(time, endTime, day, description))
						.build(),
				new CreateSpanProjectionRoot<>().id().description())
	}

	LedgerDto createLedger(String name, Duration targetDuration) {
		return graphQl.query(LedgerDto,
				CreateLedgerGraphQLQuery.newRequest()
						.ledger(new LedgerInputDto(name, targetDuration))
						.build(), 
				new CreateLedgerProjectionRoot<>().id().name().targetDuration())
	}
	
	MarkerDto modifyMarker(String ledgerId, String marker, List<String> markers) {
		graphQl.query(MarkerDto,
				ModifyMarkerGraphQLQuery.newRequest()
						.marker(marker)
						.markers(markers)
						.ledger(ledgerId)
						.build(),
				new ModifyMarkerProjectionRoot<>().__typename().id().name().title())
	}

	MarkerDto updateMarker(String markerId, String title) {
		graphQl.query(MarkerDto,
				UpdateMarkerGraphQLQuery.newRequest()
						.marker(markerId)
						.title(title)
						.build(),
				new ModifyMarkerProjectionRoot<>().__typename().id().name().title())
	}

	TimelineSpanDto stopActiveSpan(String ledger) {
		return graphQl.query(TimelineSpanDto,
				StopActiveSpanGraphQLQuery.newRequest()
						.ledger(ledger)
						.build(), 
				new StopActiveSpanProjectionRoot<>().id())
	}
}