package net.poundex.traq.web

import com.netflix.graphql.dgs.client.codegen.BaseSubProjectionNode
import com.netflix.graphql.dgs.client.codegen.GraphQLQuery
import net.poundex.traq.api.client.LedgerGraphQLQuery
import net.poundex.traq.api.client.LedgerProjectionRoot
import net.poundex.traq.api.client.LedgersGraphQLQuery
import net.poundex.traq.api.client.LedgersProjectionRoot
import net.poundex.traq.ledger.LedgerDto
import net.poundex.xmachinery.spring.graphql.client.GraphQlClient
import net.poundex.xmachinery.spring.graphql.client.QueryResponse
import net.poundex.xmachinery.spring.graphql.error.NotFoundException
import net.poundex.xmachinery.test.PublisherUtils
import org.springframework.http.HttpCookie
import org.springframework.http.HttpHeaders
import org.springframework.http.ResponseCookie
import org.springframework.http.server.reactive.ServerHttpRequest
import org.springframework.http.server.reactive.ServerHttpResponse
import org.springframework.util.MultiValueMap
import org.springframework.web.server.ServerWebExchange
import org.springframework.web.server.WebFilterChain
import reactor.core.publisher.Mono
import spock.lang.Specification
import spock.lang.Subject

class LedgerFilterSpec extends Specification implements PublisherUtils, GraphQlUtils {
	
	GraphQlClient graphQlClient = Mock()
	Map<String, Object> attributes = Mock()
	MultiValueMap<String, String> requestQueryParams = Stub() {
	}
	ServerHttpRequest request = Stub() {
		getURI() >> URI.create("http://someuri")
		getQueryParams() >> requestQueryParams
	}
	HttpHeaders responseHeaders = Mock()
	ServerHttpResponse response = Mock() {
		getHeaders() >> responseHeaders
	}
	ServerWebExchange exchange = Stub() {
		getAttributes() >> attributes
		getResponse() >> response
		getRequest() >> request
	}
	WebFilterChain chain = Mock()
	
	@Subject
	LedgerFilter filter = new LedgerFilter(graphQlClient)
	
	void "Looks up, puts into context and sets cookie for no ledger cookie"() {
		given:
		requestQueryParams.getFirst(_) >> null
		request.getCookies() >> Stub(MultiValueMap<String, HttpCookie>) {
			getFirst("ledger") >> null
		}
		
		when:
		block filter.filter(exchange, chain)

		then:
		1 * graphQlClient.query(
				{ q -> sameQuery(q, LedgersGraphQLQuery.newRequest().build()) },
				{ pr -> sameProjection(pr, new LedgersProjectionRoot<>().id().name()) }) >> Mono.just(Stub(QueryResponse) {
			extractList(LedgerDto.class) >> [new LedgerDto("defl", "Default", null, Collections.emptySet())]
		})
		
		and:
		1 * attributes.put("ledgerId", "defl")
		1 * response.addCookie({ ResponseCookie cookie -> 
			cookie.name == "ledger" && cookie.value == "defl" })
		1 * chain.filter(exchange) >> Mono.empty()
	}

	void "Checks, puts into context and sets cookie for good ledger cookie"() {
		given:
		requestQueryParams.getFirst(_) >> null
		request.getCookies() >> Stub(MultiValueMap<String, HttpCookie>) {
			getFirst("ledger") >> new HttpCookie("ledger", "defl")
		}

		when:
		block filter.filter(exchange, chain)

		then:
		1 * graphQlClient.query(
				{ q -> sameQuery(q, LedgerGraphQLQuery.newRequest().id("defl").build()) },
				{ pr -> sameProjection(pr, new LedgerProjectionRoot<>().id()) }) >> Mono.just(Stub(QueryResponse) {
			extractValue(LedgerDto.class) >> new LedgerDto("defl", "Default", null, Collections.emptySet())
		})
		
		0 * graphQlClient.query(_ as String)
		
		and:
		1 * attributes.put("ledgerId", "defl")
		1 * response.addCookie({ ResponseCookie cookie ->
			cookie.name == "ledger" && cookie.value == "defl" })
		1 * chain.filter(exchange) >> Mono.empty()
	}

	void "Checks, and uses default for bad ledger cookie"() {
		given:
		requestQueryParams.getFirst(_) >> null
		request.getCookies() >> Stub(MultiValueMap<String, HttpCookie>) {
			getFirst("ledger") >> new HttpCookie("ledgerId", "bad")
		}

		when:
		block filter.filter(exchange, chain)

		then:
		1 * graphQlClient.query(
				{ q -> sameQuery(q, LedgerGraphQLQuery.newRequest().id("bad").build()) },
				{ pr -> sameProjection(pr, new LedgerProjectionRoot<>().id()) }) >> Mono.error(new NotFoundException("Ledger", "bad"))
		1 * graphQlClient.query(
				{ q -> sameQuery(q, LedgersGraphQLQuery.newRequest().build()) },
				{ pr -> sameProjection(pr, new LedgersProjectionRoot<>().id().name()) }) >> Mono.just(Stub(QueryResponse) {
			extractList(LedgerDto.class) >> [new LedgerDto("defl", "Default", null, Collections.emptySet())]
		})
		
		and:
		1 * attributes.put("ledgerId", "defl")
		1 * response.addCookie({ ResponseCookie cookie ->
			cookie.name == "ledger" && cookie.value == "defl" })
		1 * chain.filter(exchange) >> Mono.empty()
	}
	
	void "Sets new ledger with good set ledger request"() {
		given:
		requestQueryParams.getFirst("ledger") >> "good"

		when:
		block filter.filter(exchange, chain)

		then:
		1 * graphQlClient.query(
				{ q -> sameQuery(q, LedgerGraphQLQuery.newRequest().id("good").build()) },
				{ pr -> sameProjection(pr, new LedgerProjectionRoot<>().id()) }) >> Mono.just(Stub(QueryResponse) {
			extractValue(LedgerDto.class) >> new LedgerDto("good", "Default", null, Collections.emptySet())
		})

		and:
		1 * attributes.put("ledgerId", "good")
		1 * response.addCookie({ ResponseCookie cookie ->
			cookie.name == "ledger" && cookie.value == "good"
		})
		1 * chain.filter(exchange) >> Mono.empty()

		and:
		1 * responseHeaders.add("HX-Trigger", "currentLedgerChanged")
	}
	
	void "Sets default ledger with bad set ledger request"() {
		given:
		requestQueryParams.getFirst("ledger") >> "bad"

		when:
		block filter.filter(exchange, chain)

		then:
		1 * graphQlClient.query(
				{ q -> sameQuery(q, LedgerGraphQLQuery.newRequest().id("bad").build()) },
				{ pr -> sameProjection(pr, new LedgerProjectionRoot<>().id()) }) >> Mono.error(new NotFoundException("Ledger", "bad"))
		1 * graphQlClient.query(
				{ q -> sameQuery(q, LedgersGraphQLQuery.newRequest().build()) },
				{ pr -> sameProjection(pr, new LedgersProjectionRoot<>().id().name()) }) >> Mono.just(Stub(QueryResponse) {
			extractList(LedgerDto.class) >> [new LedgerDto("defl", "Default", null, Collections.emptySet())]
		})

		and:
		1 * attributes.put("ledgerId", "defl")
		1 * response.addCookie({ ResponseCookie cookie ->
			cookie.name == "ledger" && cookie.value == "defl"
		})
		1 * chain.filter(exchange) >> Mono.empty()

		and:
		1 * responseHeaders.add("HX-Trigger", "currentLedgerChanged")
	}
}

trait GraphQlUtils {
	boolean sameQuery(GraphQLQuery left, GraphQLQuery right) {
		return left.class == right.class
				&& left.operationName == right.operationName
				&& left.variableDefinitions == right.variableDefinitions
				&& left.input == right.input
	}
	
	boolean sameProjection(BaseSubProjectionNode left, BaseSubProjectionNode right) {
		return left.class == right.class 
				&& left.fields == right.fields
				&& left.inputArguments == right.inputArguments
				&& left.fragments == right.fragments
	}
}