package net.poundex.traq.marker;

import jakarta.validation.Validator;
import net.poundex.traq.api.client.MarkerGraphQLQuery;
import net.poundex.traq.api.client.MarkerProjectionRoot;
import net.poundex.traq.api.client.MarkersGraphQLQuery;
import net.poundex.traq.api.client.MarkersProjectionRoot;
import net.poundex.traq.api.client.SetMarkersGraphQLQuery;
import net.poundex.traq.api.client.SetMarkersProjectionRoot;
import net.poundex.traq.api.client.UpdateMarkerGraphQLQuery;
import net.poundex.traq.api.client.UpdateMarkerProjectionRoot;
import net.poundex.traq.web.RequestContext;
import net.poundex.xmachinery.e1ui.entitycontroller.AbstractUnifiedInputEntityController;
import net.poundex.xmachinery.spring.graphql.client.GraphQlClient;
import net.poundex.xmachinery.spring.graphql.client.QueryRequest;
import net.poundex.xmachinery.spring.hx.Hx;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.reactive.result.view.Rendering;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.stream.Stream;

@Controller
@RequestMapping("/marker")
class MarkerController extends AbstractUnifiedInputEntityController<
		MarkerDto,
		String,
		RequestContext> {

	public MarkerController(GraphQlClient graphQlClient, Validator validator) {
		super(graphQlClient, MarkerDto.class, "marker", "Marker", validator);
	}

	@Override
	protected QueryRequest indexQueryRequest(net.poundex.traq.web.RequestContext requestContext) {
		return new QueryRequest(MarkersGraphQLQuery.newRequest().ledger(requestContext.getLedgerId()).build(),
				new MarkersProjectionRoot<>()
						.id()
						.__typename()
						.name()
						.title()
						.onProperty().value().root()
						.markers()
							.__typename()
							.id()
							.name()
							.title()
							.onProperty().value().parent()
							.root());
	}

	@Override
	protected QueryRequest viewQueryRequest(String id) {
		return QueryRequest.of(MarkerGraphQLQuery.newRequest().id(id).build(), oneWithDetailsProjection());
	}

	@Override
	protected QueryRequest saveNewQueryRequest(String markerInputDto) {
		throw new UnsupportedOperationException();
	}

	@Override
	protected QueryRequest updateExistingQueryRequest(String id, String markerInputDto) {
		throw new UnsupportedOperationException();
	}

	private MarkerProjectionRoot<?, ?> oneWithDetailsProjection() {
		return new MarkerProjectionRoot<>()
				.id()
				.__typename()
				.name()
				.title()
				.onProperty().value().root()
				.markers()
					.__typename()
					.name()
					.title()
					.id()
					.onProperty().value().parent()
					.root();
	}

	@Override
	protected String getRedirectUrl(MarkerDto saved) {
		return STR."/marker/\{saved.id()}";
	}

	@Override
	protected String getSavedMessage(MarkerDto saved) {
		return STR."\{saved.name()} saved";
	}

	@Override
	protected String toInput(MarkerDto existing) {
		return existing.title();
	}

	@GetMapping("/editTitle/{markerId}")
	public Flux<Rendering> editTitleStartEdit(@PathVariable String markerId, Hx hx) {
		return graphQlClient.query(MarkerGraphQLQuery.newRequest()
						.id(markerId)
						.build(), new MarkerProjectionRoot<>().__typename().id().name().title())
				.map(r -> r.extractValue(MarkerDto.class))
				.flatMapMany(marker -> hx
						.modelAttribute("marker", marker)
						.render("marker/edittitle"));
	}

	@PostMapping("/editTitle/{markerId}")
	public Flux<Rendering> editTitleCommitEdit(@PathVariable String markerId, Hx hx, RequestContext requestContext) {
		return requestContext.formData()
				.mapNotNull(fd -> fd.get("title"))
				.flatMap(newTitle -> graphQlClient.query(UpdateMarkerGraphQLQuery.newRequest()
										.marker(markerId)
										.title(StringUtils.hasText(newTitle) ? newTitle : null)
										.build(),
								new UpdateMarkerProjectionRoot<>().id()))
				.flatMapMany(_ -> hx
						.trigger("update-container-marker")
						.flash("success", "Marker updated")
						.getValue(
								() -> index(hx, requestContext),
								() -> view(hx, markerId, requestContext)));
	}

	@DeleteMapping("/editTitle/{markerId}")
	public Flux<Rendering> amendCommitCancel(@PathVariable String markerId, Hx hx) {
		return graphQlClient.query(MarkerGraphQLQuery.newRequest().id(markerId).build(), oneWithDetailsProjection())
				.map(r -> r.extractValue(MarkerDto.class))
				.flatMapMany(m -> hx
						.modelAttribute("item", m)
						.render("marker/editabletitle"));
	}
	
	@PostMapping("/{markerId}/remark")
	public Flux<Rendering> remark(@PathVariable String markerId, Hx hx, RequestContext requestContext) {
		return requestContext.formData()
				.mapNotNull(fd -> fd.get("markers"))
				.flatMap(markers -> graphQlClient.query(MarkerGraphQLQuery.newRequest().id(markerId).build(), oneWithDetailsProjection())
						.map(qr -> qr.extractValue(MarkerDto.class))
						.flatMap(toModify -> graphQlClient.query(SetMarkersGraphQLQuery.newRequest()
								.marker(markerId)
								.markers(Stream.concat(
												toModify.markers().stream()
														.map(MarkerDto::asString),
												Arrays.stream(markers.split(" ")))
										.toList())
								.build(), new SetMarkersProjectionRoot<>().id())))
				.flatMapMany(_ -> hx
						.flash("success", "Marker updated")
						.getValue(
						() -> index(hx, requestContext), 
						() -> view(hx, markerId, requestContext)));
	}

	@DeleteMapping("/{markerId}/remark")
	public Flux<Rendering> removeMarker(@PathVariable String markerId, Hx hx, RequestContext requestContext) {
		return requestContext.formData()
				.mapNotNull(fd -> fd.get("without"))
				.flatMap(without -> doRemoveMarker(markerId, without))
				.flatMapMany(marker -> hx
						.modelAttribute("item", marker)
						.flash("success", "Marker removed")
						.render("marker/markers"));
	}

	@PostMapping(value = "/{markerId}/remark", params = {"without"})
	public Flux<Rendering> removeMarkerMobile(
			@PathVariable String markerId, 
			Hx hx,
			@RequestParam String without) {
		
		return doRemoveMarker(markerId, without)
				.flatMapMany(marker -> hx
						.modelAttribute("existing", marker)
						.flash("success", "Marker removed")
						.render("marker/view"));
	}
	
	private Mono<MarkerDto> doRemoveMarker(String markerId, String without) {
		return graphQlClient.query(MarkerGraphQLQuery.newRequest().id(markerId).build(), oneWithDetailsProjection())
				.map(qr -> qr.extractValue(MarkerDto.class))
				.flatMap(toModify -> graphQlClient.query(SetMarkersGraphQLQuery.newRequest()
						.marker(markerId)
						.markers(toModify.markers().stream()
								.filter(it -> ! it.id().equals(without))
								.map(MarkerDto::asString)
								.toList())
						.build(), oneWithDetailsProjection()))
				.map(qr -> qr.extractValue(MarkerDto.class));
	}
}
