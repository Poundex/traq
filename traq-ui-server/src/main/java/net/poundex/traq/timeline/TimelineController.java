package net.poundex.traq.timeline;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.reactive.result.view.Rendering;
import reactor.core.publisher.Mono;

@Controller
@RequestMapping("/timeline")
class TimelineController {
	@GetMapping
	public Mono<Rendering> timelineTable() {
		return Mono.just(Rendering.view("timeline").build());
	}
}
