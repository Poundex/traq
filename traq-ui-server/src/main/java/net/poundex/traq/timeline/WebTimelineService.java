package net.poundex.traq.timeline;

import net.poundex.traq.api.client.SummaryGraphQLQuery;
import net.poundex.traq.api.client.SummaryProjectionRoot;
import net.poundex.traq.ledger.LedgerDto;
import net.poundex.traq.marker.MarkableService;
import net.poundex.traq.summary.EventSummaryDto;
import net.poundex.xmachinery.spring.graphql.client.GraphQlClient;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class WebTimelineService extends AbstractTimelineService {
	
	public WebTimelineService(GraphQlClient graphQlClient, MarkableService markableService) {
		super(graphQlClient, markableService);
	}

	@Override
	protected Mono<LedgerDto> getLedgerByName(String ledgerName) {
		throw new UnsupportedOperationException();
	}

	public Mono<EventSummaryDto> eventSummaryEffectiveMarkerSummary(String ledgerId, String query) {
		return graphQlClient.query(SummaryGraphQLQuery.newRequest()
						.ledger(ledgerId)
						.timelineQuery(query)
						.build(), new SummaryProjectionRoot<>()
						.effectiveMarkerSummary()
							.duration()
							.marker()
								.__typename()
								.id()
								.name()
								.title()
								.onProperty().value().parent()
								.parent()
							.value()
						
							.parent()
						.count()
							.marker()
								.__typename()
								.id()
								.name()
								.title()
								.onProperty()
									.value()
									.parent()
								.parent()
							.value()
							.parent()
						.propertyValues()
							.marker()
								.__typename()
								.id()
								.name()
								.title()
								.parent()
							.value())
				.map(resp -> resp.extractValue("summary", EventSummaryDto.class));
	}
}
