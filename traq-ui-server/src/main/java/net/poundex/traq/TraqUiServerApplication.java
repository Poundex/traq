package net.poundex.traq;

import com.fasterxml.jackson.databind.Module;
import graphql.ErrorClassification;
import net.poundex.autoconverters.convert.EnableAutoConverters;
import net.poundex.traq.timeline.query.QueryException;
import net.poundex.xmachinery.e1ui.graphql.GraphQlErrorHandler;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.graphql.GraphQlAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;

@SpringBootApplication(exclude = GraphQlAutoConfiguration.class)
@Import(GraphQlAutoConfiguration.class)
@ConfigurationPropertiesScan
@EnableAutoConverters
public class TraqUiServerApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(TraqUiServerApplication.class, args);
	}
	
	@Bean
	public GraphQlErrorHandler timelineQueryErrorHandler() {
		return new GraphQlErrorHandler(
				ErrorClassification.errorClassification("TimelineQueryError"),
				err -> new QueryException(err.getMessage()));
	}

	@Bean
	public Module traqModule() {
		return new TraqModule();
	}
}
