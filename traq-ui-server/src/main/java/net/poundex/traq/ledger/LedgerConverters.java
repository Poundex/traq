package net.poundex.traq.ledger;

import net.poundex.autoconverters.convert.Converters;

@Converters
interface LedgerConverters {
	LedgerInputDto toInput(LedgerDto ledgerDto);
}
