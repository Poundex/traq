package net.poundex.traq.ledger;

import jakarta.validation.Validator;
import net.poundex.traq.api.client.CreateLedgerGraphQLQuery;
import net.poundex.traq.api.client.LedgerGraphQLQuery;
import net.poundex.traq.api.client.LedgerProjectionRoot;
import net.poundex.traq.api.client.LedgersGraphQLQuery;
import net.poundex.traq.api.client.LedgersProjectionRoot;
import net.poundex.traq.api.client.UpdateLedgerGraphQLQuery;
import net.poundex.xmachinery.e1ui.entitycontroller.AbstractUnifiedInputEntityController;
import net.poundex.xmachinery.e1ui.requestcontext.RequestContext;
import net.poundex.xmachinery.spring.graphql.client.GraphQlClient;
import net.poundex.xmachinery.spring.graphql.client.QueryRequest;
import net.poundex.xmachinery.spring.graphql.client.QueryResponse;
import net.poundex.xmachinery.spring.hx.Hx;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.function.BiFunction;

@Controller
@RequestMapping("/ledger")
class LedgerController extends AbstractUnifiedInputEntityController<
		LedgerDto,
		LedgerInputDto,
		RequestContext> {

	private final LedgerConverters converters;

	public LedgerController(GraphQlClient graphQlClient, LedgerConverters converters, Validator validator) {
		super(graphQlClient, LedgerDto.class, "ledger", "Ledger", validator);
		this.converters = converters;
	}

	@Override
	protected QueryRequest indexQueryRequest(RequestContext params) {
		return new QueryRequest(LedgersGraphQLQuery.newRequest().build(), 
				new LedgersProjectionRoot<>().id().name().targetDuration());
	}

	@Override
	protected QueryRequest viewQueryRequest(String id) {
		return QueryRequest.of(LedgerGraphQLQuery.newRequest().id(id).build(), oneWithDetailsProjection());
	}

	@Override
	protected QueryRequest saveNewQueryRequest(LedgerInputDto inputDto) {
		return QueryRequest.of(CreateLedgerGraphQLQuery.newRequest()
						.ledger(inputDto)
						.build(),
				oneWithDetailsProjection());
	}

	@Override
	protected QueryRequest updateExistingQueryRequest(String id, LedgerInputDto inputDto) {
		return QueryRequest.of(UpdateLedgerGraphQLQuery.newRequest()
						.id(id)
						.ledger(inputDto)
						.build(),
				oneWithDetailsProjection());
	}

	@Override
	protected Hx populateAfterSaveModelAnd(Hx hx, QueryResponse saveResponse, QueryResponse afterSaveResponse, BiFunction<Hx, LedgerDto, Hx> thenFn, RequestContext requestContext) {
		return super.populateAfterSaveModelAnd(
				hx.trigger("ledgerUpdated"), saveResponse, afterSaveResponse, thenFn, requestContext);
	}

	private LedgerProjectionRoot<?, ?> oneWithDetailsProjection() {
		return new LedgerProjectionRoot<>()
				.id()
				.name()
				.targetDuration();
	}

	@Override
	protected String getRedirectUrl(LedgerDto saved) {
		return STR."/ledger/\{saved.id()}";
	}

	@Override
	protected String getSavedMessage(LedgerDto saved) {
		return STR."\{saved.name()} saved";
	}
	
	@Override
	protected LedgerInputDto toInput(LedgerDto existing) {
		return converters.toInput(existing);
	}
}
