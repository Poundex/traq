package net.poundex.traq.log;

import lombok.RequiredArgsConstructor;
import net.poundex.traq.marker.MarkableService;
import net.poundex.traq.timeline.TimelinePointDto;
import net.poundex.traq.timeline.WebTimelineService;
import net.poundex.traq.timeline.query.QueryException;
import net.poundex.traq.web.RequestContext;
import net.poundex.traq.web.handlerarg.LedgerId;
import net.poundex.xmachinery.spring.hx.Hx;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.reactive.result.view.Rendering;
import reactor.core.publisher.Flux;

import java.util.function.Function;

@Controller
@RequestMapping("/log")
@RequiredArgsConstructor
public class LogController {
    
    private final WebTimelineService timelineService;
    private final MarkableService markableService;
    
    @GetMapping
    public Flux<Rendering> log(@LedgerId String ledgerId, Hx hx, @RequestParam(required = false, name = "q") String query) {
        if (query == null)
            return hx.render("log/log");

        return respond(ledgerId, query, hx, h -> h
                .render("log/log"))
                .onErrorResume(QueryException.class, x -> hx
                        .modelAttribute("queryError", x.getMessage())
                        .render("log/log"));
    }
    
    @PostMapping("/query")
    public Flux<Rendering> query(Hx hx, RequestContext requestContext) {
        return requestContext.formData()
                .map(m -> m.get("query"))
                .flatMapMany(query -> respond(requestContext.getLedgerId(), query, hx, h -> h
                        .outOfBand()
                        .render(hx.getValue(new String[]{"log/_queryjumbo", "log/_eventsummary"}, new String[]{"log/log"}))))
                .onErrorResume(QueryException.class, x -> hx
                        .modelAttribute("queryError", x.getMessage())
                        .outOfBand()
                        .render(hx.getValue("log/_queryjumbo", "log/log")));
    }

    private Flux<Rendering> respond(@LedgerId String ledgerId, String query, Hx hx, Function<Hx, Flux<Rendering>> fn) {
        return timelineService.getEventSummary2(ledgerId, query)
                .flatMapMany(espm -> fn.apply(hx
                        .modelAttribute("eventSummaryPm", espm)
                        .modelAttribute("recentPoints", espm.getEventSummary().events().stream()
                                .filter(e -> e instanceof TimelinePointDto)
                                .sorted((l, r) -> r.time().compareTo(l.time()))
                                .toList())));
    }

    @GetMapping("/effectiveMarkerSummary")
    public Flux<Rendering> effectiveMarkerSummary(@LedgerId String ledgerId, @RequestParam(name = "q") String query, Hx hx) {
        return timelineService.eventSummaryEffectiveMarkerSummary(ledgerId, query)
                .map(r -> hx.modelAttribute("effectiveMarkerSummary",
                        markableService.getMarkerSummaries(r.effectiveMarkerSummary())))
                .flatMapMany(h -> h.render("timeline/_effectivemarkersummary"));
    }
}
