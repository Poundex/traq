package net.poundex.traq.web;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.poundex.traq.api.client.LedgerGraphQLQuery;
import net.poundex.traq.api.client.LedgerProjectionRoot;
import net.poundex.traq.api.client.LedgersGraphQLQuery;
import net.poundex.traq.api.client.LedgersProjectionRoot;
import net.poundex.traq.ledger.LedgerDto;
import net.poundex.xmachinery.spring.graphql.client.GraphQlClient;
import net.poundex.xmachinery.spring.graphql.error.NotFoundException;
import org.springframework.http.HttpCookie;
import org.springframework.http.ResponseCookie;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.Optional;
import java.util.Set;

@Component
@RequiredArgsConstructor
@Slf4j
class LedgerFilter implements WebFilter {
	
	private final Set<String> IGNORE_LIST = Set.of("/webjars", "/script", "/css", "/actuator");

	private final GraphQlClient graphQlClient;

	@Override
	public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
		if (IGNORE_LIST.stream().anyMatch(it -> exchange.getRequest().getURI().getPath().startsWith(it)))
			return chain.filter(exchange);

		return Mono.justOrEmpty(exchange.getRequest().getQueryParams().getFirst("ledger"))
				.doOnNext(_ -> {
					exchange.getResponse().getHeaders().add("HX-Trigger", "currentLedgerChanged");
					exchange.getAttributes().put("currentLedgerChanged", true);
				})
				.switchIfEmpty(Mono.justOrEmpty(Optional.ofNullable(exchange.getRequest().getCookies().getFirst("ledger")))
						.map(HttpCookie::getValue))
				.flatMap(id -> getLedger(id).map(LedgerDto::id))
				.switchIfEmpty(Mono.defer(() -> graphQlClient.query(LedgersGraphQLQuery.newRequest().build(), new LedgersProjectionRoot<>().id().name())
						.map(r -> r.extractList(LedgerDto.class))
						.map(m -> m.getFirst().id())))
				.doOnNext(ledgerId -> exchange.getAttributes().put("ledgerId", ledgerId))
				.doOnNext(ledgerId ->
						exchange.getResponse().addCookie(ResponseCookie
								.from("ledger", ledgerId)
								.path("/")
								.maxAge(Duration.ofDays(1_000))
								.sameSite("Strict")
								.build()))
				.then(chain.filter(exchange));
	}
	
	private Mono<LedgerDto> getLedger(String ledgerId) {
		return graphQlClient.query(LedgerGraphQLQuery.newRequest().id(ledgerId).build(), new LedgerProjectionRoot<>().id())
				.map(r -> r.extractValue(LedgerDto.class))
				.onErrorComplete(NotFoundException.class);
	}
}
