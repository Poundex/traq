package net.poundex.traq.web.thymeleaf;

import org.springframework.stereotype.Component;
import org.thymeleaf.context.IExpressionContext;
import org.thymeleaf.expression.IExpressionObjectFactory;

import java.util.Set;

@Component
class TraqExpressionObjectFactory implements IExpressionObjectFactory {

	private static final String SPANS_EXPR_OBJ_NAME = "spans";
	private static final String EVENTS_EXPR_OBJ_NAME = "events";
	private static final String MARKERS_EXPR_OBJ_NAME = "markers";
	private static final String TEMPORALS_EXPR_OBJ_NAME = "moreTemporals";

	@Override
	public Set<String> getAllExpressionObjectNames() {
		return Set.of(SPANS_EXPR_OBJ_NAME, EVENTS_EXPR_OBJ_NAME, MARKERS_EXPR_OBJ_NAME, TEMPORALS_EXPR_OBJ_NAME);
	}

	@Override
	public Object buildObject(IExpressionContext context, String expressionObjectName) {
		if(expressionObjectName.equals(SPANS_EXPR_OBJ_NAME))
			return new SpansUtility();
		if(expressionObjectName.equals(EVENTS_EXPR_OBJ_NAME))
			return new EventsUtility();
		if(expressionObjectName.equals(MARKERS_EXPR_OBJ_NAME))
			return new MarkersUtility();
		if(expressionObjectName.equals(TEMPORALS_EXPR_OBJ_NAME))
			return new TemporalsUtility();

		return null;
	}

	@Override
	public boolean isCacheable(String expressionObjectName) {
		return true;
	}
}
