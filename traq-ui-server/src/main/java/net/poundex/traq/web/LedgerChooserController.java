package net.poundex.traq.web;

import lombok.RequiredArgsConstructor;
import net.poundex.traq.api.client.LedgersGraphQLQuery;
import net.poundex.traq.api.client.LedgersProjectionRoot;
import net.poundex.traq.ledger.LedgerDto;
import net.poundex.traq.web.handlerarg.LedgerId;
import net.poundex.xmachinery.spring.graphql.client.GraphQlClient;
import net.poundex.xmachinery.spring.hx.Hx;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.reactive.result.view.Rendering;
import reactor.core.publisher.Flux;

@Controller
@RequiredArgsConstructor
@RequestMapping("/ledgerchooser")
class LedgerChooserController {
	private final GraphQlClient graphQlClient;
	
	@GetMapping
	public Flux<Rendering> ledgerChooser(@LedgerId String ledgerId, Hx hx, RequestContext requestContext) {
		return graphQlClient.query(
						LedgersGraphQLQuery.newRequest().build(),
						new LedgersProjectionRoot<>().id().name())
				.map(r -> r.extractList(LedgerDto.class))
				.map(ledgers -> hx
						.modelAttribute("ledgers", ledgers)
						.modelAttribute("activeLedger", ledgerId))
				.doOnNext(h -> {
					if (requestContext.getExchange().getAttributeOrDefault("currentLedgerChanged", false))
						h.modelAttribute("activeLedgerUpdated", true);
				})
				.flatMapMany(hh -> hh.render("_ledgerchooser"));
	}
}
