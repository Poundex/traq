package net.poundex.traq.web.handlerarg;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.MethodParameter;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.BindingContext;
import org.springframework.web.reactive.result.method.SyncHandlerMethodArgumentResolver;
import org.springframework.web.server.ServerWebExchange;

@Component
@Slf4j
@Deprecated
class LedgerArgumentResolver implements SyncHandlerMethodArgumentResolver {
	
	@Override
	public boolean supportsParameter(MethodParameter parameter) {
		return parameter.getParameterType().isAssignableFrom(String.class) && 
				parameter.hasParameterAnnotation(LedgerId.class);
	}

	@Override
	public Object resolveArgumentValue(MethodParameter parameter, BindingContext bindingContext, ServerWebExchange exchange) {
		return exchange.getAttributes().get("ledgerId");
	}
}
