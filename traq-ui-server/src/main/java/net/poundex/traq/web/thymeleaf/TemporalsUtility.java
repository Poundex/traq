package net.poundex.traq.web.thymeleaf;

import org.thymeleaf.expression.Temporals;

import java.time.temporal.Temporal;
import java.util.Locale;

import static net.poundex.traq.format.TemporalFormat.getOrd;

class TemporalsUtility {
	public String longDatePlain(Temporal temporal, Locale locale) {
		Temporals temporals = new Temporals(locale);
		String ord = getOrd(temporals.day(temporal));
		return temporals.format(temporal, "eeee, d'" + ord + "' MMMM yyyy", locale);
	}

	public String longDate(Temporal temporal, Locale locale) {
		Temporals temporals = new Temporals(locale);
		String ord = getOrd(temporals.day(temporal));
		return temporals.format(temporal, "eeee, d'<sup>" + ord + "</sup>' MMMM yyyy", locale);
	}
}
