package net.poundex.traq.web;

import lombok.Getter;
import net.poundex.xmachinery.e1ui.requestcontext.DefaultRequestContext;
import org.springframework.web.server.ServerWebExchange;

import java.util.Collections;
import java.util.Map;

@Getter
public final class RequestContext extends DefaultRequestContext {
	private final String ledgerId;

	public RequestContext(
			String ledgerId,
			Map<String, String> params,
			ServerWebExchange exchange) {
		super(exchange, params);
		this.ledgerId = ledgerId;
	}

	public static RequestContext create(ServerWebExchange exchange) {
		return create(Collections.emptyMap(), exchange);
	}

	public static RequestContext create(Map<String, String> params, ServerWebExchange exchange) {
		return new RequestContext(exchange.getAttributes().get("ledgerId").toString(), params, exchange);
	}
}
