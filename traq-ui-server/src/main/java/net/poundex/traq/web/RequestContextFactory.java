package net.poundex.traq.web;

import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;

import java.util.Map;

@Component
class RequestContextFactory implements net.poundex.xmachinery.e1ui.requestcontext.RequestContextFactory {
	@Override
	public RequestContext create(ServerWebExchange exchange, Map<String, String> queryParams) {
		return RequestContext.create(exchange.getRequest().getQueryParams().toSingleValueMap(), exchange);
	}
}
