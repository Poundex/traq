package net.poundex.traq.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.reactive.result.view.Rendering;
import reactor.core.publisher.Mono;

@Controller
class WebIndexController {
	@RequestMapping("/")
	public Mono<Rendering> index() {
		return Mono.just(Rendering.redirectTo("/today").build());
	}
}
