package net.poundex.traq.web.thymeleaf;

import lombok.RequiredArgsConstructor;
import net.poundex.traq.format.MarkerFormat;
import net.poundex.traq.marker.BasePropertyDto;
import net.poundex.traq.marker.MarkerDto;
import net.poundex.traq.marker.MentionDto;
import net.poundex.traq.marker.PropertyDto;
import net.poundex.traq.marker.TagDto;

import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.Set;
import java.util.regex.Pattern;

import static net.poundex.traq.format.MarkerFormat.formatAll;

public class MarkersUtility {

	@Deprecated
	private static final Pattern tagPattern = Pattern.compile("\\B(\\+[\\w-]+)");
	@Deprecated
	private static final Pattern mentionPattern = Pattern.compile("\\B(@[\\w-]+)");
	@Deprecated
	private static final Pattern propPattern = Pattern.compile("\\B(:[\\w-]+(=[\\w-]+)?)");

	@Deprecated
	public String marker(MarkerDto dto) {
		return String.format("<span class=\"marker\">%s</span>", formatMarkers(MarkerFormat.marker(dto)));
	}

	@Deprecated
	public String formatMarkers(String string) {
		String s = tagPattern.matcher(string)
				.replaceAll(mr -> String.format("<a href=\"/log?q=%s\" class=\"marker marker-tag\">$1</a>",
						URLEncoder.encode(mr.group(), Charset.defaultCharset())));
		s = mentionPattern.matcher(s).replaceAll(mr -> String.format("<a href=\"/log?q=%s\" class=\"marker marker-mention\">$1</a>",
				URLEncoder.encode(mr.group(), Charset.defaultCharset())));
		return propPattern.matcher(s).replaceAll(mr -> String.format("<a href=\"/log?q=%s\" class=\"marker marker-property\">$1</a>",
				URLEncoder.encode(mr.group(), Charset.defaultCharset())));
	}

	@Deprecated
	public String formatMarkersMobile(String string) {
		String s = tagPattern.matcher(string)
				.replaceAll(mr -> String.format("<text href=\"/log?q=%s\" style=\"marker marker-tag\">$1</text>",
						URLEncoder.encode(mr.group(), Charset.defaultCharset())));
		s = mentionPattern.matcher(s).replaceAll(mr -> String.format("<text href=\"/log?q=%s\" style=\"marker marker-mention\">$1</text>",
				URLEncoder.encode(mr.group(), Charset.defaultCharset())));
		return propPattern.matcher(s).replaceAll(mr -> String.format("<text href=\"/log?q=%s\" style=\"marker marker-property\">$1</text>",
				URLEncoder.encode(mr.group(), Charset.defaultCharset())));
	}

	@RequiredArgsConstructor
	private abstract static class AbstractMarkerFormatter implements MarkerFormat.MarkerFormatter {
		@Override
		public String formatBaseProperty(String markerWithSigil, BasePropertyDto dto) {
			return doFormat("property", markerWithSigil, dto);
		}

		@Override
		public String formatMention(String markerWithSigil, MentionDto dto) {
			return doFormat("mention", markerWithSigil, dto);
		}

		@Override
		public String formatPropertyDto(String markerWithSigil, PropertyDto dto) {
			return doFormat("property", markerWithSigil, dto);
		}

		@Override
		public String formatTag(String markerWithSigil, TagDto dto) {
			return doFormat("tag", markerWithSigil, dto);
		}

		protected abstract String doFormat(String cssClass, String markerWithSigil, MarkerDto dto); 
	}
	
	private static final MarkerFormat.MarkerFormatter webFormatter = new AbstractMarkerFormatter() {
		@Override
		protected String doFormat(String cssClass, String markerWithSigil, MarkerDto dto) {
			String link = "<a href=\"/log?q=%s\" class=\"marker marker-%s\">%s</a>".formatted(
					URLEncoder.encode(dto.asString(), Charset.defaultCharset()),
					cssClass,
					markerWithSigil);

			if (dto.title() == null)
				return link;

			return "<sl-tooltip content=\"%s\" placement=\"bottom\">%s</sl-tooltip>".formatted(
					dto.asString(),
					link);
		}
	};
	
	private static final MarkerFormat.MarkerFormatter mobileWithLinkFormatter = new AbstractMarkerFormatter() {
		@Override
		protected String doFormat(String cssClass, String markerWithSigil, MarkerDto dto) {
			return "<text href=\"/log?q=%s\" style=\"marker marker-%s\">%s</text>".formatted(
					URLEncoder.encode(dto.asString(), Charset.defaultCharset()),
					cssClass,
					markerWithSigil);
		}
	};

	private static final MarkerFormat.MarkerFormatter mobileWithoutLinkFormatter = new AbstractMarkerFormatter() {
		@Override
		protected String doFormat(String cssClass, String markerWithSigil, MarkerDto dto) {
			return "<text style=\"marker marker-%s\">%s</text>".formatted(cssClass, markerWithSigil);
		}
	};

	public String formatWeb(MarkerDto dto) {
		return formatWeb(dto, null);
	}

	public String formatWeb(MarkerDto dto, String original) {
		return MarkerFormat.format(dto, webFormatter, original);
	}

	public String formatMobile(MarkerDto dto) {
		return formatMobile(dto, null, mobileWithLinkFormatter);
	}
	
	public String formatMobileNoLink(MarkerDto dto) {
		return formatMobile(dto, null, mobileWithoutLinkFormatter);
	}

	public String formatMobile(MarkerDto dto, String original) {
		return formatMobile(dto, original, mobileWithLinkFormatter);
	}

	public String formatMobileNoLink(MarkerDto dto, String original) {
		return formatMobile(dto, original, mobileWithoutLinkFormatter);
	}

	public String formatMobile(MarkerDto dto, String original, MarkerFormat.MarkerFormatter formatter) {
		return MarkerFormat.format(dto, formatter, original);
	}

	public String formatAllWeb(String stringWithMarkers, Set<? extends MarkerDto> markers) {
		return formatAll(stringWithMarkers, markers, this::formatWeb);
	}

	public String formatAllMobile(String stringWithMarkers, Set<? extends MarkerDto> markers) {
		return formatAll(stringWithMarkers, markers, this::formatMobile);
	}
}
