package net.poundex.traq.web.thymeleaf;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.thymeleaf.dialect.IDialect;
import org.thymeleaf.dialect.IExpressionObjectDialect;
import org.thymeleaf.expression.IExpressionObjectFactory;

@Component
@RequiredArgsConstructor
class TraqDialect implements IDialect, IExpressionObjectDialect {
	
	private final TraqExpressionObjectFactory expressionObjectFactory;
	
	@Override
	public IExpressionObjectFactory getExpressionObjectFactory() {
		return expressionObjectFactory;
	}

	@Override
	public String getName() {
		return "traq";
	}
}
