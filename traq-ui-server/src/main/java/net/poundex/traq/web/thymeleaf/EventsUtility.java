package net.poundex.traq.web.thymeleaf;

import org.thymeleaf.expression.Temporals;

import java.time.Instant;
import java.util.Locale;

public class EventsUtility {

	public String time(Instant time, Locale locale) {
		Temporals temporals = new Temporals(locale);
		return temporals.format(time, "HH:mm");
	}
}
