package net.poundex.traq.web.thymeleaf;

import net.poundex.traq.format.SpanFormat;
import org.thymeleaf.expression.Temporals;

import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class SpansUtility {
	
	private static final DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm", Locale.getDefault());

	// TODO: Rename this method
	public String duration(Duration duration) {
		return SpanFormat.toElapsedTime(duration);
	}
	
	public String timeRange(Instant start, Instant maybeEnd, Locale locale) {
		Temporals temporals = new Temporals(locale);
		return String.format("%s%s",
				temporals.format(start, "HH:mm"),
				maybeEnd != null
						? String.format(" &dash; %s", temporals.format(maybeEnd, "HH:mm"))
						: "<i class=\"bi bi-record-fill text-danger\"></i>");
	}

	public static String timeRangePlain(Instant start, Instant maybeEnd, Locale locale) {
		return String.format("%s%s",
				timeFormatter.format(instantToDateTime(start)),
				maybeEnd != null
						? String.format(" - %s", timeFormatter.format(instantToDateTime(maybeEnd)))
						: "");
	}

	public static ZonedDateTime instantToDateTime(Instant instant) {
		return ZonedDateTime.ofInstant(instant, ZoneId.systemDefault());
	}
}
