package net.poundex.traq.day;

import net.poundex.traq.api.client.DayGraphQLQuery;
import net.poundex.traq.api.client.DayProjectionRoot;
import net.poundex.traq.api.client.SpansGraphQLQuery;
import net.poundex.traq.api.client.SpansProjectionRoot;
import net.poundex.traq.marker.MarkableService;
import net.poundex.traq.timeline.TimelineSpanDto;
import net.poundex.xmachinery.spring.graphql.client.GraphQlClient;
import net.poundex.xmachinery.spring.graphql.client.QueryRequest;
import net.poundex.xmachinery.spring.graphql.client.QueryResponse;
import net.poundex.xmachinery.spring.hx.Hx;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

@Service
class DayService extends AbstractDayService<
		WebTodayPresentationModel, 
		DayPresentationModel, 
		WebTodayPresentationModel.WebTodayPresentationModelBuilder<WebTodayPresentationModel, ?>,
		DayPresentationModel.DayPresentationModelBuilder<DayPresentationModel, ?>> {
	
	public DayService(GraphQlClient graphQlClient, MarkableService markableService) {
		super(graphQlClient, markableService);
	}

	@Override
	protected WebTodayPresentationModel.WebTodayPresentationModelBuilder<WebTodayPresentationModel, ?> 
	doWithTodayQueryResponse(DayDto day, QueryResponse queryResponse, WebTodayPresentationModel.WebTodayPresentationModelBuilder<WebTodayPresentationModel, ?> builder) {
		List<TimelineSpanDto> recentSpans = queryResponse.extractList("recentSpans", TimelineSpanDto.class);
		return super.doWithTodayQueryResponse(day, queryResponse, builder)
				.recentSpans(recentSpans)
				.lastSpanDescription(findLastSpanDescription(recentSpans));
	}

	@Override
	protected WebTodayPresentationModel.WebTodayPresentationModelBuilder<WebTodayPresentationModel, ?> newTodayPresentationModel() {
		return WebTodayPresentationModel.newWebTodayPresentationModelBuilder();
	}

	@Override
	protected DayPresentationModel.DayPresentationModelBuilder<DayPresentationModel, ?> newDayPresentationModel() {
		return DayPresentationModel.newDayPresentationModelBuilder();
	}

	@Override
	protected QueryRequest todayQueryWithExtra(String ledgerId, QueryRequest todayQueryRequest) {
		return todayQueryRequest.and("recentSpans", SpansGraphQLQuery.newRequest()
				.ledger(ledgerId)
				.last(8)
				.build(), new SpansProjectionRoot<>().description());
	}

	private String findLastSpanDescription(List<TimelineSpanDto> recentSpans) {
		return ! recentSpans.isEmpty()
				? recentSpans.getFirst().description()
				: null;
	}
	
	public Hx getCalendar(LocalDate referencePoint, LocalDate current, Hx hx) {
		List<LocalDate> dates = new ArrayList<>(35);
		LocalDate realRef = LocalDate.of(referencePoint.getYear(), referencePoint.getMonth(), 1);
		IntStream.range(0, realRef.getDayOfWeek().getValue() - 1)
				.forEach(ignored -> dates.add(null));
		
		IntStream.rangeClosed(1, realRef.getMonth().length(referencePoint.isLeapYear()))
				.forEach(date -> dates.add(LocalDate.of(realRef.getYear(), realRef.getMonth(), date)));

		return hx.modelAttribute("calendarDates", dates)
				.modelAttribute("calendarYears", 
						IntStream.range(0, 5).mapToObj(idx -> Year.now().minusYears(idx)).toList())
				.modelAttribute("calendarContext", referencePoint)
				.modelAttribute("contextStr", 
						DateTimeFormatter.ofPattern("MMMM yyyy", hx.getLocale()).format(realRef))
				.modelAttribute("calendarMonths", Month.values())
				.modelAttribute("calendarDateRows", Math.ceil(dates.size() / 7.0))
				.modelAttribute("calendarCurrent", current);
	}
	
	public Mono<Hx> effectiveMarkerSummary(String ledgerId, LocalDate day, Hx hx) {
		return graphQlClient.query(
				DayGraphQLQuery.newRequest()
						.day(day)
						.ledger(ledgerId)
						.build(), 
				new DayProjectionRoot<>()
						.effectiveMarkerSummary()
							.duration()
								.marker()
									.__typename()
									.id()
									.name()
									.title()
									.onProperty().value().parent()
									.parent()
								.value()
								.parent()
							.count()
								.marker()
									.__typename()
									.id()
									.name()
									.title()
								.onProperty()
									.value()
									.parent()
								.parent()
								.value()
								.parent()
							.propertyValues()
								.marker()
									.__typename()
									.id()
									.name()
									.title()
									.parent()
								.value())
				.map(resp -> resp.extractValue("day", DayDto.class))
				.map(r -> hx.modelAttribute("effectiveMarkerSummary",
						markableService.getMarkerSummaries(r.effectiveMarkerSummary())));
	}
}
