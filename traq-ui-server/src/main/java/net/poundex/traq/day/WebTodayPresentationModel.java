package net.poundex.traq.day;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import net.poundex.traq.marker.MarkableService;
import net.poundex.traq.marker.MarkerDto;
import net.poundex.traq.timeline.TimelineSpanDto;

import java.util.List;
import java.util.Map;

@Getter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
public class WebTodayPresentationModel extends TodayPresentationModel {

	private final List<TimelineSpanDto> recentSpans;
	private final String lastSpanDescription;

	public WebTodayPresentationModel(DayDto day, List<TimelineSpanDto> recentSpans, String lastSpanDescription, TimelineSpanDto activeSpan, Map<MarkerDto, MarkableService.MarkerSummary> markerSummaries) {
		super(day, activeSpan, markerSummaries);
		this.recentSpans = recentSpans;
		this.lastSpanDescription = lastSpanDescription;
	}

	protected WebTodayPresentationModel(WebTodayPresentationModelBuilder<?, ?> b) {
		super(b);
		this.recentSpans = b.recentSpans;
		this.lastSpanDescription = b.lastSpanDescription;
	}

	public static WebTodayPresentationModelBuilder<WebTodayPresentationModel, ?> newWebTodayPresentationModelBuilder() {
		return new WebTodayPresentationModelBuilderImpl();
	}
}
