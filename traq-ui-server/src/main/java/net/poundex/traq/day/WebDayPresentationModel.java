package net.poundex.traq.day;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import net.poundex.traq.marker.MarkableService;
import net.poundex.traq.marker.MarkerDto;

import java.util.Map;

@Getter
@ToString
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
public class WebDayPresentationModel extends DayPresentationModel {
	
	public WebDayPresentationModel(DayDto day, Map<MarkerDto, MarkableService.MarkerSummary> markerSummaries) {
		super(day, markerSummaries);
	}
}
