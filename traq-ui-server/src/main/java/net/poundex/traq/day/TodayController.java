package net.poundex.traq.day;

import lombok.RequiredArgsConstructor;
import net.poundex.traq.api.client.CreatePointGraphQLQuery;
import net.poundex.traq.api.client.CreatePointProjectionRoot;
import net.poundex.traq.api.client.EventGraphQLQuery;
import net.poundex.traq.api.client.EventProjectionRoot;
import net.poundex.traq.api.client.RepeatLastSpanGraphQLQuery;
import net.poundex.traq.api.client.RepeatLastSpanProjectionRoot;
import net.poundex.traq.api.client.SpansGraphQLQuery;
import net.poundex.traq.api.client.SpansProjectionRoot;
import net.poundex.traq.api.client.StartSpanGraphQLQuery;
import net.poundex.traq.api.client.StartSpanProjectionRoot;
import net.poundex.traq.api.client.StopActiveSpanGraphQLQuery;
import net.poundex.traq.api.client.StopActiveSpanProjectionRoot;
import net.poundex.traq.api.client.UpdateDescriptionGraphQLQuery;
import net.poundex.traq.api.client.UpdateDescriptionProjectionRoot;
import net.poundex.traq.timeline.CreatePointInputDto;
import net.poundex.traq.timeline.RepeatSpanInputDto;
import net.poundex.traq.timeline.StartSpanInputDto;
import net.poundex.traq.timeline.TimelineEventDto;
import net.poundex.traq.timeline.TimelineSpanDto;
import net.poundex.traq.web.handlerarg.LedgerId;
import net.poundex.xmachinery.e1ui.ValidationFailedException;
import net.poundex.xmachinery.e1ui.entitycontroller.EntityControllerSupport;
import net.poundex.xmachinery.e1ui.requestcontext.RequestContext;
import net.poundex.xmachinery.spring.graphql.client.GraphQlClient;
import net.poundex.xmachinery.spring.hx.Hx;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.reactive.result.view.Rendering;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;

@Controller
@RequestMapping("/today")
@RequiredArgsConstructor
public class TodayController {
    
    private final GraphQlClient graphQlClient;
    private final DayService dayService;
    
    @GetMapping
    public Flux<Rendering> today(@LedgerId String ledgerId, Hx hx) {
        return dayService.getDayModelForToday(ledgerId)
                .flatMapMany(dayPm -> hx
                        .modelAttribute("dayPm", dayPm)
                        .modelAttribute("activeSpan", dayPm.getActiveSpan())
                        .modelAttribute("today", true)
                        .render("day/today/today"));
    }

    @PostMapping("/point/add")
    public Flux<Rendering> addPoint(@LedgerId String ledgerId, Hx hx, RequestContext requestContext) {
        return requestContext.formData()
                .map(t -> t.get("description"))
                .flatMapMany(desc ->
                        graphQlClient.query(CreatePointGraphQLQuery.newRequest()
                                        .ledger(ledgerId)
                                        .point(new CreatePointInputDto(null, null, desc))
                                        .build(), new CreatePointProjectionRoot<>().id())
                                .thenMany(dayService.getDayModelForToday(ledgerId)
                                        .flatMapMany(dayPm -> hx.modelAttribute("dayPm", dayPm)
                                                .outOfBand()
                                                .render(hx.getValue(
                                                        new String[] {"timeline/_timeline", "day/today/_nowjumbo", "day/_daysummary"}, 
                                                        new String[] {"day/today/today"})))));
    }
    
    @PostMapping("/span/start")
    public Flux<Rendering> startSpan(@LedgerId String ledgerId, Hx hx, RequestContext requestContext) {
        return requestContext.formData()
                .map(fd -> fd.get("description"))
                .as(desc -> doStartSpan(desc, ledgerId, hx));
    }

    private Flux<Rendering> doStartSpan(Mono<String> description, String ledgerId, Hx hx) {
        return description.flatMapMany(desc ->
                graphQlClient.query(StartSpanGraphQLQuery.newRequest()
                                .ledger(ledgerId)
                                .span(new StartSpanInputDto(null, null, desc))
                                .build(), new StartSpanProjectionRoot<>().span().id())
                        .thenMany(dayService.getDayModelForToday(ledgerId)
                                .flatMapMany(dayPm -> hx
                                        .modelAttribute("dayPm", dayPm)
                                        .outOfBand()
                                        .render(hx.getValue(
                                                new String[]{"timeline/_timeline", "day/today/_nowjumbo", "day/_daysummary"},
                                                new String[]{"day/today/today"})))));
    }

    @PostMapping("/span/stop")
    public Flux<Rendering> stopSpan(@LedgerId String ledgerId, Hx hx) {
        return graphQlClient.query(StopActiveSpanGraphQLQuery.newRequest()
                        .ledger(ledgerId)
                        .build(), new StopActiveSpanProjectionRoot<>().id())
                .thenMany(dayService.getDayModelForToday(ledgerId)
                        .flatMapMany(dayPm -> hx.modelAttribute("dayPm", dayPm)
                                .outOfBand()
                                .render(hx.getValue(
                                        new String[]{"timeline/_timeline", "day/today/_nowjumbo", "day/_daysummary"},
                                        new String[]{"day/today/today"}))));
    }
    
    @GetMapping("/amend/{eventId}")
    public Flux<Rendering> amendStartEdit(@PathVariable String eventId, Hx hx) {
        return graphQlClient.query(EventGraphQLQuery.newRequest()
                        .id(eventId)
                        .build(), new EventProjectionRoot<>().__typename().description())
                .map(r -> r.extractValue("event", TimelineEventDto.class))
                .flatMapMany(e -> hx
                        .modelAttribute("desc", e.description())
                        .modelAttribute("eventId", eventId)
                        .render("timeline/_amenddescription"));
    }
    
    @PostMapping("/amend/{eventId}")
    public Flux<Rendering> amendCommitEdit(@PathVariable String eventId, Hx hx, RequestContext requestContext) {
        return requestContext.formData()
                .mapNotNull(fd -> fd.get("newDesc"))
                .flatMap(newDesc ->
                        graphQlClient.query(UpdateDescriptionGraphQLQuery.newRequest()
                                .event(eventId)
                                .description(newDesc)
                                .build(), new UpdateDescriptionProjectionRoot<>()
                                .id()
                                .__typename()
                                .ledger()
                                .id()
                                .parent()
                                .day()))
                .map(r -> r.extractValue("updateDescription", TimelineEventDto.class))
                .flatMapMany(e -> {
                    boolean today = LocalDate.now().equals(e.day());
                    return (today ? dayService.getDayModelForToday(e.ledger().id()) : dayService.getDayModel(e.ledger().id(), e.day()))
                            .flatMapMany(dayPm -> hx
                                    .modelAttribute("dayPm", dayPm)
                                    .modelAttribute("today", true)
                                    .outOfBand()
                                    .render(hx.getValue(today
                                                    ? new String[]{"timeline/_timeline", "day/today/_nowjumbo", "day/_daysummary"}
                                                    : new String[]{"timeline/_timeline", "day/_daysummary"},
                                            new String[]{"mobile/discardAndRefresh"})));
                });
    }

    @DeleteMapping("/amend/{eventId}")
    public Flux<Rendering> amendCommitCancel(@PathVariable String eventId, Hx hx) {
        return graphQlClient.query(EventGraphQLQuery.newRequest()
                        .id(eventId)
                        .build(), new EventProjectionRoot<>()
                        .id()
                        .__typename()
                        .description()
                        .markers()
                            .__typename()
                            .id()
                            .name()
                            .title()
                            .onProperty()
                                .value()
                                .root())
                .map(r -> r.extractValue("event", TimelineEventDto.class))
                .flatMapMany(e -> hx
                        .modelAttribute("desc", e.description())
                        .modelAttribute("eventId", eventId)
                        .modelAttribute("inlineEvent", e)
                        .modelAttribute("today", true)
                        .render("timeline/_timelinedescription"));
    }

    @PostMapping("/span/repeat")
    public Flux<Rendering> repeatLastSpan(@LedgerId String ledgerId, Hx hx) {
        return graphQlClient.query(RepeatLastSpanGraphQLQuery.newRequest()
                        .ledger(ledgerId)
                        .span(new RepeatSpanInputDto(null, null))
                        .build(), new RepeatLastSpanProjectionRoot<>().id())
                .thenMany(dayService.getDayModelForToday(ledgerId)
                        .flatMapMany(dayPm -> hx
                                .modelAttribute("dayPm", dayPm)
                                .outOfBand()
                                .render(hx.getValue(
                                        new String[]{"timeline/_timeline", "day/today/_nowjumbo", "day/_daysummary"},
                                        new String[]{"day/today/today"}))));
    }
    
    @GetMapping("/span/fromHistory")
    public Flux<Rendering> spanFromHistory(@LedgerId String ledgerId, Hx hx) {
        return graphQlClient.query(SpansGraphQLQuery.newRequest()
                        .ledger(ledgerId)
                        .last(8)
                        .build(), new SpansProjectionRoot<>().id().description().active())
                .map(qr -> qr.extractList(TimelineSpanDto.class).stream()
                        .filter(span -> ! span.active())
                        .toList())
                .flatMapMany(spans -> hx
                        .modelAttribute("spans", spans)
                        .render("day/today/fromHistory"));
    }

    
    @PostMapping("/span/fromHistory/{spanId}")
    public Flux<Rendering> spanFromHistory(@LedgerId String ledgerId, Hx hx, @PathVariable String spanId, RequestContext requestContext) {
        return graphQlClient.query(EventGraphQLQuery.newRequest()
                        .id(spanId)
                        .build(), new EventProjectionRoot<>().description())
                .map(qr -> qr.extractValue(TimelineSpanDto.class))
                .flatMapMany(span -> doStartSpan(Mono.just(span.description()), ledgerId, hx))
                .thenMany(hx.render("mobile/discardAndRefresh"));
    }
    
    @ExceptionHandler
    @RequestMapping
    public Flux<Rendering> handleValidationFailure(ValidationFailedException thrown, net.poundex.traq.web.RequestContext requestContext, Hx hx) {
        return Flux.fromIterable(thrown.getErrors())
                .map(ve -> hx.flash("danger", EntityControllerSupport.validationFailure(ve)))
                .flatMap(h -> today(requestContext.getLedgerId(), h));
    }
}