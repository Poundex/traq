package net.poundex.traq.day;

import lombok.RequiredArgsConstructor;
import net.poundex.traq.web.RequestContext;
import net.poundex.traq.web.handlerarg.LedgerId;
import net.poundex.xmachinery.spring.hx.Hx;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.reactive.result.view.Rendering;
import reactor.core.publisher.Flux;

import java.time.LocalDate;
import java.util.Optional;

@Controller
@RequestMapping("/day")
@RequiredArgsConstructor
public class DayController {
    
    private final DayService dayService;

    @GetMapping({"","/", "/{day}"})
    public Flux<Rendering> day(@LedgerId String ledgerId, @PathVariable(value = "day", required = false) Optional<LocalDate> day, Hx hx, RequestContext requestContext) {
        LocalDate actualDay = day.or(() -> requestContext.fromParameter("day").map(LocalDate::parse)).orElse(LocalDate.now());
        return dayService.getDayModel(ledgerId, actualDay)
                .map(dayPm -> hx.modelAttribute("dayPm", dayPm))
                .doOnNext(h -> dayService.getCalendar(actualDay, actualDay, h))
                .flatMapMany(h -> h.render("day/day"));
    }

    @GetMapping({"/calendar"})
    public Flux<Rendering> calendar(LocalDate context, LocalDate current, Hx hx) {
        return dayService.getCalendar(context, current, hx)
                .render("day/_bigcalendar");
    }
    
    @GetMapping("/{day}/effectiveMarkerSummary")
    public Flux<Rendering> effectiveMarkerSummary(@LedgerId String ledgerId, @PathVariable("day") LocalDate date, Hx hx) {
        return dayService.effectiveMarkerSummary(ledgerId, date, hx)
                .flatMapMany(h -> h.render("timeline/_effectivemarkersummary"));
    }
}
