package net.poundex.traq.mobile;

import lombok.RequiredArgsConstructor;
import net.poundex.traq.api.client.LedgerGraphQLQuery;
import net.poundex.traq.api.client.LedgerProjectionRoot;
import net.poundex.traq.ledger.LedgerDto;
import net.poundex.traq.web.handlerarg.LedgerId;
import net.poundex.xmachinery.e1ui.requestcontext.RequestContext;
import net.poundex.xmachinery.spring.graphql.client.GraphQlClient;
import net.poundex.xmachinery.spring.hx.Hx;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.reactive.result.view.Rendering;
import reactor.core.publisher.Flux;

@Controller
@RequiredArgsConstructor
@RequestMapping("/ledgerchooser")
class MobileLedgerChooserController {
	private final GraphQlClient graphQlClient;
	
	@GetMapping("/button")
	public Flux<Rendering> ledgerChooserButton(@LedgerId String ledgerId, Hx hx, RequestContext requestContext) {
		return graphQlClient.query(
						LedgerGraphQLQuery.newRequest().id(ledgerId).build(),
						new LedgerProjectionRoot<>().id().name())
				.map(r -> r.extractValue(LedgerDto.class))
				.flatMapMany(ledger -> hx
						.modelAttribute("screenId", requestContext.fromParameter("screenId").orElseThrow())
						.modelAttribute("activeLedger", ledger)
						.render("mobile/_ledgerchooserbutton"));
	}
}
