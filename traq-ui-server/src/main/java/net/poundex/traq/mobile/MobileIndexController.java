package net.poundex.traq.mobile;

import net.poundex.xmachinery.spring.hx.Hx;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.reactive.result.view.Rendering;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Controller
class MobileIndexController {
	@RequestMapping(value = "/", produces = "application/vnd.hyperview+xml")
	public Mono<Rendering> mobileIndex() {
		return Mono.just(Rendering.view("mobile/index.xml").build());
	}

	@RequestMapping(value = "/menu", produces = "application/vnd.hyperview+xml")
	public Flux<Rendering> mobileMenu(Hx hx) {
		return hx.render("mobile/menu");
	}
}
